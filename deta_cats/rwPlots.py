'''
rwPlots.py

Motivation: Every time we train a background estimate, there's the *same* set of plots 
that we want to make, so I think it would be q judicious if we could automate this in a 
script so that I can maybe make beamer presentations or attach pdfs to show the type 
of modelling that I'm seeing.
'''
import pandas as pd
import numpy as np
from tqdm import tqdm
import json
from itertools import product
from glob import glob
import uproot
import matplotlib.pyplot as plt
from matplotlib import gridspec

import os
os.sys.path+= ['../RRevolution/code/','../PAG-opt/','../code/']
from analysis import getXhh
from utils import mcToYr, L

from statUtils import calc_bstrap

from eventDisplays import rainbow_text

from plots import draw_SR, draw_VR, draw_CR

from argparse import ArgumentParser

import logging
for name in logging.Logger.manager.loggerDict.keys():
    logging.getLogger(name).setLevel(logging.CRITICAL)

# Let's do ATLAS style plots too!
import matplotlib as mpl
os.sys.path.append( "../PyATLASstyle/")
import PyATLASstyle as pas
pas.applyATLASstyle(mpl)

bCat_to_label = { '4b': '4b',
                  '3b1l': '3b + 1 loose',
                  '3b1nl': '3b + not loose',
                }
yr_to_mc = { 16: 'mc16a', 2016: 'mc16a',
             17: 'mc16d', 2017: 'mc16d',
             18: 'mc16e', 2018: 'mc16e' }

def getBTagMask(df,bCat):
    if bCat=='4b':
        mask = (df.ntag>=4) 
    elif bCat == '3b1l':
        mask = (df.ntag==3) & (df.minQ==2) 
    elif bCat == '3b1nl':
        mask = (df.ntag==3) & (df.minQ==1) 
    else:
        raise NotImplementedError
        
    return mask

def bkgWeights(df,bCat,title,text='',figDir=''):
    
    r = (0, np.percentile(df.loc[df.ntag==2,'w_2b'],99))
    
    plt.hist(df.loc[df[f'rw_to_{bCat}'],'w_2b'],   100,r,color='navy',
             histtype='step',lw=4,label='CR deriv')
    plt.hist(df.loc[df[f'rw_to_{bCat}'],'w_2b_VR'],100,r,color='darkorange',
             histtype='step',lw=4,label='VR deriv')
    plt.xlabel('bootstrapped NN weight',fontsize=18,loc='right')
    plt.ylabel('Entries',fontsize=18,loc='top')
    plt.legend(fontsize=18)
    plt.title(title,loc='right')
    
    ax = plt.gca()
    plt.text(0,1,text,ha='left',va='bottom',transform=ax.transAxes,fontsize=12)
    
    n_CR = np.sum(df.loc[df[f'rw_to_{bCat}'],'w_2b'])
    n_VR = np.sum(df.loc[df[f'rw_to_{bCat}_VR'],'w_2b_VR'])
    
    plt.text(.95,.5,'$n_{CR}$ / $n_{VR}$ = '+f'{n_CR / n_VR:.2f}',ha='right',va='bottom',
             transform=ax.transAxes,fontsize=18)
    
    if figDir:
        plt.savefig(f'{figDir}/normedNNweights.pdf',bbox_inches='tight')


def deriveVars(df,norm,norm_VR):
    '''
    Calculate some extra variables given the RR input
    '''

    df['minQ'] = np.min(df[[f'quantile_h{i}_j{j}' for i,j in product([1,2],[1,2])]],axis=1)
    
    df['w_2b'] = norm * df[f'NN_d24_weight_bstrap_med_{yr}']
    df['w_2b_VR'] = norm_VR * df[f'NN_d24_weight_VRderiv_bstrap_med_{yr}']
    
    df['HT'] = np.sum(df[[f'pT_h{i}_j{j}' for i,j in product([1,2],[1,2])]],axis=1)

    df['dPhi_hh'] = np.arccos(np.cos(df.phi_h1 - df.phi_h2))
    df['dPhi_h1'] = np.arccos(np.cos(df.phi_h1_j1 - df.phi_h1_j2))
    df['dPhi_h2'] = np.arccos(np.cos(df.phi_h2_j1 - df.phi_h2_j2))
    
    df['dR_hh'] = np.sqrt(df.dEta_hh**2+df.dPhi_hh**2)
    df['dRjj_h1'] = np.sqrt((df.eta_h1_j1-df.eta_h1_j2)**2+df.dPhi_h1**2)
    df['dRjj_h2'] = np.sqrt((df.eta_h2_j1-df.eta_h2_j2)**2+df.dPhi_h2**2)


def evalRw(df, col, n, r, bCat='3b1nl',title='',text='',xl='',
           color='pink',rlim=(0.5,1.5),vlines=[],sdf=None,scale=100,
           figDir='',label=''):

    fig, [ax1,ax2] = plt.subplots(2,1,figsize=(6, 6), 
                       gridspec_kw={"height_ratios": [.67, .33], 
                                    "hspace":0.1, "left":0.098, "bottom":0.09})

    m_2b = df[f'rw_to_{bCat}'] #& mi
    m_4b = getBTagMask(df,bCat) #& mi
    l = bCat_to_label[bCat]
    
    n_2b,e,_ = ax1.hist(df.loc[m_2b,col],n,r, histtype='stepfilled',
                       fc=color,ec='k',label='CR rw 2b',
                        weights=df.loc[m_2b,'w_2b'])

    n_VR = np.histogram(df.loc[m_2b,col],n,r,
                        weights=df.loc[m_2b,'w_2b_VR'])[0]

    xx = 0.5 * (e[1:] + e[:-1])
    ax1.hist(xx,e, color='darkorange', histtype='step',
             weights=n_VR, lw=2, label='VR rw 2b')

    bs = calc_bstrap(df, col, yr, norm, norm_IQR,e)
    err = np.sqrt( (n_2b-n_VR)**2 + bs**2 )
    
    ax1.fill_between(e,[0]+list(n_2b-err),[0]+list(n_2b+err), 
                     step='pre',facecolor='None',
                     edgecolor='dimgrey', hatch='\\\\\\\\', 
                     linewidth=0,zorder=5,label=r'BS+shape')
    
    n_4b = np.histogram(df.loc[m_4b,col],n,r)[0]

    ax1.errorbar(xx,n_4b,np.sqrt(n_4b),label=l,
                 color='k',marker='o',lw=0,elinewidth=2)

    if sdf is not None:
        s = np.histogram(sdf[col],n,r,weights=sdf['mc_sf'])[0]
        s_err = np.sqrt(np.histogram(sdf[col],n,r,weights=sdf['mc_sf']**2)[0])
            
        s_up = scale*np.array([0]+list(s+s_err))
        s_dn = scale * np.array([0]+list(s-s_err))
        
        ax1.hist(xx,e,histtype='step',color='rebeccapurple',
                 lw=2,label=f'{scale} x SM',weights=scale*s)
    
    # Add lines to visualize the cuts
    ylim = ax1.get_ylim()
    for vl in vlines:
        ax1.plot([vl]*2,ylim,color='C3',ls='--')
    ax1.set_ylim(ylim)
    
    ax2.plot(xx,np.ones_like(xx),'k--')
       
    dn = [0] + list(1 - err[n_2b!=0]/n_2b[n_2b!=0])
    up = [0] + list(1 + err[n_2b!=0]/n_2b[n_2b!=0])
        
    ax2.fill_between([e[0]] + list(e[1:][n_2b!=0]),dn,up,
                     step='pre',facecolor='None',
                     edgecolor='dimgrey', hatch='\\\\\\\\', 
                     linewidth=0,zorder=5)
    ax2.errorbar(xx[n_2b!=0],n_4b[n_2b!=0]/n_2b[n_2b!=0],
                 np.sqrt(n_4b[n_2b!=0])/n_2b[n_2b!=0],label='4b',
                 color='k',marker='o',lw=0,elinewidth=2)

    ax2.scatter(xx[n_2b!=0],n_VR[n_2b!=0]/n_2b[n_2b!=0],
                 color='darkorange',marker='o',lw=0)

    ax2.set_ylim(rlim)

    ax1.legend(fontsize=18)
    ax2.set_xlabel(xl if xl else col,fontsize=18)
    ax1.set_ylabel('Entries',fontsize=18)
    ax2.set_ylabel('1 / rw 2b',fontsize=18)
    ax1.set_title(title,loc='right')

    fig.text(0,1,text,ha='left',va='bottom',transform=ax1.transAxes,fontsize=12)
    
    print('rw 2b / 4b',np.sum(n_2b) / np.sum(n_4b))
    
    if figDir:
        plt.savefig(f'{figDir}/{col}{label}.pdf',bbox_inches='tight')

def normedHist(df, col, n, r, bCat='3b1nl',title='', xl='',mi=True,
               rlim=(0.5,1.5),vlines=[],sdf=None,scale=100,figDir='',label=''):

    fig, [ax1,ax2] = plt.subplots(2,1,figsize=(6, 6), sharex=True,
                       gridspec_kw={"height_ratios": [.67, .33], 
                                    "hspace":0.1, "left":0.098, "bottom":0.09})

    m_2b = df[f'rw_to_{bCat}'] & mi
    if bCat=='4b':
        m_4b = (df.ntag>=4) & mi
        l = '4b'
    elif bCat == '3b1l':
        m_4b = (df.ntag==3) & (df.minQ==2) & mi
        l = '3b + 1 loose'
    elif bCat == '3b1nl':
        m_4b = (df.ntag==3) & (df.minQ==1) & mi
        l = '3b + not loose'
    else:
        raise NotImplementedError
    
    # Get the nomminal prediction
    rwCR,e = np.histogram(df.loc[m_2b,col],n,r, weights=df.loc[m_2b,'w_2b'])
    rwVR   = np.histogram(df.loc[m_2b,col],n,r, weights=df.loc[m_2b,'w_2b_VR'])[0]
    
    # Get the bs errors
    bsCR = calc_bstrap(df[m_2b], col, yr, norm,    norm_IQR,   e)
    bsVR = calc_bstrap(df[m_2b], col, yr, norm_VR, norm_VR_IQR,e,tag='_VRderiv')

    N_CR = np.sum(rwCR)
    N_VR = np.sum(rwVR)
    
    n_CR = rwCR / N_CR
    n_VR = rwVR / N_VR
    
    print('rw CR / rw VR',N_CR / N_VR)
    
    # Draw the BS errors
    c_CR = 'C0'
    c_VR = 'C1'
    
    alpha=.3
    ax1.fill_between(e,[0]+list(n_CR - bsCR/N_CR),[0]+list(n_CR + bsCR/N_CR), 
                     step='pre',color=c_CR,alpha=alpha) 
    
    ax1.fill_between(e,[0]+list(n_VR - bsVR/N_VR),[0]+list(n_VR + bsVR/N_VR), 
                     step='pre',color=c_VR,alpha=alpha) 
    
    
    ax2.fill_between(e,[0]+list(1 - bsCR/(N_CR*n_CR)),[0]+list(1+bsCR/(N_CR*n_CR)), 
                     step='pre',color=c_CR,alpha=alpha)
    
    ax2.fill_between(e,[0]+list(n_VR/n_CR - bsVR/(N_VR*n_CR)),
                     [0]+list(n_VR/n_CR + bsVR/(N_VR*n_CR)), 
                     step='pre',color=c_VR,alpha=alpha)
    
    # Draw the nominal  
    xx = 0.5 * (e[1:] + e[:-1])
    ax1.hist(xx,n,r,histtype='step',color=c_CR,label='CR rw',weights=n_CR)
    ax1.hist(xx,n,r,histtype='step',color=c_VR,label='VR rw',weights=n_VR)

    
    ax2.plot(r,[1]*2,c_CR)
    ax2.hist(xx,n,r,histtype='step',color=c_VR,weights=n_VR/n_CR)
    
    # Include 4b?
    if bCat=='3b1nl' or ('SR' not in title):
        n_4b = np.histogram(df.loc[m_4b,col],n,r)[0]
        N_4b = np.sum(n_4b)
        ax1.errorbar(xx,n_4b/N_4b,np.sqrt(n_4b)/N_4b,label=l,
                     color='k',marker='.',lw=0,elinewidth=1.5)
        
        ax2.errorbar(xx,n_4b/(N_4b*n_CR),np.sqrt(n_4b)/(N_4b*n_CR),label=l,
                     color='k',marker='.',lw=0,elinewidth=1.5)
        print('rw CR / 4b',np.sum(rwCR) / np.sum(n_4b))
    
    # Add lines to visualize the cuts
    ylim = ax1.get_ylim()
    for vl in vlines:
        ax1.plot([vl]*2,ylim,color='C3',ls='--')
    ax1.set_ylim(ylim)

    ax1.set_xlim(r)
    ax2.set_ylim(rlim)

    ax1.legend(fontsize=18)
    ax2.set_xlabel(xl if xl else col,fontsize=18)
    ax1.set_ylabel('Normalized Entries',fontsize=18,loc='top')
    ax2.set_ylabel('1 / rw 2b',fontsize=18,loc='top')
    ax1.set_title(f'{l} {title}',loc='right')

    fig.text(0,1,text,ha='left',va='bottom',transform=ax1.transAxes,fontsize=12)
    
    if figDir:
        plt.savefig(f'{figDir}/{col}{label}.pdf',bbox_inches='tight')



def cat1dPlot(df, col, n, r, bCat='3b1nl', eta_edgs=[0,0.5,1,1.5],
              title='',text='',xl='',color='pink',rlim=(0.5,1.5),
              sdf=None,scale=100,figDir='',label=''):
    
    nb = len(eta_edgs)-1

    fig, [ax1,ax2] = plt.subplots(2,1,figsize=(6*nb, 6), sharex=True,
                                  gridspec_kw={"height_ratios": [.67, .33], 
                                               "hspace":0.1, "left":0.098, "bottom":0.09})

    l = bCat_to_label[bCat]

    for i, eta_min, eta_max in zip(range(3),eta_edgs[:-1],eta_edgs[1:]):

        mi = (df.dEta_hh > eta_min) & (df.dEta_hh < eta_max)
        
        m_2b = df[f'rw_to_{bCat}'] & mi
        m_4b = getBTagMask(df,bCat) & mi

        ri = r + (i*dr)
        
        n_2b,ei,_ = ax1.hist(df.loc[m_2b,col] + (i*dr),n,ri, histtype='stepfilled',
                            fc=color,ec='k',label='CR rw 2b' if i==0 else None,
                            weights=df.loc[m_2b,'w_2b'],log=log)
        if i == 0:
            e = ei
        
        n_VR = np.histogram(df.loc[m_2b,col] + (i*dr),n,ri,
                            weights=df.loc[m_2b,'w_2b_VR'])[0]

        xx = 0.5 * (ei[1:] + ei[:-1])

        bs = calc_bstrap(df[m_2b], col, yr, norm, norm_IQR,e)
        w_2b = np.sqrt(np.histogram(df.loc[m_2b,col].values, e, weights=df.loc[m_2b,'w_2b'].values**2)[0])
        
        err = np.sqrt( (n_2b-n_VR)**2 + bs**2 + w_2b**2)
        
        ax1.fill_between(ei,[0]+list(n_2b-err),[0]+list(n_2b+err), 
                         step='pre',facecolor='None',edgecolor='dimgrey', 
                         hatch='\\\\\\\\', linewidth=0,zorder=5) 

        n_4b = np.histogram(df.loc[m_4b,col] + (i*dr),n,ri)[0]

        ax1.errorbar(xx,n_4b,np.sqrt(n_4b),label=l if i==0 else None,
                     color='k',marker='o',lw=0,elinewidth=2)

        if sdf is not None:
            
            smask = getBTagMask(sdf,bCat) & (sdf.dEta_hh > eta_min) & (sdf.dEta_hh < eta_max)
            
            s = np.histogram(sdf.loc[smask,col],n,r,weights=sdf.loc[smask,'mc_sf'])[0]
            s_err = np.sqrt(np.histogram(sdf.loc[smask,col],n,r,weights=sdf.loc[smask,'mc_sf']**2)[0])

            s_up = scale*np.array([0]+list(s+s_err))
            s_dn = scale * np.array([0]+list(s-s_err))

            ax1.hist(xx,ei,histtype='step',color='rebeccapurple',
                     lw=2,label=f'{scale} x SM' if i == 0 else None,weights=scale*s)

        ax2.plot(xx,np.ones_like(xx),'k--')

        dn = [0] + list(1 - err[n_2b!=0]/n_2b[n_2b!=0])
        up = [0] + list(1 + err[n_2b!=0]/n_2b[n_2b!=0])

        ax2.fill_between([ei[0]] + list(ei[1:][n_2b!=0]),dn,up,
                         step='pre',facecolor='None',
                         edgecolor='dimgrey', hatch='\\\\\\\\', 
                         linewidth=0,zorder=5)
        ax2.errorbar(xx[n_2b!=0],n_4b[n_2b!=0]/n_2b[n_2b!=0],
                     np.sqrt(n_4b[n_2b!=0])/n_2b[n_2b!=0],label='4b',
                     color='k',marker='o',lw=0,elinewidth=2)

    ax2.set_xlim(r[0],ri[1])
    ax2.set_ylim(0.5,1.5)
        
    start,stop = np.log10(ax1.get_ylim())
    ax1.yaxis.set_minor_locator(LogLocator(base=10,subs=[1.,2.,3.,4.,5.,6.,7.,8.,9.],
                                           numticks=len(ax1.yaxis.get_majorticklocs())))
    ax1.set_ylim(np.power(10,start),np.power(10, stop + 0.25*(stop-start)))
        
    ax2.xaxis.set_minor_locator(MultipleLocator(25))

    ml = np.array([400,600,800,1000,1200])
    mls = []
    for i in range(nb):
        mls += list(ml+i*dr)
        
    ax2.xaxis.set_major_locator(FixedLocator(mls))

    xticklocs = ax2.xaxis.get_majorticklocs()

    xlabels = [f'{xtl:.0f}' for xtl in ml]*nb
    ax2.set_xticklabels(xlabels,rotation=70)

    '''
    Annotate the axes
    '''
    fs=15
    for i, xi, xl,eta_min, eta_max in zip(range(3),[1/6,.5,5/6],np.arange(1,nb+1)/nb,
                                          eta_edgs[:-1],eta_edgs[1:]):

        ri = r + (i*dr)
        
        ti = f'{eta_min}' + ' < $\Delta \eta_{hh}$ < ' + f'{eta_max}'
        r_avg = ri.mean()
        
        ax2.text(xl-.01,-0.4,'$m_{hh}$ [GeV]',fontsize=18,transform=ax2.transAxes,ha='right',va='top')
            
        ax2.annotate(ti, xy=(r_avg, -0.65), xytext=(r_avg, -0.7), 
                     xycoords=('data','axes fraction'), 
                     fontsize=20, ha='center', va='top',
                     arrowprops=dict(arrowstyle='-[, widthB=8.5, lengthB=0.5', lw=2.0))

        
    ax1.legend(loc='upper right',fontsize=18)
    ax1.set_ylabel('Entries',fontsize=18,loc='top')
    ax2.set_ylabel('1 / rw 2b',fontsize=18)
    ax1.set_title('3b + not loose SR',loc='right')

    fig.text(0.015,.95,text,ha='left',va='top',transform=ax1.transAxes,fontsize=15)

    plt.show()


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-y","--yr","--year", dest="yr", default=16, help ="Year (one of 16,17,18)")
    parser.add_argument("--bCat",default="3b1nl", help ="One of 4b,3b1l,3b1nl")
    parser.add_argument('--tname','--treename',dest='tname',default='sig',
                        help="region of the TTree to load and make plots for")
    parser.add_argument('--do_Xwt',action='store_true')

    args = parser.parse_args()

    yr = args.yr
    bCat = args.bCat
    tname = args.tname
    do_Xwt = args.do_Xwt
    
    fDir = '../data/RR/nr_bkts/isoMuon/'
    fname = f'{fDir}/data{yr}_sr_124_117.root'
    wname = f'{fDir}/data{yr}_sr_124_117_NN_100_bootstraps_{bCat}.root'
    
    '''
    Step 1: Open the files and read in the normalizations
    '''
    rw_cols = ['njets','pT_4','pT_2','dRjj_1','dRjj_2','eta_i','pt_hh','X_wt_tag','m_hh',
               'HT', 'dPhi_h1', 'dPhi_h2', 'dRjj_h1', 'dRjj_h2','dR_hh']

    load_cols = ['event_number','run_number','dEta_hh','m_h1','m_h2','phi_h1','phi_h2','pass_vbf_sel'] 
    load_cols += [f'{v}_h{i}_j{j}' for v,i,j in product(['pT','eta','phi','quantile'],[1,2],[1,2])]

    cols = ['ntag'] + rw_cols[:9] + load_cols  
    with uproot.open(fname) as f:
        df = f[tname].arrays(cols,library='pd')


    wcols = ['event_number','run_number', f'rw_to_{bCat}',f'rw_to_{bCat}_VR',
             f'NN_d24_weight_bstrap_med_{yr}', f'NN_d24_weight_VRderiv_bstrap_med_{yr}',
             f'NN_d24_weight_bstrap_IQR_{yr}', f'NN_d24_weight_VRderiv_bstrap_IQR_{yr}']
             
    with uproot.open(wname) as wf:
    
        w_df = wf[tname].arrays(wcols,library='pd')
    
        # Load in the normalizations
        norm     = wf[f'NN_norm_bstrap_med_{yr}'].all_members['fVal']
        norm_VR  = wf[f'NN_norm_VRderiv_bstrap_med_{yr}'].all_members['fVal']
        
        norm_IQR = wf[f'NN_norm_bstrap_IQR_{yr}'].all_members['fVal']
        norm_VR_IQR = wf[f'NN_norm_VRderiv_bstrap_IQR_{yr}'].all_members['fVal']
    
    # Concatenate the dfs    
    kwargs = {'how':'left','on':["event_number","run_number"],'validate':"one_to_one"}
    df = pd.merge(df[~df.pass_vbf_sel], w_df, **kwargs)
    
    '''
    For the SR - should I load in the signal?
    '''
    
    
    
    # Derive some of the useful rw variables
    deriveVars(df,norm,norm_VR)

    '''
    Step 2: Set up some of the configs to make the plots pretty!!
    '''
    
    bCat_to_color = {'4b': 'gold', '3b1l': 'thistle', '3b1nl': 'pink'}
    color = bCat_to_color[bCat]
    label = bCat_to_label[bCat]
    
    tree_to_region = {'sig':'SR', 'validation':'VR', 'control':'CR'}
    region = tree_to_region[tname]
    title = f'{label} {region}'
    
    text = "$\mathbf{ATLAS}$ Data"+f" 20{yr} Internal\n"
    text += "min_dR, NR bkts, SR (124,117)"
    
    figDir = f'figures/{bCat}/'
    if not os.path.exists(figDir):
        os.mkdir(figDir)
        
    figDir +=region 
    if args.do_Xwt:
        figDir += '_Xwt'
        text += ' $X_{wt}>1.5$'
        df = df[df.X_wt > 1.5]
    else:
        text +=' pre-$X_{wt}$' 
    if not os.path.exists(figDir): os.mkdir(figDir)
    
    # Sanity check plot: the background *weights*
    bkgWeights(df,bCat,title,text,figDir)
    
    '''
    Step 3: Make the (inclusive) background validation plots
    '''
    nBins = [8]+[40]*7 + [39] +[40]*6
    myRanges = [(3.5,12.5),(40,200),(40,200),(0,2*np.pi),(0,2*np.pi),(0,2.5),(0,400),
                (1.5,10),(225,1200),(0,1000),(0,np.pi),(0,np.pi),(0,4),(0,4),(0,2*np.pi)]
    xlabels = ['njets','$p_{T,4}$ [GeV]','$p_{T,2}$ [GeV]', '$\Delta R_{jj,1}$','$\Delta R_{jj,2}$',
               'eta_i','$p_{T,hh}$ [GeV]','X_wt_tag','$m_{hh}$ [GeV]',
               '$H_T$ [GeV]', '$\Delta\Phi_{h1}', '$\Delta\Phi_{h2}', 
               '$\Delta R_{h1}', '$\Delta R_{h2}','$\Delta R_{hh}']
    
    figDir0 = f'{figDir}/incl'
    if not os.path.exists(figDir0): os.mkdir(figDir0)
    
    for c,n,r,xl in zip(rw_cols, nBins, myRanges, xlabels):
        evalRw(sr, c,n,r,bCat,title,text,xl,figDir=figDir0)

    '''
    Step 4: HT plots
    I can make them, but idk if they make a lot of sense tbh.
    '''

    '''
    Step 5: Systematics
    '''


    
    



