'''
Sean's mu_finder from his non-resonant-studies repo.

But with some of the functionality taken out st it's just operating directly 
on the pyhf model and data instead of the input data object as well.
(I.e, the acutal repo has protection against bad guesses and does signal 
scaling - and I took this out.)

At the end of this module I have some additional useful stats plottting fcts 
to share b/w nbs.

'''

import pyhf 

import numpy as np
import time

import pandas as pd 
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable


def calc_bstrap(arr, col, yr, norm, norm_IQR, bins, wdf=None,tag=''):
    
    if wdf is None:
        n2,_ = np.histogram(arr[arr['ntag']==2][col],
                            weights=arr[arr['ntag']==2][f'NN_d24_weight_bstrap_med_{yr}']*norm,
                            bins=bins)
            
        n_IQRup,_ = np.histogram(arr[arr['ntag']==2][col],
                                # Sean suggested changing Mar 23rd 2021
                                #  weights=(arr[arr['ntag']==2]['NN_d24_weight_bstrap_med_%s'%yr]*norm
                                #           +arr[arr['ntag']==2]['NN_d24_weight_bstrap_IQR_%s'%yr]/2.),
                                  weights=(arr[arr['ntag']==2][f'NN_d24_weight_bstrap_med_{yr}']
                                           +arr[arr['ntag']==2][f'NN_d24_weight_bstrap_IQR_{yr}']/2.),
                                  bins=bins)
    else:
        n2,_ = np.histogram(arr[arr['ntag']==2][col],
                            weights=wdf[arr['ntag']==2][f'NN_d24_weight_bstrap_med_{yr}']*norm,
                            bins=bins)
            
        n_IQRup,_ = np.histogram(arr[arr['ntag']==2][col],
                                  weights=(wdf[arr['ntag']==2][f'NN_d24_weight_bstrap_med_{yr}']
                                           +wdf[arr['ntag']==2][f'NN_d24_weight_bstrap_IQR_{yr}']/2.),
                                  bins=bins)

    return abs(n_IQRup*np.sum(n2)/np.sum(n_IQRup)+n2*norm_IQR/2.-n2)



def plotCorrelation(corr_file,title='',cmap='RdBu_r',bcat='4b',figDir='',tag='',
                    resort=False):
    '''
    Given a .csv file returned from Sean's plot_pulls.py file (run w/ the 
    --correlations flag), visualize the correlation matrix!
    '''
    
    # Step 1: Load in the correlation file
    df = pd.read_csv(corr_file)

    df.index = df['Unnamed: 0']
    df.index.name = ''
    del df['Unnamed: 0']
    
    # Step 2: Draw the plot
    plt.figure(figsize=[len(df.index)]*2)
    ax = plt.gca()

    C = df.values
    l = df.columns
    
    if resort:
        c00 = C[0::2, 0::2]
        c01 = C[0::2, 1::2]
        c10 = C[1::2, 0::2]
        c11 = C[1::2, 1::2]

        C = np.concatenate([np.concatenate([c00,c01],axis=1),
                            np.concatenate([c10,c11],axis=1)], axis=0)

        l = list(df.columns[0::2]) + list(df.columns[1::2])

    im = ax.imshow(C,cmap=cmap,vmin=-1,vmax=1)

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(im, cax=cax)

    ax.set_xticks(range(len(l)))
    ax.set_yticks(range(len(l)))
    ax.set_xticklabels(l,rotation=90*(1-1.2/len(l)))
    ax.set_yticklabels(l)

    
    # Step 3: Overlay the #s
    for i in range(len(l)):
        for j in range(len(l)):
            cij = C[i,j]

            ax.text(i,j,f'{cij:.3f}',ha='center',va='center')

    ax.set_title(title,loc='left')
    ax.tick_params(which='minor', length=0)
    
    # Step 4: Save
    if figDir:
        plt.savefig(f'{figDir}/correlations_{bcat}_{tag}.pdf', bbox_inches='tight')


