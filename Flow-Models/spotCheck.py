import os

p = 0.01
H = 32

for s in [2, 19, 26, 33, 36, 38, 39, 41, 42, 44, 47, 48, 54, 64, 69, 79, 85, 88, 94, 97]:
#for s in [13,16,18,21,27, 32, 35, 47, 54, 61, 68, 75, 91]:

	cmd = f"bsub -R 'select[centos7 && hname != kiso0026]' -W 25:00 -oo log_files/rnvp_log_pT_h1_log_pT_h2_eta_h2_eta_h1_log_dphi_hh_L5_H{H}_decay1e-06_SM_2b_p_{p}_seed{s}.txt -J rnvp_log_pT_h1_log_pT_h2_eta_h2_eta_h1_log_dphi_hh_L5_H{H}_decay1e-06_SM_2b_p_{p}_seed{s} python densityEstimate.py --model rnvp --learning_rate 0.001 --nLayers 5 --hidden_dim {H} --decay 1e-06 --cols log_pT_h1,log_pT_h2,eta_h2,eta_h1,log_dphi_hh --detaCut _detaCut --tag _SM_2b_p_{p} --prodTag FEB20  --iter 0 --random_seed {s}"

	print(cmd)
	os.system(cmd)

