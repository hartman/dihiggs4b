'''
Classes (with minor modifications from) https://github.com/tonyduan/mdn/blob/master/mdn/models.py
'''

import torch
from torch import nn
from torch.distributions import Normal, OneHotCategorical

class MixtureDiagNormalNetwork(nn.Module):
    def __init__(self, in_dim, out_dim, n_components, hidden_dim=None):
        super().__init__()
        self.n_components = n_components
        if hidden_dim is None:
            hidden_dim = in_dim
        self.network = nn.Sequential(
            nn.Linear(in_dim, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, 2 * out_dim * n_components),
        )

    def forward(self, x,return_params=False):
        params = self.network(x)
        mean, sd = torch.split(params, params.shape[1] // 2, dim=1)
        mean = torch.stack(mean.split(mean.shape[1] // self.n_components, 1))
        sd = torch.stack(sd.split(sd.shape[1] // self.n_components, 1))
        if return_params:
            return mean.transpose(0, 1), torch.exp(sd).transpose(0, 1)
        else:
            return Normal(mean.transpose(0, 1), torch.exp(sd).transpose(0, 1))

class CategoricalNetwork(nn.Module):
    def __init__(self, in_dim, out_dim, hidden_dim=None):
        super().__init__()
        if hidden_dim is None:
            hidden_dim = in_dim
        self.network = nn.Sequential(
            nn.Linear(in_dim, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, out_dim)
        )

    def forward(self, x,return_params=False):
        params = self.network(x)
        if return_params:
            return params 
        else:
            return OneHotCategorical(logits=params)

class MixtureDensityNetwork(nn.Module):
    """
    Mixture density network.
    [ Bishop, 1994 ]
    Parameters
    ----------
    dim_in: int; dimensionality of the covariates
    dim_out: int; dimensionality of the response variable
    n_components: int; number of components in the mixture model
    
    """
    def __init__(self, dim_in, dim_out, n_components, hidden_dim=None):
        super().__init__()
        self.pi_network = CategoricalNetwork(dim_in, n_components, hidden_dim)
        self.normal_network = MixtureDiagNormalNetwork(dim_in, dim_out,
                                                       n_components, hidden_dim)

    def forward(self, x, return_params=False):
        return self.pi_network(x,return_params), self.normal_network(x,return_params)

    def loss(self, x, y):
        pi, normal = self.forward(x)
        loglik = normal.log_prob(y.unsqueeze(1).expand_as(normal.loc))
        loglik = torch.sum(loglik, dim=2)
        loss = -torch.logsumexp(torch.log(pi.probs) + loglik, dim=1)
        return loss

    def sample(self, x):
        pi, normal = self.forward(x)
        pis = pi.sample().unsqueeze(2)
        samples = torch.sum(pis * normal.sample(), dim=1)
        return samples, pis

