'''
Script to loop over options and run densityEstimate.py
'''
from itertools import combinations
import os

'''
# MDN studies
for lr in [.0005,.001]:
    for b in [0, 1e-6, 1e-5, 1e-4, 1e-3]:
        for K in range(1,10):
K = 4
i = 0 
for lr in [.0005]:
    for p in [.01,.1]:
        for seed in range(100):
            pythonCmd = f"python densityEstimate.py --tag _SM_2b_p_{p} --detaCut _detaCut --hidden_dim 100"
            pythonCmd += f" --prodTag FEB20 --lr {lr} --model mdn --weight_decay 0"
            pythonCmd += f" --nModes {K} --iter {i} --random_seed {seed}"
            
            job = f'mdn_FEB20_K{K}_lr{lr}_p_{p}_seed{seed}'
            bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0026]\' -W 25:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
            print(bsubCmd)
            os.system(bsubCmd)
'''

L, lr, detaCut,beta = 5, 1e-3, '_detaCut',1e-3
i = 0 
#for cols,H in zip(['log_m_hh_cor2,absCosThetaStar','log_pT_h1,log_pT_h2,eta_h2,eta_h1,log_dphi_hh'],[16,32]):
for cols,H in zip(['log_m_hh_cor2,absCosThetaStar'],[16]):
    for p in [.01]: #,.1]:
        for seed in [51,52]+list(range(69,86)):# range(100,400):
            #for H in [64]:
            #for beta in [0, 1e-6, 1e-5, 1e-4, 1e-3]:
            print(f'\nTraining rnvp on {cols} with L={L}, H={H}, b={beta}')
            pythonCmd = f'python densityEstimate.py --model rnvp --learning_rate {lr} --nLayers {L}'
            pythonCmd += f' --hidden_dim {H} --decay {beta} --cols {cols} --detaCut {detaCut}'
            pythonCmd += f' --tag _SM_2b_p_{p} --prodTag FEB20  --iter {i} --random_seed {seed}'
                    
            job = f'rnvp_{cols}_L{L}_H{H}_decay{beta}_SM_2b_p_{p}_seed{seed}'
            bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0026]\' -W 25:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
            #print(bsubCmd)
            #os.system(bsubCmd)

'''
# NSF experiments
L, H, lr, detaCut, p, beta = 5, 32, 1e-3, '_detaCut', 0.01, 1e-3
i = 0 

cols = 'log_pT_h1,log_pT_h2,eta_h1,eta_h2,log_dphi_hh'

for lt in ['random','lu']:
        for seed in range(50):
            print(f'\nTraining nsf {lt} permutaions on {cols} with L={L}, H={H}, b={beta}')
            pythonCmd = f'python densityEstimate.py --model nsf --linear_transform_type {lt}'
            pythonCmd += f' --learning_rate {lr} --nLayers {L}'
            pythonCmd += f' --hidden_dim {H} --decay {beta} --cols {cols} --detaCut {detaCut}'
            pythonCmd += f' --tag _SM_2b_p_{p} --prodTag FEB20  --iter {i} --random_seed {seed}'
                    
            job = f'nsf_{lt}_L{L}_H{H}_decay{beta}_SM_2b_p_{p}_seed{seed}'
            bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0026]\' -W 25:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
            print(bsubCmd)
            os.system(bsubCmd)


#cols = 'log_pT_h1,log_pT_h2,eta_h2,eta_h1,log_dphi_hh'

L, H, beta = 5, 64, 1e-6
hc_cols = ['log_pT_h1','log_pT_h2','eta_h1','eta_h2','log_dphi_hh']
for s1 in combinations(hc_cols,3):
    
    s2 = [c for c in hc_cols if not (c in s1)]
    cols = ','.join([s1[0],s2[0], s1[1],s2[1], s1[2]])
    
    
    print(f'\nTraining rnvp on {cols} with L={L}, H={H}, b={beta}')
    pythonCmd = f'python densityEstimate.py --model rnvp --learning_rate {lr} --hidden_dim {H} --nLayers {L} --decay {beta} --cols {cols}'
    job = f'rnvp_{cols}_L{L}_H{H}_decay{beta}'
    #print(job)
    bsubCmd = f"bsub -R \'select[centos7]\' -q long -W 10:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
    print(bsubCmd)
    os.system(bsubCmd)
'''
    
'''
#for detaCmd,tag in zip(['','--detaCut _detaCut'],['','_detaCut']):
for detaCmd,tag in zip([''],['']):
    for L in [4,6,8,10]:
        for H in [16,32,64]:
            for beta in [0,1e-6,1e-5,1e-4,1e-3]:
                print(f'\nTraining rnvp on {cols} with L={L}, H={H}, b={beta}')
                pythonCmd = f'python densityEstimate.py --model rnvp --learning_rate {lr} --hidden_dim {H} --nLayers {L} --decay {beta} --cols {cols} {detaCmd}'
                
                job = f'rnvp_{cols}_L{L}_H{H}_decay{beta}{tag}'
                bsubCmd = f"bsub -R \'select[centos7]\' -q long -W 10:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
                print(bsubCmd)
                os.system(bsubCmd)

'''
