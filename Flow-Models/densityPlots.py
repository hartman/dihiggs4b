'''
Goal: Aggregate some of the plotting functionality that I have so that the models
that I've saved can make analogous plots.

Summer 2020
'''

import matplotlib.pyplot as plt
from matplotlib import gridspec
import pandas as pd
import numpy as np
from scipy.stats import norm
from densityEstimate import *
import torch
from uproot_methods.classes.TLorentzVector import TLorentzVectorArray
from scipy.stats import binned_statistic_2d, chisquare
from scipy.stats import norm 

import os
os.sys.path.append('../code')
from plotting import SR_x, SR_y1, SR_y2
from plotting import CR_x, CR_y1, CR_y2
from plotting import SB_x, SB_y1, SB_y2

from eventDisplays import rainbow_text

def trainingMetric(metrics,showSR=True,title='',figName=''):
	'''
	Goal: Plot the training curves given a densityEstimate model.
	'''

	plt.plot(metrics['train_loss'],color='C0',label='train')
	plt.plot(metrics['val_loss'],color='C1',label='val')
	if showSR:
		plt.plot(metrics['SR_loss'],color='hotpink',label='SR')

	plt.xlabel('epochs',fontsize=15)
	plt.ylabel('Negative log likelihood',fontsize=15)
	plt.legend(fontsize=15)#bbox_to_anchor=(1,1.02))
	plt.title(title,loc='left',fontsize=15)
	
	if len(figName) > 0:
		plt.savefig(figName,bbox_inches='tight')
	plt.show()
	
	# Add text to denote which model had the best validation loss(?)

class Gaussian_Plots():
	def __init__(self, p=np.array([ 6.52553578e+00, -1.32096666e-02,
									-1.15254853e-02,  4.65960918e-05,
									5.41336523e-05, -3.71164789e-01,
									8.50144043e-03,  8.72376080e-03,
									-3.36149052e-05, -3.89639308e-05]), 
				 save=False):
		self.p = p
			
		# Should I set up a figDir here as well?
	def plot_mhh(self, df, nPreds=101, nb=50, r=(250,1250),title='',ylim2=(0,2)):
		
		F = np.vstack([np.ones_like(df.index), df.m_h1, df.m_h2, df.m_h1**2, df.m_h2**2])

		mus = np.dot(self.p[:5],F)
		sigmas = np.dot(self.p[5:],F)

		hists = np.zeros((nPreds,nb))

		for i in range(nPreds):
			t = norm.rvs(loc=mus, scale=sigmas)
			tx = np.exp(t)+250

			hists[i],e = np.histogram(tx, nb, r)

		# Make the plot
		fig = plt.figure(figsize=(6.4, 7))
		gs = gridspec.GridSpec(3,1)
		ax1 = fig.add_subplot(gs[:2,0])
		ax2 = fig.add_subplot(gs[2:,0],sharex=ax1)
		
		xx = 0.5*(e[1:]+e[:-1])
		n_pred,e,_ = ax1.hist(xx,nb,r,color='darkorange',label='Gaussian fits',
							  weights=np.median(hists,axis=0))
		err = np.std(hists,axis=0)
		
		n_2b,_ = np.histogram(df['m_hh_cor2'],nb,r)
		ax1.errorbar(xx,n_2b,np.sqrt(n_2b),fmt='.',color='k',label='2b')
		
		ax1.set_ylabel('Entries',fontsize=15)
		ax1.legend(fontsize=15)
		ax1.set_title(title,fontsize=15,loc='left')
		ax1.text(.975,.725,f'{nPreds}'+' samples per ($m_{h1}$,$m_{h1}$)',
				 ha='right',va='top',fontsize=15,transform=ax1.transAxes)
			
		ax2.errorbar(xx, n_pred/n_2b, err/n_2b, 
					 fmt='.', color='darkorange')
			
		ax2.plot(xx,np.ones_like(xx),'k--')
		xlabel = '$m_{hh} - m_{h1} - m_{h2}$ + 250 [GeV]'
		ax2.set_xlabel(xlabel, fontsize=15)
		ax2.set_ylabel('pred / 2b', fontsize=15)
		ax2.set_ylim(ylim2)
		
		self.n_pred = n_pred
		self.err = err 


class MDN_Plots():

	def __init__(self,mdn,save=True,figDir=''):
		'''
		Class to ease the making of some of the MDN evaluation plots
		'''
		self.mdn = mdn
		self.model = mdn.model 
		self.nModes = mdn.nModes
		self.save = save

		if len(figDir) > 0: 
			self.figDir = figDir
		else: 
			self.figDir = f'figures/{mdn.modelDir[7:]}'

		if not os.path.exists(self.figDir):
			print(f'Creating new directory {self.figDir}')
			os.mkdir(self.figDir)

	def plotAll(self,d,title=''):
		'''
		Plot *all* the relevant plots at once!!
		'''

		# Load in metrics of interest - and plot the training curve 
		X_te, Y_te = d.X_te, d.Y_te
		X_SR, Y_SR = d.X_SR, d.Y_SR
		
		filename = f'{self.mdn.modelDir}/loss_acc.json'
		if os.path.exists(filename):
			with open(filename, 'r') as varfile:
				metrics = json.load(varfile)
		else:
			print(f'Error: {f} does not exist.')
			raise NameError
		
		trainingMetric(metrics,title='2b pairAGraph $|\Delta \eta_{hh}| < 1.5$',
					   figName=f'{self.figDir}/loss.pdf')
		
		for Xi, Yi, set,c in zip([X_te,X_SR],[Y_te,Y_SR],['test','SR'],['gold','hotpink']):

			title = f'2b pairAGraph {set}' + ' $|\Delta \eta_{hh}| < 1.5$'
			# Sampling and prediction error 
			self.samplePrediction(Xi,Yi,title=title,tag=f'_{set}')
			
			title += f'\nMDN {self.nModes} modes, {self.mdn.hidden_dim} hidden units'
			# Compare errors
			self.cf_errors(n_2b, err,title=title,tag=f'_{set}')
			
			# pull plot
			self.pulls(n_pred,n_2b, err,title=title,tag=f'_{set}',color=c)

	def samplePrediction(self, Xi, Yi, nPreds=101, d=None, col='log_m_hh_cor2',
						 nb=100, r=(-5,5),  title='', rlim=(.8,1.2),tag='',
						 loc=None):
		'''
		Show the MDN prediction histogram, averaged over sampling from the 
		distribution nPreds times
		'''
		
		k = self.nModes
		self.nPreds = nPreds
		
		hists = np.zeros((nb,nPreds))
		alphas = np.zeros((k,nPreds))
		modes = np.zeros((nb,k,nPreds))
		
		for i in range(nPreds):
			
			# Generate samples from the MDN
			s, pis = self.model.sample(Xi)
		
			if col=='m_hh_cor2':
				s  = np.exp(d.scalar.mean_[2] + s*d.scalar.scale_[2]) + 250
			
			# Retrieve the *counts* that each of the modes got sampled from
			counts = pis.argmax(axis=1).squeeze()
			n, _ = np.histogram(counts,k,(-0.5,k-.5),density=True)
			alphas[:,i] = n
			
			n_pi,e = np.histogram(s.squeeze(),nb,r)
			hists[:,i] = n_pi
			
			for ki,ni in enumerate(n):
				m, _ = np.histogram(s[counts==ki].squeeze(),nb,r) 
				modes[:,ki,i] = m

		# Draw the prediction
		fig = plt.figure(figsize=(6.4, 7))
		gs = gridspec.GridSpec(3,1)
		ax1 = fig.add_subplot(gs[:2,0])
		ax2 = fig.add_subplot(gs[2:,0],sharex=ax1)
		
		self.xx = 0.5*(e[1:]+e[:-1])
		n_pred,e,_ = ax1.hist(self.xx,nb,r,color='paleturquoise',label='MDN pred',
							  weights=np.median(hists,axis=1))
		
		err = np.std(hists,axis=1)
		
		if col=='m_hh_cor2':
			Yi  = np.exp(d.scalar.mean_[2] + Yi*d.scalar.scale_[2]) + 250
		
		n_2b,_ = np.histogram(Yi.squeeze(),nb,r)
		ax1.errorbar(self.xx,n_2b,np.sqrt(n_2b),fmt='.',color='k',label='2b')
		
		ls, lc = [], []

		for ki,ni,ni_err in zip(range(alphas.shape[0]),np.median(alphas,axis=1),np.std(alphas,axis=1)):
			c = f'C{ki}'
			ax1.hist(self.xx,nb,r,histtype='step',linewidth=1.5,color=c,
					 weights=np.median(modes[:,ki,:],axis=1))
			ls.append(r'$\alpha$'+f'$_{ki}$ = {ni:.3f} $\pm$ {ni_err:.3f}')
			lc.append(c)

		ax1.set_ylabel('Entries',fontsize=15)
		ax1.legend(fontsize=15)
		if loc is None: loc = (-5.25,np.max(n_2b))
		rainbow_text(*loc,ls,lc,ax=ax1,ha='left',va='center',yoffset=1,fontsize=12)
		ax1.set_title(title,fontsize=15,loc='left')
		ax1.text(.975,.725,f'{nPreds} MDN samples\niter {self.mdn.iter}',ha='right',va='top',fontsize=15,transform=ax1.transAxes)
			
		#ax2.errorbar(self.xx, n_pred / n_2b, err/n_2b, fmt='o', color='paleturquoise',label='sampling variation')
		ax2.errorbar(self.xx, n_pred / n_2b, np.sqrt(n_2b)/n_2b, fmt='o', color='paleturquoise',label='sampling variation')
			
		ax2.plot(self.xx,np.ones_like(self.xx),'k--')
		xlabel = '$\log (m_{hh} - m_{h1} - m_{h2})$' if col == 'log_m_hh_cor2' else '$m_{hh} - m_{h1} - m_{h2}$ + 250 [GeV]'
		ax2.set_xlabel(xlabel, fontsize=15)
		ax2.set_ylabel('pred / 2b', fontsize=15)
		ax2.set_ylim(rlim)
		
		# Save the figure
		if self.save:
			plt.savefig(f'{self.figDir}/{col}_median_{nPreds}samples{tag}.pdf',bbox_inches='tight')	
		plt.show()

		# Save n_pred, n_2b, and err as *attributes*
		self.alphas = np.median(alphas,axis=1)
		self.n_pred, self.n_2b, self.err = n_pred, n_2b, err

		# Return n_pred, n_2b, and err for the pull plot
		#return n_pred, n_2b, err

	def cf_errors(self,n_2b, err, title='',tag=''):
		'''
		C.f. Poisson w/ this other source of error
		'''
		plt.plot(self.xx,err,color='paleturquoise',label='101 samples')
		plt.plot(self.xx,np.sqrt(n_2b),color='k',  label='Poisson 2b')

		plt.xlabel('$\log (m_{hh} - m_{h1} - m_{h2})$',fontsize=15)
		plt.ylabel('Error',fontsize=15)
		plt.title(title,loc='left',fontsize=15)

		plt.legend(fontsize=12,loc='upper right')
		if self.save:
			plt.savefig(f'{self.figDir}/errs_pois_vs_{self.nPreds}samples{tag}.pdf',bbox_inches='tight')	
		plt.show()

	def pulls(self, n_pred, n_2b, err,title='',color='gold',tag=''):
		'''
		Show the model fit c.f. an error on the prediction
		'''
	
		z = np.nan_to_num((n_pred-n_2b)/err)
		plt.hist(z,50,(-5,5),color=color,density=True)
		mu,sigma = np.mean(z),  np.std(z)
		
		xi = np.linspace(-5,5)
		g = np.exp(-.5*((xi-mu)/sigma)**2)/(np.sqrt(2*np.pi)*sigma)
		plt.plot(xi,g,'k--')
		
		ax = plt.gca()
		plt.text(0.05,.95,f'$\mu$ = {mu:.2f}, $\sigma$ = {sigma:.2f}', 
				 ha='left',va='top',fontsize=12,transform=ax.transAxes)
			
		plt.xlabel('(MDN median - 2b) / (MDN err)',fontsize=15)
		plt.ylabel('Normalized entries',fontsize=15)
		plt.title(title,loc='left',fontsize=15)
	
		if self.save:
			plt.savefig(f'{self.figDir}/pulls_mdn_{self.nPreds}samples_err{tag}.pdf',bbox_inches='tight')
		plt.show()
	

class RealNVP_Plots():

	def __init__(self,rnvp,save=True,figDir='',mode=0):
		'''
		Class to ease the making of some of the MDN evaluation plots
		'''
		self.rnvp = rnvp 
		self.model = rnvp.model 
		self.save = save

		self.mode = mode

		if len(figDir) > 0:
			self.figDir = figDir
		else: 
			self.figDir = f'figures/{rnvp.modelDir[7:]}'

		if save and not os.path.exists(self.figDir):
			print(f'Creating new directory {self.figDir}')
			os.mkdir(self.figDir)

	def plot_all(self):
		'''
		- Training curves
		- Correlation on the train, val, and SR
		- Marginals of mhh and cos(theta*) medianing over examples 
		'''
		pass

	def plot_correlations(self,Xi,Yi,color='C0',mvars=None,s=None,title='',tag='',
						  size=0.1,alpha=0.5):
		'''
		Monitor all the training variables, and their correlations.
		- Xi: The conditional variables
		- Yi: The variables that we're trying to model
		- mvars: The variables that we want to look at the correlations w/r.t.
		'''

		model = self.model

		ni = len(mvars) if mvars is not None else Yi.shape[1] 

		fig, axes = plt.subplots(ni,ni,figsize=(ni*3,ni*3))
		
		nb, r = 100, (-4,4)
	
		if s is None:
			# Sample from the model
			if self.mode == 0:
				s = model.sample(Xi.shape[0],cond_inputs=Xi).squeeze().detach()
			else:
				s = model.sample(1,Xi).squeeze().detach()

		Yi_np = Yi.detach().numpy()

		for i, ax_row in enumerate(axes):
			for j, ax in enumerate(ax_row):

				if i == j:
					
					# Marginals
					ax.hist(s[:,i], nb,r,label='R-NVP',histtype='step',color=color,density=True)
					ax.hist(Yi_np[:,i],nb,r,label='2b',histtype='step',color='k', density=True)
				elif i > j:

					# True data
					ax.scatter(Yi_np[:,j],Yi_np[:,i],size,'grey','.',alpha=alpha)
					ax.set_xlim(r)
					ax.set_ylim(r)

				else:

					# Predicted data
					ax.scatter(s[:,j],s[:,i],size,color,'.',alpha=alpha)
					ax.set_xlim(r)
					ax.set_ylim(r)

		for i,v in enumerate(mvars):
			
			# Add x and y-labels for the *true* data
			l = 'true '+v
			axes[i,0].set_ylabel(l,color='grey',fontsize=12)
			axes[-1,i].set_xlabel(l,color='grey',fontsize=12)
			
			# Add x and y-labels for the *pred* data
			l = 'pred '+v
			yax = axes[i,-1].twinx()
			yax.set_yticks([])
			yax.set_ylabel(l,color=color,fontsize=12,rotation=270,labelpad=20)
			
			xax = axes[0,i].twiny()
			xax.set_xticks([])
			xax.set_xlabel(l,color=color,fontsize=12,labelpad=10)

		fig.suptitle(title,fontsize=16,va='top')
		if self.save:
			plt.savefig(f'{self.figDir}/correlation{tag}.png',bbox_inches='tight')

	def medianMarginals(self,Xi,Yi,d,nPreds=101,nb=100,rs=[(250,1250),(0,1)],
						color='hotpink',text='SR',title='',figName='',t='',plot=True):
		'''
		Sample from the Real-NVP model nPred times (using the *true* conditional
		dist Xi), and then compare a histogram level medianning over these predictions.
		
		Rn this only has functionality to deal w/ the 2 variable case, but I can 
		extend this later.
		'''

		# The mean and scale of the *conditional* inputs are entries 2 through the end
		scalar = d.scalar
		mean = scalar.mean_[2:]
		scale = scalar.scale_[2:]
	
		hists = np.zeros((nPreds,nb,nb))
		
		cols = ['log_m_hh_cor2','absCosThetaStar']
		idx = pd.MultiIndex.from_product([list(np.arange(nPreds)),range(Xi.shape[0])], 
										  names=['samples','events'])
		dfi = pd.DataFrame(0, index=idx, columns=['m_h1','m_h2']+cols)

		for i in range(nPreds):
			# Sample
			si = self.model.sample(Xi.shape[0],cond_inputs=Xi).detach()
			
			# Concatenate w/ conditional variables + undo the standard scalar preprocessing
			X_pred = scalar.inverse_transform(torch.cat([Xi,si],axis=1))
			dfi.loc[i] = X_pred
		
			si = si.numpy()
			ni,_,_ = np.histogram2d(np.exp(mean[0]+scale[0]*si[:,0])+250,
									mean[1]+scale[1]*si[:,1], nb, rs)
			hists[i] = ni

		self.hists=hists

		# Get the 2b prediction
		Yi_np = Yi.detach().numpy()
		Yxform = np.zeros_like(Yi_np)
		Yxform[:,0] = np.exp(mean[0]+scale[0]*Yi_np[:,0])+250
		Yxform[:,1] = mean[1]+scale[1]*Yi_np[:,1]
		n_2b,ex,ey = np.histogram2d(*Yxform.T,nb,rs)
		
		# Add a new col to the df that undoes the scaling 
		dfi['m_hh_cor2'] = np.exp(dfi['log_m_hh_cor2'])+250

		if plot:
	
			fig = plt.figure(figsize=(10,6))
			gs = gridspec.GridSpec(3,2)
			ax1a = fig.add_subplot(gs[:2,0])
			ax1b = fig.add_subplot(gs[2:,0],sharex=ax1a)
			
			ax2a = fig.add_subplot(gs[:2,1])
			ax2b = fig.add_subplot(gs[2:,1],sharex=ax2a)
		
			# Plot
			for i, (axa, axb,e) in enumerate(zip([ax1a,ax2a],[ax1b,ax2b],[ex,ey])):
				
				xx = 0.5*(e[1:]+e[:-1])
				ni = np.sum(hists,axis=1+(i+1)%2)
				n0,_,_ = axa.hist(xx,nb,rs[i],color=color,label='R-NVP',histtype='step',
								  weights=np.median(ni,axis=0))
								
				# Show the 2b prediction
				n1 = np.sum(n_2b,axis=(i+1)%2)
				axa.errorbar(xx,n1,np.sqrt(n1),fmt='.',color='k',label='2b')
				
				axa.text(.3,.95,text,ha='center',va='top',transform=axa.transAxes,fontsize=12)
				axa.legend(loc='best')
				# Plot the ratio
				err = np.std(ni,axis=0)
				axb.errorbar(xx, n0 / n1, np.sqrt(n1)/n1, fmt='.', color=color,label='sampling variation')
				axb.plot(xx,np.ones_like(xx),'k--')
				axb.set_ylim((.85,1.15))

			# Add legends - and all that jazz
			ax1b.set_xlabel('$m_{hh}^{cor,2}$ [GeV]',fontsize=12)
			ax2b.set_xlabel('$|\cos \Theta^*|$',fontsize=12)
			
			ax1a.set_ylabel('Entries',fontsize=12)
			ax1b.set_ylabel('pred / 2b',fontsize=12)
			
			fig.suptitle(title, fontsize=16)
			plt.subplots_adjust()
			
			self.hist2d = np.median(hists,axis=0)
			self.hist3d = hists
			#self.hist2d_err = np.median(hists,axis=0)
			
			if self.save: 
				plt.savefig(f'{self.figDir}/log_m_hh_cor2_absCosThetaStar{t}.pdf',bbox_inches='tight')
			plt.show()
		return dfi

	def plot_dphi_hh(self,dfi,nPreds=1,color='C4',title='',tag='',n2b=None):
		'''
		'''
		plt.figure()
		r = (-1.5*np.pi,1.5*np.pi)
		plt.hist(dfi['dphi_hh'],90,r,histtype='step',linewidth=1.5,color=color,
				 weights=np.ones_like(dfi['dphi_hh'])/nPreds)
		plt.xlabel('predicted $\Delta \phi_{hh}$',fontsize=15)
		plt.ylabel('Entries',fontsize=15)
		plt.title(title,fontsize=15)
		plt.xlim(r)
		
		if n2b is not None:
			n,e = np.histogram(n2b,90,r)
			xx = 0.5 * (e[1:] + e[:-1])
			print(xx.shape,n.shape,np.sqrt(n).shape)
			plt.errorbar(xx,n,np.sqrt(n),color='k',fmt='.',lw=0,elinewidth=1.5)
		
		# Put text on the plot to indicate what the min and max values are
		ax = plt.gca()
		mmin, mmax = np.min(dfi['dphi_hh']), np.max(dfi['dphi_hh'])
		plt.text(.05,.95,f'min = {mmin:.2f}\nmax = {mmax:.2f}',
				 ha='left',va='top',transform=ax.transAxes,fontsize=15)
		if self.save: 
			print(f'{self.figDir}/dphi_hh{tag}')
			plt.savefig(f'{self.figDir}/dphi_hh{tag}',bbox_inches='tight')
		
	def pred_hh(self,Xi,Yi,scalar,mask,df=None,ix=None,nPreds=1,
				cols=['log_pT_h1','log_pT_h2','eta_h1','eta_h2','log_dphi_hh'],
				color='C0',title='',plot_dphi_hh=True,dphiName='',
				mhh='m_hh_cor2',nb=50,rs=[(250,1250),(-0.1,1.1)],
				ylim1=(.8,1.2),ylim2=None,tag='SR',plot=True,
				leg1='upper right',leg2='upper right'):
		'''
		Predict m_hh and cos(theta*) from the HC variables
		'''

		model = self.model

		# Make *sure* this is one of those 5 var models 
		assert Yi.shape[1] == 5

		# Set up the df that we'll store the predictions in
		idx = pd.MultiIndex.from_product([list(np.arange(nPreds)),range(Xi.shape[0])], 
										 names=['samples','events'])
		dfi = pd.DataFrame(0, index=idx, columns=['m_h1','m_h2']+cols)

		for i in range(nPreds):
			# Sample from the model
			if self.mode == 0:
				# pytorch_flows repo
				s = model.sample(Xi.shape[0],cond_inputs=Xi).squeeze().detach()
			else:
				# NSF repo
				s = model.sample(1,Xi).squeeze().detach()
			
			# Concatenate w/ conditional variables + undo the standard scalar preprocessing
			X_pred = scalar.inverse_transform(torch.cat([Xi,s],axis=1))
			
			dfi.loc[i] = X_pred

		# Undo the dedicated physics preprocessing
		for c in ['log_pT_h1','log_pT_h2','log_dphi_hh']:
			dfi[c[4:]] = np.exp(dfi[c])
		dfi['dphi_hh'] = np.pi - dfi['dphi_hh']
		
		if plot_dphi_hh and plot:
			self.plot_dphi_hh(dfi,nPreds,color,title,tag,df.loc[mask,'dphi_hh'])

		# Get the HC 4-vectors
		hc1_pred = TLorentzVectorArray.from_ptetaphim(dfi['pT_h1'], dfi['eta_h1'], 
													  np.zeros_like(dfi.index).astype(float), 
													  dfi['m_h1'])
		hc2_pred = TLorentzVectorArray.from_ptetaphim(dfi['pT_h2'], dfi['eta_h2'], 
													  dfi['dphi_hh'], dfi['m_h2'])

		# Boost into the di-higgs rest frame to get cos(theta*)
		hh_pred = hc1_pred + hc2_pred
		boost = - hh_pred.boostp3
		rest_hc1 = hc1_pred._to_cartesian().boost(boost)
		
		# Calculate the high level variables
		dfi['m_hh'] = hh_pred.mass
		dfi['m_hh_cor2'] = hh_pred.mass-hc1_pred.mass-hc2_pred.mass+250
		dfi['absCosThetaStar'] = np.abs(np.cos(rest_hc1.theta))
		dfi['pt_hh'] = hh_pred.pt
		
		if plot == False:	
			return dfi
			
		# Get the medianed hists and error bars
		mhh_hists = np.zeros((nPreds,50))
		cosThetaStar_hists = np.zeros((nPreds,50))
		for i in range(nPreds):
			mhh_hists[i], e0      = np.histogram(dfi.loc[i,mhh],nb,rs[0])
			cosThetaStar_hists[i], e1 = np.histogram(dfi.loc[i,'absCosThetaStar'],nb,rs[1])
	
		mhh_hist          = np.median(mhh_hists,axis=0)
		cosThetaStar_hist = np.median(cosThetaStar_hists,axis=0)
		
		mhh_err           = np.std(mhh_hists,axis=0)
		cosThetaStar_err  = np.std(cosThetaStar_hists,axis=0)
		
		self.mhh_hist, self.cosThetaStar_hist = mhh_hist, cosThetaStar_hist
		self.mhh_err,  self.cosThetaStar_err  = mhh_err,  cosThetaStar_err
		
		# Make the plot!!
		fig = plt.figure(figsize=(11,6))
		gs = gridspec.GridSpec(3,2)
		ax1a = fig.add_subplot(gs[:2,0])
		ax1b = fig.add_subplot(gs[2:,0],sharex=ax1a)
		
		ax2a = fig.add_subplot(gs[:2,1])
		ax2b = fig.add_subplot(gs[2:,1],sharex=ax2a)

		# Fill in the plots
		kwargs = {'histtype':'step','linewidth':1.5,'color':color,
				  'label':'Real-NVP' if self.mode == 0 else 'RQ-NSF'}

		for i,col,n_pred,err,e,axa,axb,leg in zip(range(2),[mhh, 'absCosThetaStar'],
												  [mhh_hist, cosThetaStar_hist],
												  [mhh_err, cosThetaStar_err],
												  [e0,e1], [ax1a,ax2a],[ax1b,ax2b],
												  [leg1,leg2]):
			
			xx = 0.5*(e[1:]+e[:-1])

			axa.hist(xx, nb, rs[i], weights=n_pred, **kwargs)
			
			if ix is None:
				n_2b,e = np.histogram(df.loc[mask,col],nb,rs[i])
			else:
				n_2b,e = np.histogram(df.loc[mask,col].values[ix],nb,rs[i])
					
			axa.errorbar(xx, n_2b, np.sqrt(n_2b), fmt='.', color='k', label='2b')

			# Plot the ratio
			#axb.errorbar(xx, n_pred/n_2b, err/n_2b, fmt='.', color=color)
			axb.errorbar(xx, n_pred/n_2b, np.sqrt(n_2b)/n_2b, fmt='.', color=color)
			axb.plot(xx,np.ones_like(xx),'k--')

			# Make the plot pretty
			axa.legend(loc=leg,fontsize=12)

		if ~ (ylim1 is None): ax1b.set_ylim(ylim1)
		if ~ (ylim2 is None): ax2b.set_ylim(ylim2)
	
		if mhh == 'm_hh': ax1b.set_xlabel('$m_{hh}$ [GeV]',fontsize=15)
		if mhh == 'm_hh_cor2': ax1b.set_xlabel('$m_{hh}^{cor,2}$ [GeV]',fontsize=15)
		ax2b.set_xlabel('$|\cos \Theta^*|$',fontsize=15)
		
		ax1a.set_ylabel('Entries',fontsize=15)
		ax1b.set_ylabel('pred / 2b',fontsize=15)
		
		fig.suptitle(title,y=.93, fontsize=16)
		
		if self.save: 
			plt.savefig(f'{self.figDir}/{mhh}_absCosThetaStar{tag}.pdf',bbox_inches='tight')
		
		plt.show()
		
		return dfi

	def showInterpSteps(self, fig, axs, img, cmap='coolwarm',color='k', extent=None,#):
						preText=r'$\sum_i |\nabla I_i|$ = '):
		'''
		Goal: Show the partial w/r.t. x+y images and the gradient
		'''
		
		ax1,ax2,ax3 = axs 
		
		ix = img[1:]-img[:-1]
		iy = img[:,1:]-img[:,:-1]
		g = ix[:,:-1]+iy[:-1]
		
		# Partial w/r.t. x 
		im1 = ax1.imshow(ix.T[::-1],cmap=cmap,extent=extent)
		fig.colorbar(im1, ax=ax1)
		
		# Partial w/r.t. y
		im2 = ax2.imshow(iy.T[::-1],cmap=cmap,extent=extent)
		fig.colorbar(im2, ax=ax2)
		
		# Gradient 
		im3 = ax3.imshow(g.T[::-1],cmap=cmap,extent=extent)
		fig.colorbar(im3, ax=ax3)
		
		l2 = np.sum(g**2)
		text = preText + f'{l2:.2f}'
		ax3.text(*([.975]*2),text,ha='right',va='top',fontsize=12,transform=ax3.transAxes)
		#bbox=dict(facecolor='white', alpha=0.5))
	
		# Overlay the SR on *all* of these plots
		for ax in axs:
			ax.plot(SR_x, SR_y1, color=color)
			ax.plot(SR_x, SR_y2, color=color)
		
		# Return the L2 interp norm 
		return l2

	def scaleAndShift(self,d,nb=36,nSamples=100,plot_err=True,plot_grad=True,
					  plot_grad_steps=True,color='k',
					  cmaps=['Blues','Oranges','Greens','Reds','Purples']):
		'''
		Plot the output of the scale and shift network for each layer of the flow 
		showing all of the variables one-by-one.
		'''
		
		m1_edg = np.linspace(126-45,126+45,nb+1)
		m2_edg = np.linspace(116-45,116+45,nb+1)
		
		m1s = 0.5 * (m1_edg[1:] + m1_edg[:-1])
		m2s = 0.5 * (m2_edg[1:] + m2_edg[:-1])
		
		xx, yy = np.meshgrid(m1s,m2s)
		
		xx = xx.ravel()
		yy = yy.ravel()
		
		Xi = np.vstack([xx,yy]).T
		Yi = np.ones_like(Xi) # Just dummy values st I can apply the scalar
		
		X = np.concatenate([Xi,Yi],axis=1)
		X = d.scalar.transform(X)
		
		noise = torch.Tensor(nb**2,nSamples, 2).normal_()
		mask = torch.tensor([0,1])
		
		fig1, axes1 = plt.subplots(5,2,figsize=(6,10))
		if plot_err:
			fig2, axes2 = plt.subplots(5,2,figsize=(6,10))
		if plot_grad:
			pass
			#fig3, axes3 = plt.subplots(5,2,figsize=(6,10))
		if plot_grad_steps:
			fig4, axes4 = plt.subplots(5,3,figsize=(10,10))
			fig5, axes5 = plt.subplots(5,3,figsize=(10,10))

		s_grad,t_grad = [], []
		
		for i,l in enumerate(reversed(self.model)):
			
			if i % 2 == 0:
				for mi in range(nb**2):
					noise[mi] = l(noise[mi],mode='inverse')[0]
				
			else:
				si, ti = [], []
				si_err, ti_err = [], []

				k = int(i/2)
				ii = k % 2
				#print(k,ii)
				
				for mi, (xi,yi) in enumerate(X[:,:2]):
				
					inputs = mask*noise[mi]
					Xi = torch.ones(noise.shape[1]).reshape(-1,1)*torch.tensor([xi,yi]).float()
					masked_inputs = torch.cat([inputs,Xi],-1)
				
					s = l.scale_net(masked_inputs).detach()
					s *= (1-mask)
					s = torch.exp(s)
					
					t = l.translate_net(masked_inputs).detach()
					t *= (1-mask)
					noise[mi] = (noise[mi] - t) / s
					si.append(s[:,ii].mean())
					si_err.append(s[:,ii].std())
					ti.append(t[:,ii].mean())
					ti_err.append(t[:,ii].std())

				mask = 1 - mask
				
				si = np.array(si).ravel().reshape(nb,nb)
				si_err = np.array(si_err).ravel().reshape(nb,nb)
				ti = np.array(ti).ravel().reshape(nb,nb)
				ti_err = np.array(ti_err).ravel().reshape(nb,nb)
				
			
				im = axes1[k,0].imshow(ti.T[::-1],extent=[*m1_edg[[0,-1]],*m2_edg[[0,-1]]],cmap=cmaps[k])
				fig1.colorbar(im, ax=axes1[k,0])
				axes1[k,0].plot(SR_x, SR_y1, color=color)
				axes1[k,0].plot(SR_x, SR_y2, color=color)
				
				im = axes1[k,1].imshow(si.T[::-1],extent=[*m1_edg[[0,-1]],*m2_edg[[0,-1]]],cmap=cmaps[k])
				fig1.colorbar(im, ax=axes1[k,1])
				axes1[k,1].plot(SR_x, SR_y1, color=color)
				axes1[k,1].plot(SR_x, SR_y2, color=color)
				axes1[k,0].set_ylabel('$m_{h2}$ [GeV]',fontsize=12)
				
				if plot_err:
					im = axes2[k,0].imshow(ti_err.T[::-1],extent=[*m1_edg[[0,-1]],*m2_edg[[0,-1]]],cmap=cmaps[k])
					fig2.colorbar(im, ax=axes2[k,0])
					axes2[k,0].plot(SR_x, SR_y1, color=color)
					axes2[k,0].plot(SR_x, SR_y2, color=color)
					
					im = axes2[k,1].imshow(si_err.T[::-1],extent=[*m1_edg[[0,-1]],*m2_edg[[0,-1]]],cmap=cmaps[k])
					fig2.colorbar(im, ax=axes2[k,1])
					axes2[k,1].plot(SR_x, SR_y1, color=color)
					axes2[k,1].plot(SR_x, SR_y2, color=color)
					axes2[k,0].set_ylabel('$m_{h2}$ [GeV]',fontsize=12)
			
				if plot_grad_steps:
					sgi = self.showInterpSteps(fig4, axes4[k], si, 
											   preText=r'$\sum_i |\nabla s_i|^2$ = ',
											   extent=[*m1_edg[[0,-2]],*m2_edg[[0,-2]]])
					tgi = self.showInterpSteps(fig5, axes5[k], ti, 
											   preText=r'$\sum_i |\nabla t_i|^2$ = ',
											   extent=[*m1_edg[[0,-2]],*m2_edg[[0,-2]]])
			
					s_grad.append(sgi)
					t_grad.append(tgi)

			axes1[-1,0].set_xlabel('$m_{h1}$ [GeV]',fontsize=12) 
		axes1[-1,1].set_xlabel('$m_{h1}$ [GeV]',fontsize=12) 
		axes1[0,0].set_title('shift network t')
		axes1[0,1].set_title('scale network s')
				
		if plot_err:
			axes2[-1,0].set_xlabel('$m_{h1}$ [GeV]',fontsize=12) 
			axes2[-1,1].set_xlabel('$m_{h1}$ [GeV]',fontsize=12) 
			axes2[0,0].set_title('std dev of t')
			axes2[0,1].set_title('std dev of s')
		if plot_grad_steps:
			for axs in axes4: axs[0].set_ylabel('$m_{h2}$ [GeV]',fontsize=12)
			for ax in axes4[-1]: ax.set_xlabel('$m_{h1}$ [GeV]',fontsize=12)
			for ax,title in zip(axes4[0], ['$s_x$','$s_y$',r'$\nabla s$']): 
				ax.set_title(title,fontsize=15)
	
			for axs in axes5: axs[0].set_ylabel('$m_{h2}$ [GeV]',fontsize=12)
			for ax in axes5[-1]: ax.set_xlabel('$m_{h1}$ [GeV]',fontsize=12)
			for ax,title in zip(axes5[0], ['$t_x$','$t_y$',r'$\nabla t$']): 
				ax.set_title(title, fontsize=15)
		
		if self.save:
			fig1.savefig(f'{self.figDir}/s_and_t_massplane.pdf',bbox_inches='tight')
			if plot_err: 
				fig2.savefig(f'{self.figDir}/s_and_t_err_massplane.pdf',bbox_inches='tight')
			if plot_grad_steps:
				fig4.savefig(f'{self.figDir}/sx_sy_grad_massplane.pdf',bbox_inches='tight')
				fig5.savefig(f'{self.figDir}/tx_ty_grad_massplane.pdf',bbox_inches='tight')
		plt.show()  
	
		self.s_grad = s_grad
		self.t_grad = t_grad

		return s_grad, t_grad
		

def compare_HH_plots(dfis, keys, d, SR_mask, mhh='m_hh_cor2', 
                     colors=None, labels=None,title='',
                     nb=50, rs=[[250,1250],[-0.1,1.1]], 
                     ylim1=(0.5,1.5),ylim2=(0.5,1.5),
                     figDir='',tag=''):
    '''
    Compare mhh and cos(theta*) dists over trainings
    '''
    
    if colors is None: 
        colors = [f'C{i}' for i in range(len(keys))]
    if labels is None: 
        labels = keys
    
    fig = plt.figure(figsize=(11,6))
    gs = gridspec.GridSpec(3,2)
    ax1a = fig.add_subplot(gs[:2,0])
    ax1b = fig.add_subplot(gs[2:,0],sharex=ax1a)

    ax2a = fig.add_subplot(gs[:2,1])
    ax2b = fig.add_subplot(gs[2:,1],sharex=ax2a)

    # Plot the 2b predictions
    kwargs = {'histtype':'step', 'linewidth':1.5, 'color':'k', 'label':'2b'}

    n_2b0,e0,_ = ax1a.hist(d.df.loc[SR_mask,mhh],nb,rs[0],**kwargs)
    n_2b1,e1,_ = ax2a.hist(np.abs(d.df.loc[SR_mask,'cosThetaStar']),nb,rs[1],**kwargs)

    xx0 = 0.5*(e0[1:]+e0[:-1])
    xx1 = 0.5*(e1[1:]+e1[:-1])

    ax1b.plot(xx0,np.ones_like(xx0),'k--')
    ax2b.plot(xx1,np.ones_like(xx1),'k--') 

    ls0,ls1,lc0,lc1 = [],[],[],[]

    for key, color, label in zip(keys, colors, labels):

        dfi = dfis[key]
        kwargs['color'], kwargs['label'] = color, label

        # histograms
        n_pred0,_,_ = ax1a.hist(dfi[mhh],nb,rs[0],**kwargs)
        n_pred1,_,_ = ax2a.hist(dfi['absCosThetaStar'],nb,rs[1],**kwargs)

        # ratios
        ax1b.plot(xx0,n_pred0/n_2b0,'.',color=color)
        ax2b.plot(xx1,n_pred1/n_2b1,'.',color=color)

        # get the chi^2s    
        chi2ndf = chisquare(n_2b0[n_2b0!=0],n_pred0[n_2b0!=0],nb-1)[0]/(nb-1)
        ls0.append(f"$\chi^2$/ndf = {float(f'{chi2ndf:.3g}'):g}\n")
        lc0.append(color)

        chi2ndf = chisquare(n_2b1[n_2b1!=0],n_pred1[n_2b1!=0],nb-1)[0]/(nb-1)
        ls1.append(f"$\chi^2$/ndf = {float(f'{chi2ndf:.3g}'):g}\n")
        lc1.append(color)


    # Make the plot pretty    
    ax1b.set_ylim(ylim1)    
    ax2b.set_ylim(ylim2)

    if mhh == 'm_hh': ax1b.set_xlabel('$m_{hh}$ [GeV]',fontsize=15)
    if mhh == 'm_hh_cor2': ax1b.set_xlabel('$m_{hh}^{cor,2}$ [GeV]',fontsize=15)
    ax2b.set_xlabel('$|\cos \Theta^*|$',fontsize=15)

    ax1a.set_ylabel('Entries',fontsize=15)
    ax1b.set_ylabel('pred / 2b',fontsize=15)

    rainbow_text(rs[0][1],np.max(n_2b0),ls0,lc0,ax=ax1a,ha='right',va='top',yoffset=.5,fontsize=10)
    rainbow_text(rs[1][1],np.max(n_2b1),ls1,lc1,ax=ax2a,ha='right',va='top',yoffset=.5,fontsize=10)

    ax2a.legend(bbox_to_anchor=(1,1.03),fontsize=12)

    fig.suptitle(title,y=.93, fontsize=16)

    if len(figDir) > 0:
        plt.savefig(f'{figDir}/{mhh}_absCosThetaStar{tag}.pdf',
                    bbox_inches='tight')
    plt.show()



def marginalWithErrors(seeds_hists, tag='',nb=50, r=(0,1.75), xlabel='|$\Delta \eta_{hh}|$',title='2b SR',
                       filename='',nSeeds=250,seed_key='p_0.01_beta1e-03',key_2b='p_0.01',
                       text='',varTag='',c0='thistle',c1='darkviolet',p=0.01,beta=1e-3):
    '''
    Once we calculated this seeds_hists histogram, overlay the nominal entry 
    with the error bars.
    '''

    # Get the nominal values
    hists = np.vstack([seeds_hists[f'{seed_key}_seed{s}{tag}'] for s in range(nSeeds)])

    nom = np.mean(hists,axis=0)
    std = np.std( hists,axis=0)

    e = np.linspace(*r,nb+1)
    xx = 0.5 * ( e[1:] + e[:-1] )

    # Check the spill over
    if 'Delta' in xlabel:
        f = np.sum(nom[xx > 1.5]) / np.sum(nom)
        print(f'pred deta > 1.5: {f*100:.2f}%')

    x_fine = np.hstack([r[0]]+[ee for ee in e[1:-1] for i in range(2)]+[r[1]])
    fine = np.hstack([nn for nn in nom for i in range(2)])
    std_fine = np.hstack([nn for nn in std for i in range(2)])

    betaStr = r'$\beta$'
    
    # Make the figure
    fig = plt.figure(figsize=(11,6))
    gs = gridspec.GridSpec(3,2)
    ax1 = fig.add_subplot(gs[:2,0])
    ax2 = fig.add_subplot(gs[2:,0],sharex=ax1)

    # Error bars
    ax1.fill_between(x_fine, fine+std_fine, fine-std_fine,
                     color=c0,label='$1\sigma$')
    ax2.fill_between(x_fine, (fine+std_fine)/fine, (fine-std_fine)/fine,color=c0)

    # 2b
    n_2b = seeds_hists[f'{key_2b}_2b{tag}']
    ax1.errorbar(xx, n_2b, np.sqrt(n_2b), label='2b', fmt='.', color='k')
    ax2.errorbar(xx, n_2b/nom, np.sqrt(n_2b)/nom, fmt='.', color='k',zorder=1)

    # Central value
    ax1.hist(xx, nb,r,color=c1,histtype='step',weights=nom,label='mean')
    ax2.plot(xx, np.ones_like(xx), color=c1,linestyle='--')

    if 'm_{hh}' in xlabel:
        ax1.set_yscale('log')
    
    ax1.set_ylabel('Entries',fontsize=15)
    ax1.legend(fontsize=12,loc='center left' if 'Delta' in xlabel else 'upper right')
    ax1.set_title(title,loc='right',fontsize=15 if len(title) < 10 else 12)

    ax2.set_xlabel(xlabel, fontsize=15)
    ax2.set_ylim(0,2)
    ax2.set_ylabel('ratio',fontsize=15)

    ax1.text(0,1,text,ha='left',va='bottom',transform=ax1.transAxes)
    ax1.text(0.1, 0.05, f'{p*100:1.0f}% of the 2b data\n{nSeeds} seeds, {betaStr}={beta:1.0e}\nR-NVP w/ {varTag}',
             ha='left',va='bottom',transform=ax1.transAxes,fontsize=15)

    if len(filename) > 0:
        plt.savefig(filename, bbox_inches='tight')

    plt.show()
    


