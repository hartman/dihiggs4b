'''
Some functions that are useful to apply across notebooks to train some flow models.
'''

from tqdm import tqdm
import torch

class myDataset():
    
    def __init__(self, Ntrain=10000, cols=['m_hh'], cond_cols=[]):

        self.N = Ntrain
        
        d = len(cols)
        
        mask = (df.kinematic_region == 2) & (df.ntag == 2)
        m_hh = df.loc[mask,cols].values[:Ntrain].reshape(-1, d)

        # Take the log
        m_hh = np.log(m_hh)

        # Normalize
        scalarX = StandardScaler()
        scalarX.fit(m_hh)

        self.x = scalarX.transform(m_hh,copy=True).reshape(-1,d)
        self.scalarX = scalarX
        
        if len(cond_cols) > 0:
            
            y_unnorm = df[cond_cols].values[:Ntrain]
            y_log = np.log(y_unnorm)
        
            scalarY = StandardScaler()
            scalarY.fit(y_log)
            y = scalarY.transform(y_log,copy=True)

            self.y = y
            self.scalarY = scalarY
            self.cond = True
                
        else:
            self.cond = False
        
    def sampler(self, n):
        
        indices = np.random.choice(np.arange(self.N), n, replace=False)
        
        if self.cond:
            return torch.from_numpy( self.x[indices].astype('float32') ),\
                   torch.from_numpy( self.y[indices].astype('float32') )
        else:
            return torch.from_numpy( self.x[indices].astype('float32') )

def train(epoch ,model, loader_train, device, optimizer):
    #global global_step
    model.train()
    
    train_loss = 0
    N = 0
    
    pbar = tqdm(total=len(loader_train))
    
    for batch_idx, data in enumerate(loader_train):

        if isinstance(data, list):
            if len(data) > 1:
                cond_data = data[1].float()
                cond_data = cond_data.to(device)
            else:
                cond_data = None
            data = data[0].float()
        else:
            data = data.float()
            cond_data = None
            
        data = data.to(device)
        optimizer.zero_grad()
        loss = -model.log_probs(data, cond_data).mean()
        
        ni = data.shape[0]
        train_loss += loss.item() * ni
        N += ni
        
        loss.backward()
        optimizer.step()

        pbar.update(data.size(0))
        pbar.set_description('Train, Log likelihood in nats: {:.6f}'.format(
            -train_loss / (batch_idx + 1)))
        
        #global_step += 1
        
    pbar.close()
    
    return(train_loss / N)

def validate(epoch, model, loader, device):
    #global global_step

    model.eval()
    val_loss = 0
    N = 0
    
    pbar = tqdm(total=len(loader))
    pbar.set_description('Eval')
    for data in loader:
        
        if isinstance(data, list):
            if len(data) > 1:
                cond_data = data[1].float()
                cond_data = cond_data.to(device)
                data = data[0].float()
            else:
                cond_data = None
                data = data.float()
        else:   
            data = data.float()
            cond_data=None
        
        data = data.to(device)
        with torch.no_grad():
            val_loss += -model.log_probs(data, cond_data).sum().item()
            N += data.shape[0]
                
        pbar.update(data.size(0))
        pbar.set_description('Val, Log likelihood in nats: {:.6f}'.format(
            -val_loss / pbar.n))

    pbar.close()
    return val_loss / N

