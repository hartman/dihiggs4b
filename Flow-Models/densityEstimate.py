'''
This module compares the process of doing maximum likelihood to fit conditional density estimators
(since a lot of the preprocessing and experiments that I'll want to do will all be in common).

Nicole Hartman
Summer 2020
'''

import pandas as pd
import numpy as np
import os
import json

from sklearn.preprocessing import StandardScaler

import torch
from torch import nn
from torch.nn.utils import clip_grad_norm_
from torch.distributions import Normal, Categorical,OneHotCategorical

from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SubsetRandomSampler, SequentialSampler

# Models from other repos
from mdn import MixtureDensityNetwork
os.sys.path.append("../../pytorch-flows")
import flows as fnn

# The hcnaf module (I implemented from BNAF repo + paper)
from hcnaf import HCNAF, CondMaskedWeight, CondSequential,CondTanh

# nflows package I pip installed 
from nflows.flows.base import Flow
from nflows.distributions.normal import StandardNormal, ConditionalDiagonalNormal

from nflows.nn.nets import ResidualNet

from nflows.transforms.coupling import PiecewiseRationalQuadraticCouplingTransform
from nflows.transforms.base import CompositeTransform
from nflows.transforms.permutations import RandomPermutation
from nflows.transforms.lu import LULinear

from nflows.utils import torchutils

class bkgEstDataset(Dataset):
	def __init__(self, df, mask, cols, cond_cols=['m_h1','m_h2']):
		super(bkgEstDataset, self).__init__()

		allCols = cond_cols + cols
		X = df.loc[mask, allCols]

		# Normalize the inputs
		self.scalar = StandardScaler()
		X_norm = self.scalar.fit_transform(X)

		num_cond = len(cond_cols)
		self.x = torch.from_numpy(X_norm[:,:num_cond]).float()
		self.y = torch.from_numpy(X_norm[:,num_cond:]).float()

		if len(cols) == 1:
			self.y = self.y.reshape(-1,1)
	
	def __len__(self):
		return self.x.shape[0] 

	def __getitem__(self, idx):
		return self.x[idx], self.y[idx] # self.w[idx]


class data():
	'''
	Rn I'm only going considering pairAGraph inputs.
	But I should add functionality to consider various production tags!
	'''
	
	def __init__(self,nSelectedJets=5,year=16,prodTag='MAY2019',tag='_NNT_HCs',ntag=2,
				 selection='',postTag=''):
		'''
		
		Inputs:
		- nSelectedJets: # of jets used for pairAGraph training
		- year: Which dataset to consider
		- prodTag: From MNT production 
		- tag: Extra tag I defined in my analysis processing
		'''
	
		# Load in the df
		year = f'_{year}' if prodTag == 'MAY2019' else year
		if type(year) == list:
			yrCat=''.join([str(yi) for yi in year])
			subDir = f'data{yrCat}_PFlow-{prodTag}'
		else:
			subDir = f'data{year}_PFlow-{prodTag}'
		
		jetStr = f'-{nSelectedJets}jets' if nSelectedJets != 4 else ''
		subDir += jetStr

		self.subDir = subDir
		fDir = f"../data/{subDir}/"
		
		kwargs = {'key': 'df'}
		if len(selection) > 0: 
			kwargs['where'] = selection

		if (prodTag == 'FEB20') and (tag=='_SM_2b'):

			print('loading in the even and odd portions of the dataset')
			dfs = []
			for split in ['even','odd']:
				fin = f"{fDir}df{tag}_{split}_{ntag}b{postTag}.h5"
				print(fin)
				dfs.append(pd.read_hdf(fin,**kwargs))
			
			self.df = pd.concat(dfs)
			
		elif prodTag == 'MAY2019':			
			fin = f"{fDir}df_f_0.22{tag}.h5"
			print(fin)
			self.df = pd.read_hdf(fin,key='df')
			
		elif isinstance(year,list):
	
			dfs = []			
			for yri in year:
				fin = f'../data/data{yri}_PFlow-{prodTag}{jetStr}/df{tag}_{ntag}b.h5'
				print(fin)
				dfs.append(pd.read_hdf(fin,**kwargs))
			self.df = pd.concat(dfs)
			
		else:			
			fin = f"{fDir}df{tag}_{ntag}b.h5"
			print(fin)
			self.df = pd.read_hdf(fin,**kwargs)
			
		# Add the new relevant columns
		self.newCols()


	def newCols(self):
		'''
		Add some extra columns to the df:
		m_hh_cor2, log_m_hh_cor2, abs_deta_hh
		'''
		
		eps = 1e-15
		
		self.df['m_hh_cor2'] = self.df['m_hh'] - self.df['m_h1'] - self.df['m_h2'] + 250
		self.df['log_m_hh_cor2'] = np.log(self.df['m_hh'] - self.df['m_h1'] - self.df['m_h2'])
		
		self.df['abs_deta_hh'] = np.abs(self.df['eta_h1']-self.df['eta_h2'])
		self.df['absCosThetaStar'] = np.abs(self.df['cosThetaStar'])

		self.df['log_pT_h1'] = np.log(self.df['pT_h1'])
		self.df['log_pT_h2'] = np.log(self.df['pT_h2'])

		self.df['dphi_hh'] = np.arccos(np.cos(self.df['phi_h1'] - self.df['phi_h2']))
		self.df['log_dphi_hh'] = np.log(np.pi-self.df['dphi_hh']+eps)

	def scramble(self):
		'''
		Unsort the hc values
		'''

		# Save the original Xhh
		self.df['Xhh_old'] = np.sqrt((10*(self.df.m_h1 - 120)/self.df.m_h1)**2 \
									+ (10*(self.df.m_h2 - 110)/self.df.m_h2)**2)
		# Get the random mask
		m = np.random.binomial(1, 0.5, len(self.df.index)).astype(bool)
		
		for v in ['pT','log_pT','eta','phi','m']:
			
			# Shuffle and assign to a,b
			self.df[f'{v}_ha'] = np.where( m,self.df[f'{v}_h1'],self.df[f'{v}_h2'])
			self.df[f'{v}_hb'] = np.where(~m,self.df[f'{v}_h1'],self.df[f'{v}_h2'])
			
			# Reassign to 1,2 so that the rest of my training code will work
			self.df[f'{v}_h1'] = self.df[f'{v}_ha']
			self.df[f'{v}_h2'] = self.df[f'{v}_hb']
			
		# Redefine the SR, VR, and CRs
		self.df['Xhh'] = np.sqrt((10*(self.df.m_h1 - 115)/self.df.m_h1)**2 \
								+(10*(self.df.m_h2 - 115)/self.df.m_h2)**2)
		# First reset the prev SR to be the VR
		self.df['kinematic_region'] = -1
		c = (126+116)/2
		CR_mask = np.sqrt((self.df.m_h1 - c)**2 + (self.df.m_h2-c)**2) < 45
		v = (124+113)/2
		VR_mask = np.sqrt((self.df.m_h1 - v)**2 + (self.df.m_h2-v)**2) < 30
		for mk, k in zip([CR_mask, VR_mask, (self.df.Xhh<1.6)],[2,1,0]):
			self.df.loc[mk,'kinematic_region'] = k


	def mlPrepare(self,mask,cols,random_seed=10,scramble=False,
				  N=int(3e6),batch_size=512):
		'''
		Do the ml preprocessing 
		
		I can also add some bootstrapping of the training dataset logic here later?
		
		'''
		
		assert mask is not None

		if scramble:
			self.scramble()

		self.cols = cols
		notSR = (mask & (self.df.kinematic_region != 0)).values
		dset = bkgEstDataset(self.df, notSR, cols)
		self.scalar = dset.scalar
	
		if N == -1:
			N=len(dset)
		if N > len(dset):
			print(f'Asking for {N} training events when the dataset only had {len(dset)} examples') 
		else:
			print(f'Taking {N} events from {len(dset)} available for the training / validation set')
		ix = np.arange(N)
		
		np.random.seed(random_seed)
		np.random.shuffle(ix)
		
		N_tr = np.floor(0.8*N).astype(int)
		
		ix_tr = ix[:N_tr]
		ix_val = ix[N_tr:N]
		ix_te = ix[N:]
	
		'''
		Add new columns to self.df for whether an individual event was included 
		in the train, val
		'''
		for c, ixi in zip(['train','val','test'],[ix_tr, ix_val, ix_te]):
			
			self.df[c] = False
			 
			mask_i = np.zeros(np.sum(notSR),dtype=bool)
			mask_i[ixi] = True
		
			self.df.loc[notSR,c] = mask_i 
		#self.ix_tr, self.ix_val, self.ix_te = ix_tr, ix_val, ix_te
		
		# Samplers for the train / val split
		self.loader_tr = DataLoader(dset, batch_size=batch_size, sampler=SubsetRandomSampler(ix_tr))
		self.loader_val = DataLoader(dset, batch_size=batch_size, sampler=SequentialSampler(ix_val))
		self.loader_te = DataLoader(dset, batch_size=batch_size, sampler=SequentialSampler(ix_te))

		# Process SR as well?
		SR_mask = mask & (self.df.kinematic_region == 0)
		allCols = ['m_h1','m_h2'] + cols
		SR_norm  = self.scalar.transform(self.df.loc[SR_mask,allCols])

		self.X_SR = torch.from_numpy(SR_norm[:,:2]).float()
		self.Y_SR = torch.from_numpy(SR_norm[:,2:]).float()

		if len(cols) == 1:
			self.Y_SR = self.Y_SR.reshape(-1,1)

class AverageMeter(object):
	"""Computes and stores the average/sum and current value of some quantity of interest.
	Typically this is used to log the loss value of different batches that make an epoch.
	
	Example:
	loss_meter = AverageMeter()
	for batch in ... :
		batch_loss = ...  # average loss for examples of current batch.
		loss_meter.update(batch_loss, len(batch))
		print(loss_meter.avg)
		
	Class taken from Staford CS 223 HW4 starter code
	"""

	def __init__(self):
		self.val = 0
		self.avg = 0
		self.sum = 0
		self.count = 0
		self.reset()

	def reset(self):
		self.val = 0
		self.avg = 0
		self.sum = 0
		self.count = 0

	def update(self, val, n=1):
		"""
		:param val: primitive python type (int, float, boolean)
		:param n: (int, default 1) if val is the result of a computation (e.g., average of batch) of multiple
		items, then n, should reflect the number of those items.
		"""
		self.val = val
		self.sum += val * n
		self.count += n
		self.avg = self.sum / self.count

class densityEstimate():
	'''
	Probably want some functions like, loss, sample?
	Learning rate?
	Or log prob?

	''' 

	def __init__(self, subDir, learning_rate, weight_decay, iter,cols, 
				 modelBaseDir='models',debug=True,anneal_learning_rate=False,
				 grad_norm_clip_value=None):
		'''
		'''
		
		self.modelSubDir = f'{modelBaseDir}/{subDir}/'
		if not os.path.exists(self.modelSubDir):
			os.mkdir(self.modelSubDir)
		if debug:
			print(f'lr = {learning_rate}, weight decay = {weight_decay}, iter = {iter}, cols = {cols}')
		self.learning_rate = learning_rate
		self.decay = weight_decay 
		self.iter = iter  
		self.cols = cols
			
		self.anneal_learning_rate = anneal_learning_rate
		self.grad_norm_clip_value = grad_norm_clip_value
			
	def check_loss(self,loader):
		'''
		Evaluate the NLL loss on the training dataset
		'''
		
		loss_meter = AverageMeter()
		for Xi,Yi in loader :
			batch_loss = self.loss(Xi,Yi)  # average loss for examples of current batch.
			loss_meter.update(batch_loss, Xi.shape[0])

		return loss_meter.avg.item()

			
	def train(self, data, nEpochs=1001, patience=20, device='cpu',printEvery=10,
			  max_num_training_steps=200000):
		'''
		
		Train a model with early stopping
		
		Inputs:
		- data: Instance of the data class which saves options for the data 
				loaders for the training and validation sets, as well as the 
				(ml preprocessed) conditional and predicted pairs in the SR 
		- nEpochs: Max # of passes for the training dataset to consider 
		- patience: Patience for early stopping
		- device: cpu or specific gpu # 
		- printEvery: How often to provide textual output on the training 
		- max_num_training_steps: Only used for the CosineAnnealingLR initialization
		
				
		'''

		opt = torch.optim.Adam(self.model.parameters(), lr=self.learning_rate, 
							   weight_decay=self.decay)
		
		if self.anneal_learning_rate:
			print(f'Using the CosineAnnealingLR w/ max {num_training_steps} training steps')
			scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(opt, num_training_steps, 0)
		else:
			scheduler = None
							
		metrics = {k:[] for k in [f"{s}_loss" for s in ['train','val','test','SR']]}
		
		self.model.to(device=device)
		
		bestEpoch = 0 
		bestValLoss = np.inf  
	
		loader_tr, loader_val, loader_te = d.loader_tr, d.loader_val, d.loader_te
		X_SR, Y_SR =  data.X_SR, data.Y_SR		

		for epoch in range(nEpochs):
		
			loss_meter = AverageMeter()
			for X_tr,Y_tr in loader_tr:
	
				# Step the scheduleer
				if self.anneal_learning_rate:
					# I'm not sure I'm calling this correctly tho
					scheduler.step()
	
				# Zero out the gradients
				opt.zero_grad()
				
				# Do a training step
				self.model.train()
				
				loss = self.loss(X_tr,Y_tr)
				loss.backward()
				
				if self.grad_norm_clip_value is not None:
					clip_grad_norm_(self.model.parameters(), self.grad_norm_clip_value)
				
				opt.step()
	
				loss_meter.update(loss, X_tr.shape[0])

			metrics['train_loss'].append(loss_meter.avg.item())
		
			# Evaluate the validataion and test sets
			self.model.eval()
			
			metrics['SR_loss'].append(self.loss(X_SR,Y_SR).item())
			metrics['val_loss'].append(self.check_loss(loader_val))
			if len(loader_te) > 0:
				metrics['test_loss'].append(self.check_loss(loader_te))

			# Save the model (to load in later)
			torch.save(self.model.state_dict(), self.modelDir+f'model_epoch{epoch}.pt')
			
			if epoch % printEvery == 0:
				myVars = (epoch, metrics['train_loss'][-1], metrics['val_loss'][-1])
				print("Epoch {:3d} | Train loss {: .4f} | Val loss {: .4f}".format(*myVars))

			# Check if the validation loss improved
			if metrics['val_loss'][-1] < bestValLoss:
				bestValLoss = metrics['val_loss'][-1]
				bestEpoch = epoch
				
			# See if it's time to break out of the training loop 
			if epoch - bestEpoch > patience:
				break
		# Training + validation loss and accuracy    
		self.model.load_state_dict(torch.load(self.modelDir+f'model_epoch{bestEpoch}.pt'))
		filename = self.modelDir + "loss_acc.json"
		with open(filename, 'w') as varfile:
			json.dump(metrics, varfile)
		# Also save the model w/ the best validation loss 
		torch.save(self.model.state_dict(), self.modelDir+f'model.pt')	


class parabaloidGaussianFits():
	'''
	Michael said doing this in pytorch should be faster - I'm not sure if putting it in this
	module is necessary or not.
	'''
	pass


class MDN(densityEstimate):
	'''
	'''

	def __init__(self, subDir, learning_rate, weight_decay, cols, iter, nModes, hidden_dim, 
				 load_model=False,modelBaseDir='models',debug=True):
		densityEstimate.__init__(self, subDir, learning_rate, weight_decay, iter,cols,modelBaseDir,debug)

		decay = f'_{weight_decay}' if weight_decay != 0 else ''
		self.modelDir = f'{self.modelSubDir}/mdn_{nModes}_H_{hidden_dim}_lr_{learning_rate}{decay}_iter{iter}/'
		if debug: 
			print(self.modelDir)
		if not os.path.exists(self.modelDir):
			os.mkdir(self.modelDir)
		
		self.model = MixtureDensityNetwork(2,len(self.cols),nModes,hidden_dim)
		self.nModes = nModes 
		self.hidden_dim = hidden_dim 
		
		if load_model:
			self.model.load_state_dict(torch.load(self.modelDir+f'model.pt'))

	def loss(self,Xi,Yi):
		return torch.mean(self.model.loss(Xi,Yi))
	
	def sample(self):
		pass
	

class RealNVP(densityEstimate):
	'''
	Make sure you add functionality for different masks, # of inputs, etc.
	'''
	
	def __init__(self, subDir, learning_rate, weight_decay, cols, iter, nLayers, hidden_dim, 
				 load_model=False, modelBaseDir='models',tag='',epoch=None,debug=True):
		'''
		'''
		densityEstimate.__init__(self, subDir, learning_rate, weight_decay, iter,cols,modelBaseDir,debug)

		colStr = "_".join(cols)
		decay = f'_{weight_decay}' if weight_decay != 0 else ''
		self.modelDir = f'{self.modelSubDir}/rnvp_{colStr}_{nLayers}_layers_H_{hidden_dim}_lr_{learning_rate}{decay}_iter{iter}{tag}/'

		if debug: 
			print(f'Setting up a R-NVP model with {nLayers} layers, hidden_dim={hidden_dim}')
	
		if not os.path.exists(self.modelDir):
			os.mkdir(self.modelDir)
			
		self.nLayers = nLayers 
		self.hidden_dim = hidden_dim 

		num_inputs = len(cols) 
		num_cond_inputs = 2 # hardcoded b/c always just (mh1, mh2)
		
		mask = (torch.arange(0, num_inputs) % 2).float()
		if debug: 
			print(mask)
		modules = []
		
		for _ in range(nLayers):
			modules += [
				fnn.CouplingLayer(num_inputs, self.hidden_dim, mask, num_cond_inputs),
				fnn.BatchNormFlow(num_inputs)
			]
			mask = 1 - mask

		self.model = fnn.FlowSequential(*modules)
		self.model.num_inputs = num_inputs
		
		# parameter initialization
		for module in self.model.modules():
			if isinstance(module, nn.Linear):
				nn.init.orthogonal_(module.weight)
				if hasattr(module, 'bias') and module.bias is not None:
					module.bias.data.fill_(0)
				
		if load_model:
			modelName = self.modelDir+f'model_epoch{epoch}.pt' if epoch is not None else self.modelDir+'model.pt'
			self.model.load_state_dict(torch.load(modelName))
			self.model.eval() # by default - pytorch models are in the training mode
					
	def loss(self,Xi,Yi):
		return -self.model.log_probs(Yi, Xi).mean()
	
	def getImgGrad(self,img):
		'''
		Return the gradient of a corresponding image
		'''
		ix = img[1:]-img[:-1]
		iy = img[:,1:]-img[:,:-1]
		g = ix[:,:-1]+iy[:-1]
			
		return np.sum(g**2)
				
	def interpConstraint(self,scalar,nSamples=100,nb=36,
						 m1_min=126-45,m1_max=126+45,m2_min=116-45,m2_max=116+45):
		'''
		Goal: Get the constraint of *smooth* s and t xforms in the massplane
		'''
	
		m1_edg = np.linspace(m1_min,m1_max,nb+1)
		m2_edg = np.linspace(m2_min,m2_max,nb+1)
		
		m1s = 0.5 * (m1_edg[1:] + m1_edg[:-1])
		m2s = 0.5 * (m2_edg[1:] + m2_edg[:-1])
		
		xx, yy = np.meshgrid(m1s,m2s)
		
		xx = xx.ravel()
		yy = yy.ravel()
		
		Xi = np.vstack([xx,yy]).T
		Yi = np.ones_like(Xi) # Just dummy values st I can apply the scalar
		
		X = np.concatenate([Xi,Yi],axis=1)
		X = scalar.transform(X)
		
		noise = torch.Tensor(nb**2,nSamples, 2).normal_()
		mask = torch.tensor([0,1])
		
		s_grad,t_grad = [], []
		
		for i,l in enumerate(reversed(self.model)):
			
			if i % 2 == 0:
				for mi in range(nb**2):
					noise[mi] = l(noise[mi],mode='inverse')[0]
				
			else:
				si, ti = [], []

				k = int(i/2)
				ii = k % 2
				
				for mi, (xi,yi) in enumerate(X[:,:2]):
				
					inputs = mask*noise[mi]
					Xi = torch.ones(noise.shape[1]).reshape(-1,1)*torch.tensor([xi,yi]).float()
					masked_inputs = torch.cat([inputs,Xi],-1)
				
					s = l.scale_net(masked_inputs).detach()
					s *= (1-mask)
					s = torch.exp(s)
					
					t = l.translate_net(masked_inputs).detach()
					t *= (1-mask)
					noise[mi] = (noise[mi] - t) / s
					si.append(s[:,ii].mean())
					ti.append(t[:,ii].mean())

				mask = 1 - mask
				
				si = np.array(si).ravel().reshape(nb,nb)
				ti = np.array(ti).ravel().reshape(nb,nb)
			
				sgi = self.getImgGrad(si)
				tgi = self.getImgGrad(ti)
		
				s_grad.append(sgi)
				t_grad.append(tgi)

		return s_grad, t_grad
				
						
	def sample(self):
		pass


class myHCNAF(densityEstimate):
	'''
	HCNAF module that uses my implementation of HCNAF from modifying the BNAF repo.
	'''
	
	def __init__(self, subDir, learning_rate, weight_decay, cols, iter, 
				 num_flows, nLayers, hidden_dim, hypernet_h,
				 load_model=False, modelBaseDir='models',tag='',epoch=None,debug=True):
		'''
		'''
		densityEstimate.__init__(self, subDir, learning_rate, weight_decay, iter,cols,modelBaseDir,debug)

		colStr = "_".join(cols)
		decay = f'_{weight_decay}' if weight_decay != 0 else ''
		self.modelDir = f'{self.modelSubDir}/hcnaf_{colStr}_{num_flows}flows_{nLayers}_layers_H_{hidden_dim}_hypernet_{hypernet_h}_lr_{learning_rate}{decay}_iter{iter}{tag}/'
	
		if not os.path.exists(self.modelDir):
			os.mkdir(self.modelDir)
			
		self.num_flows = nLayers 
		self.nLayers = nLayers 
		self.hidden_dim = hidden_dim 

		num_inputs = len(cols) 
		num_cond_inputs = 2 # hardcoded b/c always just (mh1, mh2)
	
		kwargs = {'dim': num_inputs, 'H': hypernet_h, 'D_in': num_cond_inputs}
		flows = []
		for f in range(nLayers):
			layers = []
			for _ in range(nLayers - 1):
				layers.append(CondMaskedWeight(num_inputs * hidden_dim, num_inputs * hidden_dim, **kwargs))
				layers.append(CondTanh())

			flows.append(
				HCNAF(*([CondMaskedWeight(num_inputs, num_inputs * hidden_dim, **kwargs), CondTanh()] + \
						layers + \
						[CondMaskedWeight(num_inputs * hidden_dim, num_inputs, **kwargs)]),\
						res='gated' if f < num_flows - 1 else False
					)
				)

			if f < num_flows - 1:
				flows.append(Permutation(dim, 'flip'))

		self.model = CondSequential(*flows)

		if load_model:
			modelName = self.modelDir+f'model_epoch{epoch}.pt' if epoch is not None else self.modelDir+'model.pt'
			self.model.load_state_dict(torch.load(modelName))
			self.model.eval() # by default - pytorch models are in the training mode
					
	def loss(self,Xi,Yi):
		
		y_mb, log_diag_j_mb = self.model(Yi,Xi)
		log_p_y_mb = Normal(torch.zeros_like(y_mb),torch.ones_like(y_mb)).log_prob(y_mb).sum(-1)
		return -(log_p_y_mb + log_diag_j_mb).mean()

	def sample(self):
		pass


class myNSF(densityEstimate):
	'''
	NSF module that uses the implementation of the flows from I. Murray's group's
	nflows repo to set up the spline flow models.
	'''
	
	def __init__(self, subDir, learning_rate, weight_decay, cols, iter, 
				 nLayers, hidden_dim, num_blocks, K, B, permutation, CouplingLayer, p,
				 load_model=False, modelBaseDir='models',tag='',epoch=None,
				 torch_seed=10,debug=True,preTrain=''):
		'''
		'''
		densityEstimate.__init__(self, subDir, learning_rate, weight_decay, iter, cols, modelBaseDir,debug)

		assert permutation == 'random' or permutation == 'lu' or permutation == 'svd'
		assert CouplingLayer == 'rq-coupling' # only spline flow included as of now

		colStr = "_".join(cols)
		decay = f'_{weight_decay}' if weight_decay != 0 else ''
		dpt = '' if p == 0 else f'_p{p}'
		
		if len(preTrain) > 0:
			tag += f'_{preTrain}'
		
		# Should I switch *out* my nomenclature for num_flows and nLayers?
		self.modelDir = f'{self.modelSubDir}/nsf_{CouplingLayer}_{colStr}_{permutation}'
		self.modelDir+= f'_{nLayers}_layers_H_{hidden_dim}_{num_blocks}_blocks_K_{K}_B_{B:.0f}'
		self.modelDir+= f'_lr_{learning_rate}{decay}{dpt}_iter{iter}{tag}/'
	
		if not os.path.exists(self.modelDir):
			os.mkdir(self.modelDir)
			
		self.num_flows = nLayers 
		self.nLayers = nLayers 
		self.hidden_dim = hidden_dim 

		self.num_inputs = len(cols) 
		self.num_cond_inputs = 2 # hardcoded b/c always just (mh1, mh2)

		if debug:
			print('Setting torch random seed',torch_seed)
		torch.manual_seed(torch_seed)
	
		base_dist = StandardNormal(shape=[self.num_inputs])
		
		transforms = []
		for _ in range(nLayers):
			transforms.append(self.create_linear_transform(permutation))
			transforms.append(
				PiecewiseRationalQuadraticCouplingTransform(
					mask = torchutils.create_mid_split_binary_mask(features=self.num_inputs),
					transform_net_create_fn=lambda in_features, out_features:
						ResidualNet(
							in_features=in_features,
							out_features=out_features,
							context_features=self.num_cond_inputs,
							hidden_features=hidden_dim,
							num_blocks=num_blocks,
							dropout_probability=p,
							use_batch_norm=True
						),
					num_bins=K,
					tails='linear',
					tail_bound=B 
				)
			)
		transform = CompositeTransform(transforms)
		self.model = Flow(transform, base_dist)

		if len(preTrain) > 0:
			
			physicsSample = subDir.split('_')[0]
			preTrainModel = self.modelDir.replace(f'_{preTrain}','').replace(physicsSample,preTrain)
			if debug: print(preTrainModel)
			self.model.load_state_dict(torch.load(os.path.join(preTrainModel,'model.pt')))
			self.model.eval() # by default - pytorch models are in the training mode

		if load_model:
			modelName = self.modelDir+f'model_epoch{epoch}.pt' if epoch is not None else self.modelDir+'model.pt'
			if debug: print(modelName)
			self.model.load_state_dict(torch.load(modelName))
			self.model.eval() # by default - pytorch models are in the training mode
		
	def create_linear_transform(self,permutation='random'):
		'''
		Copied (and slightly modified) from the NSF repo
		
		Accessed Oct 2020
		'''
		if permutation == 'random':
			return RandomPermutation(features=self.num_inputs)
		elif permutation == 'lu':
			return CompositeTransform([
				RandomPermutation(features=self.num_inputs),
				LULinear(self.num_inputs, identity_init=True)
				])
		elif perm == 'svd':
			return CompositeTransform([
				RandomPermutation(features=self.num_inputs),
				SVDLinear(self.num_inputs, num_householder=10, identity_init=True)
				])
		else:
			raise ValueError

					
	def loss(self,Xi,Yi):
		
		return - self.model.log_prob(inputs=Yi, context=Xi).mean()

	def sample(self):
		pass

if __name__ == '__main__':

	import argparse
	from argparse import ArgumentParser

	# Load in the options from the command line
	p = ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

	# Dataset arguments 
	p.add_argument('--nSelectedJets',type=int,default=5,help='max # of jets for pairAGraph training (default 5)')
	p.add_argument('--year',type=str,default=16,help='dataset to consider (default 16), can also be a comma separated list to train combination of years')
	p.add_argument('--prodTag',type=str,default='MAY2019',help='MNT production tag (default MAY2019)')
	p.add_argument('--tag',type=str,default='_NNT_HCs',help='extra tag I included for the analysis processing (default _NNT_HCs)')
	p.add_argument('--postTag',type=str,default='',help='tag appended at concat stage (i.e, _detaCut)')
	p.add_argument('--ntag',type=int,default=2,help='# of b-tags to include in the training (default 2)')
	p.add_argument('--detaCut',type=str,default='',
				   help='String specifying which deta cut to apply:\n'
				   +'  _detaCut: |deta_hh| < 1.5 \n'
				   +'  _inv_deta: |deta_hh| > 1.5 \n'
				   +'  (empty string - default): no deta cut')
	p.add_argument('--y','--cols',type=str,default='log_m_hh_cor2',
				   help='Variables we\'re modelling (comma separated list for more than 1), default: log_m_hh_cor2')
	p.add_argument('--random_seed',type=int,default=10,help='The random seed to set for the shuffle for the train / test split')
	p.add_argument('--scramble',action='store_true',help='scramble the HC ordering')
	p.add_argument('--MDpT',action='store_true',help='whether to apply the MDpT cuts during training')
	p.add_argument('--N',type=int,default=-1,help='# events for training and validation sets')
	p.add_argument('--preTrainSample',type=str,default='',help='Physics sample to load in pretrained weights from')

	# Model params
	p.add_argument('--model',type=str,default='rnvp',help='density model to train, one of mdn, rnvp (default), hcnaf, or nsf')
	p.add_argument('--load_model',action='store_true',help='Load in a previously trained model')
	p.add_argument('--hidden_dim',type=int,default=64,help='# of hidden dimensions (default 64)')
	p.add_argument('--num_flows',type=int,default=1,help='# of flows to stack for HCNAF')
	p.add_argument('--nLayers',type=int,default=5,help='# of layers (default 5)')
	p.add_argument('--nModes',type=int,default=4,help='# of modes for the mdn model (default 4)')
	p.add_argument('--hypernet_h',type=int,default=16,help='hidden dim for hypernetwork for conditioning (default 16)')

	# Arguments specific to the  NSF implementation 
	#p.add_argument('--base_dist',type='str',default='StandardNormal',
	#			   help='Only StandardNormal implemented rn - plan to add functionality for Uniform and MoG.')
	p.add_argument('--linear_transform_type',type=str,default='random',
				   help='Way to partition the input dataset for the coupling transforms:\n'
				   #+'  - ordered:  \n'
				   +'  - random (default): Randomly shuffle the variables after each flow step \n'
				   +'  - lu: Randomly shuffle - and genereralized permutation repn as the matrix PLU\n'
				   +'Note: for the nsf model, only the random and lu options are valid for the nflows package I\'m using')
	p.add_argument('--base_transform_type',type=str,default='rq-coupling',help='default rq-coupling')
	p.add_argument('--K',type=int,default=8,help='# of bins to apply the NSF xformation in')
	p.add_argument('--B',type=float,default=3.,help='boundaries to apply the spline xforms in: default [-3,3]')
	
	p.add_argument('--num_blocks',type=int,default=2,help='number of residual blocks to include')
	p.add_argument('--dropout_probability','--p',type=float,default=0,help='dropout prob (only implemented for nsf model')

	# Training arguments
	p.add_argument('--iter',type=int,default=0,help='random initialization for NN training (default 0)')
	p.add_argument('--learning_rate','--lr',type=float,default=1e-3, help='learning rate (default 1e-3)')
	p.add_argument('--patience',type=int,default=20, help='early stopping patience (default 20)')
	p.add_argument('--weight_decay','--decay',type=float,default=1e-6, help='weight decay (default 1e-6)')
	p.add_argument('--batch_size',type=int,default=512,help='batch size for the training')
	p.add_argument('--anneal_learning_rate',action='store_true',help='whether to use cosine annealing when training')
	p.add_argument('--grad_norm_clip_value', type=float, default=None,
				   help='Value by which to clip norm of gradients. Default param of None means no gradient clipping')
	args = p.parse_args()

	'''
	Step 1: Load in the dataset 
	'''
	
	prodTag, ntag, tag = args.prodTag, args.ntag, args.tag
	#s = f'm_h1>{126-45} & m_h1<{126+45} & m_h2>{116-45} & m_h2<{116+45}' if prodTag != 'MAY2019' else ''
	s = ''
	print(prodTag,f'{ntag}b',s)
	
	year = args.year 
	if ',' in  year:
		year = year.split(',')
		print(isinstance(year,list))
	else:
		year = int(year)
	
	d = data(args.nSelectedJets, year, args.prodTag,tag,ntag if ntag < 3 else 3, s, args.postTag)

	mh1_mask = (d.df.m_h1 > 126-45) & (d.df.m_h1 < 126+45)
	mh2_mask = (d.df.m_h2 > 116-45) & (d.df.m_h2 < 116+45)
	if args.scramble:
		c = (126+116)/2
		mh1_mask = (d.df.m_h1 > c-45) & (d.df.m_h1 < c+45)
		mh2_mask = (d.df.m_h2 > c-45) & (d.df.m_h2 < c+45)
	mask = mh1_mask & mh2_mask 
	
	# b-tagging cut
	d.subDir += f'{tag}_{ntag}b'
	if ntag < 4:
		mask = mask & (d.df.ntag==ntag)
	else:
		mask = mask & (d.df.ntag>=ntag)

	# deta cut
	detaTag = args.detaCut
	d.subDir += detaTag
	
	if detaTag == '_detaCut':
		print('Training on |deta_hh| < 1.5')
		mask = mask & (d.df.abs_deta_hh < 1.5) 
	elif detaTag == '_inv_deta':
		print('Training on |deta_hh| < 1.5')
		mask = mask & (d.df.abs_deta_hh > 1.5) 
	else:
		print('Training w/o the |deta_hh| cut')

	# MDpT cut 
	if args.MDpT:
		mask = mask & d.df.MDpT 
		d.subDir += '_MDpT' 


	if args.scramble:
		d.subDir += '_scrambleHCs'

	random_seed, i = args.random_seed, args.iter
	if random_seed != 10:
		d.subDir += f'_seed{random_seed}'

	cols = args.y.split(',')
	d.mlPrepare(mask, cols, random_seed, args.scramble, args.N, args.batch_size)
	print(np.sum(mask))
	print(d.subDir)
	
	# Optimizer commands 
	if args.grad_norm_clip_value is not None:
		print('I\'ve not implemented the new model name saving yet or passed for gradient clipping')
		raise NotImplementedError
	if args.anneal_learning_rate:
		print('I\'ve not implemented the new model name saving yet or passed for learning rate annealing')
		raise NotImplementedError
	
	
	'''
	Step 2: Define the model 
	'''
	if args.model == 'mdn':
		print('Setting up the mdn model')
		model = MDN(d.subDir, args.learning_rate, args.weight_decay, cols, args.iter, 
					args.nModes,args.hidden_dim)

	elif args.model == 'rnvp':
		print('Setting up the Real-NVP model for', ', '.join(cols))
		model = RealNVP(d.subDir, args.learning_rate, args.weight_decay, cols, args.iter, 
						args.nLayers, args.hidden_dim)

	elif args.model == 'hcnaf':
		print('Setting up the HCNAf model for', ', '.join(cols))
		model = myHCNAF(d.subDir, args.learning_rate, args.weight_decay, cols, args.iter, 
					  args.num_flows, args.nLayers, args.hidden_dim, args.hypernet_h)

	elif args.model == 'nsf':
		
		print('Setting up the NSF model for', ', '.join(cols))
		model = myNSF(d.subDir,args.learning_rate, args.weight_decay, cols, args.iter,
					  args.nLayers, args.hidden_dim, args.num_blocks, args.K, args.B,
					  args.linear_transform_type,args.base_transform_type,
					  args.dropout_probability,torch_seed=random_seed,preTrain=args.preTrainSample)

	else:
		print(f'Error: the {args.model} model is not defined.')
		raise NotImplementedError
	
	print("Loss before training",model.check_loss(d.loader_tr))
	'''
	Step 3: Train it!!
	'''
	model.train(d,patience=args.patience)

