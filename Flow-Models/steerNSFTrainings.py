'''
Script to loop over options and run densityEstimate.py
'''
from itertools import combinations
import os
from random import choice

# First NSF experiments
# L, H, lr, detaCut,= 5, 32, 1e-3, '_detaCut'
# p, beta, i = 0.01, 1e-3, 0 

detaCut = '_detaCut'
cols = 'log_pT_h1,log_pT_h2,eta_h1,eta_h2,log_dphi_hh'
colTag = cols.replace(',','_')
p, i = 0.01, 0 
lt = 'lu'
nSeeds = 25
nConfigs = 50

'''
Ls = [5,10]
Ks = [4,8]
lrs = [3e-4, 5e-4, 1e-3]
dpts = [0,.1,.2]
betas = [0,1e-6,1e-5,1e-4,1e-3]
Hs = [16,32,64]
for j in range(nConfigs):

    # Draw a radnom sample for each hp of interest 
    L = choice(Ls)
    K = choice(Ks)
    lr = choice(lrs)
    dpt = choice(dpts)
    beta = choice(betas)
    H = choice(Hs)

    print(f'\nTraining nsf {lt} permutaions on {cols} with L={L}, K={K}, H={H}, b={beta}, dpt={dpt}')
    
    for seed in range(nSeeds):

        pythonCmd = f'python densityEstimate.py --model nsf --linear_transform_type {lt}'
        pythonCmd += f' --learning_rate {lr} --nLayers {L} --K {K} --dropout_probability {dpt}'
        pythonCmd += f' --hidden_dim {H} --decay {beta} --cols {cols} --detaCut {detaCut}'
        pythonCmd += f' --tag _SM_2b_p_{p} --prodTag FEB20  --iter {i} --num_blocks 1 --random_seed {seed}'
            
        job = f'nsf_{lt}_L{L}_K{K}_H{H}_dpt{dpt}_decay{beta}_SM_2b_p_{p}_lr{lr}_seed{seed}'
        bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0026]\' -W 25:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
        print(bsubCmd)
        os.system(bsubCmd)

# Ok, then not all my jobs succeeded for the num_blocks = 1, so I'll just rerun 
# the jobs that failed right here.
Ls = [5,  5, 10, 10, 10,  5, 10, 10, 10, 10, 10,  5, 10, 10,  5, 10,  5,
      5,  5,  5, 10,  5,  5, 10,  5,  5,  5, 10, 10,  5, 10,  5]
Ks = [4, 8, 4, 8, 4, 8, 8, 4, 8, 8, 4, 4, 8, 8, 8, 8, 4, 8, 4, 4, 8, 4,
      8, 4, 8, 8, 8, 4, 8, 8, 8, 4]
lrs = [0.0003, 0.0003, 0.0005, 0.0005, 0.0005, 0.0003, 0.0003, 0.001 ,
       0.0005, 0.0005, 0.001 , 0.001 , 0.0003, 0.001 , 0.001 , 0.0005,
       0.0005, 0.0003, 0.0003, 0.0005, 0.0003, 0.0003, 0.001 , 0.0003,
       0.0005, 0.001 , 0.0003, 0.0003, 0.0005, 0.0005, 0.001 , 0.0003]
dpts = [0.2, 0.1, 0.1, 0.2, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1,
        0.2, 0.1, 0.2, 0.2, 0.1, 0.2, 0.1, 0.1, 0.1, 0.1, 0.2, 0.1, 0.1,
        0.2, 0.1, 0.1, 0.1, 0.1, 0.1]
betas = [1e-4, 1e-4, 1e-3, 1e-4, 1e-6,    0,    0, 1e-4,
         1e-5, 1e-4, 1e-6, 1e-3, 1e-5,    0, 1e-3,    0,
         1e-4, 1e-4, 1e-4, 1e-6, 1e-4, 1e-3, 1e-3, 1e-4,
         1e-4,    0, 1e-4, 1e-3,    0, 1e-6, 1e-3,    0]
Hs = [64, 32, 16, 32, 32, 16, 32, 32, 32, 64, 32, 16, 64, 32, 64, 32, 32,
      16, 32, 64, 32, 64, 16, 64, 32, 64, 32, 32, 32, 16, 32, 64]

for L, K, lr, dpt, beta, H in zip(Ls,Ks,lrs,dpts,betas,Hs):
    
    print(f'\nTraining nsf {lt} permutaions on {cols} with L={L}, K={K}, H={H}, b={beta}, dpt={dpt}')
    
    for seed in range(nSeeds):
            
        seedTag = f'_seed{seed}' if seed != 10 else ''
        decayTag = '' if beta == 0 else f'_{beta}'
        dptTag = '' if dpt == 0 else f'_p{dpt}'
        modelPath = f'models/data16_PFlow-FEB20-5jets_SM_2b_p_0.01_2b{detaCut}{seedTag}/'
        modelPath += f'nsf_rq-coupling_{colTag}_{lt}_{L}_layers_H_{H}_1_blocks_K_{K}_B_3_lr_{lr}{decayTag}{dptTag}_iter{i}/'
        modelPath += 'model.pt'
            
        if os.path.exists(modelPath):
            print('Already trained',modelPath)
            continue
                    
        pythonCmd = f'python densityEstimate.py --model nsf --linear_transform_type {lt}'
        pythonCmd += f' --learning_rate {lr} --nLayers {L} --K {K} --dropout_probability {dpt}'
        pythonCmd += f' --hidden_dim {H} --decay {beta} --cols {cols} --detaCut {detaCut}'
        pythonCmd += f' --tag _SM_2b_p_{p} --prodTag FEB20  --iter {i} --num_blocks 1 --random_seed {seed}'
            
        job = f'nsf_{lt}_L{L}_K{K}_H{H}_1_blocks_dpt{dpt}_decay{beta}_SM_2b_p_{p}_lr{lr}_seed{seed}'
        bsubCmd = f"bsub -R \'select[centos7 && bubble]\' -W 25:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
        print(bsubCmd)
        os.system(bsubCmd)

#Next: let's do the same thing with the models I've been training w/ the models 
#      w/ two res blocks!
Ls = [ 5,  5,  5,  5,  5, 10, 10, 10,  5,  5,  5,  5, 10,  5,  5, 10, 10,
        5, 10, 10, 10, 10,  5,  5, 10, 10, 10, 10,  5, 10, 10, 10, 10,  5,
       10, 10,  5,  5,  5, 10,  5,  5, 10,  5, 10,  5, 10,  5,  5,  5, 10,
       10, 10,  5,  5,  5,  5, 10]
Ks = [4, 4, 4, 4, 4, 4, 4, 8, 4, 4, 8, 8, 4, 8, 4, 4, 8, 8, 8, 8, 4, 4,
       8, 4, 8, 4, 8, 8, 4, 4, 4, 8, 8, 8, 8, 4, 8, 8, 4, 4, 8, 4, 8, 8,
       4, 8, 8, 8, 8, 4, 8, 8, 8, 4, 8, 4, 4, 4]
lrs = [0.0003, 0.0003, 0.0003, 0.001 , 0.001 , 0.0003, 0.001 , 0.0005,
       0.001 , 0.0005, 0.001 , 0.0003, 0.001 , 0.0003, 0.0005, 0.0003,
       0.0003, 0.001 , 0.001 , 0.0005, 0.0005, 0.0005, 0.001 , 0.0005,
       0.0005, 0.001 , 0.0003, 0.001 , 0.0005, 0.0005, 0.0003, 0.0003,
       0.001 , 0.001 , 0.001 , 0.0005, 0.0005, 0.001 , 0.001 , 0.001 ,
       0.001 , 0.0005, 0.0003, 0.001 , 0.001 , 0.001 , 0.001 , 0.001 ,
       0.0003, 0.0003, 0.001 , 0.0003, 0.001 , 0.001 , 0.001 , 0.0003,
       0.0005, 0.001 ]
dpts = [0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.1, 0.1, 0.2, 0.1, 0.1,
       0.2, 0.2, 0.1, 0.2, 0.1, 0.2, 0.1, 0.2, 0.2, 0.1, 0.2, 0.1, 0.2,
       0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.1,
       0.1, 0.2, 0.1, 0.1, 0.2, 0.2, 0.1, 0.2, 0.1, 0.2, 0.2, 0.2, 0.1,
       0.2, 0.2, 0.1, 0.2, 0.1, 0.2]
betas = [1e-06, 0.001, 1e-05, 0, 1e-06, 0, 1e-05, 0.0001,
         0, 1e-06, 1e-06, 0.001, 0.001, 0.001, 1e-05, 0.0001,
         0.001, 0, 1e-06, 1e-06, 1e-06, 1e-05, 0.001, 0.0001,
         0, 0.001, 0.0001, 0.0001, 0.0001, 1e-05, 1e-05, 1e-05,
         1e-06, 0.001, 0.001, 1e-05, 1e-05, 1e-06, 0.001, 0,
         0, 0.001, 1e-05, 0.001, 0.0001, 1e-06, 0.001, 1e-05,
         0, 1e-05, 1e-06, 0, 0.001, 1e-06, 0.0001, 1e-05, 0, 1e-05]
Hs = [32, 32, 16, 16, 32, 32, 64, 16, 16, 32, 32, 32, 32, 16, 16, 64, 64,
      16, 64, 16, 32, 16, 32, 32, 16, 16, 32, 16, 32, 32, 16, 16, 64, 64,
      16, 16, 16, 16, 32, 64, 64, 16, 16, 16, 32, 16, 16, 32, 64, 32, 32,
      64, 16, 32, 64, 64, 32, 32]
iters = [39,  0, 11, 27, 19, 48, 39, 30, 20, 42, 35, 14, 33, 47, 17, 27, 16,
         34, 36, 37, 12, 49, 19, 42, 18, 13, 29, 43,  1, 11, 21,  8, 33, 32,
         41, 49, 25, 17,  7, 48, 18, 10, 26, 36, 28, 44, 23, 26, 22, 14, 46,
         34,  6, 35,  4, 45, 22, 29] 
         
for L, K, lr, dpt, beta, H, i in zip(Ls,Ks,lrs,dpts,betas,Hs,iters):
    
    print(f'\nTraining nsf {lt} permutaions on {cols} with L={L}, K={K}, H={H}, b={beta}, dpt={dpt}')
    
    for seed in range(nSeeds):
            
        seedTag = f'_seed{seed}' if seed != 10 else ''
        decayTag = '' if beta == 0 else f'_{beta}'
        dptTag = '' if dpt == 0 else f'_p{dpt}'
        modelPath = f'models/data16_PFlow-FEB20-5jets_SM_2b_p_0.01_2b{detaCut}{seedTag}/'
        modelPath += f'nsf_rq-coupling_{colTag}_{lt}_{L}_layers_H_{H}_2_blocks_K_{K}_B_3_lr_{lr}{decayTag}{dptTag}_iter{i}/'
        modelPath += 'model.pt'
            
        if os.path.exists(modelPath):
            print('Already trained',modelPath)
            continue
                    
        pythonCmd = f'python densityEstimate.py --model nsf --linear_transform_type {lt}'
        pythonCmd += f' --learning_rate {lr} --nLayers {L} --K {K} --dropout_probability {dpt}'
        pythonCmd += f' --hidden_dim {H} --decay {beta} --cols {cols} --detaCut {detaCut}'
        pythonCmd += f' --tag _SM_2b_p_{p} --prodTag FEB20  --iter {i} --num_blocks 2 --random_seed {seed}'
            
        job = f'nsf_{lt}_L{L}_K{K}_H{H}_2_blocks_dpt{dpt}_decay{beta}_SM_2b_p_{p}_lr{lr}_seed{seed}'
        bsubCmd = f"bsub -R \'select[centos7 && deft]\' -W 25:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
        print(bsubCmd)
        os.system(bsubCmd)

'''

# Next - experiments with the MDpT cut. I ended up just choosing 5 of the trainings 
# from the random search that I did 
Ls = [10, 10, 10, 10, 5]
Hs = [64, 64, 32, 32, 16]
num_blocks = [2,2,1,2,2]
lrs = [1e-3, 3e-4, 1e-3, 1e-3, 5e-4]
betas = [1e-5, 1e-4, 1e-6, 1e-3, 1e-5]
dpts = [0.1, 0.1, 0.1, 0.1, 0.2]

K=4
'''
for mdTag,mdFlag in zip(['','_MDpT'],['','--MDpT']): #zip(['_MDpT'],['--MDpT']): #
    for year in ['16','17','18']:# '16,17,18'

        for L, H, nb, lr, beta, dpt in zip(Ls, Hs, num_blocks, lrs, betas, dpts):
            
            print(f'\nTraining  nsf {lt} permutaions on {cols} with L={L}, K={K}, H={H}, b={beta}, dpt={dpt}')
            
            yrCat = year.replace(',','')
            
            for seed in range(nSeeds):
                    
                seedTag = f'_seed{seed}' if seed != 10 else ''
                decayTag = '' if beta == 0 else f'_{beta}'
                dptTag = '' if dpt == 0 else f'_p{dpt}'
                modelPath = f'models/data{yrCat}_PFlow-FEB20-5jets_SM_2b_p_0.01_2b{detaCut}{mdTag}{seedTag}/'
                modelPath += f'nsf_rq-coupling_{colTag}_{lt}_{L}_layers_H_{H}_{nb}_blocks_K_{K}_B_3_lr_{lr}{decayTag}{dptTag}_iter{i}/'
                modelPath += 'model.pt'
                    
                if os.path.exists(modelPath):
                    print('Already trained',modelPath)
                    #continue
                            
                pythonCmd = f'python densityEstimate.py --model nsf --linear_transform_type {lt} --year {year}'
                pythonCmd += f' --learning_rate {lr} --nLayers {L} --K {K} --dropout_probability {dpt}'
                pythonCmd += f' --hidden_dim {H} --decay {beta} --cols {cols} --detaCut {detaCut} {mdFlag}'
                pythonCmd += f' --tag _SM_2b_p_{p} --prodTag FEB20  --iter {i} --num_blocks {nb}'
                pythonCmd += f' --random_seed {seed} --preTrainSample data161718'
                    
                job = f'nsf{yrCat}_{lt}_L{L}_K{K}_H{H}_{nb}_blocks_dpt{dpt}_decay{beta}_SM_2b_p_{p}_lr{lr}_seed{seed}{mdTag}'
                bsubCmd = f"bsub -R \'select[centos7 && deft]\' -W 50:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
                print(bsubCmd)
                os.system(bsubCmd)
'''
mdTag, mdFlag = '', ''  
for ntag in [4,3]:
    for year in ['16']: 

        for L, H, nb, lr, beta, dpt in zip(Ls, Hs, num_blocks, lrs, betas, dpts):
            
            print(f'\nTraining  nsf {lt} permutaions on {cols} with L={L}, K={K}, H={H}, b={beta}, dpt={dpt}')
            
            for seed in range(nSeeds):
                    
                seedTag = f'_seed{seed}' if seed != 10 else ''
                decayTag = '' if beta == 0 else f'_{beta}'
                dptTag = '' if dpt == 0 else  f'_p{dpt}'
                modelPath = f'models/data{year}_PFlow-FEB20-5jets_SM_2b_{ntag}b{detaCut}{mdTag}{seedTag}/'
                modelPath += f'nsf_rq-coupling_{colTag}_{lt}_{L}_layers_H_{H}_{nb}_blocks_K_{K}_B_3_lr_{lr}{decayTag}{dptTag}_iter{i}/'
                modelPath += 'model.pt'
                    
                if os.path.exists(modelPath):
                    print('Already trained',modelPath)
                            
                pythonCmd = f'python densityEstimate.py --model nsf --linear_transform_type {lt} --year {year}'
                pythonCmd += f' --learning_rate {lr} --nLayers {L} --K {K} --dropout_probability {dpt}'
                pythonCmd += f' --hidden_dim {H} --decay {beta} --cols {cols} --detaCut {detaCut} {mdFlag}'
                pythonCmd += f' --tag _SM_2b --prodTag FEB20  --iter {i} --num_blocks {nb}'
                pythonCmd += f' --random_seed {seed} --ntag {ntag} --postTag _detaCut'
                    
                job = f'nsf{year}_{lt}_L{L}_K{K}_H{H}_{nb}_blocks_dpt{dpt}_decay{beta}_SM_2b_{ntag}b_lr{lr}_seed{seed}{mdTag}'
                bsubCmd = f"bsub -R \'select[centos7 && deft]\' -W 50:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
                print(bsubCmd)
                os.system(bsubCmd)


