'''
My modification of the BNAF models to implement HCNAF.
'''

import torch
import math 

class HCNAF(torch.nn.Sequential):
    """
    Class that extends ``torch.nn.Sequential`` for constructing a Block Neural 
    Normalizing Flow.
    """
    
    def __init__(self, *args, res: str = None):
        """
        Parameters
        ----------
        *args : ``Iterable[torch.nn.Module]``, required.
            The modules to use.
        res : ``str``, optional (default = None).
            Which kind of residual connection to use. ``res = None`` is no residual 
            connection, ``res = 'normal'`` is ``x + f(x)`` and ``res = 'gated'`` is
            ``a * x + (1 - a) * f(x)`` where ``a`` is a learnable parameter.
        """
        
        super(HCNAF, self).__init__(*args)
        
        self.res = res
        
        if res == 'gated':
            self.gate = torch.nn.Parameter(torch.nn.init.normal_(torch.Tensor(1)))
    
    def forward(self, inputs : torch.Tensor, cond_inputs: torch.Tensor):
        """
        Parameters
        ----------
        inputs : ``torch.Tensor``, required.
            The input tensor.
        Returns
        -------
        The output tensor and the log-det-Jacobian of this transformation.
        """
            
        outputs = inputs
        grad = None
        
        for module in self._modules.values():
            outputs, grad = module(outputs, cond_inputs, grad)
            grad = grad if len(grad.shape) == 4 else grad.view(grad.shape + [1, 1])
            
        assert inputs.shape[-1] == outputs.shape[-1]
    
        grad = grad.squeeze(-1).squeeze(-1)
        
        if self.res == 'normal':
            return inputs + outputs, torch.nn.functional.softplus(grad).sum(-1)
        elif self.res == 'gated':
            return self.gate.sigmoid() * outputs + (1 - self.gate.sigmoid()) * inputs, \
                (torch.nn.functional.softplus(grad + self.gate) - \
                 torch.nn.functional.softplus(self.gate)).sum(-1)
        else:
            return outputs, grad.sum(-1)
        
    def _get_name(self):
        return 'HCNAF(res={})'.format(self.res)
        
       
class CondMaskedWeight(torch.nn.Module):
    """
    Module that implements a linear layer with block matrices with positive diagonal blocks.
    Moreover, it uses Weight Normalization (https://arxiv.org/abs/1602.07868) for stability.
    """
    
    def __init__(self, 
                 in_features : int, 
                 out_features : int, 
                 dim : int, 
                 D_in : int = 2,
                 H: int = 16,
                 device='cpu'
                ):
        """
        Parameters
        ----------
        in_features : ``int``, required.
            The number of input features per each dimension ``dim``.
        out_features : ``int``, required.
            The number of output features per each dimension ``dim``.
        dim : ``int``, required.
            The number of dimensions of the input of the flow.
        D_in: ``int``, default 2
            The number of conditional inputs
        H: ``int``, default 16
            The hidden dim for the mlp defining the hypernetwork
        """
            
        super(CondMaskedWeight, self).__init__()
        
        self.in_features = in_features
        self.out_features = out_features 
        self.dim = dim
        self.D_in = D_in
        self.H = H
    
        mask_d = torch.zeros(out_features, in_features)
        for i in range(dim):
            mask_d[i * (out_features // dim):(i + 1) * (out_features // dim),
                   i * (in_features // dim):(i + 1) * (in_features // dim)] = 1
        self.register_buffer('mask_d', mask_d)
            
        mask_o = torch.ones(out_features, in_features)
        for i in range(dim):
            mask_o[i * (out_features // dim):(i + 1) * (out_features // dim),
                   i * (in_features // dim):] = 0
        self.register_buffer('mask_o', mask_o)

        # Define the hypernetwork that sets the parameters for the weight and bias vectors
        D_out = (dim*(dim+1) // 2) * (out_features // dim) * (in_features // dim)
        D_out += 2 * out_features
        model = torch.nn.Sequential(
                                    torch.nn.Linear(D_in, H),
                                    torch.nn.ReLU(),
                                    torch.nn.Linear(H, D_out),
                                   )
        
        self.model = model
        
    def get_weights(self, cond_inputs, batch_size):
        """
        Computes the weight matrix using masks and weight normalization.
        It also compute the log diagonal blocks of it.
        """
        
        cond_outputs = self.model(cond_inputs)
        
        # Fill the matrix W st the diagonal entries are positive
        #weight = torch.zeros(batch_size, self.out_features, self.in_features).to(self.device)
        weight = cond_inputs.new_full((batch_size, self.out_features, self.in_features), fill_value=0)
    
        q = self.out_features * self.in_features // self.dim
        weight[:,self.mask_d.bool()] = cond_outputs[:,:q]
        
        r = self.in_features*self.out_features*(self.dim+1) // (2* self.dim)
        weight[:,self.mask_o.bool()] = cond_outputs[:, q:r]
        
        w = torch.exp(weight) * self.mask_d + weight * self.mask_o
        
        # Ok, I'm trying taking out this weight normalization
        w_squared_norm = (w ** 2).sum(-1, keepdim=True)
        # The diagonal weight for the weight noramalization
        diag_weight = cond_outputs[:,r:r+self.out_features].unsqueeze(-1)
        w = diag_weight.exp() * w / w_squared_norm.sqrt()
        
        wpl = diag_weight + weight - 0.5 * torch.log(w_squared_norm) 
        
        # Fill the bias vector
        bias = cond_outputs[:,r+self.out_features:]
        
        return w.transpose(-2, -1), wpl.transpose(-2, -1)[:,self.mask_d.bool().t()].view(batch_size,
            self.dim, self.in_features // self.dim, self.out_features // self.dim), bias


    def forward(self, inputs, cond_inputs, grad : torch.Tensor = None):
        """
        Parameters
        ----------
        inputs : ``torch.Tensor``, required.
            The input tensor.
        cond_inputs: ``torch.Tensor``, required
            The input tensor that we condition on
        grad : ``torch.Tensor``, optional (default = None).
            The log diagonal block of the partial Jacobian of previous transformations.
        Returns
        -------
        The output tensor and the log diagonal blocks of the partial log-Jacobian of previous 
        transformations combined with this transformation.
        """
        
        batch_size = inputs.shape[0]
        w, wpl, b = self.get_weights(cond_inputs,batch_size)
        
        # It's no longer necessary to need to repeat exs along the batch dim
        g = wpl.transpose(-2, -1)
        
        return inputs.unsqueeze(1).matmul(w).squeeze(1) + b, torch.logsumexp(
            g.unsqueeze(-2) + grad.transpose(-2, -1).unsqueeze(-3), -1) if grad is not None else g

    def __repr__(self):
        return 'CondMaskedWeight(in_features={}, out_features={}, dim={}, D_in={}, H={})'.format(
            self.in_features, self.out_features, self.dim, self.D_in, self.H)

class CondSequential(torch.nn.Sequential):
    """
    Class that extends ``torch.nn.Sequential`` for computing the output of 
    the function alongside with the log-det-Jacobian of such transformation.
    """
    
    def forward(self, inputs : torch.Tensor, cond_inputs : torch.Tensor):
        """
        Parameters
        ----------
        inputs : ``torch.Tensor``, required.
            The input tensor.
        Returns
        -------
        The output tensor and the log-det-Jacobian of this transformation.
        """
        
        log_det_jacobian = 0.
        for i, module in enumerate(self._modules.values()):
            inputs, log_det_jacobian_ = module(inputs, cond_inputs)
            log_det_jacobian += log_det_jacobian_
        return inputs, log_det_jacobian

class CondTanh(torch.nn.Tanh):
    """
    Class that extends ``torch.nn.Tanh`` additionally computing the log diagonal
    blocks of the Jacobian.
    """

    def forward(self, inputs, cond_input: torch.Tensor = None, grad : torch.Tensor = None):
        """
        Parameters
        ----------
        inputs : ``torch.Tensor``, required.
            The input tensor.
        grad : ``torch.Tensor``, optional (default = None).
            The log diagonal blocks of the partial Jacobian of previous transformations.
        Returns
        -------
        The output tensor and the log diagonal blocks of the partial log-Jacobian of previous 
        transformations combined with this transformation.
        """
        
        g = - 2 * (inputs - math.log(2) + torch.nn.functional.softplus(- 2 * inputs))
        return torch.tanh(inputs), (g.view(grad.shape) + grad) if grad is not None else g

def create_cond_model(num_flows=1, num_layers=2, hidden_dim=50, dim=1,
                      cond_dim=2, hypernet_h=16,
                      device='cpu', verbose=False):
    '''
    Inputs:
    - num_flows: # of flows to stack
    - num_layers: # of layers / flow
    - hidden_dim: a,b for the weight matrix  
    - dim: # of dimensions that we're trying to predict
    - cond_dim: The dimension of the conditioning space
    - batch_size: The batch_size (needed for the CondMaskedWeight network)
    - hypernet_h: The hidden dim defining the the dimension of the
                  mlp defining the (unconstrainesd) hypernetwork
    - verbose
    
    Returns: 
    - model: An instance of a pytorch Sequential model
    '''
    
    kwargs = {'dim': dim, 'H': hypernet_h, 'D_in': cond_dim, 'device':device}
    
    flows = []
    for f in range(num_flows):
        layers = []
        for _ in range(num_layers - 1):
            layers.append(CondMaskedWeight(dim * hidden_dim,
                                           dim * hidden_dim, **kwargs))
            layers.append(CondTanh())

        flows.append(
            HCNAF(*([CondMaskedWeight(dim, dim * hidden_dim, **kwargs), CondTanh()] + \
                   layers + \
                   [CondMaskedWeight(dim * hidden_dim, dim, **kwargs)]),\
                 res='gated' if f < num_flows - 1 else False
            )
        )

        if f < num_flows - 1:
            flows.append(Permutation(dim, 'flip'))

    model = CondSequential(*flows).to(device)
    
    if verbose:
        print('{}'.format(model))
        print('Parameters={}, n_dims={}'.format(sum((p != 0).sum() 
                                                    if len(p.shape) > 1 else torch.tensor(p.shape).item() 
                                                    for p in model.parameters()), 2))
    
    return model


