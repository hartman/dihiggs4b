# diHiggs4b

Some of my first R&D for the $hh\rightarrow 4b$ resolved analysis.

The code for running the analysis will live in `code`, which will process miniNtuples in parallel, and create pandas DataFrames for the events in the analysis.
The output DataFrames will be put in the `data` directory. As I run the Notebooks, I'll be saving the output to the `figures` directory to save all the studies that I've been looking into!

- `Background Sculpting`: One of the main parts where this analysis can be improved is the sculpting of th QCD background, so we wanted to be able to take apart the current analysis, and see which cuts was sculpting the background, and how additional cuts might impacting our performance.
- `code`: Where the code to go from miniNtuples -> pd.DataFrames lives
- `data`: Where I save my pd.DataFrames get saves to study and make plots in Notebooks
- `figures`: Saving the figures I make with my notebooks, since they often run with different flags 
- `oldStudies`: This is the first pass for some of the studies that I was doing that I wanted to redo in the new setup!
- `Intuition-Studies`: These are some of the exploratory plots that I was making when I first started the anlaysis, but it's not super streamlined or flexible. There's a README inside this folder for further explanations for what is inside.
- `GraphNN`: I was thinking it might be fun to look at a graph neural network for the HC pairings, but it's unclear right now whether this is over kill or not for the problem. All that's inside rn is the first step of the pre-processing.

-
