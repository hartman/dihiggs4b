FROM nvidia/cuda:10.1-cudnn7-runtime-ubuntu18.04 as base

FROM base as builder
RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y \
      python3 \
      python3-pip && \
    apt-get -y autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt-get/lists/*
RUN python3 -m pip install --upgrade --no-cache-dir pip setuptools wheel && \
    python3 -m pip install --no-cache-dir tensorflow torch && \
    python3 -m pip list

FROM base
# Use C.UTF-8 locale to avoid issues with ASCII encoding
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
COPY --from=builder /lib/x86_64-linux-gnu /lib/x86_64-linux-gnu
COPY --from=builder /usr/local /usr/local
COPY --from=builder /usr/bin/python3 /usr/bin/python3
COPY --from=builder /usr/bin/python3.6 /usr/bin/python3.6
COPY --from=builder /usr/bin/pip3 /usr/bin/pip3
COPY --from=builder /usr/lib/python3 /usr/lib/python3
COPY --from=builder /usr/lib/python3.6 /usr/lib/python3.6
COPY --from=builder /usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu


# FROM pytorch/pytorch:1.2-cuda10.0-cudnn7-runtime
# 
## ensure locale is set during build
ENV LANG C.UTF-8
 
ARG DEBIAN_FRONTEND=noninteractive
 
RUN pip install dgl-cu100 && \
    pip install pytorch-lightning && \
    pip install torchsummary && \
    pip install nodejs && \
    pip install tensorflow && \
    pip install keras && \
    pip install uproot && \
    pip install jupyter && \
    pip install jupyterhub && \
    pip install jupyterlab && \
    pip install matplotlib && \
    pip install seaborn && \
    pip install hep_ml && \
    pip install sklearn && \
    pip install tables && \
    pip install papermill pydot Pillow && \
    pip install pyhf nflows
     
RUN apt-get update && \
    apt-get install -y git debconf-utils && \
    echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
    echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
    apt-get install -y krb5-user && \
    apt-get install -y vim less screen graphviz python3-tk wget && \
    apt-get install -y jq tree hdf5-tools bash-completion && \
    apt-get update && apt-get install -y git
# 
# RUN pip install git+https://github.com/rtqichen/torchdiffeq
#     
# ## run jupyter notebook by default unless a command is specified
# CMD ["jupyter", "notebook", "--port", "33333", "--no-browser"]

