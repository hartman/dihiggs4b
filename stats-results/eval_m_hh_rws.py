'''
Want to check (now j for the 4b config) the limits with stats and sys for the 
min(dR), pag Db, pag b-tag for both sorts *and* both SRs
'''

import os
import numpy as np

# cd into Sean's folder
os.chdir('../non-resonant-studies')
    
'''
min_dR: Both *inclusive* above 350 GeV trainings
'''
outDir = '../stats-results/min_dR_mhh_rws'
if not os.path.exists(outDir): os.mkdir(outDir)
yrs = "16 17 18"

baseSig = '../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16{}/NanoNTuple.root'
baseBkg = '../data/RR/nom_trigs_unblind/data{}_min_dR_VEC_sr_124_117_NN_100_bootstraps.root'

sigList = [baseSig.format(i) for i in ['a','d','e']]
bkgList = [baseBkg.format(yr) for yr in yrs.split(' ')]

sFiles = ' '.join(sigList)
bFiles = ' '.join(bkgList)

kl_str = "1 3 5 10"

print('\n\n\n\nBaseline limits:\n\n\n\n\n')

# First nominal (inclusive training and HT split)
cmd =  f'python run_limits.py -d {bFiles} -s {sFiles} -y {yrs} --format resolved-recon'
cmd += f' --SR-center 124 117 --bins 40 --range 200 1200 '
cmd += f' --no-norm --outDir {outDir} --kl {kl_str} --label baseline'

for e in ['',' --stat-only']:
    cmd += e
    print(cmd)
    os.system(cmd)

print('\n\n\n\nm_hh > 350 GeV limits:\n\n\n\n\n')

# Next: Inclusive training: but start and 350 GeV, and w/ 1 NP
cmd =  f'python run_limits.py -d {bFiles} -s {sFiles} -y {yrs} --format resolved-recon'
cmd += f' --SR-center 124 117 --bins 34 --range 350 1200 '
cmd += f' --no-norm --systvar None --outDir {outDir} --kl {kl_str} --label eval_350_cut'

for e in ['',' --stat-only']:
    cmd += e
    print(cmd)
    os.system(cmd)


print('\n\n\n\nReweighting *and* eval with m_hh > 350 GeV limits:\n\n\n\n\n')
# Finally: Train with m_hh cut - also 1 NP
label = f'm_hh_gt_350'
cmd =  f'python run_limits.py -d {bFiles} -s {sFiles} -y {yrs} --format resolved-recon'
cmd += f' --SR-center 124 117 --bins {b} --range {r[0]} {r[1]} --wtag _{label}'
cmd += f' --no-norm --systvar None --outDir {outDir} --kl {kl_str} --label rw_{label}'

for e in ['',' --stat-only']:
    cmd += e
    print(cmd)
    os.system(cmd)

'''
DHH: Both for above and below 350 GeV trainings
'''
# outDir = '../stats-results/DHH_mhh_rws'
# if not os.path.exists(outDir): os.mkdir(outDir)
# yrs = "16 17 18"
# 
# baseSig = '../data/RR/NNT_DEC20_DHH/MC/600043_mc16{}/NanoNTuple.root'
# baseBkg = '../data/RR/nom_trigs_unblind/data{}_DHH_sr_124_117.root'
# 
# sigList = [baseSig.format(i) for i in ['a','d','e']]
# bkgList = [baseBkg.format(yr) for yr in yrs.split(' ')]
# 
# sFiles = ' '.join(sigList)
# bFiles = ' '.join(bkgList)
# 
# bsTag = '_NN_100_bootstraps'
# rwConfig = 'dPhi_HC_202020'
# 
# kl_str = ' '.join[str(kl) for kl in np.arange(-20,21)]
# 
# #for op, b, r in zip(['gt','lt'],[34,6],[(350,1200),(200,350)]):
# op, b, r = 'lt',6,(200,350) 
# 
# label = f'm_hh_{op}_350'
# wtag = f'{bsTag}_{label}_{rwConfig}' 
# 
# cmd =  f'python run_limits.py -d {bFiles} -s {sFiles} -y {yrs} --format resolved-recon'
# cmd += f' --SR-center 124 117 --bins {b} --range {r[0]} {r[1]} --wtag {wtag}'
# cmd += f' --no-norm --systvar None --outDir {outDir} --kl {kl_str} --label {label}'
# 
# for e in ['',' --stat-only']:
# 
#     cmd += e
#     print(cmd)
#     os.system(cmd)