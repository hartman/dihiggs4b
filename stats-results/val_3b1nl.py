'''
val_3b1nl.py

Steering script to make the workspaces for 3b + not loose
'''
import os

yr=16
sFile = '../../hh4b/hh4b-resolved-reconstruction/SMNR_mc16a_merge.root'
bFile = '../data/RR/nr_bkts/isoMuon/data16_sr_124_117.root'

eta_edgs='0 0.75 1.5'
srTag = '--Xhh-cut 0 0.95 1.6'
outDir = f'../stats-results/first_3b1nl_test'
bCat = '3b1nl'

cmd =  f'python run_limits.py -d {bFile} -s {sFile} -y {yr} --format resolved-recon'
cmd += f' --SR-center 124 117 {srTag} --categorize dEta_hh --cat-edges {eta_edgs} '
cmd += f' --wtag _NN_100_bootstraps_{bCat} --no-norm --unblind --outDir {outDir} --label {bCat}_2d_cats'

print(cmd)
# ci = cTag.replace('--','').replace(' ','_')
# job=f'rev_dEta_hh_{eta_bins}_bins_{nSR}SR_{ti}Cat{ci}_unblind'
# bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q long -W 2000 -oo ../stats-results/log_files/{job}.txt -J {job} {cmd}"
# os.system(bsubCmd)



