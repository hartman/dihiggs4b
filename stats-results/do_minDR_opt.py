'''
Want to check (now j for the 4b config) the limits with stats and sys for the 
min(dR), pag Db, pag b-tag for both sorts *and* both SRs
'''

import os
import numpy as np
from argparse import ArgumentParser
from doAnalysisScalarPAG import getOutput

def getOutput(doSyst,b_cats,tag_str,yrs,new_SR,label=''):
    '''
    Recreate the output name structure that Sean uses in run_limits.py.
    '''
    
    out_name = 'lim-'
    HTcut=300
    if doSyst:
        out_name += 'systs-HTcut-%d-' % HTcut 
    else:
        out_name += 'stat-only-'

    # b-tagging categories
    for b_cat in b_cats:
        out_name+= '%s-' % b_cat

    # ntag_pag (if not ntag)
    if tag_str == 'ntag_pag':
        out_name+= '%s-' % tag_str 

    # if cat_var:
    #     out_name += '%s-cat-' % cat_var

    for yr in yrs:
        out_name+= '%s-' % yr

    # if kl == 1:
    out_name+='SM-HH'
    # else:
    #     out_name+='kl-%d' % kl

    if new_SR is not None:
        out_name+='-sr'
        for xi,yi,Xhh in zip(new_SR['SR_x'],new_SR['SR_y'],new_SR['Xhh_cuts']):
            out_name+=f'-{xi}_{yi}_{Xhh}'

    if label:
        out_name+=f'-{label}'

    out_name+= '.csv'

    out_name = os.path.join(out_dir,out_name)

    return out_name


if __name__ == '__main__':

    parser = ArgumentParser()

    parser.add_argument("--batch", action="store_true", help="Submit jobs to LSF batch system")
    parser.add_argument("--overwrite", action="store_true", 
                        help="Need to pass if you want to overwrite files that already exist")
    #parser.add_argument("--tag_str", type=str,default='ntag_pag', help="ntag or ntag_pag (defaults)")
    parser.add_argument("--yr", type=int,default=17, help="Year (default 17)")

    args = parser.parse_args()

    batch,yr = args.batch, args.yr

    # cd into Sean's folder
    os.chdir('../non-resonant-studies')
    
    b_cat = '4b'

    # Define an output dir to save the files to 
    out_dir = '../stats-results/min_dR_sr_opt'
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    sFile = '../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16d/NanoNTuple.root'
    bFile = '../data/RR/bkg_ests/test_Xwt_change/4b/MDR_VEC/data17_NN_100_bootstraps_IQR.root'

    # Loop over SR center
    for xi,yi in zip([120,124,125],[110,117,116]):

        # Loop over stat v syst
        for setting,doSyst in zip(['--stat-only',''],[False,True]):

            for Xhh in np.arange(1,2.5,.1): 

                pythonCmd = f'python run_limits.py -d {bFile} -s {sFile} -y {yr} {setting}'
                pythonCmd += f' --format resolved-recon --SR_x {xi} --SR_y {yi} --Xhh_cut {Xhh} --out_dir {out_dir} '
                
                job = f'sys_{b_cat}' if doSyst else f'stat_{b_cat}'
                
                new_SR = { 'SR_x':[float(xi)], 'SR_y':[float(yi)], 'Xhh_cuts':[Xhh] }
                out_file = getOutput(doSyst,[b_cat],'ntag',[yr],new_SR)
                job += f'_{xi}_{yi}_{Xhh}'
                
                print(pythonCmd)
                
                if args.overwrite or not os.path.exists(out_file):
                    print(f'Running to create {out_file}')
                    
                    if args.batch:
                        bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q short -oo ../stats-results/log_files/{job}.txt -J {job} {pythonCmd}"
                        os.system(bsubCmd)

                    else:
                        os.system(pythonCmd)
                else:
                    print(f'Not running: {out_file} exists')

