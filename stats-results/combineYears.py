'''
Goal: One of the time intensive steps of the combination is loading in all the NNTs
and making the corresponding histograms, so if I've already run the limits for each 
of the years separately, this script takes as input the given workspaces and then 
slices these workspaces together to geta a combined workspace.

To run the combine limit, I'm using Sean's muFinder class (slightly modified to j 
rely on the histograms in the workspaces and be decoupled from his data_prep_ntup
class
'''

import pyhf 
import numpy as np
from scipy.optimize import root_scalar
import time

from argparse import ArgumentParser
import json
from multiprocessing import Pool

# Import fcts used by Sean's muFinder
import os
os.sys.path.append('../non-resonant-studies')
from lim_utils import probit, norm, quick_bands


class mu_finder:
    def __init__(self, model, data, backend='pytorch', precision='64b'):
        
        inits = model.config.suggested_init()
        inits[ model.config.poi_index ] = 0.0
        asi_data = model.expected_data(inits)
        
        print(f'Running with {backend} with {precision} precision')
        pyhf.set_backend(backend, precision=precision)
        
        if backend=='numpy':
            from multiprocessing import Pool
        
        self.model = model
        self.data = data
        self.asi_data = asi_data
        self.scale = 1
        self.backend = backend

        
    def test_mu(self, mu_test, idx=2, alpha=0.05, obs=False):
        try:
            result = pyhf.infer.hypotest(mu_test, self.data, self.model, qtilde=False, return_expected_set=True)
        except:
            result = pyhf.infer.hypotest(mu_test, self.data, self.model, test_stat='q', return_expected_set=True)
        if obs:
            CLs_val=result[0]
            print("obs", CLs_val)
        else:
            CLs_val = result[1][idx]
        print("Param: %d, mu: %.3f, CLs: %.3f" % (idx, mu_test, CLs_val))
        return CLs_val-alpha
    
    def min_for_lim(self, args):
        idx,alpha,guess,xtol,window,obs = args
        return root_scalar(self.test_mu, bracket=(guess*(1-window),guess*(1+window)), 
                           args=(idx, alpha,obs), xtol =xtol)

    def run_scan(self, alpha=0.05,xtol=1e-2, threads=4):

        t0 = time.time()

        #Calculate qmu, asimov
        init_pars = self.model.config.suggested_init()
        fixed_params = self.model.config.suggested_fixed()
        par_bounds = self.model.config.suggested_bounds()
        par_bounds[self.model.config.poi_index] = [-40.0, 40.0]

        init_mu = init_pars[self.model.config.poi_index]
        
        qmu = pyhf.infer.test_statistics.qmu(init_mu, self.asi_data, self.model, 
                                             par_bounds=par_bounds,
                                             init_pars=init_pars, 
                                             fixed_params=fixed_params)
        
        #Guess - sigma from eq 31 in arXiv:1007.1727, then see quick_bands 
        med_guess = np.sqrt(init_mu**2/qmu)*probit(1-0.5*alpha)

        band_guess = quick_bands(med_guess)

        scale = self.scale 
        print("Initial guess for bands (mu guess = %.2f):" % init_mu)
        print(np.array(band_guess)*scale)

        print("Beginning optimization, mu valid to %.2f" % (xtol*scale))

        print("Finding med_mu for alpha = %.2f" % alpha)
        if self.backend == 'numpy':
            p_med = Pool(2)
        try:
            window = (-band_guess[0] + band_guess[4])/(band_guess[0] + band_guess[4])
            guess = band_guess[0]/(1-window)
            args_med = [(2, alpha, guess, xtol, window, True),
                        (2, alpha, guess, xtol, window, False)]
            if self.backend == 'numpy':
                out_obs_exp= p_med.map(self.min_for_lim,args_med)
            else:
                out_obs_exp = [self.min_for_lim(args) for args in args_med]
        except:
            print("Looks like initial guess was bad? Expanding for one more try")
            window = (-band_guess[0]/2. + band_guess[4]*2.)/(band_guess[0]/2. + band_guess[4]*2.)
            guess = (band_guess[0]/2.)/(1-window)
            args_med = [(2, alpha, guess, xtol, window, True),
                        (2, alpha, guess, xtol, window, False)]
            if self.backend == 'numpy':
                out_obs_exp= p_med.map(self.min_for_lim,args_med)
            else:
                out_obs_exp = [self.min_for_lim(args) for args in args_med]


        s_guess = quick_bands(out_obs_exp[1].root)
        print("Guess for bands with proper med:")
        print(np.array(s_guess)*scale)

        if self.backend == 'numpy':
            print("Refining bands in %d threads" % threads)
            p = Pool(threads)
            args = [(i, alpha, s_guess[i], xtol, 0.05, False) for i in [0,1,3,4]]
            band_out = p.map(self.min_for_lim,args)
        else:
            print("Refining bands")
            band_out = [self.min_for_lim((i, alpha, s_guess[i], xtol, 0.05, False)) for i in [0,1,3,4]]

        final_bands = [out_obs_exp[0].root*scale,
                       band_out[0].root*scale,
                       band_out[1].root*scale,
                       out_obs_exp[1].root*scale,
                       band_out[2].root*scale,
                       band_out[3].root*scale]
        print("Final bands:")
        print(final_bands)

        calls = 0
        for out in band_out:
            calls+=out.function_calls
        for out in out_obs_exp:
            calls+=out.function_calls

        t1 = time.time()
        interval = t1-t0
        print("Finished in", interval//60,"min", interval % 60, "s")
        print("%d function calls" % calls)
        return final_bands

    def run_scan_linear(self, lower_bound=0, upper_bound=16, npoints=51):
        '''
        Just run a linear scan (to compare the timing metrics)
        '''
        t0 = time.time()

        #Calculate qmu, asimov
        init_pars = self.model.config.suggested_init()
        fixed_params = self.model.config.suggested_fixed()
        par_bounds = self.model.config.suggested_bounds()
        par_bounds[self.model.config.poi_index] = [0, 40.0]

        init_mu = init_pars[self.model.config.poi_index]
        print("Using the qmu_tilde test statistic")
        
        mu_tests = np.linspace(0, 16, 5)
        
        obs_limit, exp_limits, (poi_tests, tests) = pyhf.infer.intervals.upperlimit(
            d, m, mu_tests, level=0.05, return_results=True,
        )
        
        t1 = time.time()
        interval = t1-t0
        print("Finished in", interval//60,"min", interval % 60, "s")
        print(obs_limit)
        print(exp_limits)

        final_bands = [obs_limit] + exp_limits

        print("Final bands:")
        print(final_bands)

        return final_bands



def combineWorkspaces(ws_in_list, ws_out=''):
    '''
    Goal: Given a *list* of the input workspace names, concat them, save the output 
    (if ws_out is passed) and return the pyhf workspace.
    '''
    
    ws = {k: [] for k in ['channels','observations']}
    
    for i, fname in enumerate(ws_in_list):
    
        with open(fname) as f:
            wsi = json.load(f)
        
        ws['channels']     += wsi['channels']
        ws['observations'] += wsi['observations']
        
        if i == 0:
            ws['measurements'] = wsi['measurements']
            ws['version']      = wsi['version']

    if ws_out:
        # Save the workspace
        with open(ws_out, 'w') as outfile:
            json.dump(ws, outfile)

    return pyhf.Workspace(ws)
    

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--ws_in", type=str,default=[],nargs="+" ,
                        help="Input workspaces passed, either a list of .json files, "\
                        +"or a dummy file name with the XX to be filled with the years 16,17,18")
    parser.add_argument("--backend",type=str,default='pytorch',help='pyhf backend (default pytorch)')
    parser.add_argument("--precision",type=str,default='64b',help='precision for backend (64b default)')
    parser.add_argument("--label",type=str,default='')

    # Arguments to test out a linear scan 
    parser.add_argument('--linear',action='store_true',help="Use the linear scan functionality")


    args = parser.parse_args()

    # Step 1: Parse the input workspaces 
    ws_in = args.ws_in 
    combine=True
    if len(ws_in) == 0:
        print('Need to pass a ws_in argument to specify which workspaces to run over')
    elif (len(ws_in) == 1) and ('XX' in ws_in):
        print('ws_in',ws_in)
        print('Filling in XX with the years: 16,17,18')
        ws_in = ws_in[0]
        ws_in_list = [ws_in.replace('XX',f'{yr}') for yr in [16,17,18]]
        ws_out = ws_in.replace('XX','16-17-18')
    elif (len(ws_in) == 1):
        ws_in = ws_in[0]
        print('ws_in',ws_in)
        print('Single (not wildcarded) workspace: no need to combine')
        combine = False
        
        assert len(args.label) > 0
        # for the limits that get written out
        ws_out = ws_in.replace('.json',args.label+'.json')
        
    else:
        print('Input .json ws passed as list.')
        print('WARNING: The output workspace + limits will not be saved rn.') 
        ws_in_list = ws_in
        # Need to add functionality
        ws_out = ''
    
    if combine:
        # Step 2: Create the combined workspace 
        ws = combineWorkspaces(ws_in_list,ws_out)
    else:
        with open(ws_in) as f:
            wsi = json.load(f)
        ws = pyhf.Workspace(wsi)
    
    # Step 3: Define the pyhf model and (Aimov - B only) dataset
    m = ws.model( measurement_name="Measurement",
                  modifier_settings={
                        "normsys": {"interpcode": "code4"},
                        "histosys": {"interpcode": "code4p"},
                    },
                )
    d = ws.data(m)
        
    # Step 4: Call the mu finder 
    backend, precision = args.backend, args.precision
    find = mu_finder(m, d, backend, precision)
    if args.linear:
        print('Calling linear scan')
        lims = find.run_scan_linear()
    else:
        lims = find.run_scan()
    
    # Step 5: Save the limits
    if ws_out:
        lim_out = ws_out.replace('ws','lim').replace('.json','.csv')
        with open(lim_out, 'w') as out_file:
            out_file.write("Obs,-2s,-1s,Exp,1s,2s\n")
            out_file.write(','.join([f'{li:.3f}' for li in lims]))
            out_file.write('\n')
    
        print('Saving output lim to',ws_out)
