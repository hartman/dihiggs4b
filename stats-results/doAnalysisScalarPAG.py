'''
Feb 2020

Sean made me some trainings for some different pairAGraph configs, so 
rn I'm going to take a look at them and see how the limits can be 
improved by optimizing the 
(1) SR center
(2) Xhh cutoff
'''

import os
import numpy as np
from argparse import ArgumentParser

def getOutput(doSyst,b_cats,tag_str,yrs,xi=0,yi=0,Xhh=0,label=''):
    '''
    Recreate the output name structure that Sean uses in run_limits.py.
    '''
    
    out_name = 'lim-'
    HTcut=300
    if doSyst:
        out_name += 'systs-HTcut-%d-' % HTcut 
    else:
        out_name += 'stat-only-'

    # b-tagging categories
    for b_cat in b_cats:
        out_name+= '%s-' % b_cat

    # ntag_pag (if not ntag)
    if tag_str == 'ntag_pag':
        out_name+= '%s-' % tag_str 

    # if cat_var:
    #     out_name += '%s-cat-' % cat_var

    for yr in yrs:
        out_name+= '%s-' % yr

    # if kl == 1:
    out_name+='SM-HH'
    # else:
    #     out_name+='kl-%d' % kl

    if xi>0:
        out_name+='-sr'
        #for xi,yi,cut in zip(new_SR['SR_x'],new_SR['SR_y'],new_SR['Xhh_cuts']):
        out_name+=f'-{xi}_{yi}_{Xhh}'

    if label:
        out_name+=f'-{label}'

    out_name+= '.csv'

    out_name = os.path.join(out_dir,out_name)

    return out_name

if __name__ == '__main__':

    parser = ArgumentParser()

    parser.add_argument("--batch", action="store_true", help="Submit jobs to LSF batch system")
    parser.add_argument("--overwrite", action="store_true", 
                        help="Need to pass if you want to overwrite files that already exist")
    parser.add_argument("--tag_str", type=str,default='ntag_pag', help="ntag or ntag_pag (defaults)")
    parser.add_argument("--yr", type=int,default=17, help="Year (default 17)")

    args = parser.parse_args()

    batch, tag_str, yr = args.batch, args.tag_str, args.yr

    # cd into Sean's folder
    os.chdir('../non-resonant-studies')

    # Looking @ 2017 rn, define the files
    bkgFile = '../data/SeanFiles/Db_sort/pairAGraph/{}_{}/pairAGraph_Db_sort_{}_NN_100_bootstraps.root'
    sFile = '../data/SMNR_mc16d-JUN20-5jets_2b1jBucketBug/df_preLN_lr_0p003_3b_Db_sort.parquet'

    if yr == 17: assert 'mc16d' in sFile

    # Define an output dir to save the files to 
    out_dir = '../stats-results/pag_Db_sort_scalarHC_2b1jBucketBug'
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    
    # First, I want to optimize all of the b-tag channels individualy
    # xi,yi = 125,114
    # for b_cat in ['3b1l','3b1nl']:
    #for b_cat,xi,yi in zip(['4b','3b1l','3b1nl'],[125]*3,[116,114,114]):
        bFile = bkgFile.format(b_cat,tag_str,yr)
        for setting,doSyst in zip(['--stat-only',''],[False,True]):
            for Xhh in [1,1.1,1.2, 1.3, 1.4, 1.5, 1.6,1.7,1.8,1.9,2]: #[]:# 
            
                pythonCmd = f'python run_limits.py -d {bFile} -s {sFile} -y {yr} {setting} --tag_str {tag_str}'
                pythonCmd += f' --Xhh_cut {Xhh} --SR_x {xi} --SR_y {yi} --out_dir {out_dir}'
                
                out_file = getOutput(doSyst,[b_cat],tag_str,[yr],xi,yi,Xhh)
                
                print(pythonCmd)
                
                if args.overwrite or not os.path.exists(out_file):
                    print(f'Running to create {out_file}')
                    
                    if args.batch:
                        job = f'{setting[2:]}_{b_cat}_{xi}_{yi}_{Xhh}'
                        bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q short -oo ../stats-results/log_files/{job}.txt -J {job} {pythonCmd}"
                        os.system(bsubCmd)

                    else:
                        os.system(pythonCmd)
                else:
                    print(f'Not running: {out_file} exists')
                
