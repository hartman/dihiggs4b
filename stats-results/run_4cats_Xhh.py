'''
Run the options for the Xhh categorization that we discussed w/ Nikos:
2 deta_hh + 2 X_hh categories
'''

import os
from subTimingStudies import writeSlurmFile
from var_bins_studies import get_bin_edgs

# 14th Jul - re-run this script for quads
#B_Q45     = " ".join([f"{BDIR}quad_45/data{yr}_Xhh_45_NN_100_bootstraps.root" for yr in [16,17,18]])

if __name__ == '__main__':

    SLURM_DIR = 'slurm_scripts'

    SDIR = "../../hh4b/hh4b-resolved-reconstruction"
    BDIR = "../data/RR/cryptotuples/ggF_rw"
    ODIR = '../stats-results/var_bins/'

    SFILE = " ".join([f"{SDIR}/SMNR_pythia_mc16{mc}.root" for mc in ['a','d','e']])
    #BFILE=" ".join([f"{BDIR}/data{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])
    BFILE = " ".join([f"../data/RR/cryptotuples/quad_45/data{yr}_Xhh_45_NN_100_bootstraps.root" for yr in [16,17,18]])

    base_cmd  = f"python run_limits.py -d {BFILE} -s {SFILE} -y 16 17 18 --no-norm"
    base_cmd += f" --underflow --overflow --backend jax --outDir {ODIR}"
    base_cmd += " --categorize dEta_hh --cat-edges 0 0.75 1.5 --Xhh-cut 0 0.95 1.6 --unblind"

    r = (275,1050)

    for bcat, blab in zip(['4b','4b 3b1l'],['4b','4b_3b1l']):
        #for res in [0.05, 0.06, 0.07, 0.08, 0.09, 0.1]:
        for res in [0.08]:
            for op1, clab1 in zip(['','--corr_cat'],['2_deta','2_deta_corr']):
                for op2, clab2 in zip(['','--corr_Xhh'],['2_Xhh','2_Xhh_corr']):

                    edg = get_bin_edgs(r,res=res)
                    edg_str = ' '.join([str(e) for e in edg])
                    label = f'{r[0]}_{r[1]}_res_' + f'{res}'.replace('.','p')
                    label += f"_{clab1}_{clab2}_Q45"
                    cmd = f'{base_cmd} --btagCats {bcat} --bins {edg_str} {op1} {op2} --label {label}'
                    print(cmd+"\n\n")

                    job_name = f'xhh_{blab}_{label}'
                    writeSlurmFile(cmd, job_name)
                    os.system(f"sbatch {SLURM_DIR}/{job_name}.sh")
