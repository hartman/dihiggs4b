'''
yrsSepFits.py

Goal: Since the yearsSplitTogether nb, I think I've zoned in a bit more on what 
exactly I'm wanting to plot, so I think I'll zone in now on the 3b1f studies, and 
I believe I've now editted the limit script for the outstanding Qs that Max had 
said were missing from my studies.
- quad 
- quadNP 
-
'''
import os
from subTimingStudies import writeSlurmFile 
from var_bins_studies import get_bin_edgs

SDIR = "../../hh4b/hh4b-resolved-reconstruction"
BDIR  = '../data/RR/cryptotuples/'

ODIR = '../stats-results/ggF_PUSH/'
SLURM_DIR = 'slurm_scripts'

SFILE = " ".join([f"{SDIR}/SMNR_pythia_mc16{mc}.root" for mc in ['a','d','e']])

os.chdir('../non-resonant-studies')

r = (275,1050)
res = 0.05
edg = get_bin_edgs(r,res=res)
bins = ' '.join([str(x) for x in edg])

B_RSEP    = " ".join([f"{BDIR}/splityr_bkt/data_baseline_bl_{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])
B_ROHE    = " ".join([f"{BDIR}/seanFixOHEyr_bkt/data_baseline_bl_{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])
B_QOHE    = " ".join([f"{BDIR}/quad_OHEyr_bkt/data_bl_{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])
B_QSEP    = " ".join([f"{BDIR}/quad_splityr_bkt/data_bl_{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])
B_QSEP_CG = " ".join([f"{BDIR}CG_quad_splityr_bkt/data{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])
B_QTILT   = " ".join([f"{BDIR}tilt_ellipse/data{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])

B_Q30 = " ".join([f"{BDIR}quad_30/data{yr}_Xhh_30_NN_100_bootstraps.root" for yr in [16,17,18]])
B_Q45 = " ".join([f"{BDIR}quad_45/data{yr}_Xhh_45_NN_100_bootstraps.root" for yr in [16,17,18]])
B_Q60 = " ".join([f"{BDIR}quad_60/data{yr}_Xhh_60_NN_100_bootstraps.root" for yr in [16,17,18]])

B_Q45_OHE = " ".join([f"{BDIR}quad_45_OHEyr/data{yr}_Xhh_45_NN_100_bootstraps.root" for yr in [16,17,18]])

# For CG's stuff, need to make sure we j select the nominal region get a ... 
#for bcat, unblind in zip(['3b1f'],[' --unblind --bkg_scale 0.1 --BS_scale 0.1']):
for bcat, unblind in zip(['4b','3b1f'],['',' --unblind --bkg_scale 0.1']):
    # for BFILE, cut,label in zip([B_RSEP, B_ROHE, B_QOHE, B_QSEP, B_QSEP_CG,B_QTILT],
    #                             ['']*4+[" --Xhh-cut  0 1.6" ]*2,
    #                             ['RSEP', 'ROHE', 'QOHE', 'QSEP', 'QSEP_CG','QTILT']):
    for BFILE, cut,label in zip([B_Q45_OHE],[''],['Q45_OHE']):
    #for BFILE, cut,label in zip([B_QTILT],[' --Xhh-cut  0 1.6'],['QTILT']):

        for op in ['','--systvar m_h1 m_h2 --HTcut 124 117','--stat-only','--stat-only --no-bootstrap']:
        #for op in ['','--systvar m_h1 m_h2 --HTcut 124 117']: 
    
            cmd  = f"python run_limits.py -d {BFILE} -s {SFILE} -y 16 17 18 --no-norm"
            cmd += f" --underflow --overflow  --outDir {ODIR} {cut}" # --backend jax
            cmd += f" --btagCats {bcat} {op} --bins {bins} --label {label}_res_p05_nomSR {unblind}"  
             
            print(cmd+"\n")
                
            # No need for slurm jobs b/c inclusive fits run hella fast :)
            os.system(cmd)
    #break

