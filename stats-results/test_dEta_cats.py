import os
import numpy as np

yrs = [16, 17, 18]
yrStr = ' '.join([str(yr) for yr in yrs])

baseSig = '../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16{}/NanoNTuple.root'
baseBkg = '../data/RR/nom_trigs_unblind/data{}_min_dR_VEC_sr_124_117_NN_100_bootstraps.root'

sigList = [baseSig.format(i) for i in ['a','d','e']]
bkgList = [baseBkg.format(yr) for yr in yrs]

sFiles = ' '.join(sigList)
bFiles = ' '.join(bkgList)

outDir = '../stats-results/dEta_cats'
os.chdir('../non-resonant-studies')

'''
Study 0: Baseline!!
'''
# label = 'baseline'
# for yr, sFile,bFile in zip([yrStr]+yrs, [sFiles]+sigList, [bFiles]+bkgList):
#     for option, opTag in zip(['--stat-only',''], ['stat','sepNP']):
# 
#         cmd =  f'python run_limits.py -d {bFile} -s {sFile} -y {yr} --format resolved-recon'
#         cmd += f' --SR-center 124 117 --no-norm --outDir {outDir} --label {label} {option}'
# 
#         print(cmd)
#         yrTag = str(yr).replace(' ','-')
#         job = f'{label}_{yrTag}_{opTag}'
#         bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q short -oo ../stats-results/log_files/{job}.txt -J {job} {cmd}"
#         os.system(bsubCmd)
# 


'''
Study 1: 3 cats, look at the impact of correlating them or not
'''
# label = '3_bins'
# for yr, sFile,bFile in zip([yrStr]+yrs, [sFiles]+sigList, [bFiles]+bkgList):
# 
#     for option, opTag in zip(['--stat-only','','--corr_cat'], ['stat','sepNP','corrNP']):
# 
#         cmd =  f'python run_limits.py -d {bFile} -s {sFile} -y {yr} --format resolved-recon'
#         cmd += f' --SR-center 124 117 --categorize dEta_hh --cat-edges 0 0.5 1 1.5 '
#         cmd += f' --no-norm --outDir {outDir} --label {label} {option}'
# 
#         print(cmd)
#         #os.system(cmd)
# 
#         yrTag = str(yr).replace(' ','-')
#         job = f'dEta_{label}_{yrTag}_{opTag}'
#         bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q short -oo ../stats-results/log_files/{job}.txt -J {job} {cmd}"
#         os.system(bsubCmd)

'''
Study 2: Vary the # of deta bins
'''
# for bins in [6,12]:
# 
#     label = f'{bins}_bins'
# 
#     eta_edgs = np.linspace(0,1.5,bins+1)
#     cat_edgs = ' '.join([str(ei) for ei in eta_edgs])
# 
#     for option, opTag in zip(['--stat-only','','--corr_cat'], ['stat','sepNP','corrNP']):
# 
#         cmd =  f'python run_limits.py -d {bFiles} -s {sFiles} -y {yrStr} --format resolved-recon'
#         cmd += f' --SR-center 124 117 --categorize dEta_hh --cat-edges {cat_edgs} '
#         cmd += f' --no-norm --outDir {outDir} --label {label} {option}'
# 
#         print(cmd)
# 
#         yrTag = yrStr.replace(' ','-')
#         job = f'dEta_{label}_{yrTag}_{opTag}'
#         bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q long -W 720 -oo ../stats-results/log_files/{job}.txt -J {job} {cmd}"
#         os.system(bsubCmd)

'''
Study 3: Look at the *exclusive* trainings
'''
# eta_edgs = np.linspace(0,1.5,4)
# wtags = [f'_dEta_hh_{t}_dPhi_HC_202020' for t in ['lt_0.5','0.5_1','1_1.5']]
# 
# for yr, sFile, bFile in zip([16,17,18],sigList,bkgList):
# 
#     if yr != 18: continue
# 
#     for eta_min, eta_max, wtag in zip(eta_edgs[:-1], eta_edgs[1:], wtags):
# 
#         label = f'exl_rw_{eta_min}_{eta_max}'
# 
#         for option, opTag in zip(['--stat-only',''], ['stat','sepNP']):
# 
#             cmd =  f'python run_limits.py -d {bFile} -s {sFile} -y {yr} --format resolved-recon'
#             cmd += f' --SR-center 124 117 --categorize dEta_hh --cat-edges {eta_min} {eta_max} '
#             cmd += f' --no-norm --outDir {outDir} --label {label} --wtag {wtag} {option}'
# 
#             print(cmd)
#             os.system(cmd)

'''
Study 4: Try w/ inside / outside SRs 
'''
# for yr, sFile, bFile in zip([16,17,18],sigList,bkgList):
# 
#     for eta_bins in [1,2,3,6]:
#         eta_edgs = ' '.join([str(ei) for ei in np.linspace(0,1.5,eta_bins+1)])
# 
#         for nSR,srTag in zip([1,2], ['','--Xhh-cut 0 0.95 1.6']):
# 
#             cmd =  f'python run_limits.py -d {bFile} -s {sFile} -y {yr} --format resolved-recon'
#             cmd += f' --SR-center 124 117 {srTag} --categorize dEta_hh --cat-edges {eta_edgs} '
#             cmd += f' --bins 34 --range 350 1200 --systvar None --corr_cat'
#             cmd += f' --no-norm --outDir {outDir} --label {eta_bins}_dEta_hh_{nSR}SR'
# 
#             print(cmd)
#             job=f'dEta_hh_{eta_bins}_bins_{nSR}SR_1NPCat'
#             bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q long -W 2000 -oo ../stats-results/log_files/{job}.txt -J {job} {cmd}"
#             os.system(bsubCmd)
            
# For the concatenation of all of the years, use the combineYears script
# os.chdir('../stats-results')
# for eta_bins in [1,2,3,6]:
#     for nSR,srTag in zip([1,2], ['','--Xhh-cut 0 0.95 1.6']):
# 
#         ws_in = f'dEta_cats/ws-systs-1NP-dEta_hh-cat-corr-XX-SM-HH-{eta_bins}_dEta_hh_{nSR}SR.json'
#         cmd = f"python combineYears.py --ws_in {ws_in}"
# 
#         print(cmd)
#         job=f'dEta_hh_{eta_bins}_bins_corr_{nSR}SR_combYrs'
#         bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q long -W 2000 -oo log_files/{job}.txt -J {job} {cmd}"
#         os.system(bsubCmd)

# os.chdir('../stats-results')
# for eta_bins in [1,2,3,6]:
#     for nSR,srTag in zip([1,2], ['','--Xhh-cut 0 0.95 1.6']):
# 
#         ws_in = f'dEta_cats/ws-systs-1NP-dEta_hh-cat-corr-XX-SM-HH-{eta_bins}_dEta_hh_{nSR}SR.json'
#         cmd = f"python combineYears.py --ws_in {ws_in}"
# 
#         print(cmd)
#         job=f'dEta_hh_{eta_bins}_bins_corr_{nSR}SR_combYrs'
#         bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q long -W 2000 -oo log_files/{job}.txt -J {job} {cmd}"
#         os.system(bsubCmd)

'''
Study 5: Rev deta checks!!! 
Try w/ inside / outside SRs with deta as well
'''
revSig = '../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16{}/NanoNTuple.root'
revBkg = '../data/RR/nom_trigs_unblind/rev_deta/data/data{}_NN_100_bootstraps.root'

sRevList = [revSig.format(i) for i in ['a','d','e']]
bRevList = [revBkg.format(yr) for yr in yrs]
print(sRevList)
print(bRevList)

edgs2 = [1.5, 3, 8]
edgs3 = [1.5, 2.5, 3.6, 8]
edgs6 = [1.5, 2, 2.5, 3, 3.6, 4.6, 8]

var_bins = [ [1.5, 10], edgs2, edgs3, edgs6 ]

# for yr, sFile, bFile in zip([16,17,18],sRevList,bRevList):
# 
#     for eta_bins,edg in zip([1,2,3,6], var_bins):
#         eta_edgs = ' '.join([f'{ei}' for ei in edg])
# 
#         for nSR,srTag in zip([1,2], ['','--Xhh-cut 0 0.95 1.6']):
# 
#             for cTag in ['--corr_cat','--stat-only','--stat-only --no-bootstrap','--corr_cat --no-bootstrap']:
# 
#                 cmd =  f'python run_limits.py -d {bFile} -s {sFile} -y {yr} --format resolved-recon'
#                 cmd += f' --SR-center 124 117 {srTag} --categorize dEta_hh --cat-edges {eta_edgs} '
#                 cmd += f' --bins 34 --range 350 1200 --systvar None {cTag} --unblind'
#                 cmd += f' --no-norm --outDir {outDir} --label {eta_bins}_dEta_hh_{nSR}SR_rev_dEta'
# 
#                 #print(cmd)
#                 ci = cTag.replace('--','').replace(' ','_')
#                 job=f'rev_dEta_hh_{eta_bins}_bins_{nSR}SR_1NPCat{ci}_unblind'
#                 bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q long -W 2000 -oo ../stats-results/log_files/{job}.txt -J {job} {cmd}"
#                 os.system(bsubCmd)
# 

# Combine the years in the limit
# These are having a hard time converging actually
# os.chdir('../stats-results')
# for eta_bins in [1,2,3,6]:
#     for nSR in [1,2]:
#         for cID in ['systs-1NP-dEta_hh-cat-corr','stat-only-dEta_hh-cat',
#                     'stat-only-dEta_hh-cat-noBS','systs-1NP-noBS-dEta_hh-cat-corr']:
#             ws_in = f'dEta_cats/ws-{cID}-XX-SM-HH-unblind-{eta_bins}_dEta_hh_{nSR}SR_rev_dEta.json'
#             cmd = f"python combineYears.py --ws_in {ws_in}"
# 
#             print(cmd)
#             job=f'dEta_hh_{eta_bins}_{nSR}SR_{cID}_combYrs'
#             bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q long -W 4000 -oo log_files/{job}.txt -J {job} {cmd}"
#             os.system(bsubCmd)
# 
# 
# os.chdir('../stats-results')

'''
Study 5b: Rev deta checks - but now *don't* correlate the NPs 
Keep the inside / outside SRs again
'''
for yr, sFile, bFile in zip([16,17,18],sRevList,bRevList):

    for eta_bins,edg in zip([1,2,3,6], var_bins):
        eta_edgs = ' '.join([f'{ei}' for ei in edg])

        for nSR,srTag in zip([1,2], ['','--Xhh-cut 0 0.95 1.6']):

            for cTag in ['--normalize-syst']: #, '--no-norm']:

                for mhhOpt,ti in zip(['--bins 34 --range 350 1200 --systvar None',''],['1NP','HTsplit']):

                    cmd =  f'python run_limits.py -d {bFile} -s {sFile} -y {yr} --format resolved-recon'
                    cmd += f' --SR-center 124 117 {srTag} --categorize dEta_hh --cat-edges {eta_edgs} '
                    cmd += f' {mhhOpt} {cTag} --unblind'
                    cmd += f' --outDir {outDir} --label {eta_bins}_dEta_hh_{nSR}SR_rev_dEta'

                    #print(cmd)
                    ci = cTag.replace('--','').replace(' ','_')
                    job=f'rev_dEta_hh_{eta_bins}_bins_{nSR}SR_{ti}Cat{ci}_unblind'
                    bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -q long -W 2000 -oo ../stats-results/log_files/{job}.txt -J {job} {cmd}"
                    os.system(bsubCmd)

