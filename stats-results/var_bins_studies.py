'''
Goal: Sub exps for limits vs resolution.

I'll check:
1. 4b + 3b1l incl (+ maybe toss 3b1f in the mix as well)
2. W/ the deta_hh bins (correlated + not)
3. Vary the resolution from 0.05 - 0.1
Jun 18th 2021
'''

import os
from subTimingStudies import writeSlurmFile 

def get_bin_edgs(r=(250,1100),res=.05):
    '''
    Goal: Return a list for the bin edges between X and Y 
    which use a fixed resolution (default 5% to match the baseline analysis).
    '''
    
    edgs = [r[0]]
    
    while edgs[-1] < r[1]:
        edgs.append( round((1+res)*edgs[-1]) )
    
    return edgs

if __name__ == '__main__':
    SLURM_DIR = 'slurm_scripts'

    SDIR = "../../hh4b/hh4b-resolved-reconstruction"
    BDIR = "../data/RR/cryptotuples/ggF_rw"
    ODIR = '../stats-results/var_bins/'

    SFILE = " ".join([f"{SDIR}/SMNR_pythia_mc16{mc}.root" for mc in ['a','d','e']])
    BFILE=" ".join([f"{BDIR}/data{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])

    base_cmd  = f"python run_limits.py -d {BFILE} -s {SFILE} -y 16 17 18 --no-norm"
    base_cmd += f" --underflow --overflow --backend jax --outDir {ODIR}"  

    r = (275,1050)

    for btag, blab in zip(['--btagCats 4b','--btagCats 3b1l','--btagCats 3b1f'],['4b','3b1l','3b1f']):
        for cat,clab in zip(['','--categorize dEta_hh --cat-edges 0 0.5 1 1.5','--categorize dEta_hh --cat-edges 0 0.5 1 1.5 --corr_cat'],
                            ['incl','3_deta','3_deta_corr']):
            for res in [0.05, 0.06, 0.07, 0.08, 0.09, 0.1]:

                edg = get_bin_edgs(r,res=res)
                edg_str = ' '.join([str(e) for e in edg])
                label = f'{r[0]}_{r[1]}_res_' + f'{res}'.replace('.','p')
                cmd = f'{base_cmd} {btag} {cat} --bins {edg_str} --label {label}'
                print(cmd+"\n\n")

                job_name = f'varBins_{blab}_{clab}_{res}'
                writeSlurmFile(cmd, job_name)
                os.system(f"sbatch {SLURM_DIR}/{job_name}.sh")

