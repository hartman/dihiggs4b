'''
yearsSplitTogether.py

Goal: We want to compare the limits, pulls, ad post-fit plots for 3b1f 
'''
import os
from subTimingStudies import writeSlurmFile 
from var_bins_studies import get_bin_edgs

SDIR = "../../hh4b/hh4b-resolved-reconstruction"
BDIR_SEP  = '../data/RR/cryptotuples/splityr_bkt/'
BDIR_OHE  = '../data/RR/cryptotuples/OHEyr_bkt/' # buggy 3b1f

# rws from (or fixed by) Sean
BDIR_SOHE  = '../data/RR/cryptotuples/seanFixOHEyr_bkt/'
BDIR_QUAD  = '../data/RR/cryptotuples/quad_OHEyr_bkt/'
BDIR_QSEP = '../data/RR/cryptotuples/quad_splityr_bkt' # quadrants, rw the years separately 
BDIR_QSEP_CG = '../data/RR/cryptotuples/CG_quad_splityr_bkt' # quadrants, rw the years separately 

ODIR = '../stats-results/ggF_PUSH/'

SLURM_DIR = 'slurm_scripts'

SFILE = " ".join([f"{SDIR}/SMNR_pythia_mc16{mc}.root" for mc in ['a','d','e']])

os.chdir('../non-resonant-studies')



'''
Study 1: Get workspaces for the separate years 
'''
# for bcat, unblind in zip(['4b','3b1f'],['',' --unblind']):
# #for bcat, unblind in zip(['3b1f'],[' --unblind']):
#     for BDIR, label in zip([BDIR_SEP, BDIR_OHE, BDIR_SOHE],['SEP','OHE','SOHE']):
#         if label == 'SEP': continue
# 
#         BFILE=" ".join([f"{BDIR}/data_baseline_bl_{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])
# 
#         cmd  = f"python run_limits.py -d {BFILE} -s {SFILE} -y 16 17 18 --no-norm"
#         cmd += f" --underflow --overflow --backend jax --outDir {ODIR}"
#         cmd += f" --btagCats {bcat} --label {label} {unblind}"  
# 
#         print(cmd+"\n")
# 
#         # No need for slurm jobs b/c inclusive fits run hella fast :)
#         os.system(cmd)


'''
Study 2: Get workspaces for the incusive fit
'''
BFILE_ring = " ".join([f"{BDIR_SOHE}/data_baseline_bl_{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])
BFILE_quad = " ".join([f"{BDIR_QUAD}/data_bl_{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])
# for bcat, unblind in zip(['4b','3b1f'],['',' --unblind']):
#     for BFILE, label in zip([BFILE_ring,BFILE_quad],['SOHE','QUAD']):
#         for bins in [39,117]:
# 
#             cmd  = f"python run_limits.py -d {BFILE} -s {SFILE} -y all --no-norm"
#             cmd += f" --stat-only --underflow --overflow --backend jax --outDir {ODIR}"
#             cmd += f" --btagCats {bcat} --bins {bins} --label {label}_{bins}bins {unblind}"  
# 
#             print(cmd+"\n")
# 
#             # No need for slurm jobs b/c inclusive fits run hella fast :)
#             os.system(cmd)

'''
Sanity check for study 2: fit as separate channels
'''
bins=39
# for bcat, unblind in zip(['4b','3b1f'],['',' --unblind']):
#     for BFILE, label in zip([BFILE_ring,BFILE_quad],['SOHE','QUAD']):
#         for op in ['','--stat-only','--stat-only --no-bootstrap']:
# 
#             cmd  = f"python run_limits.py -d {BFILE} -s {SFILE} -y 16 17 18 --no-norm"
#             cmd += f" --underflow --overflow --backend jax --outDir {ODIR}"
#             cmd += f" --btagCats {bcat} {op} --bins {bins} --label {label}_{bins}bins {unblind}"  
# 
#             print(cmd+"\n")
# 
#             # No need for slurm jobs b/c inclusive fits run hella fast :)
#             os.system(cmd)


# BFILE_qsep = " ".join([f"{BDIR_QSEP_CG}/data_bl_{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])
# label = 'QSEP_CG'

BFILE = " ".join([f"{BDIR_QSEP_CG}/data{yr}_NN_100_bootstraps.root" for yr in [16,17,18]])
label = 'QSEP_CG'
for bcat, unblind in zip(['4b','3b1f'],['',' --unblind']):
    for op in ['','--stat-only','--stat-only --no-bootstrap']:
            
        cmd  = f"python run_limits.py -d {BFILE} -s {SFILE} -y 16 17 18 --no-norm"
        cmd += f" --underflow --overflow --backend jax --outDir {ODIR}"
        cmd += f" --btagCats {bcat} {op} --bins {bins} --label {label}_{bins}bins_nomSR {unblind}"  
        cmd += " --Xhh-cut  0 1.6"    
        print(cmd+"\n")
            
        # No need for slurm jobs b/c inclusive fits run hella fast :)
        os.system(cmd)


'''
Step (many down the road): Get workspaces for the iclusive m_hh fit with *variable width* bins
- m_hh (5% resolution)
- 2 deta_hh + 2 X_hh (8% resolution)
''' 
# Message from the meeting today: We will do whatever is best for 4b +  
# adopt the same configuration for 3b1f
# base_cmd  = f"python run_limits.py -d {BFILE} -s {SFILE} -y 16 17 18 --no-norm"
# base_cmd += f" --underflow --overflow --backend jax --outDir {ODIR}"  
# base_cmd += " --categorize dEta_hh --cat-edges 0 0.75 1.5 --Xhh-cut 0 0.95 1.6 --unblind"
# 
# r = (275,1050)
# 
# # Message from the meeting today: We will do whatever is best for 4b +  
# # adopt the same configuration for 3b1f
# for bcat in ['4b','3b1f']:
#     for res, ctag, clab in zip([0.05, 0.07, 0.08],[]):
#         #for op1, clab1 in zip(['','--corr_cat'],['2_deta','2_deta_corr']):
#         for op1, clab1 in zip(['','--corr_cat'],['2_deta','2_deta_corr']):
#             for op2, clab2 in zip(['','--corr_Xhh'],['2_Xhh','2_Xhh_corr']):
# 
#                 edg = get_bin_edgs(r,res=res)
#                 edg_str = ' '.join([str(e) for e in edg])
#                 label = f'{r[0]}_{r[1]}_res_' + f'{res}'.replace('.','p')
#                 label += f"_{clab1}_{clab2}"
#                 cmd = f'{base_cmd} --btagCats {bcat} --bins {edg_str} {op1} {op2} --label {label}'
#                 print(cmd+"\n\n")
# 
#                 job_name = f'xhh_{blab}_{label}'
#                 writeSlurmFile(cmd, job_name)
#                 os.system(f"sbatch {SLURM_DIR}/{job_name}.sh")

'''
Step 3: Get workspaces in the 2x2 categories 
'''


