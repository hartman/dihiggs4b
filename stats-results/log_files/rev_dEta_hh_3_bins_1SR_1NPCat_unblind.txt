Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst 1 shape NP
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.5, 3.6, 8.0] Xhh [] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
Running norm error
qmu(mu = 1.0) = 1.4137663129076827e-06
Unexpectedly large med guess (1648.39), verifying by rescaling signal
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
Running norm error
qmu_test 0.22425837957325712
Initial guess was wrong, subbing in new guess 82.77586862702191
Initial guess for bands (mu guess = 1.00):
[ 44.41950602  59.63327058  82.77586863 115.17818434 154.4045111 ]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.3154)
Param: 2, mu: 44.420, CLs: 0.315
obs tensor(0.0004)
Param: 2, mu: 154.405, CLs: 0.000
obs tensor(0.0017)
Param: 2, mu: 137.083, CLs: 0.002
obs tensor(0.0381)
Param: 2, mu: 90.751, CLs: 0.038
obs tensor(0.0768)
Param: 2, mu: 77.595, CLs: 0.077
obs tensor(0.0477)
Param: 2, mu: 86.695, CLs: 0.048
obs tensor(0.0501)
Param: 2, mu: 85.790, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 85.818, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 85.817, CLs: 0.050
Param: 2, mu: 44.420, CLs: 0.295
Param: 2, mu: 154.405, CLs: 0.000
Param: 2, mu: 135.858, CLs: 0.001
Param: 2, mu: 90.139, CLs: 0.034
Param: 2, mu: 71.175, CLs: 0.094
Param: 2, mu: 85.113, CLs: 0.045
Param: 2, mu: 83.254, CLs: 0.050
Param: 2, mu: 83.365, CLs: 0.050
Param: 2, mu: 83.361, CLs: 0.050
Param: 2, mu: 83.361, CLs: 0.050
Param: 2, mu: 83.307, CLs: 0.050
Param: 2, mu: 83.360, CLs: 0.050
Guess for bands with proper med:
[ 44.7332786   60.05451085  83.36058467 115.99178537 155.49520089]
Refining bands
Param: 0, mu: 42.497, CLs: 0.059
Param: 0, mu: 46.970, CLs: 0.042
Param: 0, mu: 44.794, CLs: 0.049
Param: 0, mu: 44.613, CLs: 0.050
Param: 0, mu: 44.615, CLs: 0.050
Param: 0, mu: 44.615, CLs: 0.050
Param: 1, mu: 57.052, CLs: 0.060
Param: 1, mu: 63.057, CLs: 0.041
Param: 1, mu: 60.211, CLs: 0.049
Param: 1, mu: 59.953, CLs: 0.050
Param: 1, mu: 59.956, CLs: 0.050
Param: 1, mu: 59.956, CLs: 0.050
Param: 3, mu: 110.192, CLs: 0.067
Param: 3, mu: 121.791, CLs: 0.038
Param: 3, mu: 116.936, CLs: 0.048
Param: 3, mu: 116.254, CLs: 0.050
Param: 3, mu: 116.268, CLs: 0.050
Param: 3, mu: 116.268, CLs: 0.050
Param: 4, mu: 147.720, CLs: 0.074
Param: 4, mu: 163.270, CLs: 0.035
Param: 4, mu: 157.438, CLs: 0.047
Param: 4, mu: 156.283, CLs: 0.050
Param: 4, mu: 156.322, CLs: 0.050
Param: 4, mu: 156.321, CLs: 0.050
Param: 4, mu: 156.320, CLs: 0.050
Final bands:
[85.81700330591143, 44.6146546406283, 59.95577763216033, 83.36058466685311, 116.26763328744686, 156.3208580434775]
Finished in 9.0 min 27.89331340789795 s
46 function calls
[85.81700330591143, 44.6146546406283, 59.95577763216033, 83.36058466685311, 116.26763328744686, 156.3208580434775]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-dEta_hh-cat-18-SM-HH-unblind-3_dEta_hh_1SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-dEta_hh-cat-18-SM-HH-unblind-3_dEta_hh_1SR_rev_dEta.json
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
Running norm error
 w/o an NP split
Running norm error
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
Running norm error

------------------------------------------------------------
Sender: LSF System <lsf@deft0010>
Subject: Job 855445: <rev_dEta_hh_3_bins_1SR_1NPCat_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_1SR_1NPCat_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:09 2021
Job was executed on host(s) <deft0010>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:12 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:12 2021
Terminated at Tue Apr 20 18:06:57 2021
Results reported at Tue Apr 20 18:06:57 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --bins 34 --range 350 1200 --systvar None --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   454.95 sec.
    Max Memory :                                 261 MB
    Average Memory :                             248.62 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   465 sec.
    Turnaround time :                            468 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0058>
Subject: Job 855400: <rev_dEta_hh_3_bins_1SR_1NPCat_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_1SR_1NPCat_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:07 2021
Job was executed on host(s) <kiso0058>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:09 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:09 2021
Terminated at Tue Apr 20 18:08:08 2021
Results reported at Tue Apr 20 18:08:08 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --bins 34 --range 350 1200 --systvar None --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   525.18 sec.
    Max Memory :                                 284 MB
    Average Memory :                             272.05 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   538 sec.
    Turnaround time :                            541 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0015>
Subject: Job 855489: <rev_dEta_hh_3_bins_1SR_1NPCat_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_1SR_1NPCat_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:12 2021
Job was executed on host(s) <kiso0015>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:01:54 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:01:54 2021
Terminated at Tue Apr 20 18:11:39 2021
Results reported at Tue Apr 20 18:11:39 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --bins 34 --range 350 1200 --systvar None --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   573.31 sec.
    Max Memory :                                 502 MB
    Average Memory :                             479.84 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   585 sec.
    Turnaround time :                            747 sec.

The output (if any) is above this job summary.

