Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst 1 shape NP
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.5, 3.6, 8.0] Xhh [0.0, 0.95, 1.6] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
Running norm error
qmu(mu = 1.0) = 1.1075690053985454e-07
Unexpectedly large med guess (5889.29), verifying by rescaling signal
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
Running norm error
qmu_test 0.29258660322602736
Initial guess was wrong, subbing in new guess 72.46876632728728
Initial guess for bands (mu guess = 1.00):
[ 38.88846901  52.20784297  72.46876633 100.83640396 135.17833906]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.2939)
Param: 2, mu: 38.888, CLs: 0.294
obs tensor(0.0003)
Param: 2, mu: 135.178, CLs: 0.000
obs tensor(0.0015)
Param: 2, mu: 118.886, CLs: 0.002
obs tensor(0.0345)
Param: 2, mu: 78.887, CLs: 0.034
obs tensor(0.0917)
Param: 2, mu: 62.753, CLs: 0.092
obs tensor(0.0457)
Param: 2, mu: 74.509, CLs: 0.046
obs tensor(0.0503)
Param: 2, mu: 72.969, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 73.056, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 73.053, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 73.052, CLs: 0.050
Param: 2, mu: 38.888, CLs: 0.294
Param: 2, mu: 135.178, CLs: 0.000
Param: 2, mu: 118.881, CLs: 0.001
Param: 2, mu: 78.885, CLs: 0.034
Param: 2, mu: 62.219, CLs: 0.094
Param: 2, mu: 74.462, CLs: 0.045
Param: 2, mu: 72.820, CLs: 0.050
Param: 2, mu: 72.919, CLs: 0.050
Param: 2, mu: 72.915, CLs: 0.050
Param: 2, mu: 72.915, CLs: 0.050
Guess for bands with proper med:
[ 39.12818133  52.5296572   72.91547087 101.45796939 136.01159152]
Refining bands
Param: 0, mu: 37.172, CLs: 0.059
Param: 0, mu: 41.085, CLs: 0.041
Param: 0, mu: 39.125, CLs: 0.049
Param: 0, mu: 38.979, CLs: 0.050
Param: 0, mu: 38.968, CLs: 0.050
Param: 0, mu: 38.967, CLs: 0.050
Param: 1, mu: 49.903, CLs: 0.060
Param: 1, mu: 55.156, CLs: 0.041
Param: 1, mu: 52.621, CLs: 0.049
Param: 1, mu: 52.395, CLs: 0.050
Param: 1, mu: 52.397, CLs: 0.050
Param: 1, mu: 52.397, CLs: 0.050
Param: 3, mu: 96.385, CLs: 0.067
Param: 3, mu: 106.531, CLs: 0.038
Param: 3, mu: 102.401, CLs: 0.048
Param: 3, mu: 101.809, CLs: 0.050
Param: 3, mu: 101.822, CLs: 0.050
Param: 3, mu: 101.821, CLs: 0.050
Param: 4, mu: 129.211, CLs: 0.075
Param: 4, mu: 142.812, CLs: 0.036
Param: 4, mu: 138.042, CLs: 0.047
Param: 4, mu: 137.062, CLs: 0.050
Param: 4, mu: 137.094, CLs: 0.050
Param: 4, mu: 137.093, CLs: 0.050
Param: 4, mu: 137.093, CLs: 0.050
Final bands:
[73.05270501278895, 38.967173282692386, 52.397497805509964, 72.91547087106461, 101.82168001925886, 137.0930376138703]
Finished in 23.0 min 59.537312030792236 s
45 function calls
[73.05270501278895, 38.967173282692386, 52.397497805509964, 72.91547087106461, 101.82168001925886, 137.0930376138703]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-dEta_hh-cat-18-SM-HH-unblind-3_dEta_hh_2SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-dEta_hh-cat-18-SM-HH-unblind-3_dEta_hh_2SR_rev_dEta.json
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
Running norm error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
Running norm error
ning syst error w/o an NP split
Running norm error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
Running norm error
ror
Running syst error w/o an NP split
Running norm error

------------------------------------------------------------
Sender: LSF System <lsf@bubble0001>
Subject: Job 855450: <rev_dEta_hh_3_bins_2SR_1NPCat_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_1NPCat_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:10 2021
Job was executed on host(s) <bubble0001>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:12 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:12 2021
Terminated at Tue Apr 20 18:16:07 2021
Results reported at Tue Apr 20 18:16:07 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --bins 34 --range 350 1200 --systvar None --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1002.03 sec.
    Max Memory :                                 265 MB
    Average Memory :                             259.11 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1039 sec.
    Turnaround time :                            1017 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@deft0002>
Subject: Job 855406: <rev_dEta_hh_3_bins_2SR_1NPCat_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_1NPCat_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:07 2021
Job was executed on host(s) <deft0002>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:09 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:09 2021
Terminated at Tue Apr 20 18:19:44 2021
Results reported at Tue Apr 20 18:19:44 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --bins 34 --range 350 1200 --systvar None --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1217.75 sec.
    Max Memory :                                 285 MB
    Average Memory :                             279.63 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1234 sec.
    Turnaround time :                            1237 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0029>
Subject: Job 855495: <rev_dEta_hh_3_bins_2SR_1NPCat_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_1NPCat_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:12 2021
Job was executed on host(s) <kiso0029>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:02:10 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:02:10 2021
Terminated at Tue Apr 20 18:26:29 2021
Results reported at Tue Apr 20 18:26:29 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --bins 34 --range 350 1200 --systvar None --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1445.08 sec.
    Max Memory :                                 502 MB
    Average Memory :                             492.74 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1459 sec.
    Turnaround time :                            1637 sec.

The output (if any) is above this job summary.

