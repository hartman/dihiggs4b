Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst 1 shape NP
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 3.0, 8.0] Xhh [0.0, 0.95, 1.6] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
qmu(mu = 1.0) = 7.522839950979687e-08
Unexpectedly large med guess (7145.90), verifying by rescaling signal
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
qmu_test 0.2717166864995306
Initial guess was wrong, subbing in new guess 75.20036209443177
Initial guess for bands (mu guess = 1.00):
[ 40.35430847  54.17573521  75.20036209 104.63727305 140.27367319]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.2182)
Param: 2, mu: 40.354, CLs: 0.218
obs tensor(9.7829e-05)
Param: 2, mu: 140.274, CLs: 0.000
obs tensor(0.0010)
Param: 2, mu: 117.408, CLs: 0.001
obs tensor(0.0222)
Param: 2, mu: 78.881, CLs: 0.022
obs tensor(0.0773)
Param: 2, mu: 59.618, CLs: 0.077
obs tensor(0.0428)
Param: 2, mu: 69.163, CLs: 0.043
obs tensor(0.0486)
Param: 2, mu: 67.167, CLs: 0.049
obs tensor(0.0500)
Param: 2, mu: 66.720, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 66.727, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 66.728, CLs: 0.050
Param: 2, mu: 40.354, CLs: 0.295
Param: 2, mu: 140.274, CLs: 0.000
Param: 2, mu: 123.409, CLs: 0.001
Param: 2, mu: 81.882, CLs: 0.034
Param: 2, mu: 64.742, CLs: 0.094
Param: 2, mu: 77.317, CLs: 0.045
Param: 2, mu: 75.644, CLs: 0.050
Param: 2, mu: 75.743, CLs: 0.050
Param: 2, mu: 75.740, CLs: 0.050
Param: 2, mu: 75.740, CLs: 0.050
Guess for bands with proper med:
[ 40.64374649  54.56430629  75.73973062 105.38777544 141.27977478]
Refining bands
Param: 0, mu: 38.612, CLs: 0.059
Param: 0, mu: 42.676, CLs: 0.041
Param: 0, mu: 40.669, CLs: 0.049
Param: 0, mu: 40.505, CLs: 0.050
Param: 0, mu: 40.506, CLs: 0.050
Param: 0, mu: 40.506, CLs: 0.050
Param: 1, mu: 51.836, CLs: 0.060
Param: 1, mu: 57.293, CLs: 0.041
Param: 1, mu: 54.682, CLs: 0.049
Param: 1, mu: 54.448, CLs: 0.050
Param: 1, mu: 54.450, CLs: 0.050
Param: 1, mu: 54.450, CLs: 0.050
Param: 3, mu: 100.118, CLs: 0.067
Param: 3, mu: 110.657, CLs: 0.038
Param: 3, mu: 106.308, CLs: 0.048
Param: 3, mu: 105.691, CLs: 0.050
Param: 3, mu: 105.703, CLs: 0.050
Param: 3, mu: 105.704, CLs: 0.050
Param: 4, mu: 134.216, CLs: 0.075
Param: 4, mu: 148.344, CLs: 0.036
Param: 4, mu: 143.222, CLs: 0.047
Param: 4, mu: 142.187, CLs: 0.050
Param: 4, mu: 142.222, CLs: 0.050
Param: 4, mu: 142.221, CLs: 0.050
Final bands:
[66.72713820463629, 40.506063765400526, 54.44982887475436, 75.73973061827166, 105.70338459215462, 142.22098132165206]
Finished in 8.0 min 1.045328140258789 s
44 function calls
[66.72713820463629, 40.506063765400526, 54.44982887475436, 75.73973061827166, 105.70338459215462, 142.22098132165206]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-dEta_hh-cat-18-SM-HH-unblind-2_dEta_hh_2SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-dEta_hh-cat-18-SM-HH-unblind-2_dEta_hh_2SR_rev_dEta.json
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
b <rev_dEta_hh_2_bins_2SR_1NPCatno-norm_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:09 2021
Job was executed on host(s) <deft0010>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:12 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:12 2021
Terminated at Tue Apr 20 18:04:44 2021
Results reported at Tue Apr 20 18:04:44 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 3 8 --bins 34 --range 350 1200 --systvar None --no-norm --unblind --outDir ../stats-results/dEta_cats --label 2_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   321.44 sec.
    Max Memory :                                 261 MB
    Average Memory :                             244.50 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   332 sec.
    Turnaround time :                            335 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0056>
Subject: Job 855398: <rev_dEta_hh_2_bins_2SR_1NPCatno-norm_unblind> in cluster <slac> Done

Job <rev_dEta_hh_2_bins_2SR_1NPCatno-norm_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:07 2021
Job was executed on host(s) <kiso0056>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:09 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:09 2021
Terminated at Tue Apr 20 18:07:15 2021
Results reported at Tue Apr 20 18:07:15 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 3 8 --bins 34 --range 350 1200 --systvar None --no-norm --unblind --outDir ../stats-results/dEta_cats --label 2_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   470.93 sec.
    Max Memory :                                 284 MB
    Average Memory :                             271.06 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   485 sec.
    Turnaround time :                            488 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0054>
Subject: Job 855486: <rev_dEta_hh_2_bins_2SR_1NPCatno-norm_unblind> in cluster <slac> Done

Job <rev_dEta_hh_2_bins_2SR_1NPCatno-norm_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:12 2021
Job was executed on host(s) <kiso0054>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:01:42 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:01:42 2021
Terminated at Tue Apr 20 18:10:08 2021
Results reported at Tue Apr 20 18:10:08 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 3 8 --bins 34 --range 350 1200 --systvar None --no-norm --unblind --outDir ../stats-results/dEta_cats --label 2_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   490.80 sec.
    Max Memory :                                 502 MB
    Average Memory :                             470.47 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   506 sec.
    Turnaround time :                            656 sec.

The output (if any) is above this job summary.

