Using uproot4
No direct category match for 17 assuming 4b
{'17': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root
data17_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data17_NN_100_bootstraps.root
[124.0, 117.0]
353415
Xwt_str X_wt_tag
yr 17 NN_norm_bstrap_med_17
4354.787880398217
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
5873
Not able to flatten, maybe double check signal counts
4b
S = 0.035241626203060
Running with syst 1 shape NP
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.0, 2.5, 3.0, 3.6, 4.6, 8.0] Xhh [] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
cat_4b_3
['cat', '4b', '3']
Running stat error
Running syst error w/o an NP split
cat_4b_4
['cat', '4b', '4']
Running stat error
Running syst error w/o an NP split
cat_4b_5
['cat', '4b', '5']
Running stat error
Running syst error w/o an NP split
qmu(mu = 1.0) = 5.59238742425805e-07
Unexpectedly large med guess (2620.89), verifying by rescaling signal
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
cat_4b_3
['cat', '4b', '3']
Running stat error
Running syst error w/o an NP split
cat_4b_4
['cat', '4b', '4']
Running stat error
Running syst error w/o an NP split
cat_4b_5
['cat', '4b', '5']
Running stat error
Running syst error w/o an NP split
qmu_test 0.29721896347382426
Initial guess was wrong, subbing in new guess 71.90181098225311
Initial guess for bands (mu guess = 1.00):
[ 38.58422725  51.79939782  71.90181098 100.04751599 134.12077887]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.0497)
Param: 2, mu: 38.584, CLs: 0.050
obs tensor(4.8209e-07)
Param: 2, mu: 134.121, CLs: 0.000
Looks like initial guess was bad? Expanding for one more try
obs tensor(0.2519)
Param: 2, mu: 19.292, CLs: 0.252
obs tensor(4.0189e-18)
Param: 2, mu: 268.242, CLs: 0.000
obs tensor(1.6158e-13)
Param: 2, mu: 218.818, CLs: 0.000
obs tensor(4.3568e-06)
Param: 2, mu: 119.055, CLs: 0.000
obs tensor(0.0023)
Param: 2, mu: 69.174, CLs: 0.002
obs tensor(0.0295)
Param: 2, mu: 44.233, CLs: 0.030
obs tensor(0.0907)
Param: 2, mu: 31.762, CLs: 0.091
obs tensor(0.0435)
Param: 2, mu: 40.061, CLs: 0.043
obs tensor(0.0507)
Param: 2, mu: 38.378, CLs: 0.051
obs tensor(0.0500)
Param: 2, mu: 38.531, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 38.521, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 38.521, CLs: 0.050
Param: 2, mu: 19.292, CLs: 0.598
Param: 2, mu: 268.242, CLs: 0.000
Param: 2, mu: 247.430, CLs: 0.000
Param: 2, mu: 133.361, CLs: 0.000
Param: 2, mu: 76.327, CLs: 0.039
Param: 2, mu: 61.013, CLs: 0.098
Param: 2, mu: 73.416, CLs: 0.047
Param: 2, mu: 72.306, CLs: 0.050
Param: 2, mu: 72.351, CLs: 0.050
Param: 2, mu: 72.350, CLs: 0.050
Param: 2, mu: 72.350, CLs: 0.050
Guess for bands with proper med:
[ 38.82485841  52.12244561  72.35022779 100.67146394 134.95722528]
Refining bands
Param: 0, mu: 36.884, CLs: 0.058
Param: 0, mu: 40.766, CLs: 0.041
Param: 0, mu: 38.749, CLs: 0.049
Param: 0, mu: 38.605, CLs: 0.050
Param: 0, mu: 38.594, CLs: 0.050
Param: 0, mu: 38.594, CLs: 0.050
Param: 1, mu: 49.516, CLs: 0.060
Param: 1, mu: 54.729, CLs: 0.041
Param: 1, mu: 52.154, CLs: 0.049
Param: 1, mu: 51.930, CLs: 0.050
Param: 1, mu: 51.932, CLs: 0.050
Param: 1, mu: 51.932, CLs: 0.050
Param: 3, mu: 95.638, CLs: 0.068
Param: 3, mu: 105.705, CLs: 0.039
Param: 3, mu: 101.760, CLs: 0.048
Param: 3, mu: 101.182, CLs: 0.050
Param: 3, mu: 101.194, CLs: 0.050
Param: 3, mu: 101.193, CLs: 0.050
Param: 4, mu: 128.209, CLs: 0.077
Param: 4, mu: 141.705, CLs: 0.037
Param: 4, mu: 137.407, CLs: 0.048
Param: 4, mu: 136.482, CLs: 0.050
Param: 4, mu: 136.510, CLs: 0.050
Param: 4, mu: 136.510, CLs: 0.050
Final bands:
[38.52135018026821, 38.593653300172434, 51.93247724613623, 72.35022779186158, 101.19361113000873, 136.50968097360274]
Finished in 25.0 min 10.812111854553223 s
47 function calls
[38.52135018026821, 38.593653300172434, 51.93247724613623, 72.35022779186158, 101.19361113000873, 136.50968097360274]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-dEta_hh-cat-corr-17-SM-HH-unblind-6_dEta_hh_1SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-dEta_hh-cat-corr-17-SM-HH-unblind-6_dEta_hh_1SR_rev_dEta.json
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
cat_4b_3
['cat', '4b', '3']
Running stat error
Running syst error w/o an NP split
cat_4b_4
['cat', '4b', '4']
Running stat error
Running syst error w/o an NP split
cat_4b_5
['cat', '4b', '5']
Running stat error
Running syst error w/o an NP split
deft0028>, in queue <long>, as user <nhartman> in cluster <slac> at Fri Apr  9 12:00:32 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Fri Apr  9 12:00:32 2021
Terminated at Fri Apr  9 12:17:19 2021
Results reported at Fri Apr  9 12:17:19 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --bins 34 --range 350 1200 --systvar None --corr_cat --unblind --no-norm --outDir ../stats-results/dEta_cats --label 6_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1002.98 sec.
    Max Memory :                                 285 MB
    Average Memory :                             280.39 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1006 sec.
    Turnaround time :                            1213 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@deft0011>
Subject: Job 707951: <rev_dEta_hh_6_bins_1SR_1NPCatcorr_cat_unblind> in cluster <slac> Done

Job <rev_dEta_hh_6_bins_1SR_1NPCatcorr_cat_unblind> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Fri Apr  9 11:57:20 2021
Job was executed on host(s) <deft0011>, in queue <long>, as user <nhartman> in cluster <slac> at Fri Apr  9 12:14:55 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Fri Apr  9 12:14:55 2021
Terminated at Fri Apr  9 12:32:10 2021
Results reported at Fri Apr  9 12:32:10 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --bins 34 --range 350 1200 --systvar None --corr_cat --unblind --no-norm --outDir ../stats-results/dEta_cats --label 6_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1032.98 sec.
    Max Memory :                                 502 MB
    Average Memory :                             497.58 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1035 sec.
    Turnaround time :                            2090 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0037>
Subject: Job 707890: <rev_dEta_hh_6_bins_1SR_1NPCatcorr_cat_unblind> in cluster <slac> Done

Job <rev_dEta_hh_6_bins_1SR_1NPCatcorr_cat_unblind> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Fri Apr  9 11:57:13 2021
Job was executed on host(s) <kiso0037>, in queue <long>, as user <nhartman> in cluster <slac> at Fri Apr  9 12:09:18 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Fri Apr  9 12:09:18 2021
Terminated at Fri Apr  9 12:34:36 2021
Results reported at Fri Apr  9 12:34:36 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --bins 34 --range 350 1200 --systvar None --corr_cat --unblind --no-norm --outDir ../stats-results/dEta_cats --label 6_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1516.87 sec.
    Max Memory :                                 262 MB
    Average Memory :                             260.35 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1518 sec.
    Turnaround time :                            2243 sec.

The output (if any) is above this job summary.

