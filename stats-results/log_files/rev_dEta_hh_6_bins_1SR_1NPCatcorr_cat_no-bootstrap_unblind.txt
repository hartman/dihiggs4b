Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst 1 shape NP
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.0, 2.5, 3.0, 3.6, 4.6, 8.0] Xhh [] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
cat_4b_3
['cat', '4b', '3']
Running stat error
Running syst error w/o an NP split
cat_4b_4
['cat', '4b', '4']
Running stat error
Running syst error w/o an NP split
cat_4b_5
['cat', '4b', '5']
Running stat error
Running syst error w/o an NP split
qmu(mu = 1.0) = 1.685766619630158e-07
Unexpectedly large med guess (4773.64), verifying by rescaling signal
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
cat_4b_3
['cat', '4b', '3']
Running stat error
Running syst error w/o an NP split
cat_4b_4
['cat', '4b', '4']
Running stat error
Running syst error w/o an NP split
cat_4b_5
['cat', '4b', '5']
Running stat error
Running syst error w/o an NP split
qmu_test 0.31132156592047977
Initial guess was wrong, subbing in new guess 70.25439263530276
Initial guess for bands (mu guess = 1.00):
[ 37.70018326  50.6125671   70.25439264  97.75522166 131.04779603]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.3468)
Param: 2, mu: 37.700, CLs: 0.347
obs tensor(0.0006)
Param: 2, mu: 131.048, CLs: 0.001
obs tensor(0.0021)
Param: 2, mu: 117.730, CLs: 0.002
obs tensor(0.0455)
Param: 2, mu: 77.715, CLs: 0.046
obs tensor(0.0571)
Param: 2, mu: 74.083, CLs: 0.057
obs tensor(0.0497)
Param: 2, mu: 76.308, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 76.225, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 76.226, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 76.226, CLs: 0.050
Param: 2, mu: 37.700, CLs: 0.295
Param: 2, mu: 131.048, CLs: 0.000
Param: 2, mu: 115.288, CLs: 0.002
Param: 2, mu: 76.494, CLs: 0.034
Param: 2, mu: 60.754, CLs: 0.092
Param: 2, mu: 72.249, CLs: 0.046
Param: 2, mu: 70.737, CLs: 0.050
Param: 2, mu: 70.823, CLs: 0.050
Param: 2, mu: 70.820, CLs: 0.050
Param: 2, mu: 70.820, CLs: 0.050
Guess for bands with proper med:
[ 38.00355045  51.01983813  70.81971821  98.5418419  132.10231615]
Refining bands
Param: 0, mu: 36.103, CLs: 0.059
Param: 0, mu: 39.904, CLs: 0.041
Param: 0, mu: 37.983, CLs: 0.049
Param: 0, mu: 37.842, CLs: 0.050
Param: 0, mu: 37.831, CLs: 0.050
Param: 0, mu: 37.831, CLs: 0.050
Param: 0, mu: 37.832, CLs: 0.050
Param: 1, mu: 48.469, CLs: 0.060
Param: 1, mu: 53.571, CLs: 0.041
Param: 1, mu: 51.095, CLs: 0.049
Param: 1, mu: 50.875, CLs: 0.050
Param: 1, mu: 50.878, CLs: 0.050
Param: 1, mu: 50.877, CLs: 0.050
Param: 3, mu: 93.615, CLs: 0.067
Param: 3, mu: 103.469, CLs: 0.038
Param: 3, mu: 99.494, CLs: 0.048
Param: 3, mu: 98.922, CLs: 0.050
Param: 3, mu: 98.933, CLs: 0.050
Param: 3, mu: 98.933, CLs: 0.050
Param: 4, mu: 125.497, CLs: 0.076
Param: 4, mu: 138.707, CLs: 0.037
Param: 4, mu: 134.177, CLs: 0.048
Param: 4, mu: 133.235, CLs: 0.050
Param: 4, mu: 133.265, CLs: 0.050
Param: 4, mu: 133.265, CLs: 0.050
Param: 4, mu: 133.264, CLs: 0.050
Final bands:
[76.22587738699296, 37.83101970705344, 50.877848078318536, 70.81971821387896, 98.93334612977866, 133.26493701236117]
Finished in 28.0 min 17.905774354934692 s
45 function calls
[76.22587738699296, 37.83101970705344, 50.877848078318536, 70.81971821387896, 98.93334612977866, 133.26493701236117]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-noBS-dEta_hh-cat-corr-18-SM-HH-unblind-6_dEta_hh_1SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-noBS-dEta_hh-cat-corr-18-SM-HH-unblind-6_dEta_hh_1SR_rev_dEta.json
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
cat_4b_3
['cat', '4b', '3']
Running stat error
Running syst error w/o an NP split
cat_4b_4
['cat', '4b', '4']
Running stat error
Running syst error w/o an NP split
cat_4b_5
['cat', '4b', '5']
Running stat error
Running syst error w/o an NP split
r w/o an NP split
cat_4b_3
['cat', '4b', '3']
Running stat error
Running syst error w/o an NP split
cat_4b_4
['cat', '4b', '4']
Running stat error
Running syst error w/o an NP split
cat_4b_5
['cat', '4b', '5']
Running stat error
Running syst error w/o an NP split
ft0017>, in queue <long>, as user <nhartman> in cluster <slac> at Fri Apr  9 12:01:35 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Fri Apr  9 12:01:35 2021
Terminated at Fri Apr  9 12:24:12 2021
Results reported at Fri Apr  9 12:24:12 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --bins 34 --range 350 1200 --systvar None --corr_cat --no-bootstrap --unblind --no-norm --outDir ../stats-results/dEta_cats --label 6_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1352.18 sec.
    Max Memory :                                 284 MB
    Average Memory :                             278.25 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1357 sec.
    Turnaround time :                            1626 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@deft0011>
Subject: Job 707896: <rev_dEta_hh_6_bins_1SR_1NPCatcorr_cat_no-bootstrap_unblind> in cluster <slac> Done

Job <rev_dEta_hh_6_bins_1SR_1NPCatcorr_cat_no-bootstrap_unblind> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Fri Apr  9 11:57:14 2021
Job was executed on host(s) <deft0011>, in queue <long>, as user <nhartman> in cluster <slac> at Fri Apr  9 12:09:47 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Fri Apr  9 12:09:47 2021
Terminated at Fri Apr  9 12:30:18 2021
Results reported at Fri Apr  9 12:30:18 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --bins 34 --range 350 1200 --systvar None --corr_cat --no-bootstrap --unblind --no-norm --outDir ../stats-results/dEta_cats --label 6_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1230.33 sec.
    Max Memory :                                 343 MB
    Average Memory :                             263.86 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1231 sec.
    Turnaround time :                            1984 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@deft0010>
Subject: Job 707958: <rev_dEta_hh_6_bins_1SR_1NPCatcorr_cat_no-bootstrap_unblind> in cluster <slac> Done

Job <rev_dEta_hh_6_bins_1SR_1NPCatcorr_cat_no-bootstrap_unblind> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Fri Apr  9 11:57:20 2021
Job was executed on host(s) <deft0010>, in queue <long>, as user <nhartman> in cluster <slac> at Fri Apr  9 12:15:30 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Fri Apr  9 12:15:30 2021
Terminated at Fri Apr  9 12:44:01 2021
Results reported at Fri Apr  9 12:44:01 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --bins 34 --range 350 1200 --systvar None --corr_cat --no-bootstrap --unblind --no-norm --outDir ../stats-results/dEta_cats --label 6_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1705.55 sec.
    Max Memory :                                 502 MB
    Average Memory :                             494.79 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1711 sec.
    Turnaround time :                            2801 sec.

The output (if any) is above this job summary.

