Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst 1 shape NP
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 10.0] Xhh [0.0, 0.95, 1.6] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
qmu(mu = 1.0) = 3.77102651327732e-08
Unexpectedly large med guess (10092.95), verifying by rescaling signal
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
qmu_test 0.21585914043566845
Initial guess was wrong, subbing in new guess 84.37093558703216
Initial guess for bands (mu guess = 1.00):
[ 45.27545701  60.78238639  84.37093559 117.39763452 157.37984121]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.2937)
Param: 2, mu: 45.275, CLs: 0.294
obs tensor(0.0003)
Param: 2, mu: 157.380, CLs: 0.000
obs tensor(0.0014)
Param: 2, mu: 138.388, CLs: 0.001
obs tensor(0.0337)
Param: 2, mu: 91.832, CLs: 0.034
obs tensor(0.0967)
Param: 2, mu: 71.746, CLs: 0.097
obs tensor(0.0451)
Param: 2, mu: 86.632, CLs: 0.045
obs tensor(0.0503)
Param: 2, mu: 84.589, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 84.721, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 84.715, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 84.715, CLs: 0.050
Param: 2, mu: 45.275, CLs: 0.294
Param: 2, mu: 157.380, CLs: 0.000
Param: 2, mu: 138.388, CLs: 0.001
Param: 2, mu: 91.832, CLs: 0.034
Param: 2, mu: 71.746, CLs: 0.097
Param: 2, mu: 86.632, CLs: 0.045
Param: 2, mu: 84.589, CLs: 0.050
Param: 2, mu: 84.721, CLs: 0.050
Param: 2, mu: 84.715, CLs: 0.050
Param: 2, mu: 84.715, CLs: 0.050
Guess for bands with proper med:
[ 45.46035583  61.03061341  84.71549505 117.87707054 158.02255914]
Refining bands
Param: 0, mu: 43.187, CLs: 0.059
Param: 0, mu: 47.733, CLs: 0.042
Param: 0, mu: 45.534, CLs: 0.049
Param: 0, mu: 45.350, CLs: 0.050
Param: 0, mu: 45.352, CLs: 0.050
Param: 0, mu: 45.351, CLs: 0.050
Param: 1, mu: 57.979, CLs: 0.060
Param: 1, mu: 64.082, CLs: 0.041
Param: 1, mu: 61.199, CLs: 0.049
Param: 1, mu: 60.937, CLs: 0.050
Param: 1, mu: 60.940, CLs: 0.050
Param: 1, mu: 60.939, CLs: 0.050
Param: 3, mu: 111.983, CLs: 0.067
Param: 3, mu: 123.771, CLs: 0.038
Param: 3, mu: 118.810, CLs: 0.048
Param: 3, mu: 118.116, CLs: 0.050
Param: 3, mu: 118.130, CLs: 0.050
Param: 3, mu: 118.130, CLs: 0.050
Param: 4, mu: 150.121, CLs: 0.074
Param: 4, mu: 165.924, CLs: 0.035
Param: 4, mu: 159.920, CLs: 0.047
Param: 4, mu: 158.740, CLs: 0.050
Param: 4, mu: 158.780, CLs: 0.050
Param: 4, mu: 158.780, CLs: 0.050
Param: 4, mu: 158.779, CLs: 0.050
Final bands:
[84.71549504587841, 45.35150943821112, 60.939870933400826, 84.71549504587841, 118.13028482774966, 158.77959022778836]
Finished in 0.0 min 43.644460678100586 s
45 function calls
[84.71549504587841, 45.35150943821112, 60.939870933400826, 84.71549504587841, 118.13028482774966, 158.77959022778836]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-dEta_hh-cat-corr-18-SM-HH-1_dEta_hh_2SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-dEta_hh-cat-corr-18-SM-HH-1_dEta_hh_2SR_rev_dEta.json
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split

------------------------------------------------------------
Sender: LSF System <lsf@deft0027>
Subject: Job 419260: <rev_dEta_hh_1_bins_2SR_1NPCatcorr_cat> in cluster <slac> Done

Job <rev_dEta_hh_1_bins_2SR_1NPCatcorr_cat> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Thu Apr  8 16:34:56 2021
Job was executed on host(s) <deft0027>, in queue <long>, as user <nhartman> in cluster <slac> at Thu Apr  8 16:39:59 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Thu Apr  8 16:39:59 2021
Terminated at Thu Apr  8 16:40:52 2021
Results reported at Thu Apr  8 16:40:52 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 10 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 1_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   51.04 sec.
    Max Memory :                                 502 MB
    Average Memory :                             450.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   53 sec.
    Turnaround time :                            356 sec.

The output (if any) is above this job summary.

