Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst 1 shape NP
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.5, 3.6, 8.0] Xhh [0.0, 0.95, 1.6] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
qmu(mu = 1.0) = 1.1071597327827476e-07
Unexpectedly large med guess (5890.38), verifying by rescaling signal
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
qmu_test 0.29282483765200595
Initial guess was wrong, subbing in new guess 72.4392810076512
Initial guess for bands (mu guess = 1.00):
[ 38.87264649  52.18660119  72.43928101 100.79537672 135.12333914]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.2942)
Param: 2, mu: 38.873, CLs: 0.294
obs tensor(0.0003)
Param: 2, mu: 135.123, CLs: 0.000
obs tensor(0.0015)
Param: 2, mu: 118.853, CLs: 0.001
obs tensor(0.0342)
Param: 2, mu: 78.863, CLs: 0.034
obs tensor(0.0935)
Param: 2, mu: 62.338, CLs: 0.093
obs tensor(0.0454)
Param: 2, mu: 74.457, CLs: 0.045
obs tensor(0.0503)
Param: 2, mu: 72.842, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 72.938, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 72.934, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 72.934, CLs: 0.050
Param: 2, mu: 38.873, CLs: 0.294
Param: 2, mu: 135.123, CLs: 0.000
Param: 2, mu: 118.853, CLs: 0.001
Param: 2, mu: 78.863, CLs: 0.034
Param: 2, mu: 62.338, CLs: 0.093
Param: 2, mu: 74.457, CLs: 0.045
Param: 2, mu: 72.842, CLs: 0.050
Param: 2, mu: 72.938, CLs: 0.050
Param: 2, mu: 72.934, CLs: 0.050
Param: 2, mu: 72.934, CLs: 0.050
Guess for bands with proper med:
[ 39.1381999   52.54310715  72.93414051 101.48394719 136.0464166 ]
Refining bands
Param: 0, mu: 37.181, CLs: 0.059
Param: 0, mu: 41.095, CLs: 0.041
Param: 0, mu: 39.137, CLs: 0.049
Param: 0, mu: 38.992, CLs: 0.050
Param: 0, mu: 38.980, CLs: 0.050
Param: 0, mu: 38.981, CLs: 0.050
Param: 1, mu: 49.916, CLs: 0.060
Param: 1, mu: 55.170, CLs: 0.041
Param: 1, mu: 52.635, CLs: 0.049
Param: 1, mu: 52.410, CLs: 0.050
Param: 1, mu: 52.412, CLs: 0.050
Param: 1, mu: 52.411, CLs: 0.050
Param: 3, mu: 96.410, CLs: 0.067
Param: 3, mu: 106.558, CLs: 0.038
Param: 3, mu: 102.425, CLs: 0.048
Param: 3, mu: 101.833, CLs: 0.050
Param: 3, mu: 101.845, CLs: 0.050
Param: 3, mu: 101.845, CLs: 0.050
Param: 4, mu: 129.244, CLs: 0.075
Param: 4, mu: 142.849, CLs: 0.036
Param: 4, mu: 138.072, CLs: 0.047
Param: 4, mu: 137.091, CLs: 0.050
Param: 4, mu: 137.123, CLs: 0.050
Param: 4, mu: 137.122, CLs: 0.050
Param: 4, mu: 137.121, CLs: 0.050
Final bands:
[72.93414050650911, 38.98026283204165, 52.41197414553607, 72.93414050650911, 101.84541596467021, 137.12193478879843]
Finished in 3.0 min 58.30890417098999 s
45 function calls
[72.93414050650911, 38.98026283204165, 52.41197414553607, 72.93414050650911, 101.84541596467021, 137.12193478879843]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-dEta_hh-cat-corr-18-SM-HH-3_dEta_hh_2SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-dEta_hh-cat-corr-18-SM-HH-3_dEta_hh_2SR_rev_dEta.json
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
1: <rev_dEta_hh_3_bins_2SR_1NPCatcorr_cat> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_1NPCatcorr_cat> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Thu Apr  8 16:34:52 2021
Job was executed on host(s) <deft0010>, in queue <long>, as user <nhartman> in cluster <slac> at Thu Apr  8 16:38:53 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Thu Apr  8 16:38:53 2021
Terminated at Thu Apr  8 16:41:18 2021
Results reported at Thu Apr  8 16:41:18 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   143.15 sec.
    Max Memory :                                 261 MB
    Average Memory :                             249.29 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   145 sec.
    Turnaround time :                            386 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@deft0026>
Subject: Job 419276: <rev_dEta_hh_3_bins_2SR_1NPCatcorr_cat> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_1NPCatcorr_cat> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Thu Apr  8 16:34:59 2021
Job was executed on host(s) <deft0026>, in queue <long>, as user <nhartman> in cluster <slac> at Thu Apr  8 16:41:07 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Thu Apr  8 16:41:07 2021
Terminated at Thu Apr  8 16:45:13 2021
Results reported at Thu Apr  8 16:45:13 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   245.98 sec.
    Max Memory :                                 502 MB
    Average Memory :                             480.67 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   248 sec.
    Turnaround time :                            614 sec.

The output (if any) is above this job summary.

