Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 39 bins from 225 to 1200 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst, HT cut 300.0
Running with bootstrap
bins [ 225.  250.  275.  300.  325.  350.  375.  400.  425.  450.  475.  500.
  525.  550.  575.  600.  625.  650.  675.  700.  725.  750.  775.  800.
  825.  850.  875.  900.  925.  950.  975. 1000. 1025. 1050. 1075. 1100.
 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.5, 3.6, 8.0] Xhh [0.0, 0.95, 1.6] in btag regs ['4b']
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error
qmu(mu = 1.0) = 1.3946191756986082e-07
Unexpectedly large med guess (5248.32), verifying by rescaling signal
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error
qmu_test 0.13449580842052455
Initial guess was wrong, subbing in new guess 106.886710553604
Initial guess for bands (mu guess = 1.00):
[ 57.35795905  77.00316816 106.88671055 148.72712852 199.37924615]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.1431)
Param: 2, mu: 57.358, CLs: 0.143
obs tensor(9.5063e-06)
Param: 2, mu: 199.379, CLs: 0.000
obs tensor(0.0006)
Param: 2, mu: 149.749, CLs: 0.001
obs tensor(0.0127)
Param: 2, mu: 103.553, CLs: 0.013
obs tensor(0.0464)
Param: 2, mu: 80.456, CLs: 0.046
obs tensor(0.0515)
Param: 2, mu: 78.478, CLs: 0.051
obs tensor(0.0500)
Param: 2, mu: 79.064, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 79.045, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 79.045, CLs: 0.050
Param: 2, mu: 57.358, CLs: 0.290
Param: 2, mu: 199.379, CLs: 0.000
Param: 2, mu: 174.989, CLs: 0.001
Param: 2, mu: 116.173, CLs: 0.032
Param: 2, mu: 87.815, CLs: 0.106
Param: 2, mu: 109.345, CLs: 0.044
Param: 2, mu: 106.151, CLs: 0.051
Param: 2, mu: 106.404, CLs: 0.050
Param: 2, mu: 106.391, CLs: 0.050
Param: 2, mu: 106.391, CLs: 0.050
Guess for bands with proper med:
[ 57.09216126  76.64633413 106.39139567 148.03792441 198.45531924]
Refining bands
Param: 0, mu: 54.238, CLs: 0.059
Param: 0, mu: 59.947, CLs: 0.042
Param: 0, mu: 57.234, CLs: 0.049
Param: 0, mu: 57.003, CLs: 0.050
Param: 0, mu: 57.006, CLs: 0.050
Param: 0, mu: 57.005, CLs: 0.050
Param: 1, mu: 72.814, CLs: 0.060
Param: 1, mu: 80.479, CLs: 0.041
Param: 1, mu: 76.900, CLs: 0.049
Param: 1, mu: 76.571, CLs: 0.050
Param: 1, mu: 76.575, CLs: 0.050
Param: 1, mu: 76.574, CLs: 0.050
Param: 3, mu: 140.636, CLs: 0.067
Param: 3, mu: 155.440, CLs: 0.037
Param: 3, mu: 149.090, CLs: 0.048
Param: 3, mu: 148.213, CLs: 0.050
Param: 3, mu: 148.232, CLs: 0.050
Param: 3, mu: 148.231, CLs: 0.050
Param: 3, mu: 148.231, CLs: 0.050
Param: 4, mu: 188.533, CLs: 0.073
Param: 4, mu: 208.378, CLs: 0.035
Param: 4, mu: 200.479, CLs: 0.047
Param: 4, mu: 198.970, CLs: 0.050
Param: 4, mu: 199.023, CLs: 0.050
Param: 4, mu: 199.022, CLs: 0.050
Param: 4, mu: 199.022, CLs: 0.050
Final bands:
[79.0447708143545, 57.00561721836914, 76.57483960849098, 106.39139566951339, 148.23104296899072, 199.0218393424772]
Finished in 37.0 min 3.538905620574951 s
45 function calls
[79.0447708143545, 57.00561721836914, 76.57483960849098, 106.39139566951339, 148.23104296899072, 199.0218393424772]
Writing to ../stats-results/dEta_cats/lim-systs-HTcut-300.0-dEta_hh-cat-18-SM-HH-unblind-3_dEta_hh_2SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-HTcut-300.0-dEta_hh-cat-18-SM-HH-unblind-3_dEta_hh_2SR_rev_dEta.json
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error
1>
Subject: Job 855411: <rev_dEta_hh_3_bins_2SR_HTsplitCatno-norm_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_HTsplitCatno-norm_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:08 2021
Job was executed on host(s) <deft0021>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:09 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:09 2021
Terminated at Tue Apr 20 18:21:00 2021
Results reported at Tue Apr 20 18:21:00 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --no-norm --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1290.90 sec.
    Max Memory :                                 285 MB
    Average Memory :                             279.75 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1311 sec.
    Turnaround time :                            1312 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@bubble0006>
Subject: Job 855454: <rev_dEta_hh_3_bins_2SR_HTsplitCatno-norm_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_HTsplitCatno-norm_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:10 2021
Job was executed on host(s) <bubble0006>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:12 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:12 2021
Terminated at Tue Apr 20 18:22:41 2021
Results reported at Tue Apr 20 18:22:41 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --no-norm --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1395.16 sec.
    Max Memory :                                 262 MB
    Average Memory :                             257.66 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1409 sec.
    Turnaround time :                            1411 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0029>
Subject: Job 855499: <rev_dEta_hh_3_bins_2SR_HTsplitCatno-norm_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_HTsplitCatno-norm_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:13 2021
Job was executed on host(s) <kiso0029>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:02:30 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:02:30 2021
Terminated at Tue Apr 20 18:39:45 2021
Results reported at Tue Apr 20 18:39:45 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --no-norm --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   2228.12 sec.
    Max Memory :                                 502 MB
    Average Memory :                             496.08 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   2235 sec.
    Turnaround time :                            2432 sec.

The output (if any) is above this job summary.

