Using uproot4
No direct category match for 17 assuming 4b
{'17': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root']
Running with 39 bins from 225 to 1200 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root
data17_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data17_NN_100_bootstraps.root
[124.0, 117.0]
353415
Xwt_str X_wt_tag
yr 17 NN_norm_bstrap_med_17
4354.787880398217
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
5873
Not able to flatten, maybe double check signal counts
4b
S = 0.035241626203060
Running with syst, HT cut 300.0
Running with bootstrap
bins [ 225.  250.  275.  300.  325.  350.  375.  400.  425.  450.  475.  500.
  525.  550.  575.  600.  625.  650.  675.  700.  725.  750.  775.  800.
  825.  850.  875.  900.  925.  950.  975. 1000. 1025. 1050. 1075. 1100.
 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.5, 3.6, 8.0] Xhh [0.0, 0.95, 1.6] in btag regs ['4b']
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
qmu(mu = 1.0) = 2.9674947654712014e-07
Unexpectedly large med guess (3597.93), verifying by rescaling signal
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
qmu_test 0.17940756511461586
Initial guess was wrong, subbing in new guess 92.54601238477834
Initial guess for bands (mu guess = 1.00):
[ 49.66239826  66.67186329  92.54601238 128.77281569 172.62907697]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.1295)
Param: 2, mu: 49.662, CLs: 0.130
obs tensor(5.8321e-06)
Param: 2, mu: 172.629, CLs: 0.000
obs tensor(0.0006)
Param: 2, mu: 125.160, CLs: 0.001
obs tensor(0.0119)
Param: 2, mu: 87.411, CLs: 0.012
obs tensor(0.0426)
Param: 2, mu: 68.537, CLs: 0.043
obs tensor(0.0534)
Param: 2, mu: 64.911, CLs: 0.053
obs tensor(0.0498)
Param: 2, mu: 66.039, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 65.966, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 65.964, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 65.964, CLs: 0.050
Param: 2, mu: 49.662, CLs: 0.289
Param: 2, mu: 172.629, CLs: 0.000
Param: 2, mu: 151.411, CLs: 0.001
Param: 2, mu: 100.537, CLs: 0.032
Param: 2, mu: 75.900, CLs: 0.105
Param: 2, mu: 94.591, CLs: 0.044
Param: 2, mu: 91.802, CLs: 0.051
Param: 2, mu: 92.024, CLs: 0.050
Param: 2, mu: 92.013, CLs: 0.050
Param: 2, mu: 92.012, CLs: 0.050
Guess for bands with proper med:
[ 49.37624322  66.28769962  92.0127617  128.03082595 171.63438719]
Refining bands
Param: 0, mu: 46.907, CLs: 0.059
Param: 0, mu: 51.845, CLs: 0.041
Param: 0, mu: 49.410, CLs: 0.049
Param: 0, mu: 49.210, CLs: 0.050
Param: 0, mu: 49.212, CLs: 0.050
Param: 0, mu: 49.212, CLs: 0.050
Param: 1, mu: 62.973, CLs: 0.060
Param: 1, mu: 69.602, CLs: 0.041
Param: 1, mu: 66.434, CLs: 0.049
Param: 1, mu: 66.149, CLs: 0.050
Param: 1, mu: 66.152, CLs: 0.050
Param: 1, mu: 66.152, CLs: 0.050
Param: 3, mu: 121.629, CLs: 0.067
Param: 3, mu: 134.432, CLs: 0.038
Param: 3, mu: 129.139, CLs: 0.048
Param: 3, mu: 128.389, CLs: 0.050
Param: 3, mu: 128.405, CLs: 0.050
Param: 3, mu: 128.404, CLs: 0.050
Param: 4, mu: 163.053, CLs: 0.075
Param: 4, mu: 180.216, CLs: 0.036
Param: 4, mu: 173.968, CLs: 0.047
Param: 4, mu: 172.709, CLs: 0.050
Param: 4, mu: 172.751, CLs: 0.050
Param: 4, mu: 172.750, CLs: 0.050
Param: 4, mu: 172.750, CLs: 0.050
Final bands:
[65.96350459446542, 49.21208115300782, 66.15230243969133, 92.01276170283838, 128.4045441902627, 172.750040636817]
Finished in 42.0 min 24.502342224121094 s
45 function calls
[65.96350459446542, 49.21208115300782, 66.15230243969133, 92.01276170283838, 128.4045441902627, 172.750040636817]
Writing to ../stats-results/dEta_cats/lim-systs-HTcut-300.0-dEta_hh-cat-17-SM-HH-unblind-3_dEta_hh_2SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-HTcut-300.0-dEta_hh-cat-17-SM-HH-unblind-3_dEta_hh_2SR_rev_dEta.json
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
rror
Running syst error
Running norm error

Running syst error
Running norm error

------------------------------------------------------------
Sender: LSF System <lsf@deft0014>
Subject: Job 860325: <rev_dEta_hh_3_bins_2SR_HTsplitCatnormalize-syst_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_HTsplitCatnormalize-syst_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 18:11:55 2021
Job was executed on host(s) <deft0014>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:15:47 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:15:47 2021
Terminated at Tue Apr 20 18:47:14 2021
Results reported at Tue Apr 20 18:47:14 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --normalize-syst --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   1884.81 sec.
    Max Memory :                                 502 MB
    Average Memory :                             499.39 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   1886 sec.
    Turnaround time :                            2119 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0055>
Subject: Job 860285: <rev_dEta_hh_3_bins_2SR_HTsplitCatnormalize-syst_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_HTsplitCatnormalize-syst_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 18:11:53 2021
Job was executed on host(s) <kiso0055>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:11:55 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:11:55 2021
Terminated at Tue Apr 20 18:51:27 2021
Results reported at Tue Apr 20 18:51:27 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --normalize-syst --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   2346.02 sec.
    Max Memory :                                 284 MB
    Average Memory :                             280.64 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   2 MB
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   2371 sec.
    Turnaround time :                            2374 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0010>
Subject: Job 860308: <rev_dEta_hh_3_bins_2SR_HTsplitCatnormalize-syst_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_HTsplitCatnormalize-syst_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 18:11:54 2021
Job was executed on host(s) <kiso0010>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:13:08 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:13:08 2021
Terminated at Tue Apr 20 18:55:46 2021
Results reported at Tue Apr 20 18:55:46 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --normalize-syst --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   2533.80 sec.
    Max Memory :                                 262 MB
    Average Memory :                             259.65 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   2557 sec.
    Turnaround time :                            2632 sec.

The output (if any) is above this job summary.

