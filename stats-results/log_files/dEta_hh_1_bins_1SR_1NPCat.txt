Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/data18_min_dR_VEC_sr_124_117_NN_100_bootstraps.root']}}
../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16e/NanoNTuple.root
['../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16e/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/data18_min_dR_VEC_sr_124_117_NN_100_bootstraps.root
data18_min_dR_VEC_sr_124_117_NN_100_bootstraps.root
Loading in sig
(124.0,117.0) center already applied: skipping validation tree
Closing data18_min_dR_VEC_sr_124_117_NN_100_bootstraps.root
[124.0, 117.0]
866578
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7120.438637929156
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
66004
SR is different! If what you want for this sample, ignore this warning
Not able to flatten, maybe double check signal counts
4b
S = 0.256860941648483
Running with syst 1 shape NP
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [0.0, 1.5] Xhh [] in btag regs ['4b']
Running w/o any variable split
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
qmu(mu = 1.0) = 0.03108769733501049
Initial guess for bands (mu guess = 1.00):
[ 5.96518327  8.00826979 11.11613503 15.46750606 20.73528702]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.2944)
Param: 2, mu: 5.965, CLs: 0.294
obs tensor(0.0003)
Param: 2, mu: 20.735, CLs: 0.000
obs tensor(0.0015)
Param: 2, mu: 18.239, CLs: 0.001
obs tensor(0.0340)
Param: 2, mu: 12.102, CLs: 0.034
obs tensor(0.0945)
Param: 2, mu: 9.533, CLs: 0.094
obs tensor(0.0453)
Param: 2, mu: 11.424, CLs: 0.045
obs tensor(0.0503)
Param: 2, mu: 11.170, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 11.186, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 11.185, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 11.185, CLs: 0.050
Param: 2, mu: 5.965, CLs: 0.294
Param: 2, mu: 20.735, CLs: 0.000
Param: 2, mu: 18.239, CLs: 0.001
Param: 2, mu: 12.102, CLs: 0.034
Param: 2, mu: 9.533, CLs: 0.094
Param: 2, mu: 11.424, CLs: 0.045
Param: 2, mu: 11.170, CLs: 0.050
Param: 2, mu: 11.186, CLs: 0.050
Param: 2, mu: 11.185, CLs: 0.050
Param: 2, mu: 11.185, CLs: 0.050
Guess for bands with proper med:
[ 6.00217289  8.05792841 11.18506527 15.5634188  20.86386484]
Refining bands
Param: 0, mu: 5.702, CLs: 0.059
Param: 0, mu: 6.302, CLs: 0.041
Param: 0, mu: 6.008, CLs: 0.049
Param: 0, mu: 5.983, CLs: 0.050
Param: 0, mu: 5.984, CLs: 0.050
Param: 1, mu: 7.655, CLs: 0.060
Param: 1, mu: 8.461, CLs: 0.041
Param: 1, mu: 8.077, CLs: 0.049
Param: 1, mu: 8.042, CLs: 0.050
Param: 1, mu: 8.043, CLs: 0.050
Param: 3, mu: 14.785, CLs: 0.067
Param: 3, mu: 16.342, CLs: 0.038
Param: 3, mu: 15.694, CLs: 0.048
Param: 3, mu: 15.602, CLs: 0.050
Param: 3, mu: 15.604, CLs: 0.050
Param: 3, mu: 15.604, CLs: 0.050
Param: 4, mu: 19.821, CLs: 0.074
Param: 4, mu: 21.907, CLs: 0.036
Param: 4, mu: 21.135, CLs: 0.047
Param: 4, mu: 20.980, CLs: 0.050
Param: 4, mu: 20.986, CLs: 0.050
Param: 4, mu: 20.985, CLs: 0.050
Final bands:
[11.185065273570729, 5.983366862403129, 8.042976881080024, 11.185065273570729, 15.604342634612182, 20.985661297529862]
Finished in 0.0 min 57.66173958778381 s
42 function calls
[11.185065273570729, 5.983366862403129, 8.042976881080024, 11.185065273570729, 15.604342634612182, 20.985661297529862]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-dEta_hh-cat-corr-18-SM-HH-1_dEta_hh_1SR.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-dEta_hh-cat-corr-18-SM-HH-1_dEta_hh_1SR.json
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
g syst error w/o an NP split
sf@deft0027>
Subject: Job 63936: <dEta_hh_1_bins_1SR_1NPCat> in cluster <slac> Done

Job <dEta_hh_1_bins_1SR_1NPCat> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Wed Apr  7 09:41:24 2021
Job was executed on host(s) <deft0027>, in queue <long>, as user <nhartman> in cluster <slac> at Wed Apr  7 09:41:26 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Wed Apr  7 09:41:26 2021
Terminated at Wed Apr  7 09:42:42 2021
Results reported at Wed Apr  7 09:42:42 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/data17_min_dR_VEC_sr_124_117_NN_100_bootstraps.root -s ../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 0.0 1.5 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 1_dEta_hh_1SR
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   62.06 sec.
    Max Memory :                                 184 MB
    Average Memory :                             149.40 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   75 sec.
    Turnaround time :                            78 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0006>
Subject: Job 63921: <dEta_hh_1_bins_1SR_1NPCat> in cluster <slac> Done

Job <dEta_hh_1_bins_1SR_1NPCat> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Wed Apr  7 09:41:23 2021
Job was executed on host(s) <kiso0006>, in queue <long>, as user <nhartman> in cluster <slac> at Wed Apr  7 09:41:24 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Wed Apr  7 09:41:24 2021
Terminated at Wed Apr  7 09:43:03 2021
Results reported at Wed Apr  7 09:43:03 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/data16_min_dR_VEC_sr_124_117_NN_100_bootstraps.root -s ../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 0.0 1.5 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 1_dEta_hh_1SR
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   83.11 sec.
    Max Memory :                                 186 MB
    Average Memory :                             158.50 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   99 sec.
    Turnaround time :                            100 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@deft0025>
Subject: Job 64083: <dEta_hh_1_bins_1SR_1NPCat> in cluster <slac> Done

Job <dEta_hh_1_bins_1SR_1NPCat> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Wed Apr  7 09:41:58 2021
Job was executed on host(s) <deft0025>, in queue <long>, as user <nhartman> in cluster <slac> at Wed Apr  7 09:42:00 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Wed Apr  7 09:42:00 2021
Terminated at Wed Apr  7 09:43:16 2021
Results reported at Wed Apr  7 09:43:16 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/data18_min_dR_VEC_sr_124_117_NN_100_bootstraps.root -s ../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 0.0 1.5 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 1_dEta_hh_1SR
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   65.74 sec.
    Max Memory :                                 311 MB
    Average Memory :                             254.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   76 sec.
    Turnaround time :                            78 sec.

The output (if any) is above this job summary.

