Using uproot4
No direct category match for 16 assuming 4b
{'16': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/data16_min_dR_VEC_sr_124_117_NN_100_bootstraps.root']}}
../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16a/NanoNTuple.root
['../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16a/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/data16_min_dR_VEC_sr_124_117_NN_100_bootstraps.root
data16_min_dR_VEC_sr_124_117_NN_100_bootstraps.root
Loading in sig
(124.0,117.0) center already applied: skipping validation tree
Closing data16_min_dR_VEC_sr_124_117_NN_100_bootstraps.root
[124.0, 117.0]
400452
Xwt_str X_wt_tag
yr 16 NN_norm_bstrap_med_16
3023.984801438291
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16a/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
35075
SR is different! If what you want for this sample, ignore this warning
Not able to flatten, maybe double check signal counts
4b
S = 0.259862482547760
Running with syst 1 shape NP
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [0.0, 0.75, 1.5] Xhh [0.0, 0.95, 1.6] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
qmu(mu = 1.0) = 0.024909285757303223
Initial guess for bands (mu guess = 1.00):
[ 6.66403749  8.94648289 12.41845173 17.27960994 23.16454058]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.3007)
Param: 2, mu: 6.664, CLs: 0.301
obs tensor(0.0005)
Param: 2, mu: 23.165, CLs: 0.001
obs tensor(0.0021)
Param: 2, mu: 20.445, CLs: 0.002
obs tensor(0.0384)
Param: 2, mu: 13.554, CLs: 0.038
obs tensor(0.0736)
Param: 2, mu: 11.663, CLs: 0.074
obs tensor(0.0479)
Param: 2, mu: 12.932, CLs: 0.048
obs tensor(0.0501)
Param: 2, mu: 12.807, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 12.810, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 12.810, CLs: 0.050
Param: 2, mu: 6.664, CLs: 0.301
Param: 2, mu: 23.165, CLs: 0.001
Param: 2, mu: 20.445, CLs: 0.002
Param: 2, mu: 13.554, CLs: 0.038
Param: 2, mu: 11.663, CLs: 0.074
Param: 2, mu: 12.932, CLs: 0.048
Param: 2, mu: 12.807, CLs: 0.050
Param: 2, mu: 12.810, CLs: 0.050
Param: 2, mu: 12.810, CLs: 0.050
Guess for bands with proper med:
[ 6.87420944  9.22863914 12.81010774 17.82457828 23.89510922]
Refining bands
Param: 0, mu: 6.530, CLs: 0.057
Param: 0, mu: 7.218, CLs: 0.040
Param: 0, mu: 6.799, CLs: 0.049
Param: 0, mu: 6.775, CLs: 0.050
Param: 0, mu: 6.774, CLs: 0.050
Param: 0, mu: 6.774, CLs: 0.050
Param: 1, mu: 8.767, CLs: 0.058
Param: 1, mu: 9.690, CLs: 0.040
Param: 1, mu: 9.186, CLs: 0.049
Param: 1, mu: 9.150, CLs: 0.050
Param: 1, mu: 9.148, CLs: 0.050
Param: 1, mu: 9.148, CLs: 0.050
Param: 3, mu: 16.933, CLs: 0.070
Param: 3, mu: 18.716, CLs: 0.040
Param: 3, mu: 18.130, CLs: 0.049
Param: 3, mu: 18.036, CLs: 0.050
Param: 3, mu: 18.038, CLs: 0.050
Param: 3, mu: 18.037, CLs: 0.050
Param: 4, mu: 22.700, CLs: 0.083
Param: 4, mu: 25.090, CLs: 0.042
Param: 4, mu: 24.633, CLs: 0.048
Param: 4, mu: 24.517, CLs: 0.050
Param: 4, mu: 24.520, CLs: 0.050
Param: 4, mu: 24.519, CLs: 0.050
Final bands:
[12.810107735178175, 6.7736596274638865, 9.147855633050288, 12.810107735178175, 18.037960003096757, 24.51969477941194]
Finished in 5.0 min 27.12795877456665 s
42 function calls
[12.810107735178175, 6.7736596274638865, 9.147855633050288, 12.810107735178175, 18.037960003096757, 24.51969477941194]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-dEta_hh-cat-corr-16-SM-HH-2_dEta_hh_2SR.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-dEta_hh-cat-corr-16-SM-HH-2_dEta_hh_2SR.json
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error w/o an NP split
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error w/o an NP split
--------------------------------------------------------
Sender: LSF System <lsf@deft0027>
Subject: Job 63941: <dEta_hh_2_bins_2SR_1NPCat> in cluster <slac> Done

Job <dEta_hh_2_bins_2SR_1NPCat> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Wed Apr  7 09:41:25 2021
Job was executed on host(s) <deft0027>, in queue <long>, as user <nhartman> in cluster <slac> at Wed Apr  7 09:41:27 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Wed Apr  7 09:41:27 2021
Terminated at Wed Apr  7 09:44:23 2021
Results reported at Wed Apr  7 09:44:23 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/data17_min_dR_VEC_sr_124_117_NN_100_bootstraps.root -s ../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 0.0 0.75 1.5 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 2_dEta_hh_2SR
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   163.68 sec.
    Max Memory :                                 201 MB
    Average Memory :                             178.86 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   175 sec.
    Turnaround time :                            178 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@deft0025>
Subject: Job 64086: <dEta_hh_2_bins_2SR_1NPCat> in cluster <slac> Done

Job <dEta_hh_2_bins_2SR_1NPCat> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Wed Apr  7 09:41:59 2021
Job was executed on host(s) <deft0025>, in queue <long>, as user <nhartman> in cluster <slac> at Wed Apr  7 09:42:00 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Wed Apr  7 09:42:00 2021
Terminated at Wed Apr  7 09:45:04 2021
Results reported at Wed Apr  7 09:45:04 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/data18_min_dR_VEC_sr_124_117_NN_100_bootstraps.root -s ../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 0.0 0.75 1.5 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 2_dEta_hh_2SR
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   173.81 sec.
    Max Memory :                                 336 MB
    Average Memory :                             291.71 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   184 sec.
    Turnaround time :                            185 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0018>
Subject: Job 63927: <dEta_hh_2_bins_2SR_1NPCat> in cluster <slac> Done

Job <dEta_hh_2_bins_2SR_1NPCat> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Wed Apr  7 09:41:23 2021
Job was executed on host(s) <kiso0018>, in queue <long>, as user <nhartman> in cluster <slac> at Wed Apr  7 09:41:24 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Wed Apr  7 09:41:24 2021
Terminated at Wed Apr  7 09:47:13 2021
Results reported at Wed Apr  7 09:47:13 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/data16_min_dR_VEC_sr_124_117_NN_100_bootstraps.root -s ../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 0.0 0.75 1.5 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 2_dEta_hh_2SR
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   333.06 sec.
    Max Memory :                                 212 MB
    Average Memory :                             196.38 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   360 sec.
    Turnaround time :                            350 sec.

The output (if any) is above this job summary.

