Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst 1 shape NP
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 3.0, 8.0] Xhh [] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
qmu(mu = 1.0) = 6.06537469138857e-08
Unexpectedly large med guess (7958.28), verifying by rescaling signal
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
qmu_test 0.30714844243561856
Initial guess was wrong, subbing in new guess 70.73004396213996
Initial guess for bands (mu guess = 1.00):
[ 37.95542911  50.95523513  70.73004396  98.41706499 131.93504387]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.2940)
Param: 2, mu: 37.955, CLs: 0.294
obs tensor(0.0003)
Param: 2, mu: 131.935, CLs: 0.000
obs tensor(0.0015)
Param: 2, mu: 116.032, CLs: 0.001
obs tensor(0.0341)
Param: 2, mu: 76.994, CLs: 0.034
obs tensor(0.0943)
Param: 2, mu: 60.673, CLs: 0.094
obs tensor(0.0454)
Param: 2, mu: 72.673, CLs: 0.045
obs tensor(0.0503)
Param: 2, mu: 71.063, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 71.160, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 71.156, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 71.155, CLs: 0.050
Param: 2, mu: 37.955, CLs: 0.294
Param: 2, mu: 131.935, CLs: 0.000
Param: 2, mu: 116.032, CLs: 0.001
Param: 2, mu: 76.994, CLs: 0.034
Param: 2, mu: 60.673, CLs: 0.094
Param: 2, mu: 72.673, CLs: 0.045
Param: 2, mu: 71.063, CLs: 0.050
Param: 2, mu: 71.160, CLs: 0.050
Param: 2, mu: 71.156, CLs: 0.050
Param: 2, mu: 71.155, CLs: 0.050
Guess for bands with proper med:
[ 38.18383893  51.26187575  71.15568628  99.00932346 132.72900828]
Refining bands
Param: 0, mu: 36.275, CLs: 0.059
Param: 0, mu: 40.093, CLs: 0.041
Param: 0, mu: 38.191, CLs: 0.049
Param: 0, mu: 38.036, CLs: 0.050
Param: 0, mu: 38.036, CLs: 0.050
Param: 1, mu: 48.699, CLs: 0.060
Param: 1, mu: 53.825, CLs: 0.041
Param: 1, mu: 51.359, CLs: 0.049
Param: 1, mu: 51.139, CLs: 0.050
Param: 1, mu: 51.141, CLs: 0.050
Param: 1, mu: 51.141, CLs: 0.050
Param: 3, mu: 94.059, CLs: 0.067
Param: 3, mu: 103.960, CLs: 0.038
Param: 3, mu: 99.911, CLs: 0.048
Param: 3, mu: 99.333, CLs: 0.050
Param: 3, mu: 99.344, CLs: 0.050
Param: 3, mu: 99.344, CLs: 0.050
Param: 4, mu: 126.093, CLs: 0.075
Param: 4, mu: 139.365, CLs: 0.036
Param: 4, mu: 134.658, CLs: 0.047
Param: 4, mu: 133.697, CLs: 0.050
Param: 4, mu: 133.728, CLs: 0.050
Param: 4, mu: 133.727, CLs: 0.050
Final bands:
[71.1556862799516, 38.03566029958132, 51.141420091881784, 71.1556862799516, 99.34409298669107, 133.7274475178734]
Finished in 1.0 min 2.0457839965820312 s
43 function calls
[71.1556862799516, 38.03566029958132, 51.141420091881784, 71.1556862799516, 99.34409298669107, 133.7274475178734]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-noBS-dEta_hh-cat-corr-18-SM-HH-2_dEta_hh_1SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-noBS-dEta_hh-cat-corr-18-SM-HH-2_dEta_hh_1SR_rev_dEta.json
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split

------------------------------------------------------------
Sender: LSF System <lsf@bubble0002>
Subject: Job 419267: <rev_dEta_hh_2_bins_1SR_1NPCatcorr_cat_no-bootstrap> in cluster <slac> Done

Job <rev_dEta_hh_2_bins_1SR_1NPCatcorr_cat_no-bootstrap> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Thu Apr  8 16:34:57 2021
Job was executed on host(s) <bubble0002>, in queue <long>, as user <nhartman> in cluster <slac> at Thu Apr  8 16:40:29 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Thu Apr  8 16:40:29 2021
Terminated at Thu Apr  8 16:41:38 2021
Results reported at Thu Apr  8 16:41:38 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 3 8 --bins 34 --range 350 1200 --systvar None --corr_cat --no-bootstrap --no-norm --outDir ../stats-results/dEta_cats --label 2_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   67.56 sec.
    Max Memory :                                 502 MB
    Average Memory :                             501.50 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   69 sec.
    Turnaround time :                            401 sec.

The output (if any) is above this job summary.

