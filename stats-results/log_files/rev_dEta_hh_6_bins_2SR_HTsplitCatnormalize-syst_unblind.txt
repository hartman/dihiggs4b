Using uproot4
No direct category match for 17 assuming 4b
{'17': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root']
Running with 39 bins from 225 to 1200 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root
data17_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data17_NN_100_bootstraps.root
[124.0, 117.0]
353415
Xwt_str X_wt_tag
yr 17 NN_norm_bstrap_med_17
4354.787880398217
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
5873
Not able to flatten, maybe double check signal counts
4b
S = 0.035241626203060
Running with syst, HT cut 300.0
Running with bootstrap
bins [ 225.  250.  275.  300.  325.  350.  375.  400.  425.  450.  475.  500.
  525.  550.  575.  600.  625.  650.  675.  700.  725.  750.  775.  800.
  825.  850.  875.  900.  925.  950.  975. 1000. 1025. 1050. 1075. 1100.
 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.0, 2.5, 3.0, 3.6, 4.6, 8.0] Xhh [0.0, 0.95, 1.6] in btag regs ['4b']
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_3_Xhh-0.0-0.95
['cat', '4b', '3', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_3_Xhh-0.95-1.6
['cat', '4b', '3', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_4_Xhh-0.0-0.95
['cat', '4b', '4', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_4_Xhh-0.95-1.6
['cat', '4b', '4', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_5_Xhh-0.0-0.95
['cat', '4b', '5', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_5_Xhh-0.95-1.6
['cat', '4b', '5', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
qmu(mu = 1.0) = 7.069138519000262e-07
Unexpectedly large med guess (2331.12), verifying by rescaling signal
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_3_Xhh-0.0-0.95
['cat', '4b', '3', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_3_Xhh-0.95-1.6
['cat', '4b', '3', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_4_Xhh-0.0-0.95
['cat', '4b', '4', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_4_Xhh-0.95-1.6
['cat', '4b', '4', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_5_Xhh-0.0-0.95
['cat', '4b', '5', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_5_Xhh-0.95-1.6
['cat', '4b', '5', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
qmu_test 0.26750982496741926
Initial guess was wrong, subbing in new guess 75.78935622763338
Initial guess for bands (mu guess = 1.00):
[ 40.67037678  54.6000575   75.78935623 105.45682682 141.37234304]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.1308)
Param: 2, mu: 40.670, CLs: 0.131
obs tensor(7.3327e-06)
Param: 2, mu: 141.372, CLs: 0.000
obs tensor(0.0006)
Param: 2, mu: 102.893, CLs: 0.001
obs tensor(0.0122)
Param: 2, mu: 71.782, CLs: 0.012
obs tensor(0.0431)
Param: 2, mu: 56.226, CLs: 0.043
obs tensor(0.0530)
Param: 2, mu: 53.486, CLs: 0.053
obs tensor(0.0498)
Param: 2, mu: 54.328, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 54.277, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 54.276, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 54.276, CLs: 0.050
Param: 2, mu: 40.670, CLs: 0.290
Param: 2, mu: 141.372, CLs: 0.000
Param: 2, mu: 124.122, CLs: 0.001
Param: 2, mu: 82.396, CLs: 0.034
Param: 2, mu: 64.185, CLs: 0.097
Param: 2, mu: 77.658, CLs: 0.045
Param: 2, mu: 75.784, CLs: 0.050
Param: 2, mu: 75.907, CLs: 0.050
Param: 2, mu: 75.902, CLs: 0.050
Param: 2, mu: 75.901, CLs: 0.050
Guess for bands with proper med:
[ 40.7305862   54.68088876  75.90155665 105.61294769 141.58163412]
Refining bands
Param: 0, mu: 38.694, CLs: 0.058
Param: 0, mu: 42.767, CLs: 0.041
Param: 0, mu: 40.619, CLs: 0.049
Param: 0, mu: 40.468, CLs: 0.050
Param: 0, mu: 40.456, CLs: 0.050
Param: 0, mu: 40.457, CLs: 0.050
Param: 1, mu: 51.947, CLs: 0.060
Param: 1, mu: 57.415, CLs: 0.041
Param: 1, mu: 54.690, CLs: 0.049
Param: 1, mu: 54.455, CLs: 0.050
Param: 1, mu: 54.458, CLs: 0.050
Param: 1, mu: 54.457, CLs: 0.050
Param: 3, mu: 100.332, CLs: 0.068
Param: 3, mu: 110.894, CLs: 0.039
Param: 3, mu: 106.805, CLs: 0.048
Param: 3, mu: 106.202, CLs: 0.050
Param: 3, mu: 106.214, CLs: 0.050
Param: 3, mu: 106.213, CLs: 0.050
Param: 4, mu: 134.503, CLs: 0.077
Param: 4, mu: 148.661, CLs: 0.038
Param: 4, mu: 144.273, CLs: 0.048
Param: 4, mu: 143.318, CLs: 0.050
Param: 4, mu: 143.347, CLs: 0.050
Param: 4, mu: 143.346, CLs: 0.050
Param: 4, mu: 143.346, CLs: 0.050
Final bands:
[54.27560597279014, 40.45696839879701, 54.45774632101582, 75.90155664546873, 106.21364663817259, 143.34624131999604]
Finished in 205.0 min 3.6357996463775635 s
45 function calls
[54.27560597279014, 40.45696839879701, 54.45774632101582, 75.90155664546873, 106.21364663817259, 143.34624131999604]
Writing to ../stats-results/dEta_cats/lim-systs-HTcut-300.0-dEta_hh-cat-17-SM-HH-unblind-6_dEta_hh_2SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-HTcut-300.0-dEta_hh-cat-17-SM-HH-unblind-6_dEta_hh_2SR_rev_dEta.json
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_3_Xhh-0.0-0.95
['cat', '4b', '3', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_3_Xhh-0.95-1.6
['cat', '4b', '3', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_4_Xhh-0.0-0.95
['cat', '4b', '4', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_4_Xhh-0.95-1.6
['cat', '4b', '4', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
cat_4b_5_Xhh-0.0-0.95
['cat', '4b', '5', 'Xhh-0.0-0.95']
Running stat error
Running syst error
Running norm error
cat_4b_5_Xhh-0.95-1.6
['cat', '4b', '5', 'Xhh-0.95-1.6']
Running stat error
Running syst error
Running norm error
ning syst error
Running norm error
nd> in cluster <slac> Done

Job <rev_dEta_hh_6_bins_2SR_HTsplitCatnormalize-syst_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 18:11:53 2021
Job was executed on host(s) <deft0010>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:11:55 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:11:55 2021
Terminated at Tue Apr 20 20:38:59 2021
Results reported at Tue Apr 20 20:38:59 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --normalize-syst --unblind --outDir ../stats-results/dEta_cats --label 6_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   8816.07 sec.
    Max Memory :                                 289 MB
    Average Memory :                             285.23 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              5
    Max Threads :                                10
    Run time :                                   8839 sec.
    Turnaround time :                            8826 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@bubble0008>
Subject: Job 860332: <rev_dEta_hh_6_bins_2SR_HTsplitCatnormalize-syst_unblind> in cluster <slac> Done

Job <rev_dEta_hh_6_bins_2SR_HTsplitCatnormalize-syst_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 18:11:56 2021
Job was executed on host(s) <bubble0008>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:16:09 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:16:09 2021
Terminated at Tue Apr 20 21:12:05 2021
Results reported at Tue Apr 20 21:12:05 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --normalize-syst --unblind --outDir ../stats-results/dEta_cats --label 6_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   10525.37 sec.
    Max Memory :                                 504 MB
    Average Memory :                             501.03 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   1 MB
    Max Processes :                              4
    Max Threads :                                6
    Run time :                                   10558 sec.
    Turnaround time :                            10809 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0041>
Subject: Job 860313: <rev_dEta_hh_6_bins_2SR_HTsplitCatnormalize-syst_unblind> in cluster <slac> Done

Job <rev_dEta_hh_6_bins_2SR_HTsplitCatnormalize-syst_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 18:11:54 2021
Job was executed on host(s) <kiso0041>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:13:28 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:13:28 2021
Terminated at Tue Apr 20 21:38:48 2021
Results reported at Tue Apr 20 21:38:48 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --normalize-syst --unblind --outDir ../stats-results/dEta_cats --label 6_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   12309.33 sec.
    Max Memory :                                 272 MB
    Average Memory :                             271.27 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              4
    Max Threads :                                6
    Run time :                                   12320 sec.
    Turnaround time :                            12414 sec.

The output (if any) is above this job summary.

