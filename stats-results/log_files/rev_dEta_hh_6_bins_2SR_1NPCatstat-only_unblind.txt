Using uproot4
No direct category match for 17 assuming 4b
{'17': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root
data17_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data17_NN_100_bootstraps.root
[124.0, 117.0]
353415
Xwt_str X_wt_tag
yr 17 NN_norm_bstrap_med_17
4354.787880398217
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
5873
Not able to flatten, maybe double check signal counts
4b
S = 0.035241626203060
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.0, 2.5, 3.0, 3.6, 4.6, 8.0] Xhh [0.0, 0.95, 1.6] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
cat_4b_3_Xhh-0.0-0.95
['cat', '4b', '3', 'Xhh-0.0-0.95']
Running stat error
cat_4b_3_Xhh-0.95-1.6
['cat', '4b', '3', 'Xhh-0.95-1.6']
Running stat error
cat_4b_4_Xhh-0.0-0.95
['cat', '4b', '4', 'Xhh-0.0-0.95']
Running stat error
cat_4b_4_Xhh-0.95-1.6
['cat', '4b', '4', 'Xhh-0.95-1.6']
Running stat error
cat_4b_5_Xhh-0.0-0.95
['cat', '4b', '5', 'Xhh-0.0-0.95']
Running stat error
cat_4b_5_Xhh-0.95-1.6
['cat', '4b', '5', 'Xhh-0.95-1.6']
Running stat error
qmu(mu = 1.0) = 5.041874828748405e-08
Unexpectedly large med guess (8728.75), verifying by rescaling signal
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
cat_4b_3_Xhh-0.0-0.95
['cat', '4b', '3', 'Xhh-0.0-0.95']
Running stat error
cat_4b_3_Xhh-0.95-1.6
['cat', '4b', '3', 'Xhh-0.95-1.6']
Running stat error
cat_4b_4_Xhh-0.0-0.95
['cat', '4b', '4', 'Xhh-0.0-0.95']
Running stat error
cat_4b_4_Xhh-0.95-1.6
['cat', '4b', '4', 'Xhh-0.95-1.6']
Running stat error
cat_4b_5_Xhh-0.0-0.95
['cat', '4b', '5', 'Xhh-0.0-0.95']
Running stat error
cat_4b_5_Xhh-0.95-1.6
['cat', '4b', '5', 'Xhh-0.95-1.6']
Running stat error
qmu_test 0.38188772932880966
Initial guess was wrong, subbing in new guess 63.43226726176618
Initial guess for bands (mu guess = 1.00):
[ 34.03926802  45.69778149  63.43226726  88.26259987 118.32226441]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.0486)
Param: 2, mu: 34.039, CLs: 0.049
obs tensor(5.5269e-07)
Param: 2, mu: 118.322, CLs: 0.000
Looks like initial guess was bad? Expanding for one more try
obs tensor(0.2479)
Param: 2, mu: 17.020, CLs: 0.248
obs tensor(1.2379e-17)
Param: 2, mu: 236.645, CLs: 0.000
obs tensor(3.5775e-13)
Param: 2, mu: 192.346, CLs: 0.000
obs tensor(5.0116e-06)
Param: 2, mu: 104.683, CLs: 0.000
obs tensor(0.0023)
Param: 2, mu: 60.851, CLs: 0.002
obs tensor(0.0291)
Param: 2, mu: 38.935, CLs: 0.029
obs tensor(0.0891)
Param: 2, mu: 27.978, CLs: 0.089
obs tensor(0.0434)
Param: 2, mu: 35.121, CLs: 0.043
obs tensor(0.0507)
Param: 2, mu: 33.626, CLs: 0.051
obs tensor(0.0500)
Param: 2, mu: 33.763, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 33.755, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 33.754, CLs: 0.050
Param: 2, mu: 17.020, CLs: 0.599
Param: 2, mu: 236.645, CLs: 0.000
Param: 2, mu: 218.300, CLs: 0.000
Param: 2, mu: 117.660, CLs: 0.000
Param: 2, mu: 67.340, CLs: 0.040
Param: 2, mu: 55.603, CLs: 0.089
Param: 2, mu: 64.934, CLs: 0.047
Param: 2, mu: 64.180, CLs: 0.050
Param: 2, mu: 64.204, CLs: 0.050
Param: 2, mu: 64.203, CLs: 0.050
Guess for bands with proper med:
[ 34.4528549   46.25302266  64.20298753  89.33501581 119.75991391]
Refining bands
Param: 0, mu: 32.730, CLs: 0.058
Param: 0, mu: 36.175, CLs: 0.041
Param: 0, mu: 34.282, CLs: 0.049
Param: 0, mu: 34.155, CLs: 0.050
Param: 0, mu: 34.147, CLs: 0.050
Param: 0, mu: 34.147, CLs: 0.050
Param: 1, mu: 43.940, CLs: 0.059
Param: 1, mu: 48.566, CLs: 0.040
Param: 1, mu: 46.198, CLs: 0.049
Param: 1, mu: 46.017, CLs: 0.050
Param: 1, mu: 46.002, CLs: 0.050
Param: 1, mu: 46.002, CLs: 0.050
Param: 3, mu: 84.868, CLs: 0.068
Param: 3, mu: 93.802, CLs: 0.039
Param: 3, mu: 90.506, CLs: 0.048
Param: 3, mu: 90.007, CLs: 0.050
Param: 3, mu: 90.017, CLs: 0.050
Param: 3, mu: 90.016, CLs: 0.050
Param: 4, mu: 113.772, CLs: 0.079
Param: 4, mu: 125.748, CLs: 0.039
Param: 4, mu: 122.499, CLs: 0.048
Param: 4, mu: 121.755, CLs: 0.050
Param: 4, mu: 121.775, CLs: 0.050
Param: 4, mu: 121.775, CLs: 0.050
Final bands:
[33.75470695811932, 34.14650345656028, 46.00245620071787, 64.2029875273172, 90.01679094950183, 121.77461270532474]
Finished in 103.0 min 1.2656760215759277 s
46 function calls
[33.75470695811932, 34.14650345656028, 46.00245620071787, 64.2029875273172, 90.01679094950183, 121.77461270532474]
Writing to ../stats-results/dEta_cats/lim-stat-only-dEta_hh-cat-17-SM-HH-unblind-6_dEta_hh_2SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-stat-only-dEta_hh-cat-17-SM-HH-unblind-6_dEta_hh_2SR_rev_dEta.json
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
cat_4b_3_Xhh-0.0-0.95
['cat', '4b', '3', 'Xhh-0.0-0.95']
Running stat error
cat_4b_3_Xhh-0.95-1.6
['cat', '4b', '3', 'Xhh-0.95-1.6']
Running stat error
cat_4b_4_Xhh-0.0-0.95
['cat', '4b', '4', 'Xhh-0.0-0.95']
Running stat error
cat_4b_4_Xhh-0.95-1.6
['cat', '4b', '4', 'Xhh-0.95-1.6']
Running stat error
cat_4b_5_Xhh-0.0-0.95
['cat', '4b', '5', 'Xhh-0.0-0.95']
Running stat error
cat_4b_5_Xhh-0.95-1.6
['cat', '4b', '5', 'Xhh-0.95-1.6']
Running stat error
 <kiso0032>, in queue <long>, as user <nhartman> in cluster <slac> at Fri Apr  9 12:15:59 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Fri Apr  9 12:15:59 2021
Terminated at Fri Apr  9 13:23:04 2021
Results reported at Fri Apr  9 13:23:04 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --bins 34 --range 350 1200 --systvar None --stat-only --unblind --no-norm --outDir ../stats-results/dEta_cats --label 6_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   4007.87 sec.
    Max Memory :                                 734 MB
    Average Memory :                             495.93 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   18 MB
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   4025 sec.
    Turnaround time :                            5143 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0037>
Subject: Job 707837: <rev_dEta_hh_6_bins_2SR_1NPCatstat-only_unblind> in cluster <slac> Done

Job <rev_dEta_hh_6_bins_2SR_1NPCatstat-only_unblind> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Fri Apr  9 11:57:07 2021
Job was executed on host(s) <kiso0037>, in queue <long>, as user <nhartman> in cluster <slac> at Fri Apr  9 12:02:17 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Fri Apr  9 12:02:17 2021
Terminated at Fri Apr  9 13:25:53 2021
Results reported at Fri Apr  9 13:25:53 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --bins 34 --range 350 1200 --systvar None --stat-only --unblind --no-norm --outDir ../stats-results/dEta_cats --label 6_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   4936.83 sec.
    Max Memory :                                 285 MB
    Average Memory :                             283.98 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   5016 sec.
    Turnaround time :                            5326 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0049>
Subject: Job 707901: <rev_dEta_hh_6_bins_2SR_1NPCatstat-only_unblind> in cluster <slac> Done

Job <rev_dEta_hh_6_bins_2SR_1NPCatstat-only_unblind> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Fri Apr  9 11:57:14 2021
Job was executed on host(s) <kiso0049>, in queue <long>, as user <nhartman> in cluster <slac> at Fri Apr  9 12:10:14 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Fri Apr  9 12:10:14 2021
Terminated at Fri Apr  9 13:53:29 2021
Results reported at Fri Apr  9 13:53:29 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2 2.5 3 3.6 4.6 8 --bins 34 --range 350 1200 --systvar None --stat-only --unblind --no-norm --outDir ../stats-results/dEta_cats --label 6_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   6188.41 sec.
    Max Memory :                                 262 MB
    Average Memory :                             258.71 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   5 MB
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   6194 sec.
    Turnaround time :                            6975 sec.

The output (if any) is above this job summary.

