Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.5, 3.6, 8.0] Xhh [0.0, 0.95, 1.6] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
qmu(mu = 1.0) = 1.0064013622468337e-08
Unexpectedly large med guess (19537.21), verifying by rescaling signal
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
qmu_test 0.4452303631937866
Initial guess was wrong, subbing in new guess 58.74700081267459
Initial guess for bands (mu guess = 1.00):
[ 31.52504226  42.32242866  58.74700081  81.74330274 109.58268502]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.1479)
Param: 2, mu: 31.525, CLs: 0.148
obs tensor(2.5940e-05)
Param: 2, mu: 109.583, CLs: 0.000
obs tensor(0.0009)
Param: 2, mu: 83.199, CLs: 0.001
obs tensor(0.0158)
Param: 2, mu: 57.362, CLs: 0.016
obs tensor(0.0522)
Param: 2, mu: 44.443, CLs: 0.052
obs tensor(0.0488)
Param: 2, mu: 45.212, CLs: 0.049
obs tensor(0.0500)
Param: 2, mu: 44.937, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 44.932, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 44.932, CLs: 0.050
Param: 2, mu: 31.525, CLs: 0.294
Param: 2, mu: 109.583, CLs: 0.000
Param: 2, mu: 96.381, CLs: 0.002
Param: 2, mu: 63.953, CLs: 0.034
Param: 2, mu: 50.742, CLs: 0.092
Param: 2, mu: 60.392, CLs: 0.046
Param: 2, mu: 59.117, CLs: 0.050
Param: 2, mu: 59.190, CLs: 0.050
Param: 2, mu: 59.189, CLs: 0.050
Param: 2, mu: 59.189, CLs: 0.050
Guess for bands with proper med:
[ 31.76216947  42.6407724   59.18888801  82.3581651  110.40695154]
Refining bands
Param: 0, mu: 30.174, CLs: 0.058
Param: 0, mu: 33.350, CLs: 0.041
Param: 0, mu: 31.726, CLs: 0.049
Param: 0, mu: 31.608, CLs: 0.050
Param: 0, mu: 31.599, CLs: 0.050
Param: 0, mu: 31.599, CLs: 0.050
Param: 1, mu: 40.509, CLs: 0.060
Param: 1, mu: 44.773, CLs: 0.041
Param: 1, mu: 42.685, CLs: 0.049
Param: 1, mu: 42.503, CLs: 0.050
Param: 1, mu: 42.505, CLs: 0.050
Param: 1, mu: 42.505, CLs: 0.050
Param: 3, mu: 78.240, CLs: 0.068
Param: 3, mu: 86.476, CLs: 0.038
Param: 3, mu: 83.199, CLs: 0.048
Param: 3, mu: 82.724, CLs: 0.050
Param: 3, mu: 82.734, CLs: 0.050
Param: 3, mu: 82.733, CLs: 0.050
Param: 4, mu: 104.887, CLs: 0.076
Param: 4, mu: 115.927, CLs: 0.037
Param: 4, mu: 112.273, CLs: 0.048
Param: 4, mu: 111.500, CLs: 0.050
Param: 4, mu: 111.524, CLs: 0.050
Param: 4, mu: 111.523, CLs: 0.050
Final bands:
[44.931886909654175, 31.59867911548259, 42.505035548973396, 59.18888801378345, 82.7331568849095, 111.5234950325694]
Finished in 10.0 min 14.280494689941406 s
43 function calls
[44.931886909654175, 31.59867911548259, 42.505035548973396, 59.18888801378345, 82.7331568849095, 111.5234950325694]
Writing to ../stats-results/dEta_cats/lim-stat-only-noBS-dEta_hh-cat-18-SM-HH-unblind-3_dEta_hh_2SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-stat-only-noBS-dEta_hh-cat-18-SM-HH-unblind-3_dEta_hh_2SR_rev_dEta.json
cat_4b_0_Xhh-0.0-0.95
['cat', '4b', '0', 'Xhh-0.0-0.95']
Running stat error
cat_4b_0_Xhh-0.95-1.6
['cat', '4b', '0', 'Xhh-0.95-1.6']
Running stat error
cat_4b_1_Xhh-0.0-0.95
['cat', '4b', '1', 'Xhh-0.0-0.95']
Running stat error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error
'1', 'Xhh-0.0-0.95']
Running stat error
cat_4b_1_Xhh-0.95-1.6
['cat', '4b', '1', 'Xhh-0.95-1.6']
Running stat error
cat_4b_2_Xhh-0.0-0.95
['cat', '4b', '2', 'Xhh-0.0-0.95']
Running stat error
cat_4b_2_Xhh-0.95-1.6
['cat', '4b', '2', 'Xhh-0.95-1.6']
Running stat error

------------------------------------------------------------
Sender: LSF System <lsf@bubble0006>
Subject: Job 707886: <rev_dEta_hh_3_bins_2SR_1NPCatstat-only_no-bootstrap_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_1NPCatstat-only_no-bootstrap_unblind> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Fri Apr  9 11:57:12 2021
Job was executed on host(s) <bubble0006>, in queue <long>, as user <nhartman> in cluster <slac> at Fri Apr  9 12:07:53 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Fri Apr  9 12:07:53 2021
Terminated at Fri Apr  9 12:19:28 2021
Results reported at Fri Apr  9 12:19:28 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --bins 34 --range 350 1200 --systvar None --stat-only --no-bootstrap --unblind --no-norm --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   692.50 sec.
    Max Memory :                                 262 MB
    Average Memory :                             261.65 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   697 sec.
    Turnaround time :                            1336 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@bubble0007>
Subject: Job 707947: <rev_dEta_hh_3_bins_2SR_1NPCatstat-only_no-bootstrap_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_2SR_1NPCatstat-only_no-bootstrap_unblind> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Fri Apr  9 11:57:19 2021
Job was executed on host(s) <bubble0007>, in queue <long>, as user <nhartman> in cluster <slac> at Fri Apr  9 12:14:10 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Fri Apr  9 12:14:10 2021
Terminated at Fri Apr  9 12:24:31 2021
Results reported at Fri Apr  9 12:24:31 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --bins 34 --range 350 1200 --systvar None --stat-only --no-bootstrap --unblind --no-norm --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   617.43 sec.
    Max Memory :                                 502 MB
    Average Memory :                             495.86 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   622 sec.
    Turnaround time :                            1632 sec.

The output (if any) is above this job summary.

