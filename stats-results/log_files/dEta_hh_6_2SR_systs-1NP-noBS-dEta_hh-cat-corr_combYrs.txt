User defined signal 2

------------------------------------------------------------
Sender: LSF System <lsf@kiso0029>
Subject: Job 810166: <dEta_hh_6_2SR_systs-1NP-noBS-dEta_hh-cat-corr_combYrs> in cluster <slac> Exited

Job <dEta_hh_6_2SR_systs-1NP-noBS-dEta_hh-cat-corr_combYrs> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 15:14:28 2021
Job was executed on host(s) <kiso0029>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 15:14:30 2021
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b/stats-results> was used as the working directory.
Started at Tue Apr 20 15:14:30 2021
Terminated at Fri Apr 23 09:54:43 2021
Results reported at Fri Apr 23 09:54:43 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python combineYears.py --ws_in dEta_cats/ws-systs-1NP-noBS-dEta_hh-cat-corr-XX-SM-HH-unblind-6_dEta_hh_2SR_rev_dEta.json
------------------------------------------------------------

TERM_RUNLIMIT: job killed after reaching LSF run time limit.
Exited with exit code 140.

Resource usage summary:

    CPU time :                                   238868.77 sec.
    Max Memory :                                 719 MB
    Average Memory :                             526.59 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              5
    Max Threads :                                10
    Run time :                                   240012 sec.
    Turnaround time :                            240015 sec.

The output (if any) is above this job summary.

