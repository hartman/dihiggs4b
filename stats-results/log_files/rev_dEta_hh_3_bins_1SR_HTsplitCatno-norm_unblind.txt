Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 39 bins from 225 to 1200 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst, HT cut 300.0
Running with bootstrap
bins [ 225.  250.  275.  300.  325.  350.  375.  400.  425.  450.  475.  500.
  525.  550.  575.  600.  625.  650.  675.  700.  725.  750.  775.  800.
  825.  850.  875.  900.  925.  950.  975. 1000. 1025. 1050. 1075. 1100.
 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.5, 3.6, 8.0] Xhh [] in btag regs ['4b']
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error
qmu(mu = 1.0) = 7.420157999149524e-08
Unexpectedly large med guess (7195.18), verifying by rescaling signal
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error
qmu_test 0.09152653802675559
Initial guess was wrong, subbing in new guess 129.57003285748152
Initial guess for bands (mu guess = 1.00):
[ 69.53037099  93.34465414 129.57003286 180.28975566 241.69118257]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.1562)
Param: 2, mu: 69.530, CLs: 0.156
obs tensor(1.2606e-05)
Param: 2, mu: 241.691, CLs: 0.000
obs tensor(0.0005)
Param: 2, mu: 186.583, CLs: 0.001
obs tensor(0.0134)
Param: 2, mu: 128.057, CLs: 0.013
obs tensor(0.0503)
Param: 2, mu: 98.794, CLs: 0.050
obs tensor(0.0498)
Param: 2, mu: 99.017, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 98.928, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 98.927, CLs: 0.050
Param: 2, mu: 69.530, CLs: 0.290
Param: 2, mu: 241.691, CLs: 0.000
Param: 2, mu: 212.118, CLs: 0.001
Param: 2, mu: 140.824, CLs: 0.032
Param: 2, mu: 105.177, CLs: 0.109
Param: 2, mu: 132.471, CLs: 0.044
Param: 2, mu: 128.312, CLs: 0.051
Param: 2, mu: 128.667, CLs: 0.050
Param: 2, mu: 128.647, CLs: 0.050
Param: 2, mu: 128.647, CLs: 0.050
Guess for bands with proper med:
[ 69.03520882  92.67989799 128.64729682 179.00581793 239.96997314]
Refining bands
Param: 0, mu: 65.583, CLs: 0.060
Param: 0, mu: 72.487, CLs: 0.042
Param: 0, mu: 69.386, CLs: 0.049
Param: 0, mu: 69.109, CLs: 0.050
Param: 0, mu: 69.111, CLs: 0.050
Param: 0, mu: 69.111, CLs: 0.050
Param: 1, mu: 88.046, CLs: 0.061
Param: 1, mu: 97.314, CLs: 0.041
Param: 1, mu: 93.132, CLs: 0.049
Param: 1, mu: 92.735, CLs: 0.050
Param: 1, mu: 92.738, CLs: 0.050
Param: 1, mu: 92.739, CLs: 0.050
Param: 1, mu: 92.739, CLs: 0.050
Param: 3, mu: 170.056, CLs: 0.066
Param: 3, mu: 187.956, CLs: 0.037
Param: 3, mu: 179.909, CLs: 0.048
Param: 3, mu: 178.834, CLs: 0.050
Param: 3, mu: 178.857, CLs: 0.050
Param: 3, mu: 178.857, CLs: 0.050
Param: 4, mu: 227.971, CLs: 0.071
Param: 4, mu: 251.968, CLs: 0.033
Param: 4, mu: 241.361, CLs: 0.047
Param: 4, mu: 239.480, CLs: 0.050
Param: 4, mu: 239.549, CLs: 0.050
Param: 4, mu: 239.547, CLs: 0.050
Param: 4, mu: 239.548, CLs: 0.050
Final bands:
[98.92719240259551, 69.11125613701012, 92.73877198507456, 128.64729682483366, 178.8569453385734, 239.54714930308867]
Finished in 10.0 min 43.91322636604309 s
44 function calls
[98.92719240259551, 69.11125613701012, 92.73877198507456, 128.64729682483366, 178.8569453385734, 239.54714930308867]
Writing to ../stats-results/dEta_cats/lim-systs-HTcut-300.0-dEta_hh-cat-18-SM-HH-unblind-3_dEta_hh_1SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-HTcut-300.0-dEta_hh-cat-18-SM-HH-unblind-3_dEta_hh_1SR_rev_dEta.json
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error
------------------------------------------------------
Sender: LSF System <lsf@deft0028>
Subject: Job 855449: <rev_dEta_hh_3_bins_1SR_HTsplitCatno-norm_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_1SR_HTsplitCatno-norm_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:10 2021
Job was executed on host(s) <deft0028>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:12 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:12 2021
Terminated at Tue Apr 20 18:06:01 2021
Results reported at Tue Apr 20 18:06:01 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --no-norm --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   397.95 sec.
    Max Memory :                                 261 MB
    Average Memory :                             248.50 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   409 sec.
    Turnaround time :                            411 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0065>
Subject: Job 855405: <rev_dEta_hh_3_bins_1SR_HTsplitCatno-norm_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_1SR_HTsplitCatno-norm_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:07 2021
Job was executed on host(s) <kiso0065>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:09 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:09 2021
Terminated at Tue Apr 20 18:08:21 2021
Results reported at Tue Apr 20 18:08:21 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --no-norm --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   539.30 sec.
    Max Memory :                                 284 MB
    Average Memory :                             271.74 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   552 sec.
    Turnaround time :                            554 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0010>
Subject: Job 855493: <rev_dEta_hh_3_bins_1SR_HTsplitCatno-norm_unblind> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_1SR_HTsplitCatno-norm_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:12 2021
Job was executed on host(s) <kiso0010>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:02:03 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:02:03 2021
Terminated at Tue Apr 20 18:13:06 2021
Results reported at Tue Apr 20 18:13:06 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --no-norm --unblind --outDir ../stats-results/dEta_cats --label 3_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   646.74 sec.
    Max Memory :                                 502 MB
    Average Memory :                             484.67 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   663 sec.
    Turnaround time :                            834 sec.

The output (if any) is above this job summary.

