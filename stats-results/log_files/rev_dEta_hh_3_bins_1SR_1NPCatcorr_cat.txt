Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst 1 shape NP
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 2.5, 3.6, 8.0] Xhh [] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
qmu(mu = 1.0) = 1.4203046703187283e-06
Unexpectedly large med guess (1644.59), verifying by rescaling signal
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
qmu_test 0.2243796242589724
Initial guess was wrong, subbing in new guess 82.75350142156537
Initial guess for bands (mu guess = 1.00):
[ 44.40750324  59.61715683  82.75350142 115.14706157 154.36278883]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.2939)
Param: 2, mu: 44.408, CLs: 0.294
obs tensor(0.0003)
Param: 2, mu: 154.363, CLs: 0.000
obs tensor(0.0014)
Param: 2, mu: 135.748, CLs: 0.001
obs tensor(0.0338)
Param: 2, mu: 90.078, CLs: 0.034
obs tensor(0.0959)
Param: 2, mu: 70.562, CLs: 0.096
obs tensor(0.0452)
Param: 2, mu: 84.993, CLs: 0.045
obs tensor(0.0503)
Param: 2, mu: 83.026, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 83.150, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 83.145, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 83.145, CLs: 0.050
Param: 2, mu: 44.408, CLs: 0.294
Param: 2, mu: 154.363, CLs: 0.000
Param: 2, mu: 135.748, CLs: 0.001
Param: 2, mu: 90.078, CLs: 0.034
Param: 2, mu: 70.562, CLs: 0.096
Param: 2, mu: 84.993, CLs: 0.045
Param: 2, mu: 83.026, CLs: 0.050
Param: 2, mu: 83.150, CLs: 0.050
Param: 2, mu: 83.145, CLs: 0.050
Param: 2, mu: 83.145, CLs: 0.050
Guess for bands with proper med:
[ 44.61762836  59.89925019  83.14507014 115.69190846 155.09319466]
Refining bands
Param: 0, mu: 42.387, CLs: 0.059
Param: 0, mu: 46.849, CLs: 0.042
Param: 0, mu: 44.677, CLs: 0.049
Param: 0, mu: 44.497, CLs: 0.050
Param: 0, mu: 44.499, CLs: 0.050
Param: 0, mu: 44.498, CLs: 0.050
Param: 1, mu: 56.904, CLs: 0.060
Param: 1, mu: 62.894, CLs: 0.041
Param: 1, mu: 60.055, CLs: 0.049
Param: 1, mu: 59.797, CLs: 0.050
Param: 1, mu: 59.800, CLs: 0.050
Param: 1, mu: 59.800, CLs: 0.050
Param: 3, mu: 109.907, CLs: 0.067
Param: 3, mu: 121.477, CLs: 0.038
Param: 3, mu: 116.636, CLs: 0.048
Param: 3, mu: 115.955, CLs: 0.050
Param: 3, mu: 115.970, CLs: 0.050
Param: 3, mu: 115.969, CLs: 0.050
Param: 4, mu: 147.339, CLs: 0.074
Param: 4, mu: 162.848, CLs: 0.035
Param: 4, mu: 157.038, CLs: 0.047
Param: 4, mu: 155.886, CLs: 0.050
Param: 4, mu: 155.925, CLs: 0.050
Param: 4, mu: 155.924, CLs: 0.050
Param: 4, mu: 155.924, CLs: 0.050
Final bands:
[83.14507014311889, 44.49852916113279, 59.80021101405265, 83.14507014311889, 115.96973150140788, 155.92420135488393]
Finished in 1.0 min 19.080451726913452 s
45 function calls
[83.14507014311889, 44.49852916113279, 59.80021101405265, 83.14507014311889, 115.96973150140788, 155.92420135488393]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-dEta_hh-cat-corr-18-SM-HH-3_dEta_hh_1SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-dEta_hh-cat-corr-18-SM-HH-3_dEta_hh_1SR_rev_dEta.json
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split

------------------------------------------------------------
Sender: LSF System <lsf@bubble0004>
Subject: Job 419272: <rev_dEta_hh_3_bins_1SR_1NPCatcorr_cat> in cluster <slac> Done

Job <rev_dEta_hh_3_bins_1SR_1NPCatcorr_cat> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Thu Apr  8 16:34:58 2021
Job was executed on host(s) <bubble0004>, in queue <long>, as user <nhartman> in cluster <slac> at Thu Apr  8 16:40:43 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Thu Apr  8 16:40:43 2021
Terminated at Thu Apr  8 16:42:10 2021
Results reported at Thu Apr  8 16:42:10 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 2.5 3.6 8 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 3_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   85.21 sec.
    Max Memory :                                 502 MB
    Average Memory :                             376.75 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   90 sec.
    Turnaround time :                            432 sec.

The output (if any) is above this job summary.

