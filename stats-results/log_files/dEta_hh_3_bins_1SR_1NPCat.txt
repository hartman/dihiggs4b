Using uproot4
No direct category match for 16 assuming 4b
{'16': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/data16_min_dR_VEC_sr_124_117_NN_100_bootstraps.root']}}
../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16a/NanoNTuple.root
['../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16a/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/data16_min_dR_VEC_sr_124_117_NN_100_bootstraps.root
data16_min_dR_VEC_sr_124_117_NN_100_bootstraps.root
Loading in sig
(124.0,117.0) center already applied: skipping validation tree
Closing data16_min_dR_VEC_sr_124_117_NN_100_bootstraps.root
[124.0, 117.0]
400452
Xwt_str X_wt_tag
yr 16 NN_norm_bstrap_med_16
3023.984801438291
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16a/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
35075
SR is different! If what you want for this sample, ignore this warning
Not able to flatten, maybe double check signal counts
4b
S = 0.259862482547760
Running with syst 1 shape NP
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [0.0, 0.5, 1.0, 1.5] Xhh [] in btag regs ['4b']
Running w/o any variable split
Running w/o any variable split
Running w/o any variable split
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split
qmu(mu = 1.0) = 0.022107890739789582
Initial guess for bands (mu guess = 1.00):
[ 7.07366379  9.49640697 13.18179145 18.34175624 24.5884229 ]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.3004)
Param: 2, mu: 7.074, CLs: 0.300
obs tensor(0.0005)
Param: 2, mu: 24.588, CLs: 0.000
obs tensor(0.0020)
Param: 2, mu: 21.697, CLs: 0.002
obs tensor(0.0379)
Param: 2, mu: 14.385, CLs: 0.038
obs tensor(0.0756)
Param: 2, mu: 12.266, CLs: 0.076
obs tensor(0.0477)
Param: 2, mu: 13.705, CLs: 0.048
obs tensor(0.0501)
Param: 2, mu: 13.558, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 13.562, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 13.562, CLs: 0.050
Param: 2, mu: 7.074, CLs: 0.300
Param: 2, mu: 24.588, CLs: 0.000
Param: 2, mu: 21.697, CLs: 0.002
Param: 2, mu: 14.385, CLs: 0.038
Param: 2, mu: 12.266, CLs: 0.076
Param: 2, mu: 13.705, CLs: 0.048
Param: 2, mu: 13.558, CLs: 0.050
Param: 2, mu: 13.562, CLs: 0.050
Param: 2, mu: 13.562, CLs: 0.050
Guess for bands with proper med:
[ 7.27776024  9.77040685 13.56212574 18.87097102 25.29787278]
Refining bands
Param: 0, mu: 6.914, CLs: 0.057
Param: 0, mu: 7.642, CLs: 0.040
Param: 0, mu: 7.214, CLs: 0.049
Param: 0, mu: 7.188, CLs: 0.050
Param: 0, mu: 7.186, CLs: 0.050
Param: 0, mu: 7.187, CLs: 0.050
Param: 1, mu: 9.282, CLs: 0.059
Param: 1, mu: 10.259, CLs: 0.040
Param: 1, mu: 9.739, CLs: 0.049
Param: 1, mu: 9.701, CLs: 0.050
Param: 1, mu: 9.698, CLs: 0.050
Param: 1, mu: 9.698, CLs: 0.050
Param: 3, mu: 17.927, CLs: 0.069
Param: 3, mu: 19.815, CLs: 0.040
Param: 3, mu: 19.158, CLs: 0.049
Param: 3, mu: 19.056, CLs: 0.050
Param: 3, mu: 19.058, CLs: 0.050
Param: 3, mu: 19.057, CLs: 0.050
Param: 4, mu: 24.033, CLs: 0.081
Param: 4, mu: 26.563, CLs: 0.041
Param: 4, mu: 25.978, CLs: 0.048
Param: 4, mu: 25.838, CLs: 0.050
Param: 4, mu: 25.841, CLs: 0.050
Param: 4, mu: 25.841, CLs: 0.050
Final bands:
[13.562125736295632, 7.186185790868312, 9.69785294493608, 13.562125736295632, 19.057711675080704, 25.841053001591433]
Finished in 3.0 min 53.86047148704529 s
42 function calls
[13.562125736295632, 7.186185790868312, 9.69785294493608, 13.562125736295632, 19.057711675080704, 25.841053001591433]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-dEta_hh-cat-corr-16-SM-HH-3_dEta_hh_1SR.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-dEta_hh-cat-corr-16-SM-HH-3_dEta_hh_1SR.json
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
cat_4b_1
['cat', '4b', '1']
Running stat error
Running syst error w/o an NP split
cat_4b_2
['cat', '4b', '2']
Running stat error
Running syst error w/o an NP split


------------------------------------------------------------
Sender: LSF System <lsf@deft0027>
Subject: Job 63943: <dEta_hh_3_bins_1SR_1NPCat> in cluster <slac> Done

Job <dEta_hh_3_bins_1SR_1NPCat> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Wed Apr  7 09:41:25 2021
Job was executed on host(s) <deft0027>, in queue <long>, as user <nhartman> in cluster <slac> at Wed Apr  7 09:41:27 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Wed Apr  7 09:41:27 2021
Terminated at Wed Apr  7 09:44:11 2021
Results reported at Wed Apr  7 09:44:11 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/data17_min_dR_VEC_sr_124_117_NN_100_bootstraps.root -s ../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 0.0 0.5 1.0 1.5 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 3_dEta_hh_1SR
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   152.02 sec.
    Max Memory :                                 201 MB
    Average Memory :                             178.86 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   164 sec.
    Turnaround time :                            166 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@deft0025>
Subject: Job 64087: <dEta_hh_3_bins_1SR_1NPCat> in cluster <slac> Done

Job <dEta_hh_3_bins_1SR_1NPCat> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Wed Apr  7 09:41:59 2021
Job was executed on host(s) <deft0025>, in queue <long>, as user <nhartman> in cluster <slac> at Wed Apr  7 09:42:01 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Wed Apr  7 09:42:01 2021
Terminated at Wed Apr  7 09:44:36 2021
Results reported at Wed Apr  7 09:44:36 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/data18_min_dR_VEC_sr_124_117_NN_100_bootstraps.root -s ../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 0.0 0.5 1.0 1.5 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 3_dEta_hh_1SR
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   144.94 sec.
    Max Memory :                                 336 MB
    Average Memory :                             300.29 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   154 sec.
    Turnaround time :                            157 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0018>
Subject: Job 63929: <dEta_hh_3_bins_1SR_1NPCat> in cluster <slac> Done

Job <dEta_hh_3_bins_1SR_1NPCat> was submitted from host <cent7b> by user <nhartman> in cluster <slac> at Wed Apr  7 09:41:23 2021
Job was executed on host(s) <kiso0018>, in queue <long>, as user <nhartman> in cluster <slac> at Wed Apr  7 09:41:24 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Wed Apr  7 09:41:24 2021
Terminated at Wed Apr  7 09:45:40 2021
Results reported at Wed Apr  7 09:45:40 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/data16_min_dR_VEC_sr_124_117_NN_100_bootstraps.root -s ../data/RR/NNT_DEC20_MDR_VEC/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 0.0 0.5 1.0 1.5 --bins 34 --range 350 1200 --systvar None --corr_cat --no-norm --outDir ../stats-results/dEta_cats --label 3_dEta_hh_1SR
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   239.10 sec.
    Max Memory :                                 212 MB
    Average Memory :                             192.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   267 sec.
    Turnaround time :                            257 sec.

The output (if any) is above this job summary.

