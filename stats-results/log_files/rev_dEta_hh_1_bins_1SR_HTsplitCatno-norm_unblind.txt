Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 39 bins from 225 to 1200 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst, HT cut 300.0
Running with bootstrap
bins [ 225.  250.  275.  300.  325.  350.  375.  400.  425.  450.  475.  500.
  525.  550.  575.  600.  625.  650.  675.  700.  725.  750.  775.  800.
  825.  850.  875.  900.  925.  950.  975. 1000. 1025. 1050. 1075. 1100.
 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 10.0] Xhh [] in btag regs ['4b']
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error
qmu(mu = 1.0) = 2.694321210583439e-08
Unexpectedly large med guess (11940.52), verifying by rescaling signal
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error
qmu_test 0.06551442610509639
Initial guess was wrong, subbing in new guess 153.147395778406
Initial guess for bands (mu guess = 1.00):
[ 82.18254645 110.33022355 153.14739578 213.0963924  285.67080194]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.0911)
Param: 2, mu: 82.183, CLs: 0.091
obs tensor(2.3146e-06)
Param: 2, mu: 285.671, CLs: 0.000
obs tensor(0.0018)
Param: 2, mu: 173.944, CLs: 0.002
obs tensor(0.0177)
Param: 2, mu: 124.397, CLs: 0.018
obs tensor(0.0377)
Param: 2, mu: 105.826, CLs: 0.038
obs tensor(0.0529)
Param: 2, mu: 97.047, CLs: 0.053
obs tensor(0.0496)
Param: 2, mu: 98.713, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 98.528, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 98.523, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 98.523, CLs: 0.050
Param: 2, mu: 82.183, CLs: 0.286
Param: 2, mu: 285.671, CLs: 0.000
Param: 2, mu: 250.259, CLs: 0.001
Param: 2, mu: 166.221, CLs: 0.031
Param: 2, mu: 124.202, CLs: 0.107
Param: 2, mu: 155.489, CLs: 0.043
Param: 2, mu: 150.281, CLs: 0.051
Param: 2, mu: 150.757, CLs: 0.050
Param: 2, mu: 150.729, CLs: 0.050
Param: 2, mu: 150.728, CLs: 0.050
Guess for bands with proper med:
[ 80.88468953 108.58784818 150.72883592 209.73109599 281.15938383]
Refining bands
Param: 0, mu: 76.840, CLs: 0.060
Param: 0, mu: 84.929, CLs: 0.042
Param: 0, mu: 81.409, CLs: 0.049
Param: 0, mu: 81.084, CLs: 0.050
Param: 0, mu: 81.087, CLs: 0.050
Param: 0, mu: 81.087, CLs: 0.050
Param: 1, mu: 103.158, CLs: 0.061
Param: 1, mu: 114.017, CLs: 0.041
Param: 1, mu: 109.209, CLs: 0.049
Param: 1, mu: 108.745, CLs: 0.050
Param: 1, mu: 108.750, CLs: 0.050
Param: 1, mu: 108.749, CLs: 0.050
Param: 3, mu: 199.245, CLs: 0.066
Param: 3, mu: 220.218, CLs: 0.037
Param: 3, mu: 210.546, CLs: 0.048
Param: 3, mu: 209.279, CLs: 0.050
Param: 3, mu: 209.307, CLs: 0.050
Param: 3, mu: 209.306, CLs: 0.050
Param: 4, mu: 267.101, CLs: 0.070
Param: 4, mu: 295.217, CLs: 0.032
Param: 4, mu: 282.067, CLs: 0.047
Param: 4, mu: 279.841, CLs: 0.050
Param: 4, mu: 279.925, CLs: 0.050
Param: 4, mu: 279.923, CLs: 0.050
Param: 4, mu: 279.922, CLs: 0.050
Final bands:
[98.52313110782818, 81.08737148984709, 108.74973421221718, 150.72883591704928, 209.30601817801255, 279.9227098584557]
Finished in 1.0 min 36.464699268341064 s
45 function calls
[98.52313110782818, 81.08737148984709, 108.74973421221718, 150.72883591704928, 209.30601817801255, 279.9227098584557]
Writing to ../stats-results/dEta_cats/lim-systs-HTcut-300.0-dEta_hh-cat-18-SM-HH-unblind-1_dEta_hh_1SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-HTcut-300.0-dEta_hh-cat-18-SM-HH-unblind-1_dEta_hh_1SR_rev_dEta.json
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-HTcut-300.0-dEta_hh-cat-16-SM-HH-unblind-1_dEta_hh_1SR_rev_dEta.json
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error
> at Tue Apr 20 17:59:08 2021
Job was executed on host(s) <kiso0002>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:11 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:11 2021
Terminated at Tue Apr 20 18:01:49 2021
Results reported at Tue Apr 20 18:01:49 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 10 --no-norm --unblind --outDir ../stats-results/dEta_cats --label 1_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   147.30 sec.
    Max Memory :                                 271 MB
    Average Memory :                             239.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   159 sec.
    Turnaround time :                            161 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0015>
Subject: Job 855382: <rev_dEta_hh_1_bins_1SR_HTsplitCatno-norm_unblind> in cluster <slac> Done

Job <rev_dEta_hh_1_bins_1SR_HTsplitCatno-norm_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:06 2021
Job was executed on host(s) <kiso0015>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:08 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:08 2021
Terminated at Tue Apr 20 18:01:52 2021
Results reported at Tue Apr 20 18:01:52 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 10 --no-norm --unblind --outDir ../stats-results/dEta_cats --label 1_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   149.41 sec.
    Max Memory :                                 251 MB
    Average Memory :                             225.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   164 sec.
    Turnaround time :                            166 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@deft0014>
Subject: Job 855471: <rev_dEta_hh_1_bins_1SR_HTsplitCatno-norm_unblind> in cluster <slac> Done

Job <rev_dEta_hh_1_bins_1SR_HTsplitCatno-norm_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:11 2021
Job was executed on host(s) <deft0014>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:00:37 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:00:37 2021
Terminated at Tue Apr 20 18:02:26 2021
Results reported at Tue Apr 20 18:02:26 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 10 --no-norm --unblind --outDir ../stats-results/dEta_cats --label 1_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   104.45 sec.
    Max Memory :                                 530 MB
    Average Memory :                             455.33 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   109 sec.
    Turnaround time :                            195 sec.

The output (if any) is above this job summary.

