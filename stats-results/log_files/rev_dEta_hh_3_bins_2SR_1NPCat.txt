Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': []}}
/
[]
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
[124.0, 117.0]
Running with SR center [124.0, 117.0]: might need to check the validation tree
[124.0, 117.0]
Traceback (most recent call last):
  File "run_limits.py", line 147, in main
    sig_SR_in[yr] = load(fnames_sig[yr], 
  File "/gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies/utils.py", line 100, in load
    arr['X_hh'] = Xhh(arr['m_h1'],arr['m_h2'], center_x=SR_center[0], center_y=SR_center[1], res=0.1)
UnboundLocalError: local variable 'arr' referenced before assignment

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "run_limits.py", line 415, in <module>
    main(fnames_dat, fnames_sig, yrs, doBootstrap, doSyst, doTheoryplusLumi, args.in_format, args.label, bins, 
  File "run_limits.py", line 151, in main
    sig_SR_in[yr] = load(fnames_sig[yr], 
  File "/gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies/utils.py", line 100, in load
    arr['X_hh'] = Xhh(arr['m_h1'],arr['m_h2'], center_x=SR_center[0], center_y=SR_center[1], res=0.1)
UnboundLocalError: local variable 'arr' referenced before assignment

------------------------------------------------------------
Sender: LSF System <lsf@deft0017>
Subject: Job 348128: <rev_dEta_hh_3_bins_2SR_1NPCat> in cluster <slac> Exited

Job <rev_dEta_hh_3_bins_2SR_1NPCat> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Thu Apr  8 09:14:48 2021
Job was executed on host(s) <deft0017>, in queue <long>, as user <nhartman> in cluster <slac> at Thu Apr  8 09:14:50 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Thu Apr  8 09:14:50 2021
Terminated at Thu Apr  8 09:14:52 2021
Results reported at Thu Apr  8 09:14:52 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d / -s / -y 18 --format resolved-recon --SR-center 124 117 --Xhh-cut 0 0.95 1.6 --categorize dEta_hh --cat-edges 0.0 0.5 1.0 1.5 --bins 34 --range 350 1200 --systvar None --no-norm --outDir ../stats-results/dEta_cats --label 3_dEta_hh_2SR
------------------------------------------------------------

Exited with exit code 1.

Resource usage summary:

    CPU time :                                   1.71 sec.
    Max Memory :                                 42 MB
    Average Memory :                             42.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                4
    Run time :                                   2 sec.
    Turnaround time :                            4 sec.

The output (if any) is above this job summary.

