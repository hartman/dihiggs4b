Using uproot4
No direct category match for 18 assuming 4b
{'18': {'3b': [], '3b1l': [], '3b1nl': [], '4b': ['../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root']}}
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
['../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root']
Running with 34 bins from 350.0 to 1200.0 for variable m_hh
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root
data18_NN_100_bootstraps.root
Loading in sig
Loading in validation
Closing data18_NN_100_bootstraps.root
[124.0, 117.0]
947177
SR is different! If what you want for this sample, ignore this warning
Xwt_str X_wt_tag
yr 18 NN_norm_bstrap_med_18
7965.602190566633
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Running with SR center [124.0, 117.0]: might need to check the validation tree
../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root
NanoNTuple.root
Loading in sig
Loading in validation
Closing NanoNTuple.root
[124.0, 117.0]
10327
Not able to flatten, maybe double check signal counts
4b
S = 0.041213996708393
Running with syst 1 shape NP
Running with bootstrap
bins [ 350.  375.  400.  425.  450.  475.  500.  525.  550.  575.  600.  625.
  650.  675.  700.  725.  750.  775.  800.  825.  850.  875.  900.  925.
  950.  975. 1000. 1025. 1050. 1075. 1100. 1125. 1150. 1175. 1200.]
Categorizing in dEta_hh with boundaries [1.5, 10.0] Xhh [] in btag regs ['4b']
Running w/o any variable split
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
Running norm error
qmu(mu = 1.0) = 2.5947542781068478e-08
Unexpectedly large med guess (12167.46), verifying by rescaling signal
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
Running norm error
qmu_test 0.15999561145360985
Initial guess was wrong, subbing in new guess 97.99954322228632
Initial guess for bands (mu guess = 1.00):
[ 52.58889302  70.60068803  97.99954322 136.36111154 182.80172483]
Beginning optimization, mu valid to 0.00
Finding med_mu for alpha = 0.05
obs tensor(0.2144)
Param: 2, mu: 52.589, CLs: 0.214
obs tensor(8.1775e-05)
Param: 2, mu: 182.802, CLs: 0.000
obs tensor(0.0009)
Param: 2, mu: 152.467, CLs: 0.001
obs tensor(0.0213)
Param: 2, mu: 102.528, CLs: 0.021
obs tensor(0.0753)
Param: 2, mu: 77.559, CLs: 0.075
obs tensor(0.0428)
Param: 2, mu: 89.273, CLs: 0.043
obs tensor(0.0487)
Param: 2, mu: 86.690, CLs: 0.049
obs tensor(0.0500)
Param: 2, mu: 86.151, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 86.160, CLs: 0.050
obs tensor(0.0500)
Param: 2, mu: 86.160, CLs: 0.050
Param: 2, mu: 52.589, CLs: 0.295
Param: 2, mu: 182.802, CLs: 0.000
Param: 2, mu: 160.874, CLs: 0.001
Param: 2, mu: 106.731, CLs: 0.034
Param: 2, mu: 83.912, CLs: 0.095
Param: 2, mu: 100.764, CLs: 0.045
Param: 2, mu: 98.495, CLs: 0.050
Param: 2, mu: 98.635, CLs: 0.050
Param: 2, mu: 98.630, CLs: 0.050
Param: 2, mu: 98.629, CLs: 0.050
Guess for bands with proper med:
[ 52.92702075  71.05462514  98.62964515 137.23786459 183.97707438]
Refining bands
Param: 0, mu: 50.281, CLs: 0.059
Param: 0, mu: 55.573, CLs: 0.042
Param: 0, mu: 53.082, CLs: 0.049
Param: 0, mu: 52.868, CLs: 0.050
Param: 0, mu: 52.870, CLs: 0.050
Param: 0, mu: 52.869, CLs: 0.050
Param: 1, mu: 67.502, CLs: 0.060
Param: 1, mu: 74.607, CLs: 0.041
Param: 1, mu: 71.308, CLs: 0.049
Param: 1, mu: 71.003, CLs: 0.050
Param: 1, mu: 71.006, CLs: 0.050
Param: 1, mu: 71.006, CLs: 0.050
Param: 3, mu: 130.376, CLs: 0.067
Param: 3, mu: 144.100, CLs: 0.037
Param: 3, mu: 138.175, CLs: 0.048
Param: 3, mu: 137.360, CLs: 0.050
Param: 3, mu: 137.377, CLs: 0.050
Param: 3, mu: 137.377, CLs: 0.050
Param: 4, mu: 174.778, CLs: 0.073
Param: 4, mu: 193.176, CLs: 0.034
Param: 4, mu: 185.754, CLs: 0.047
Param: 4, mu: 184.349, CLs: 0.050
Param: 4, mu: 184.399, CLs: 0.050
Param: 4, mu: 184.397, CLs: 0.050
Param: 4, mu: 184.398, CLs: 0.050
Final bands:
[86.16011394258962, 52.869849448828596, 71.0061294525406, 98.62964514556361, 137.37713869499024, 184.39745701548557]
Finished in 2.0 min 50.519033432006836 s
45 function calls
[86.16011394258962, 52.869849448828596, 71.0061294525406, 98.62964514556361, 137.37713869499024, 184.39745701548557]
Writing to ../stats-results/dEta_cats/lim-systs-1NP-dEta_hh-cat-18-SM-HH-unblind-1_dEta_hh_1SR_rev_dEta.csv
outDir ../stats-results/dEta_cats
Calling save_wkspace
Saving ws to ../stats-results/dEta_cats/ws-systs-1NP-dEta_hh-cat-18-SM-HH-unblind-1_dEta_hh_1SR_rev_dEta.json
cat_4b_0
['cat', '4b', '0']
Running stat error
Running syst error w/o an NP split
Running norm error
itted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:08 2021
Job was executed on host(s) <deft0023>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:10 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:10 2021
Terminated at Tue Apr 20 18:01:20 2021
Results reported at Tue Apr 20 18:01:20 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data17_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16d/NanoNTuple.root -y 17 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 10 --bins 34 --range 350 1200 --systvar None --unblind --outDir ../stats-results/dEta_cats --label 1_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   117.66 sec.
    Max Memory :                                 271 MB
    Average Memory :                             240.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   130 sec.
    Turnaround time :                            132 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0010>
Subject: Job 855378: <rev_dEta_hh_1_bins_1SR_1NPCat_unblind> in cluster <slac> Done

Job <rev_dEta_hh_1_bins_1SR_1NPCat_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:06 2021
Job was executed on host(s) <kiso0010>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 17:59:08 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 17:59:08 2021
Terminated at Tue Apr 20 18:02:02 2021
Results reported at Tue Apr 20 18:02:02 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data16_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16a/NanoNTuple.root -y 16 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 10 --bins 34 --range 350 1200 --systvar None --unblind --outDir ../stats-results/dEta_cats --label 1_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   158.31 sec.
    Max Memory :                                 251 MB
    Average Memory :                             224.62 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   173 sec.
    Turnaround time :                            176 sec.

The output (if any) is above this job summary.


------------------------------------------------------------
Sender: LSF System <lsf@kiso0052>
Subject: Job 855467: <rev_dEta_hh_1_bins_1SR_1NPCat_unblind> in cluster <slac> Done

Job <rev_dEta_hh_1_bins_1SR_1NPCat_unblind> was submitted from host <cent7a> by user <nhartman> in cluster <slac> at Tue Apr 20 17:59:11 2021
Job was executed on host(s) <kiso0052>, in queue <long>, as user <nhartman> in cluster <slac> at Tue Apr 20 18:00:25 2021
</u/ki/nhartman> was used as the home directory.
</gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies> was used as the working directory.
Started at Tue Apr 20 18:00:25 2021
Terminated at Tue Apr 20 18:03:40 2021
Results reported at Tue Apr 20 18:03:40 2021

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python run_limits.py -d ../data/RR/nom_trigs_unblind/rev_deta/data/data18_NN_100_bootstraps.root -s ../data/RR/nom_trigs_unblind/rev_deta/MC/600043_mc16e/NanoNTuple.root -y 18 --format resolved-recon --SR-center 124 117 --categorize dEta_hh --cat-edges 1.5 10 --bins 34 --range 350 1200 --systvar None --unblind --outDir ../stats-results/dEta_cats --label 1_dEta_hh_1SR_rev_dEta
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   170.91 sec.
    Max Memory :                                 530 MB
    Average Memory :                             480.22 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                5
    Run time :                                   195 sec.
    Turnaround time :                            269 sec.

The output (if any) is above this job summary.

