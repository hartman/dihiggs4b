#!/bin/bash -l
#SBATCH --account shared
#SBATCH --partition shared
#SBATCH --ntasks 1
#SBATCH --cpus-per-task 2
#SBATCH --mem-per-cpu 1g
#SBATCH --time 48:00:00
#SBATCH --job-name Q45_4b_3b1l_res_p08_2_Xhh_2_deta_hh_uncorr
#SBATCH --output log_files/Q45_4b_3b1l_res_p08_2_Xhh_2_deta_hh_uncorr.log
#SBATCH --error log_files/Q45_4b_3b1l_res_p08_2_Xhh_2_deta_hh_uncorr.log
export SINGULARITY_IMAGE_PATH=/sdf/sw/ml/slac-ml/20200227.0/slac-jupyterlab@20200227.0.sif
cd /gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/non-resonant-studies
singularity exec --nv -B /sdf,/gpfs ${SINGULARITY_IMAGE_PATH} python run_limits.py -d ../data/RR/cryptotuples/quad_45/data16_Xhh_45_NN_100_bootstraps.root ../data/RR/cryptotuples/quad_45/data17_Xhh_45_NN_100_bootstraps.root ../data/RR/cryptotuples/quad_45/data18_Xhh_45_NN_100_bootstraps.root -s ../../hh4b/hh4b-resolved-reconstruction/SMNR_pythia_mc16a.root ../../hh4b/hh4b-resolved-reconstruction/SMNR_pythia_mc16d.root ../../hh4b/hh4b-resolved-reconstruction/SMNR_pythia_mc16e.root -y 16 17 18 --no-norm --underflow --overflow  --outDir ../stats-results/Q45/  --backend jax --systvar m_h1 m_h2 --HTcut 124 117   --btagCats 4b 3b1l --bins 280 302 327 353 381 411 444 480 518 560 604 653 705 761 822 888 959  --categorize dEta_hh --Xhh-cut 0 0.95 1.6 --cat-edges 0 0.75 1.5 --label res_p08_2_Xhh_2_deta_hh_uncorr