'''
Goal: We have some background estimates that we ~ trust which involve
1. Quadrants, rotating by 45 degrees
2. Training the years separately

We have some open Qs, so here I'm going to address the open items from my side,
all I need (rn) is to *make* the workspaces / pull since the limits can be
evaluated later :)

23 June 2021
'''

import os
import numpy as np
from subTimingStudies import writeSlurmFile
from quad45_2x2 import res_bin_edges

if __name__ == '__main__':

    SLURM_DIR = 'slurm_scripts'

    # For m_hh inclusive - j use 5% resolution
    SDIR = "../../hh4b/hh4b-resolved-reconstruction"
    BDIR  = '../data/RR/cryptotuples/ggF_rw_pre_Xwt'

    ODIR = '../stats-results/ttbar/'
    SLURM_DIR = 'slurm_scripts'

    r = (275,1050)
    res = 0.05
    edg = res_bin_edges(*r,res)
    bins = ' '.join([str(x) for x in edg])

    os.chdir('../non-resonant-studies')

    SFILE = " ".join([f'{SDIR}/SMNR_pythia_mc16{mc}.root' for mc in ['a','d','e']])
    BFILE = " ".join([f'{BDIR}/data{yr}_NN_100_bootstraps.root' for yr in [16, 17, 18]])
        
    AHFILE =" ".join([f'{BDIR}/NanoNTuple_allhad_mc16{mc}_NN_100_bootstraps.root' for mc in ['a','d','e']])
    SLFILE =" ".join([f'{BDIR}/NanoNTuple_semilep_mc16{mc}_NN_100_bootstraps.root' for mc in ['a','d','e']])


    for yr, mc in zip([16, 17, 18],['a','d','e']):
    #for yr in ['16 17 18']:

        SFILE = f'{SDIR}/SMNR_pythia_mc16{mc}.root'
        BFILE = f'{BDIR}/data{yr}_NN_100_bootstraps.root'
        
        AHFILE =f'{BDIR}/NanoNTuple_allhad_mc16{mc}_NN_100_bootstraps.root'
        SLFILE =f'{BDIR}/NanoNTuple_semilep_mc16{mc}_NN_100_bootstraps.root'

        # Note: For 3b1f, I think I will wannt to runn --bkg_scale 0.1, but I haven't
        # incorporated this functionality for ttbar (yet)
        for bcat, unblind in zip(['4b'],['',' --unblind --bkg_scale 0.1']):
        #for bcat, unblind in zip(['3b1f'],[' --unblind --bkg_scale 0.1 ']):

            base_cmd =  f'python run_limits.py -d {BFILE} -s {SFILE} -y {yr} '
            base_cmd += f' --bins {bins} --underflow --overflow --btagCats {bcat}'
            base_cmd += f' --outDir ../stats-results/ttbarStudies {unblind}'

            # inclusive - no normalization
            cmd = base_cmd + ' --no-norm --label incl'
            #os.system(cmd)
            # inclusive - norm the shape
            cmd = base_cmd + ' --normalize-syst --label norm'
            #os.system(cmd)

            # ttbar - norm the variation components
            #cmd = base_cmd + f' --ah {AHFILE} --sl {SLFILE} --normalize --label ttbarComp'
            
            cmd = base_cmd + f' --ah {AHFILE} --sl {SLFILE} --label ttbarNoNorm'
            print(cmd)
            os.system(cmd)
