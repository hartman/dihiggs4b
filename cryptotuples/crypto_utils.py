'''
crypto_utils.py

Motivation: We've been working on ggF PUSH for a few weeks now,
so as we're zoning on on what are the right message plots that
we want to look at, I'm going to start dragging + dropping some
of the relevant functions that I have here so that I can use them
across notebooks as we are *focusing and finishing*!!

June  / July 2021

'''
import pandas as pd
import numpy as np
import json
import uproot
import matplotlib.pyplot as plt
from matplotlib import gridspec

from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import AutoLocator, AutoMinorLocator, MultipleLocator,FixedLocator, LogLocator

# useful functions from other nbs
from crypto_utils import *

import os
os.sys.path += ['../non-resonant-studies','../code/', "../PyATLASstyle/"]

from lim_utils import *
from utils import *
from pull_imp_utils import *
from eventDisplays import rainbow_text

# Let's do ATLAS style plots too!
import matplotlib as mpl
import PyATLASstyle as pas
pas.applyATLASstyle(mpl)

def getTime(test_file,return_band=False):

    mins, fCalls, mu = 0, 0,0
    with open(test_file, "r") as f:

        for l in f:
            if len(l.split(',')) == 6 and 'Categorizing' not in l:
                label=[]
                #print(l)
                band = [float(val) for val in l[1:-2].split(',')]
                obs = float(l.split(',')[3])

            if 'Finished in' in l:

                m = float(l.split(' ')[2])
                s = float(l.split(' ')[4])
                mins = m + s/60

            if 'function calls' in l:
                fCalls = l.split(' ')[0]
                break

    if return_band:
        return [mins, fCalls] + band
    else:
        mu = band[3]
        return mins, fCalls, mu


def load_df(BFILES):

    di = {}

    for yr, BFILE in zip([16,17,18],BFILES):


        cols = ['ntag', 'm_hh', 'minQ', 'm_h1', 'm_h2','X_hh','dEta_hh']
        extra_cols = ['X_wt_tag', 'pass_vbf_sel','pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']

        for b_cat in ['4b','3b1f']:
            wlab = get_wlab(b_cat)
            cols = cols + [f'NN_{wlab}_weight_bstrap_med_{yr}',
                           f'NN_{wlab}_weight_VRderiv_bstrap_med_{yr}',
                           f'NN_{wlab}_weight_bstrap_IQR_{yr}',
                           f'rw_to_{b_cat}']


        with uproot.open(BFILE) as f:

            df = f['sig'].arrays(cols + extra_cols, library='pd')
            df['HT'] = np.sum(df[['pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']],axis=1)
            df = df.loc[ ~df.pass_vbf_sel & (df.X_wt_tag > 1.5), cols+['HT']]

            di[f'df_{yr}'] = df

            for b_cat in ['4b','3b1f']:
                blabel = f'_{b_cat}' if b_cat != '4b' else ''

                di[f'norm{blabel}_{yr}']     = f[f'NN_norm{blabel}_bstrap_med_{yr}'].all_members['fVal']
                di[f'norm_VR{blabel}_{yr}']  = f[f'NN_norm_VRderiv{blabel}_bstrap_med_{yr}'].all_members['fVal']
                di[f'norm_IQR{blabel}_{yr}'] = f[f'NN_norm{blabel}_bstrap_IQR_{yr}'].all_members['fVal']

                '''
                Maybe add some functionality for "all" in the df?
                '''

    return di


def get_hists(d,e,nomSR=False,catList=[]):
    '''
    Accept a dictionary, and modify it *in place* for the corresponding histograms
    for each of the years.
    '''

    for yr in [16,17,18]:

        df = d[f'df_{yr}']

        # Loop over the category options
        if len(catList) == 0:
            keys = ['']
            masks = [True]
        else:
            keys = [f":{cat['name']}" for cat in catList]
            masks = []

            for cat in catList:

                vars = cat['vars']
                ops  = cat['ops']
                vals = cat['vals']

                assert len(vars) == len(ops) and len(vars) == len(vals)

                mask = True
                for var, op, val in zip(vars, ops, vals):
                    mask = mask & op(df[var], val)

                masks.append(mask)

        for key,mask in zip(keys,masks):
            # Loop over the btag cats
            for bcat in ['4b','3b1f']:

                blabel   = f'_{bcat}' if bcat != '4b' else ''

                norm     = d[f'norm{blabel}_{yr}']
                norm_VR  = d[f'norm_VR{blabel}_{yr}']
                norm_IQR = d[f'norm_IQR{blabel}_{yr}']

                wlab = get_wlab(bcat)
                w = f'NN_{wlab}_weight_bstrap_med_{yr}'
                w_VR = f'NN_{wlab}_weight_VRderiv_bstrap_med_{yr}'
                w_IQR = f'NN_{wlab}_weight_bstrap_IQR_{yr}'

                if nomSR:
                    m_2b = df[f'rw_to_{bcat}'] & (df['X_hh'] < 1.6) & mask
                else:
                    m_2b = df[f'rw_to_{bcat}'] & mask


                n_2b = np.histogram(df.loc[m_2b,'m_hh'],e, weights=norm*df.loc[m_2b,w])[0]

                n_vr = np.histogram(df.loc[m_2b,'m_hh'],e, weights=norm_VR*df.loc[m_2b,w_VR])[0]

                bs = calc_bstrap(df[m_2b], 'm_hh', yr, norm, norm_IQR, e, bcat)
                poiss = np.sqrt(np.histogram(df.loc[m_2b,'m_hh'],e,weights=df.loc[m_2b,w]**2)[0])
                poiss *= norm

                d[f'bkg{blabel}_{yr}{key}']   = n_2b
                d[f'b_vr{blabel}_{yr}{key}']  = n_vr
                d[f'bs{blabel}_{yr}{key}']    = bs
                d[f'poiss{blabel}_{yr}{key}'] = poiss

                # Let's also investigate some of the relevant NP splits here!

                # low / high HT
                _, lowVRw,highVRw, _,_ = systs(df[m_2b], norm, norm_VR, yr, e, bcat, 'm_hh', var='HT')

                d[f'lowHT{blabel}_{yr}{key}'] = lowVRw
                d[f'highHT{blabel}_{yr}{key}'] = highVRw

                # quadrants
                _, Q1VRw,Q2VRw,Q3VRw,Q4VRw, _,_,_,_ = systs(df[m_2b], norm, norm_VR, yr, e, bcat, 'm_hh',
                                                            var=['m_h1','m_h2'], cutoff=[124,117])

                d[f'Q1{blabel}_{yr}{key}'] = Q1VRw
                d[f'Q2{blabel}_{yr}{key}'] = Q2VRw
                d[f'Q3{blabel}_{yr}{key}'] = Q3VRw
                d[f'Q4{blabel}_{yr}{key}'] = Q4VRw

                if bcat=='3b1f':
                    if nomSR:
                        obs = np.histogram(cat_sel(df, bcat).loc[(df['X_hh'] < 1.6)&mask,'m_hh'],e)[0]
                    elif type(mask) == bool:
                        obs = np.histogram(cat_sel(df, bcat)['m_hh'],e)[0]
                    else:
                        obs = np.histogram(cat_sel(df, bcat).loc[mask,'m_hh'],e)[0]
                    d[f'obs{blabel}_{yr}{key}'] =obs


def unc_by_yr(d,e,bcat='4b',shape='total',underoverflow=False,mergeGammas=False,
              title='',figDir='',label='',nomSR=False,scale="poiss"):
    '''
    Goal: plot the total uncertainty by year

    Inputs:
    - d: dictionary w/ the df / norms to create the relevant histos
    - e: edges for the histogram
    - bcat: the b-tag category to use for drawing
    - shape: NP decomposition to use, one of total (default), HT, quad
    - underoverflow:
    '''

    if underoverflow:
        e_in = e.copy().astype(float)
        e_in[0] = 0
        e_in[-1] = np.inf
        get_hists(d,e_in,nomSR)
    else:
        get_hists(d,e,nomSR)

    blabel = blabel = f'_{bcat}' if bcat != '4b' else ''

    fig, ax = plt.subplots(1,3, figsize=(18, 4))

    xx = 0.5 * ( e[1:] + e[:-1] )
    lw = 2

    for i,yr in enumerate([16,17,18]):

        # Pull out the histos that I saved
        n_2b = d[f'bkg{blabel}_{yr}']
        n_VR = d[f'b_vr{blabel}_{yr}']
        bs   = d[f'bs{blabel}_{yr}']
        w_2b = d[f'poiss{blabel}_{yr}']


        if bcat == '3b1f':
            if 'poiss' in scale:
                w_2b *= np.sqrt(10)
            if 'bs' in scale:
                bs *= np.sqrt(10)

        err = np.sqrt( (n_2b-n_VR)**2 + bs**2 + w_2b**2)

        ax[i].hist(xx[n_2b!=0], e, histtype='stepfilled', fc='lightcyan',ec='k',lw=lw,
                 label='total error', weights=(err/n_2b)[n_2b!=0])

        if shape == 'total':
            ax[i].hist(xx[n_2b!=0], e, histtype='step', color='darkorange',
                       weights=(np.abs(n_2b-n_VR)/n_2b)[n_2b!=0],
                       label='shape syst',lw=lw)
        elif shape == 'HT':
            for ki, color in zip(['lowHT','highHT'],['royalblue','darkorange']):
                n_VR = d[f'{ki}{blabel}_{yr}']
                ax[i].hist(xx[n_2b!=0], e, histtype='step', color=color,
                           weights=(np.abs(n_2b-n_VR)/n_2b)[n_2b!=0],
                           label=f'{ki} syst',lw=lw)
        elif shape == 'quad':
            for qi, color in zip(['Q1','Q2','Q3','Q4'],['forestgreen','C3','royalblue','darkorange']):
                n_VR = d[f'{qi}{blabel}_{yr}']
                ax[i].hist(xx[n_2b!=0], e, histtype='step', color=color,
                           weights=(np.abs(n_2b-n_VR)/n_2b)[n_2b!=0],
                           label=f'{qi} syst',lw=lw)
        else:
            raise NotImplementedError

        if mergeGammas:
            gamma = np.sqrt(bs**2 + w_2b**2)
            ax[i].hist(xx[n_2b!=0], e, histtype='step', color='navy',weights=gamma[n_2b!=0]/n_2b[n_2b!=0],
                       label='2b pois + BS',lw=lw)
        else:
            ax[i].hist(xx[n_2b!=0], e, histtype='step', color='navy',weights=bs[n_2b!=0]/n_2b[n_2b!=0],
                     label='bootstrap',lw=lw)
            ax[i].hist(xx[n_2b!=0], e, histtype='step', color='mediumpurple',weights=w_2b[n_2b!=0]/n_2b[n_2b!=0],
                     label='2b poisson',lw=lw)

        if bcat == '3b1f':
            obs = d[f'obs{blabel}_{yr}']
            ax[i].scatter(xx[n_2b!=0],(np.abs(obs-n_2b)/n_2b)[n_2b!=0],
                          color='deeppink',label=f'3b + 1 fail',zorder=5)

        ax[i].set_xlabel('$m_{hh}$ [GeV]',x=1,ha='right',fontsize=15)
        ax[i].set_ylabel('Relative error',y=1,ha='right',fontsize=15)


        # Add the ATLAS tag
        ax[i].text(0.05, 0.9, "$\mathbf{ATLAS}$ Internal", transform=ax[i].transAxes,
               va='center', ha='left', fontsize=14)
        ax[i].text(0.05, 0.81, '$\\sqrt{s} = 13$ TeV, 20%d %.1f fb$^{-1}$' % (yr,lumi[str(yr)]),
                   transform=ax[i].transAxes, va='center', ha='left', fontsize=12)

        ax[i].set_xlim(e[0],e[-1])
        ax[i].set_ylim(0,0.35)

    # Put the legend outside of all of the plots
    ax[i].legend(bbox_to_anchor=(1,1.03))
    if title:
        ax[-1].set_title(title,loc='right')
    else:
        ax[-1].set_title(bcat,loc='right')


    if figDir:

        figName = f'preFit_unc_{bcat}_161718_{shape}'
        if mergeGammas:
            figName+='_mergeGammas'
        if label:
            figName += label

        plt.savefig(f'{figDir}/{figName}.pdf',bbox_inches='tight')
