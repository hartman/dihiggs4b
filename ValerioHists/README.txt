TUNC_NBLS: extended Zprime Pythia 427081, nominal + tracking uncertainties except IP resolution variations
TUNC_BLS: extended Zprime Pythia 427081, nominal (IP resolution corrected to data) + IP resolution variations

JUNC: extended Zprime Pythia 427081, nominal + JES / JER variations

MUNC_FSR/Nominal: extended Zprime Pythia 800030
MUNC_FSR/MC_FSR_UP: extended Zprime Pythia 800030, isr:muRfac=1.0_fsr:muRfac=2.0
MUNC_FSR/MC_FSR_DOWN: extended Zprime Pythia 800030, isr:muRfac=1.0_fsr:muRfac=0.5

MUNC_PS/Nominal: extended Zprime MG+P8 500568
MUNC_PS/MC_FRAG: extended Zprime MG+H7 500567

Each file contains a number of bootstrap histograms for all b-jets and tagged b-jets (for a specific cumulative WP).
Histograms with suffix _bootstrap_0 are filled with the original weight. Histograms with suffix _bootstrap_[1-200] have in addition a weight drawn from Poisson(1).
Bootstraps are used to compute the stat. unc. for syst. variations of the efficiency in case both "nominal efficiency" and "alternative efficiency" come from the same sample (e.g. for tracking & jet uncs).
