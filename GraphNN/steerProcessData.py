'''
Loop over the options for processData.py
'''

import os

'''
for physicsSample in ['data16','data17','data18']: 
	for pairing in ['MDR','min_dR1']:
		for ntag in [2,3]: # ,
			pythonCmd = f'python processData.py --physicsSample {physicsSample} --prodTag FEB20'
			pythonCmd+= f' --pairing {pairing} --ntag {ntag}  --nSelectedJets 4 --period all'#'
			pythonCmd+= ' --regions --deta_cut ' #
			os.system(pythonCmd)

'''
for physicsSample in ['data16','data17','data18']: # 
	for pconfig in ['SM_2b_even','SM_2b_odd']: # 'kappa_10_even','kappa_10_odd',
		for ntag in [3]: # ,
			pythonCmd = f'python processData.py --physicsSample {physicsSample} --prodTag FEB20'
			pythonCmd+= f' --pairAGraphConfig {pconfig} --ntag {ntag} --period all' 
			pythonCmd+= ' --deta_cut ' #'--regions'  #
			os.system(pythonCmd)

