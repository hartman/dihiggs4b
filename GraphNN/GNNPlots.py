'''
Plots for my GNNs :)
'''

import matplotlib.pyplot as plt
import numpy as np

def trainingMetrics(metrics, figDir=''):
    
    x = np.arange(len(metrics['train_loss']))
    
    plt.figure()
    plt.plot(x,metrics['train_loss'],label='train')
    plt.plot(x,np.array(metrics['val_loss'])*4,label='val')

    plt.xlabel('Epochs',fontsize=14)
    plt.ylabel('Negative log likelihood',fontsize=14)
    plt.legend(fontsize=12)
    if len(figDir) > 0:
        plt.savefig(f"{figDir}/loss.pdf")

    plt.figure()
    plt.plot(x,metrics['train_acc'],label='train')
    plt.plot(x,metrics['train_acc_3b'],color='C0',label='train 3b',linestyle='--')
    plt.plot(x,metrics['train_acc_4b'],color='C0',label='train 4b',linestyle='-.')

    plt.plot(x,metrics['val_acc'],label='val')
    plt.plot(x,metrics['val_acc_3b'],color='C1',label='val 3b',linestyle='--')
    plt.plot(x,metrics['val_acc_4b'],color='C1',label='val 4b',linestyle='-.')

    plt.xlabel('Epochs',fontsize=14)
    plt.ylabel('Accuracy',fontsize=14)
    plt.legend(fontsize=12)

    if len(figDir) > 0:
        plt.savefig(f"{figDir}/acc.pdf")

    plt.show()

