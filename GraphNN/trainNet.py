'''

Goal: Use transformers for the jet selection and pairing alg for the
hh->4b analysis. This file contains the pytorch preprocessing, modules,
and training scripts for the network that I tried out for this task.

Winter 2020

'''

import numpy as np
import pandas as pd
import copy 
import json
from itertools import combinations

from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SubsetRandomSampler, SequentialSampler

import torch
import torch.nn as nn
import torch.nn.functional as F

import os
import sys
from argparse import ArgumentParser

from preprocess import getNumPairs, prepareData
from GNNPlots import trainingMetrics

def _get_clones(module, N):
    return nn.ModuleList([copy.deepcopy(module) for i in range(N)])

class pairAGraph(nn.Module):
    
    def __init__(self,inpt_dim=4,embed_dim=10,ff_dim=10,nAttnBlocks=1,nHeads=1,p=0.1,njets=4,
                 scoreFct='jetCompatibility',spatialInfo=False):
        '''
        Inputs:
        - in_dim: # of features used to repn each jet
        - embed_dim: size of the latent space representing the jets
        - nAttnBlocks: # of attn blocks to use before classifying 
                       the edges
        - nHeads:
        - p: dropout fraction
        - njets: Max # of jets to include
        - scoreFct: The model to use for defining the score 
            * jetCompatibility:
            * edgeMax:
            * minL2:
            * gaussianSimilarity:
            * tDist:
        '''
        super(pairAGraph, self).__init__()
    
        self.spatialInfo = spatialInfo
        self.toLatent = nn.Linear(inpt_dim,embed_dim-2 if spatialInfo else embed_dim,bias=True)
        
        #self.encoderLayers = _get_clones(nn.TransformerEncoderLayer(*params), nAttnBlocks)
        params = (embed_dim if spatialInfo else embed_dim, nHeads, ff_dim, p)
        self.encoderLayer1 = nn.TransformerEncoderLayer(*params)
        
        assert nAttnBlocks <= 2 # I've only added in functionality for 2 layers so far

        self.nLayers = nAttnBlocks
        if nAttnBlocks > 1:
            self.encoderLayer2 = nn.TransformerEncoderLayer(*params)
          
        self.finalAttnLayer = nn.MultiheadAttention(embed_dim+2 if spatialInfo else embed_dim, 
                                                    num_heads=2 if spatialInfo else nHeads)
        
        self.embed_dim = embed_dim + 2 if spatialInfo else embed_dim
        self.njets=njets
    
        if scoreFct == 'jetCompatibility':
            self.score = lambda h,e: \
                torch.cat([(torch.bmm(h[h11].view(-1,1,self.embed_dim), h[h12].view(-1,self.embed_dim,1)) \
                           + torch.bmm(h[h21].view(-1,1,self.embed_dim), h[h22].view(-1,self.embed_dim,1))).squeeze(-1)
                           for i0,i1,i2,i3 in combinations(range(self.njets),4) \
                           for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)]) ],
                          axis=1)
        elif scoreFct == 'edgeMax':
            self.score = lambda h,e: \
                torch.cat([(e[:,h11,h12] + e[:,h12,h21] + e[:,h21,h22] + e[:,h22,h21]).view(-1,1)  
                          for i0,i1,i2,i3 in combinations(range(self.njets),4) \
                          for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)])],axis=1)
        elif scoreFct == 'minL2':
            self.score = lambda h,e: \
                torch.cat([(torch.bmm(h[h11].view(-1,1,self.embed_dim), h[h12].view(-1,self.embed_dim,1)) \
                           + torch.bmm(h[h21].view(-1,1,self.embed_dim), h[h22].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h11].view(-1,1,self.embed_dim), h[h11].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h12].view(-1,1,self.embed_dim), h[h12].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h21].view(-1,1,self.embed_dim), h[h21].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h22].view(-1,1,self.embed_dim), h[h22].view(-1,self.embed_dim,1))
                           ).squeeze(-1)
                           for i0,i1,i2,i3 in combinations(range(self.njets),4) \
                           for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)]) ],
                          axis=1)
        elif scoreFct == 'gaussianSimilarity':
            self.score = lambda h,e: \
                torch.exp(torch.cat([(torch.bmm(h[h11].view(-1,1,self.embed_dim), h[h12].view(-1,self.embed_dim,1)) \
                           + torch.bmm(h[h21].view(-1,1,self.embed_dim), h[h22].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h11].view(-1,1,self.embed_dim), h[h11].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h12].view(-1,1,self.embed_dim), h[h12].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h21].view(-1,1,self.embed_dim), h[h21].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h22].view(-1,1,self.embed_dim), h[h22].view(-1,self.embed_dim,1))
                           ).squeeze(-1)
                           for i0,i1,i2,i3 in combinations(range(self.njets),4) \
                           for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)]) ],
                          axis=1))
        elif scoreFct == 'tDist':
            self.score = lambda h,e: \
                torch.pow(1-torch.cat([(torch.bmm(h[h11].view(-1,1,self.embed_dim), h[h12].view(-1,self.embed_dim,1)) \
                           - torch.bmm(h[h21].view(-1,1,self.embed_dim), h[h22].view(-1,self.embed_dim,1))
                           + 0.5 * torch.bmm(h[h11].view(-1,1,self.embed_dim), h[h11].view(-1,self.embed_dim,1))
                           + 0.5 * torch.bmm(h[h12].view(-1,1,self.embed_dim), h[h12].view(-1,self.embed_dim,1))
                           + 0.5 * torch.bmm(h[h21].view(-1,1,self.embed_dim), h[h21].view(-1,self.embed_dim,1))
                           + 0.5 * torch.bmm(h[h22].view(-1,1,self.embed_dim), h[h22].view(-1,self.embed_dim,1))
                           ).squeeze(-1)
                           for i0,i1,i2,i3 in combinations(range(self.njets),4) \
                           for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)]) ],
                          axis=1),-1)
        else:
            print(f'Error: {scoreFct} not a recognized input for scoreFct - setting score to 0')
            self.score = lambda h: 0 

        self.drSqd = lambda x: \
            torch.cat([(torch.pow(x[:,h11,0]-x[:,h12,0],2) + torch.pow(x[:,h21,0]-x[:,h22,0],2)+\
                    torch.pow(torch.acos(torch.cos(x[:,h11,1]-x[:,h12,1])),2)+\
                    torch.pow(torch.acos(torch.cos(x[:,h21,1]-x[:,h22,1])),2)).view(x.shape[0],-1)
                    for i0,i1,i2,i3 in combinations(range(self.njets),4) \
                    for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)])],axis=1)

    
    def forward(self, x, src_key_padding_mask=None,returnPoint=-1):
        '''
        Inputs:
        - x has shape (batchSize, nJets, nFeatures)
        '''
    
        self.njets = x.shape[1]
    
        xi = x.permute(1,0,2) 
        h = self.toLatent(xi)
        
        h = nn.ReLU()(h)
        if self.spatialInfo:
            h = torch.cat([h,xi[:,:,:2]],axis=-1)
        if returnPoint == 0:
            return h
        
        h = self.encoderLayer1(h,src_key_padding_mask=src_key_padding_mask)
        
        if self.spatialInfo:
            h = torch.cat([h,xi[:,:,:2]],axis=-1)
        if returnPoint == 1:
            return h
        
        if self.nLayers > 1:
            h = self.encoderLayer2(h,src_key_padding_mask=src_key_padding_mask)
        
        h, e = self.finalAttnLayer(h,h,h,key_padding_mask=src_key_padding_mask)

        if returnPoint == 2:
            return h

        score = self.score(h,e)
        
        if self.spatialInfo:
            score -= self.drSqd(x)
        
        return score, e

class basicAttn(nn.Module):
    
    def __init__(self, inpt_dim=5, embed_dim=20, nHeads=4, njets=4):
        '''
        Just use a (couple) layers of an attention mechansim, b/c I'm not sure if all the bells
        and whistles of the transformer models was necessary - or if this was just *complicating*
        matters.
        
        Inputs:    
        - in_dim: # of features used to repn each jet
        - embed_dim: size of the latent space representing the jets
        - nAttnBlocks: # of attn blocks to use before classifying 
                       the edges
        - nHeads:
        - p: dropout fraction
        - njets: Max # of jets to include

        '''
        super(basicAttn, self).__init__()
    
        self.toLatent = nn.Linear(inpt_dim,embed_dim,bias=True)

        self.attnLayer = nn.MultiheadAttention(embed_dim, num_heads=nHeads)
        
        self.embed_dim = embed_dim
        self.njets = njets
    
        self.score = lambda h: \
            torch.cat([(torch.bmm(h[h11].view(-1,1,embed_dim), h[h12].view(-1,embed_dim,1)) \
                       + torch.bmm(h[h21].view(-1,1,embed_dim), h[h22].view(-1,embed_dim,1))).squeeze(-1)
                       for i0,i1,i2,i3 in combinations(range(self.njets),4) \
                       for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)]) ],
                      axis=1)

    def forward(self, x):
        '''
        Inputs:
        - x has shape (batchSize, nJets, nFeatures)
        '''
        self.njets = x.shape[1]
        
        xi = x.permute(1,0,2) 
        h = self.toLatent(xi)
        
        h = nn.ReLU()(h)

        h, e = self.attnLayer(h,h,h)       
        score = self.score(h)
        
        return score, e


class hh4bDataset(Dataset): 
    
    def __init__(self, nSelectedJets=5, jetVars = ['pt','eta','phi','E','Db'],
                 physicsSample='SMNR',mcs=['mc16a','mc16d','mc16e'],prodTag='AUG2019',
                 pTcut=40,ntag=3,w_cut=0,trainingEvents='odd',split='eventNumber'):        
        '''
        Goal: Load in the jet level variables into a pytorch dataframe to be ready
        for training, and do the necessary ml pre-processing.
        '''

        super(hh4bDataset, self).__init__()

        # Load in the transformed data and access the training part of the dataset
        mc = ''.join([m[-1] for m in mcs])
        trainDir = f'{physicsSample}_mc16{mc}_PFlow-{prodTag}'
        if nSelectedJets != 4:
            trainDir += f'-{nSelectedJets}jets'
        if pTcut != 40:
            trainDir += f'-{pTcut}GeV'
        
        fDir = f'../data/{trainDir}/'
        #trainFile = fDir + "df_f_0.22_3b_scaledInputs_train.h5"
        trainFile = fDir + f'df_{ntag}b_scaledInputs_{trainingEvents}.h5'
        if not os.path.exists(trainFile): 
            prepareData(nSelectedJets,physicsSample=physicsSample,
                        mcs=mcs,prodTag=prodTag,pTcut=pTcut,
                        ntag=ntag,trainingEvents=trainingEvents,split=split)
        df = pd.read_hdf(trainFile,key='df')

        # I only want to train on evts where the correct pair exists
        mask = df.correctPair != -1
        print('mask',np.sum(mask))
        print(w_cut)
        if w_cut != 0: 
            mask = mask & (np.abs(df.mc_sf) < w_cut)
        print('mask',np.sum(mask))
        df = df[mask]
        
        print(len(df),' train + val events')
        
        X_norm = np.dstack([df[['ml_j{}_{}'.format(i,v) for i in range(nSelectedJets)]].values for v in jetVars])
        y = df['correctPair'].values
        w = df['mc_sf'].values
              
        # Calculate the mask for the jets
        mask = ~ np.all(X_norm==0, axis=-1)
            
        # Put into the torch tensors
        eventNumber = df.index 
        
        self.x = torch.from_numpy(X_norm).float()
        self.y = torch.from_numpy(y).long()
        self.w = torch.from_numpy(w).float()

        njets = mask.sum(axis=1)
        
        ttlPairs = getNumPairs(nSelectedJets)
        scoreMask = np.ones((X_norm.shape[0],ttlPairs)).astype(bool)
        
        for ji in range(4,nSelectedJets):
            
            nPairs = getNumPairs(ji)
            scoreMask[njets==ji] *= np.array([True]*nPairs + [False]*(ttlPairs-nPairs))
        
        self.scoreMask = torch.from_numpy(scoreMask)

        self.ntag = df['ntag'].values
        self.njets  = df['njets'].values
        
        # Ok, so I also want to pass a src_key_padding mask to the attn layers as well
        mask = np.ones((X_norm.shape[0],nSelectedJets)).astype(bool)
        
        for ji in range(4,nSelectedJets+1):
            mask[njets==ji] *= np.array([False]*ji + [True]*(nSelectedJets-ji))
        self.mask = mask
    
    def __len__(self):
        return self.x.shape[0] 

    def __getitem__(self, idx):
        return self.x[idx], self.y[idx], self.w[idx], self.scoreMask[idx], \
               self.ntag[idx], self.njets[idx], self.mask[idx]

def getGNNDataLoaders(batch_size=256, trainFrac=.8, valFrac=.1,nSelectedJets=5,
                      physicsSample='SMNR',mcs=['mc16a','mc16d','mc16e'],prodTag='MAR2020',
                      jetVars = ['pt','eta','phi','E','Db'],pTcut=40,ntag=3,w_cut=0,
                      trainingEvents='odd',split='eventNumber'):
    '''
    Return the DataLoaders for my pytorch function.
    
    Inputs: 
    - 
    
 
    Returns: loader_train, loader_val, loader_test
        DataLoaders for the train, val
    '''
     
    dset = hh4bDataset(nSelectedJets=nSelectedJets,physicsSample=physicsSample,
                       mcs=mcs,prodTag=prodTag,pTcut=pTcut,ntag=ntag,w_cut=w_cut,
                       trainingEvents=trainingEvents,split=split)
    nEvts = len(dset)
        
    # Shuffle the indices
    indices = np.random.permutation(nEvts)   
    idxTrain = indices[:int(trainFrac*nEvts)]
    idxVal   = indices[int(trainFrac*nEvts):]
    
    '''
    Note: I could possibly save in terms of size if I batched events w/ the
    same # of jets together, but I can prob just come back to this later.
    '''
    loader_train = DataLoader(dset, batch_size=batch_size, sampler=SubsetRandomSampler(idxTrain))
    loader_val = DataLoader(dset, batch_size=batch_size, sampler=SequentialSampler(idxVal))
        
    return loader_train, loader_val
    
def my_nll_loss(input, target, sample_weight=None):
    '''
    Goal: Implementation of the NLL loss that can accept sample weights.
    '''
    
    batch_size = target.shape[0]
    Li = - sample_weight * input[torch.arange(batch_size),target]
    return torch.mean(Li)
    
def check_loss(loader,model,device='cpu',masked=False): 
    '''
    Calculate the loss
    '''
    
    model.eval()
    L = 0
    
    model = model.to(device=device)
    
    with torch.no_grad():
        for xi,yi,wi,mi,bi,ni,si in loader:
            
            xi = xi.to(device=device)
            yi = yi.to(device=device)
            wi = wi.to(device=device)
            mi = mi.to(device=device)
            bi = bi.to(device=device)
            si = si.to(device=device)
            
            if masked:
                logits1, _ = model(xi[ni==4,:4]) 
                logp1 = F.log_softmax(logits1, 1)
                loss1 = my_nll_loss(logp1, yi[ni==4], wi[ni==4])

                logits2, _ = model(xi[ni==5])
                logp2 = F.log_softmax(logits2, 1)
                loss2 = my_nll_loss(logp2, yi[ni==5], wi[ni==5])

                loss = (loss1 * torch.sum(wi[ni==4]) + loss2 * torch.sum(wi[ni==5]))/ wi.sum() 
                   
            else:
                logits, edgeWeights = model(xi) #,si)
                logits[~mi] = float('-inf') 
                logp = F.log_softmax(logits, 1)
                loss = my_nll_loss(logp, yi, wi)

            L += loss.item()
            
    return L
    
def check_accuracy(loader, model, btag=None, njets=None, device='cpu', masked=False):

    '''
    Check the accuracy of the model
    Inputs:
        loader: A DataLoader object, i.e, for the val or test st
        model: A Pytorch model to check the accuracy on
        returnAcc: If true, the function will return the calculated accuracy
        btag: Mask only events w/ this many btags for the accuracy
        njets: Mask only events w/ this many jets for the accuracy
    '''
    
    num_correct = 0
    num_samples = 0
    model.eval()  # set model to evaluation mode

    model = model.to(device=device)

    with torch.no_grad():
        for xi, yi, wi, mi, bi, ni, si in loader:

            xi = xi.to(device=device)
            yi = yi.to(device=device)
            wi = wi.to(device=device)
            mi = mi.to(device=device)
            bi = bi.to(device=device)
            ni = ni.to(device=device)
            si = si.to(device=device)
        
            if masked:
            
                y_pred = torch.zeros_like(yi)
                
                logits1, _ = model(xi[ni==4,:4])
                y_pred[ni==4] = torch.max(logits1,1)[1]
        
                logits2, _ = model(xi[ni==5])
                y_pred[ni==5] = torch.max(logits2,1)[1]

            else:
                logits, _ = model(xi,si)
                logits[~mi] = float('-inf') 
                _, y_pred = torch.max(logits,1)

            if btag is None:
                if njets is None:
                    num_correct += torch.sum(wi[yi==y_pred])
                    num_samples += torch.sum(wi)
                else:
                    num_correct += torch.sum(wi[(yi==y_pred)&(ni==njets)])
                    num_samples += torch.sum(wi[ni==njets])
            else:
                bmask = (bi>=4) if (btag == 4) else (bi == btag) 
                if njets is None:
                    num_correct += torch.sum(wi[(yi==y_pred) & bmask])
                    num_samples += torch.sum(wi[bmask])
                else:
                    num_correct += torch.sum(wi[(yi==y_pred) & bmask & (ni==njets)])
                    num_samples += torch.sum(wi[bmask & (ni==njets)])
                
        if num_samples > 0:
            acc = (num_correct.cpu() / num_samples.cpu()).numpy()
        else:
            acc = 0 

    return float(acc)


def train(model,loader_train,loader_val,lr,nEpochs,patience,device='cpu',modelDir='',figDir='',masked=False):
    '''
    Goal: Implement the training loop with early stopping.
    '''

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)

    keys = ["_".join([s,m]) for s in ['train','val'] for m in ['loss','acc','acc_3b','acc_4b','acc_4j','acc_5j']]
    metrics = {k:[] for k in keys}

    model.to(device=device)

    bestEpoch = 0 
    #bestValLoss = 10000
    bestValAcc = 0

    for epoch in range(nEpochs):

        L = 0
        model.train()

        for xi,yi,wi,mi,bi,ni,si in loader_train:

            xi = xi.to(device=device)
            yi = yi.to(device=device)
            wi = wi.to(device=device)
            mi = mi.to(device=device)
            bi = bi.to(device=device)
            #si = si.to(device=device)
            
            optimizer.zero_grad()

            if masked:
                logits1, _ = model(xi[ni==4,:4]) 
                logp1 = F.log_softmax(logits1, 1)
                loss1 = my_nll_loss(logp1, yi[ni==4], wi[ni==4])
                
                logits2, _ = model(xi[ni==5])
                logp2 = F.log_softmax(logits2, 1)
                loss2 = my_nll_loss(logp2, yi[ni==5], wi[ni==5])

                loss = (loss1 * torch.sum(wi[ni==4]) + loss2 * torch.sum(wi[ni==5]))/ wi.sum() 
                
            else:
                logits, edgeWeights = model(xi) #,si)
                logits[~mi] = float('-inf') 
                logp = F.log_softmax(logits, 1)
                loss = my_nll_loss(logp, yi, wi)

            loss.backward()
            optimizer.step()

            L += loss.item()

        # Save the model (to load in later)
        torch.save(model.state_dict(), modelDir+f'model_epoch{epoch}.pt')
        
        metrics['train_loss'].append(L)
        metrics['val_loss'].append(check_loss(loader_val,model,device=device,masked=masked))

        for s, loader in zip(['train','val'],[loader_train, loader_val]):
            metrics[f'{s}_acc'].append(   check_accuracy(loader,model,device=device,masked=masked))
            metrics[f'{s}_acc_3b'].append(check_accuracy(loader,model,btag=3,device=device,masked=masked))
            metrics[f'{s}_acc_4b'].append(check_accuracy(loader,model,btag=4,device=device,masked=masked))
            metrics[f'{s}_acc_4j'].append(check_accuracy(loader,model,njets=4,device=device,masked=masked))
            metrics[f'{s}_acc_5j'].append(check_accuracy(loader,model,njets=5,device=device,masked=masked))

        myVars = (epoch, L, metrics['val_loss'][-1]*4, metrics['train_acc'][-1], metrics['val_acc'][-1])
        print("Epoch {:2d} | Train loss {: .3f} | Val loss {: .3f} | Train acc {:.3f} | Val acc {:.3f}".format(*myVars))
        
        # Check if the validation acc improved
        if metrics['val_acc'][-1] > bestValAcc:
            bestValAcc = metrics['val_acc'][-1]
            bestEpoch = epoch
        
        # See if it's time to break out of the training loop 
        if epoch - bestEpoch > patience:
            break
        
    # Training + validation loss and accuracy    
    trainingMetrics(metrics, figDir)

    model.load_state_dict(torch.load(modelDir+f'model_epoch{bestEpoch}.pt'))
    
    if len(modelDir) > 0:
        filename = modelDir + "loss_acc.json"
        with open(filename, 'w') as varfile:
            json.dump(metrics, varfile)

        # Also save the model w/ the best validation loss 
        torch.save(model.state_dict(), modelDir+f'model.pt')


if __name__ == '__main__':

    p = ArgumentParser()
    
    # Paramters for the training dataset
    p.add_argument('--batch_size', type=int, default=2048, help='batch size for the training dataset (default 2048)')
    p.add_argument('--nSelectedJets', type=int, default=5, help='max # of jets to include (default 5)')
    p.add_argument('--pTcut', type=int, default=40, help='pT cut used for the jet selection (default 40)')
    p.add_argument('--physicsSample', type=str, default='SMNR', help='physics sample to train on (defaultt SMNR)')
    p.add_argument('--mc', type=str, default='mc16a,mc16d,mc16e', 
                   help='Comma separated list of mc campaigns to use (default, mc16a,mc16d,mc16e)')
    p.add_argument('--prodTag', type=str, default='JUN2020', help='Prod tag to use for the training (default JUN2020)')
    p.add_argument('--ntag', type=int, default=2, help='Min cut on the # of b-tags to use in the training (default 2)')
    p.add_argument('--trainingEvents', type=str, default='odd', help='Events to use in training: one of odd (default), even, or all')
    p.add_argument('--split', type=str, default='eventNumber', 
                   help='If trainingEvents is not "all", the variable to split on, i.e, eventNumber (defuault) or index')
    
    # Parameters of the model
    p.add_argument('--embed_dim', type=int, default=20, help="dimension of the hidden space (default 20)")
    p.add_argument('--ff_dim', type=int, default=20, help="dim of the feedforward layer in the attn block (default 20)")
    p.add_argument('--nHeads', type=int, default=4, help="# of heads for the attn mech (default 4)")
    p.add_argument('--nLayers', type=int, default=1, help="# of encoder layer blocks (default 1)")
    p.add_argument('--dropout', type=float,default=0.3,help='dropout to use in the attention encoder layer (default 0.3)')
    p.add_argument('--scoreFct',type=str,default='jetCompatibility',help='form to use for the score (default jetCompatibility)')
    p.add_argument('--masked',action='store_true',help='Whether to calculate loss for different batches separately')
    p.add_argument('--w_cut',type=float,default=0,help='Cut on the absolute value of the weights for training (default 0 -> no cut)')
            
    # Parameters of the optimizer   
    p.add_argument('--lr', type=float, default=0.005,
                   help="The learning rate to use for the optimizer ()")
    p.add_argument('--nMaxEpochs',type=int, default=50, 
                   help="max # of epochs")
    p.add_argument('--patience',type=int, default=5, 
                   help="if the validation loss doesn't improve in this many epochs, stop training")

    p.add_argument('--gpuNum','--gpu_num',type=int,default=0,help='which gpu to use')
    
    args = p.parse_args()
    
    nSelectedJets, batch_size, prodTag = args.nSelectedJets, args.batch_size, args.prodTag
    physicsSample, mcs = args.physicsSample, args.mc.split(',')
    pTcut, ntag = args.pTcut, args.ntag
    mc = 'mc16' + ''.join([m[-1] for m in mcs])
    subDir = f"{physicsSample}_{mc}_PFlow-{prodTag}"
    if nSelectedJets != 4:
        subDir += f"-{nSelectedJets}jets"
    if pTcut != 40:
        subDir += f"-{pTcut}GeV"

    # Read in the parameters of the model 
    embed_dim, ff_dim = args.embed_dim, args.ff_dim
    nHeads, nLayers = args.nHeads, args.nLayers
    p = args.dropout
    scoreFct = args.scoreFct

    if embed_dim % nHeads != 0:
        print(f'embed_dim {embed_dim} is not divisible by the # of heads {nHeads}')
        sys.exit()
        
    lr = args.lr 
    nEpochs, patience = args.nMaxEpochs, args.patience
    gpuNum = args.gpuNum

    # If the directory doesn't exist - create it!
    mTag = '_mask' if args.masked else ''
    wTag = f'_wcut_{args.w_cut}' if args.w_cut != 0 else ''
    bTag = f'_{ntag}b' if ntag != 3 else ''
    trainTag = '' if args.trainingEvents == 'odd' else f'_{args.trainingEvents}'
    specs = f"xformer_{nLayers}layers_dim{embed_dim}_ff{ff_dim}_{nHeads}heads_dpt{p}_{scoreFct}_lr{lr}_batch{batch_size}{trainTag}{mTag}{wTag}{bTag}/"
    modelDir = f"models/{subDir}/"
    figDir = f"figures/{subDir}/"
    if not os.path.exists(modelDir): os.mkdir(modelDir)
    if not os.path.exists(figDir): os.mkdir(figDir)
    modelDir += specs
    figDir += specs
    if not os.path.exists(modelDir): os.mkdir(modelDir)
    if not os.path.exists(figDir): os.mkdir(figDir)

    device = f'cuda:{gpuNum}' if torch.cuda.is_available() else 'cpu'

    # Load in the training + validation data 
    loader_train, loader_val = getGNNDataLoaders(batch_size,nSelectedJets=nSelectedJets,
                                                 physicsSample=physicsSample,mcs=mcs,prodTag=prodTag,
                                                 pTcut=pTcut, ntag=ntag, w_cut=args.w_cut,
                                                 trainingEvents=args.trainingEvents, split=args.split)

    # Load in the model
    model = pairAGraph(inpt_dim=5,embed_dim=embed_dim,ff_dim=ff_dim,
                       nAttnBlocks=nLayers,nHeads=nHeads,p=p,njets=nSelectedJets,scoreFct=scoreFct)

    params = sum([np.prod(p.size()) for p in model.parameters()])
    print(f"Training a transformer model with {nLayers} encoder layers with {nHeads} heads")
    print(f"embed_dim = {embed_dim}, ff_dim = {ff_dim}, dpt = {p}, lr = {lr}.")
    print(f"{params} trainable parameters")

    train(model,loader_train,loader_val,lr,nEpochs,patience,device,modelDir,figDir,args.masked)
