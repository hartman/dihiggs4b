time="2020-01-27T01:33:19-08:00" level=warning msg="\"/run/user/14660\" directory set by $XDG_RUNTIME_DIR does not exist. Either create the directory or unset $XDG_RUNTIME_DIR.: stat /run/user/14660: no such file or directory: Trying to pull image in the event that it is a public image."
df_f_0.22_2b.h5
df_f_0.22_2b_SB_rw_vars.h5
df_f_0.22_3b.h5
df_f_0.22_4b_SB_rw_vars.h5
df_f_0.22_NNT_HCs.h5
df_f_0.22_fullmassplane.h5
df_f_0.22_rw_vars.h5
df_periodABCDEFG_f_0.22_fullmassplane.h5
df_periodABC_f_0.22_fullmassplane.h5
files
old_files
0it [00:00, ?it/s]0it [00:00, ?it/s]
['run_number', 'event_number', 'mc_sf', 'ntag', 'njets', 'kinematic_region', 'm_hh', 'pt_hh', 'X_wt', 'm_hh_cor', 'pT_h1', 'eta_h1', 'phi_h1', 'm_h1', 'dRjj_h1', 'pT_h2', 'eta_h2', 'phi_h2', 'm_h2', 'dRjj_h2', 'pT_2', 'pT_4', 'eta_i', 'dRjj_1', 'dRjj_2', 'cosThetaStar', 'cosTheta1', 'cosTheta2', 'Phi', 'Phi1', 'HT', 'MDR', 'MDpT', 'cut_deta_hh', 'cut_Xwt']

------------------------------------------------------------
Sender: LSF System <lsf@atlprf10>
Subject: Job 823985: <data_16_PFlow-MAY2019-5jets_periodH_-1b_all_NNT_vars> in cluster <slac> Done

Job <data_16_PFlow-MAY2019-5jets_periodH_-1b_all_NNT_vars> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Mon Jan 27 01:33:14 2020
Job was executed on host(s) <atlprf10>, in queue <atlas-t3>, as user <nhartman> in cluster <slac> at Mon Jan 27 01:33:18 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b> was used as the working directory.
Started at Mon Jan 27 01:33:18 2020
Terminated at Mon Jan 27 01:33:28 2020
Results reported at Mon Jan 27 01:33:28 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
singularity exec docker://gitlab-registry.cern.ch/hartman/ml-gpu/ml-gpu:latest ./tmp_data_16_PFlow-MAY2019-5jets_periodH_-1b_all_NNT_vars.sh
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   6.50 sec.
    Max Memory :                                 40 MB
    Average Memory :                             40.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   763 MB
    Max Processes :                              5
    Max Threads :                                29
    Run time :                                   10 sec.
    Turnaround time :                            14 sec.

The output (if any) is above this job summary.

