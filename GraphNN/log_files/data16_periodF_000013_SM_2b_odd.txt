
Opening ../../public/hh4b/data/user.mswiatlo.HH4B.periodF.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664141._000013.MiniNTuple.root
Loading in the event df
Traceback (most recent call last):
  File "processData.py", line 135, in <module>
    pairAndProcess(args.filename, ntag, GNNParams, save=True, fileTag=tag)
  File "/gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/GraphNN/preprocess.py", line 1034, in pairAndProcess
    df = processDf(inputFile, nJetsMax=GNNParams['nSelectedJets'], pT_min=40, year=year,mc=False)
  File "/gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/GraphNN/preprocess.py", line 63, in processDf
    df = tree.pandas.df(evt_vars)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/_connect/_pandas.py", line 32, in df
    return self._tree.arrays(branches=branches, outputtype=pandas.DataFrame, namedecode=namedecode, entrystart=entrystart, entrystop=entrystop, flatten=flatten, flatname=flatname, awkwardlib=awkwardlib, cache=cache, basketcache=basketcache, keycache=keycache, executor=executor, blocking=blocking)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 529, in arrays
    futures = [(branch.name if namedecode is None else branch.name.decode(namedecode), interpretation, branch.array(interpretation=interpretation, entrystart=entrystart, entrystop=entrystop, flatten=(flatten and not ispandas), awkwardlib=awkward, cache=cache, basketcache=basketcache, keycache=keycache, executor=executor, blocking=False)) for branch, interpretation in branches]
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 529, in <listcomp>
    futures = [(branch.name if namedecode is None else branch.name.decode(namedecode), interpretation, branch.array(interpretation=interpretation, entrystart=entrystart, entrystop=entrystop, flatten=(flatten and not ispandas), awkwardlib=awkward, cache=cache, basketcache=basketcache, keycache=keycache, executor=executor, blocking=False)) for branch, interpretation in branches]
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 1393, in array
    basket_itemoffset = self._basket_itemoffset(interpretation, basketstart, basketstop, keycache)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 1346, in _basket_itemoffset
    for j, key in enumerate(self._threadsafe_iterate_keys(keycache, True, basketstart, basketstop)):
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 1052, in _threadsafe_iterate_keys
    key = self._basketkey(keysource, i, complete)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 1769, in _basketkey
    return self._BasketKey(source.parent(), Cursor(self._fBasketSeek[i]), self.compression, complete)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 1647, in __init__
    self._fNbytes, self._fVersion, self._fObjlen, self._fDatime, self._fKeylen, self._fCycle, self._fSeekKey, self._fSeekPdir = cursor.fields(source, TBranchMethods._BasketKey._format_small)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/source/cursor.py", line 46, in fields
    return format.unpack(source.data(start, stop))
KeyboardInterrupt

------------------------------------------------------------
Sender: LSF System <lsf@kiso0049>
Subject: Job 60445: <data16_periodF_000013_SM_2b_odd> in cluster <slac> Exited

Job <data16_periodF_000013_SM_2b_odd> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Thu Aug  6 15:03:20 2020
Job was executed on host(s) <kiso0049>, in queue <short>, as user <nhartman> in cluster <slac> at Thu Aug  6 15:44:19 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b/GraphNN> was used as the working directory.
Started at Thu Aug  6 15:44:19 2020
Terminated at Thu Aug  6 15:44:23 2020
Results reported at Thu Aug  6 15:44:23 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python processData.py --physicsSample data16 --prodTag FEB20 --nSelectedJets 5 --pTcut 40 --filename ../../public/hh4b/data/user.mswiatlo.HH4B.periodF.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664141._000013.MiniNTuple.root --ntag 3 --pairAGraphConfig SM_2b_odd
------------------------------------------------------------

TERM_OWNER: job killed by owner.
Exited with exit code 1.

Resource usage summary:

    CPU time :                                   2.40 sec.
    Max Memory :                                 127 MB
    Average Memory :                             121.67 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                4
    Run time :                                   3 sec.
    Turnaround time :                            2463 sec.

The output (if any) is above this job summary.

