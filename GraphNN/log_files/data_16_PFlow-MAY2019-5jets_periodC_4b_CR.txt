df_f_0.22_2b.h5
df_f_0.22_2b_SB_rw_vars.h5
df_f_0.22_3b.h5
df_f_0.22_4b_SB_rw_vars.h5
df_f_0.22_rw_vars.h5
files
old_files
  0%|          | 0/26 [00:00<?, ?it/s]Scaling pt.
Scaling E.
Saving  /u/ki/nhartman/gpfs/diHiggs4b/data/data_16_PFlow-MAY2019-5jets/files/df_periodC_f_0.22_000011_entry_0_500000_scaledInputs.h5
Traceback (most recent call last):
  File "processData.py", line 85, in <module>
    concatDfs(fDir,period,nbtags,region)
  File "processData.py", line 26, in concatDfs
    dfi = pairAndProcess(myFile,nbtags,region)
  File "/gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/GraphNN/preprocess.py", line 402, in pairAndProcess
    df = transformData(inputFile,scalingFile)
  File "/gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/GraphNN/preprocess.py", line 131, in transformData
    df.to_hdf(outputFile, key='df', mode='w')
  File "/opt/conda/lib/python3.6/site-packages/pandas/core/generic.py", line 2530, in to_hdf
    pytables.to_hdf(path_or_buf, key, self, **kwargs)
  File "/opt/conda/lib/python3.6/site-packages/pandas/io/pytables.py", line 276, in to_hdf
    path_or_buf, mode=mode, complevel=complevel, complib=complib
  File "/opt/conda/lib/python3.6/site-packages/pandas/io/pytables.py", line 505, in __init__
    self.open(mode=mode, **kwargs)
  File "/opt/conda/lib/python3.6/site-packages/pandas/io/pytables.py", line 627, in open
    self._handle = tables.open_file(self._path, self._mode, **kwargs)
  File "/opt/conda/lib/python3.6/site-packages/tables/file.py", line 315, in open_file
    return File(filename, mode, title, root_uep, filters, **kwargs)
  File "/opt/conda/lib/python3.6/site-packages/tables/file.py", line 778, in __init__
    self._g_new(filename, mode, **params)
  File "tables/hdf5extension.pyx", line 492, in tables.hdf5extension.File._g_new
tables.exceptions.HDF5ExtError: HDF5 error back trace

  File "H5F.c", line 444, in H5Fcreate
    unable to create file
  File "H5Fint.c", line 1364, in H5F__create
    unable to open file
  File "H5Fint.c", line 1615, in H5F_open
    unable to lock the file
  File "H5FD.c", line 1640, in H5FD_lock
    driver lock request failed
  File "H5FDsec2.c", line 941, in H5FD_sec2_lock
    unable to lock file, errno = 11, error message = 'Resource temporarily unavailable'

End of HDF5 error back trace

Unable to open/create file '/u/ki/nhartman/gpfs/diHiggs4b/data/data_16_PFlow-MAY2019-5jets/files/df_periodC_f_0.22_000011_entry_0_500000_scaledInputs.h5'
  0%|          | 0/26 [00:38<?, ?it/s]

------------------------------------------------------------
Sender: LSF System <lsf@atlprf26>
Subject: Job 816274: <data_16_PFlow-MAY2019-5jets_periodC_4b_CR> in cluster <slac> Exited

Job <data_16_PFlow-MAY2019-5jets_periodC_4b_CR> was submitted from host <rhel6-64d> by user <nhartman> in cluster <slac> at Mon Jan 13 04:43:04 2020
Job was executed on host(s) <atlprf26>, in queue <atlas-t3>, as user <nhartman> in cluster <slac> at Mon Jan 13 04:43:09 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b> was used as the working directory.
Started at Mon Jan 13 04:43:09 2020
Terminated at Mon Jan 13 04:44:03 2020
Results reported at Mon Jan 13 04:44:03 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
singularity exec docker://gitlab-registry.cern.ch/hartman/ml-gpu/ml-gpu:latest ./tmp_data_16_PFlow-MAY2019-5jets_periodC_4b_CR.sh
------------------------------------------------------------

Exited with exit code 1.

Resource usage summary:

    CPU time :                                   24.75 sec.
    Max Memory :                                 1060 MB
    Average Memory :                             799.75 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   3904 MB
    Max Processes :                              5
    Max Threads :                                23
    Run time :                                   54 sec.
    Turnaround time :                            59 sec.

The output (if any) is above this job summary.

