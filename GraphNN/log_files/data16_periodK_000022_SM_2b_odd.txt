
Opening ../../public/hh4b/data/user.mswiatlo.HH4B.periodK.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664145._000022.MiniNTuple.root
Loading in the event df
Loading in the jet array
Traceback (most recent call last):
  File "processData.py", line 135, in <module>
    pairAndProcess(args.filename, ntag, GNNParams, save=True, fileTag=tag)
  File "/gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/GraphNN/preprocess.py", line 1034, in pairAndProcess
    df = processDf(inputFile, nJetsMax=GNNParams['nSelectedJets'], pT_min=40, year=year,mc=False)
  File "/gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/GraphNN/preprocess.py", line 78, in processDf
    arr = tree.arrays(jet_vars)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 529, in arrays
    futures = [(branch.name if namedecode is None else branch.name.decode(namedecode), interpretation, branch.array(interpretation=interpretation, entrystart=entrystart, entrystop=entrystop, flatten=(flatten and not ispandas), awkwardlib=awkward, cache=cache, basketcache=basketcache, keycache=keycache, executor=executor, blocking=False)) for branch, interpretation in branches]
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 529, in <listcomp>
    futures = [(branch.name if namedecode is None else branch.name.decode(namedecode), interpretation, branch.array(interpretation=interpretation, entrystart=entrystart, entrystop=entrystop, flatten=(flatten and not ispandas), awkwardlib=awkward, cache=cache, basketcache=basketcache, keycache=keycache, executor=executor, blocking=False)) for branch, interpretation in branches]
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 1434, in array
    _delayedraise(fill(j))
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 1402, in fill
    source = self._basket(i, interpretation, local_entrystart, local_entrystop, awkward, basketcache, keycache)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 1185, in _basket
    basketdata = key.basketdata()
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/tree.py", line 1692, in basketdata
    return self.cursor.copied().bytes(datasource, self._fObjlen)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/source/cursor.py", line 54, in bytes
    return source.data(start, stop)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/source/compressed.py", line 186, in data
    self._prepare()
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/source/compressed.py", line 159, in _prepare
    asstr = compression.decompress(self._compressed, cursor, compressedbytes, uncompressedbytes)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/uproot/source/compressed.py", line 60, in decompress
    return zlib_decompress(cursor.bytes(source, compressedbytes))
KeyboardInterrupt

------------------------------------------------------------
Sender: LSF System <lsf@kiso0052>
Subject: Job 60411: <data16_periodK_000022_SM_2b_odd> in cluster <slac> Exited

Job <data16_periodK_000022_SM_2b_odd> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Thu Aug  6 15:03:16 2020
Job was executed on host(s) <kiso0052>, in queue <short>, as user <nhartman> in cluster <slac> at Thu Aug  6 15:43:47 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b/GraphNN> was used as the working directory.
Started at Thu Aug  6 15:43:47 2020
Terminated at Thu Aug  6 15:44:22 2020
Results reported at Thu Aug  6 15:44:22 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python processData.py --physicsSample data16 --prodTag FEB20 --nSelectedJets 5 --pTcut 40 --filename ../../public/hh4b/data/user.mswiatlo.HH4B.periodK.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664145._000022.MiniNTuple.root --ntag 3 --pairAGraphConfig SM_2b_odd
------------------------------------------------------------

TERM_OWNER: job killed by owner.
Exited with exit code 1.

Resource usage summary:

    CPU time :                                   6.10 sec.
    Max Memory :                                 296 MB
    Average Memory :                             246.25 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                4
    Run time :                                   35 sec.
    Turnaround time :                            2466 sec.

The output (if any) is above this job summary.

