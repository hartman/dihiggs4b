  File "processData.py", line 27
    periods = ['A','B','C','D','E','F','G','H','I','J','K','L']
          ^
IndentationError: expected an indented block

------------------------------------------------------------
Sender: LSF System <lsf@kiso0035>
Subject: Job 59584: <data16_periodK_000005_SM_2b_even> in cluster <slac> Exited

Job <data16_periodK_000005_SM_2b_even> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Thu Aug  6 15:01:20 2020
Job was executed on host(s) <kiso0035>, in queue <short>, as user <nhartman> in cluster <slac> at Thu Aug  6 15:29:09 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b/GraphNN> was used as the working directory.
Started at Thu Aug  6 15:29:09 2020
Terminated at Thu Aug  6 15:29:12 2020
Results reported at Thu Aug  6 15:29:12 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python processData.py --physicsSample data16 --prodTag FEB20 --nSelectedJets 5 --pTcut 40 --filename ../../public/hh4b/data/user.mswiatlo.HH4B.periodK.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664145._000005.MiniNTuple.root --ntag 2 --pairAGraphConfig SM_2b_even
------------------------------------------------------------

Exited with exit code 1.

Resource usage summary:

    CPU time :                                   0.13 sec.
    Max Memory :                                 3 MB
    Average Memory :                             3.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              2
    Max Threads :                                5
    Run time :                                   3 sec.
    Turnaround time :                            1672 sec.

The output (if any) is above this job summary.

