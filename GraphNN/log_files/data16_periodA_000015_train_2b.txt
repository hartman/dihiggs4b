
Opening ../../public/hh4b/data/user.mswiatlo.HH4B.periodA.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664130._000015.MiniNTuple.root
Loading in the event df
Loading in the jet array

  Jet sfs for evts with 1 selected jets out of 4 resolved jets
  Jet sfs for evts with 2 selected jets out of 4 resolved jets
  Jet sfs for evts with 3 selected jets out of 4 resolved jets
  Jet sfs for evts with 4 selected jets out of 4 resolved jets

  Jet sfs for evts with 1 selected jets out of 5 resolved jets
  Jet sfs for evts with 2 selected jets out of 5 resolved jets
  Jet sfs for evts with 3 selected jets out of 5 resolved jets
  Jet sfs for evts with 4 selected jets out of 5 resolved jets
  Jet sfs for evts with 5 selected jets out of 5 resolved jets

  Jet sfs for evts with 1 selected jets out of 6 resolved jets
  Jet sfs for evts with 2 selected jets out of 6 resolved jets
  Jet sfs for evts with 3 selected jets out of 6 resolved jets
  Jet sfs for evts with 4 selected jets out of 6 resolved jets
  Jet sfs for evts with 5 selected jets out of 6 resolved jets
  Jet sfs for evts with 6 selected jets out of 6 resolved jets

  Jet sfs for evts with 1 selected jets out of 7 resolved jets
  Jet sfs for evts with 2 selected jets out of 7 resolved jets
  Jet sfs for evts with 3 selected jets out of 7 resolved jets
  Jet sfs for evts with 4 selected jets out of 7 resolved jets
  Jet sfs for evts with 5 selected jets out of 7 resolved jets
  Jet sfs for evts with 6 selected jets out of 7 resolved jets
  Jet sfs for evts with 7 selected jets out of 7 resolved jets

  Jet sfs for evts with 2 selected jets out of 8 resolved jets
  Jet sfs for evts with 3 selected jets out of 8 resolved jets
  Jet sfs for evts with 4 selected jets out of 8 resolved jets
  Jet sfs for evts with 5 selected jets out of 8 resolved jets
  Jet sfs for evts with 6 selected jets out of 8 resolved jets
  Jet sfs for evts with 7 selected jets out of 8 resolved jets
  Jet sfs for evts with 8 selected jets out of 8 resolved jets

  Jet sfs for evts with 2 selected jets out of 9 resolved jets
  Jet sfs for evts with 3 selected jets out of 9 resolved jets
  Jet sfs for evts with 4 selected jets out of 9 resolved jets
  Jet sfs for evts with 5 selected jets out of 9 resolved jets
  Jet sfs for evts with 6 selected jets out of 9 resolved jets
  Jet sfs for evts with 7 selected jets out of 9 resolved jets
  Jet sfs for evts with 8 selected jets out of 9 resolved jets
  Jet sfs for evts with 9 selected jets out of 9 resolved jets

  Jet sfs for evts with 3 selected jets out of 10 resolved jets
  Jet sfs for evts with 4 selected jets out of 10 resolved jets
  Jet sfs for evts with 5 selected jets out of 10 resolved jets
  Jet sfs for evts with 6 selected jets out of 10 resolved jets
  Jet sfs for evts with 7 selected jets out of 10 resolved jets
  Jet sfs for evts with 8 selected jets out of 10 resolved jets
  Jet sfs for evts with 9 selected jets out of 10 resolved jets

  Jet sfs for evts with 3 selected jets out of 11 resolved jets
  Jet sfs for evts with 4 selected jets out of 11 resolved jets
  Jet sfs for evts with 5 selected jets out of 11 resolved jets
  Jet sfs for evts with 6 selected jets out of 11 resolved jets
  Jet sfs for evts with 7 selected jets out of 11 resolved jets
  Jet sfs for evts with 8 selected jets out of 11 resolved jets
  Jet sfs for evts with 9 selected jets out of 11 resolved jets

  Jet sfs for evts with 7 selected jets out of 12 resolved jets

  Jet sfs for evts with 4 selected jets out of 13 resolved jets
  Jet sfs for evts with 7 selected jets out of 13 resolved jets

  Jet sfs for evts with 9 selected jets out of 14 resolved jets
Applying 2016 triggers
../data/data16_PFlow-FEB20-5jets/files/df_periodA_000015.h5

Scaling pt.
Scaling E.
Saving  ../data/data16_PFlow-FEB20-5jets/files/df_periodA_000015_scaledInputs.h5
max(njets) 5
Getting jet pts
Getting jet etas
Getting jet phis
Getting jet Es
Calculating Xwt
Calculating Xhh
Saving ../data/data16_PFlow-FEB20-5jets/files/df_periodA_000015_train_2b_3b.h5

------------------------------------------------------------
Sender: LSF System <lsf@deft0012>
Subject: Job 464733: <data16_periodA_000015_train_2b> in cluster <slac> Done

Job <data16_periodA_000015_train_2b> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Mon Jul 20 17:55:33 2020
Job was executed on host(s) <deft0012>, in queue <short>, as user <nhartman> in cluster <slac> at Mon Jul 20 17:55:51 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b/GraphNN> was used as the working directory.
Started at Mon Jul 20 17:55:51 2020
Terminated at Mon Jul 20 17:55:59 2020
Results reported at Mon Jul 20 17:55:59 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python processData.py --physicsSample data16 --prodTag FEB20 --nSelectedJets 5 --pTcut 40 --filename ../../public/hh4b/data/user.mswiatlo.HH4B.periodA.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664130._000015.MiniNTuple.root --ntag 3 --pairAGraphConfig train_2b
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   4.17 sec.
    Max Memory :                                 118 MB
    Average Memory :                             118.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                4
    Run time :                                   7 sec.
    Turnaround time :                            26 sec.

The output (if any) is above this job summary.

