Traceback (most recent call last):
  File "processData.py", line 107, in <module>
    GNNParams = json.load(varfile)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/json/__init__.py", line 296, in load
    parse_constant=parse_constant, object_pairs_hook=object_pairs_hook, **kw)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/json/__init__.py", line 348, in loads
    return _default_decoder.decode(s)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/json/decoder.py", line 337, in decode
    obj, end = self.raw_decode(s, idx=_w(s, 0).end())
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/json/decoder.py", line 353, in raw_decode
    obj, end = self.scan_once(s, idx)
json.decoder.JSONDecodeError: Expecting property name enclosed in double quotes: line 13 column 1 (char 204)

------------------------------------------------------------
Sender: LSF System <lsf@deft0005>
Subject: Job 369789: <data16_period_000031> in cluster <slac> Exited

Job <data16_period_000031> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Mon Jul 20 12:00:43 2020
Job was executed on host(s) <deft0005>, in queue <short>, as user <nhartman> in cluster <slac> at Mon Jul 20 12:00:45 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b/GraphNN> was used as the working directory.
Started at Mon Jul 20 12:00:45 2020
Terminated at Mon Jul 20 12:00:48 2020
Results reported at Mon Jul 20 12:00:48 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python processData.py --physicsSample data16 --prodTag FEB20 --nSelectedJets 5 --pTcut 40 --filename ../../public/hh4b/data/user.mswiatlo.HH4B.periodL.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664147._000031.MiniNTuple.root --ntag 3
------------------------------------------------------------

Exited with exit code 1.

Resource usage summary:

    CPU time :                                   1.91 sec.
    Max Memory :                                 -
    Average Memory :                             -
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              -
    Max Threads :                                -
    Run time :                                   16 sec.
    Turnaround time :                            5 sec.

The output (if any) is above this job summary.

