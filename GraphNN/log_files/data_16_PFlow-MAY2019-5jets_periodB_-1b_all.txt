
------------------------------------------------------------
Sender: LSF System <lsf@atlprf03>
Subject: Job 849248: <data_16_PFlow-MAY2019-5jets_periodB_-1b_all> in cluster <slac> Exited

Job <data_16_PFlow-MAY2019-5jets_periodB_-1b_all> was submitted from host <rhel6-64d> by user <nhartman> in cluster <slac> at Mon Jan 13 06:50:23 2020
Job was executed on host(s) <atlprf03>, in queue <atlas-t3>, as user <nhartman> in cluster <slac> at Mon Jan 13 06:50:28 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b> was used as the working directory.
Started at Mon Jan 13 06:50:28 2020
Terminated at Mon Jan 13 06:57:37 2020
Results reported at Mon Jan 13 06:57:37 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
singularity exec docker://gitlab-registry.cern.ch/hartman/ml-gpu/ml-gpu:latest ./tmp_data_16_PFlow-MAY2019-5jets_periodB_-1b_all.sh
------------------------------------------------------------

TERM_OWNER: job killed by owner.
Exited with exit code 143.

Resource usage summary:

    CPU time :                                   0.46 sec.
    Max Memory :                                 24 MB
    Average Memory :                             15.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   946 MB
    Max Processes :                              3
    Max Threads :                                23
    Run time :                                   429 sec.
    Turnaround time :                            434 sec.

The output (if any) is above this job summary.

