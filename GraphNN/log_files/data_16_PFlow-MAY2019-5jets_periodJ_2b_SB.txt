df_f_0.22_2b.h5
df_f_0.22_2b_SB_rw_vars.h5
df_f_0.22_3b.h5
df_f_0.22_4b_SB_rw_vars.h5
df_f_0.22_rw_vars.h5
files
old_files
0it [00:00, ?it/s]0it [00:00, ?it/s]

------------------------------------------------------------
Sender: LSF System <lsf@atlprf18>
Subject: Job 816368: <data_16_PFlow-MAY2019-5jets_periodJ_2b_SB> in cluster <slac> Done

Job <data_16_PFlow-MAY2019-5jets_periodJ_2b_SB> was submitted from host <rhel6-64d> by user <nhartman> in cluster <slac> at Mon Jan 13 04:43:13 2020
Job was executed on host(s) <atlprf18>, in queue <atlas-t3>, as user <nhartman> in cluster <slac> at Mon Jan 13 04:43:41 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b> was used as the working directory.
Started at Mon Jan 13 04:43:41 2020
Terminated at Mon Jan 13 04:43:50 2020
Results reported at Mon Jan 13 04:43:50 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
singularity exec docker://gitlab-registry.cern.ch/hartman/ml-gpu/ml-gpu:latest ./tmp_data_16_PFlow-MAY2019-5jets_periodJ_2b_SB.sh
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   6.07 sec.
    Max Memory :                                 15 MB
    Average Memory :                             15.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   528 MB
    Max Processes :                              4
    Max Threads :                                13
    Run time :                                   9 sec.
    Turnaround time :                            37 sec.

The output (if any) is above this job summary.

