
Opening ../../public/hh4b/data/user.mswiatlo.HH4B.periodK.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664145._000028.MiniNTuple.root
Loading in the event df
Loading in the jet array

  Jet sfs for evts with 1 selected jets out of 4 resolved jets
  Jet sfs for evts with 2 selected jets out of 4 resolved jets
  Jet sfs for evts with 3 selected jets out of 4 resolved jets
  Jet sfs for evts with 4 selected jets out of 4 resolved jets

  Jet sfs for evts with 1 selected jets out of 5 resolved jets
  Jet sfs for evts with 2 selected jets out of 5 resolved jets
  Jet sfs for evts with 3 selected jets out of 5 resolved jets
  Jet sfs for evts with 4 selected jets out of 5 resolved jets
  Jet sfs for evts with 5 selected jets out of 5 resolved jets

  Jet sfs for evts with 1 selected jets out of 6 resolved jets
Traceback (most recent call last):
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/pandas/core/series.py", line 1014, in __setitem__
    self._set_with_engine(key, value)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/pandas/core/series.py", line 1054, in _set_with_engine
    self.index._engine.set_value(values, key, value)
  File "pandas/_libs/index.pyx", line 96, in pandas._libs.index.IndexEngine.set_value
  File "pandas/_libs/index.pyx", line 106, in pandas._libs.index.IndexEngine.set_value
  File "pandas/_libs/index.pyx", line 116, in pandas._libs.index.IndexEngine.get_loc
TypeError: 'entry
0          True
1         False
2         False
3         False
4          True
          ...  
504679    False
504680    False
504681     True
504682     True
504683    False
Name: nresolvedJets, Length: 504684, dtype: bool' is an invalid key

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "processData.py", line 135, in <module>
    pairAndProcess(args.filename, ntag, GNNParams, save=True, fileTag=tag)
  File "/gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/GraphNN/preprocess.py", line 1034, in pairAndProcess
    df = processDf(inputFile, nJetsMax=GNNParams['nSelectedJets'], pT_min=40, year=year,mc=False)
  File "/gpfs/slac/atlas/fs1/d/nhartman/diHiggs4b/GraphNN/preprocess.py", line 97, in processDf
    m[mask] = (njets == nj)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/pandas/core/series.py", line 1037, in __setitem__
    self._where(~key, value, inplace=True)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/pandas/core/generic.py", line 8719, in _where
    elif len(cond[icond]) == len(other):
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/pandas/core/series.py", line 910, in __getitem__
    return self._get_with(key)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/pandas/core/series.py", line 947, in _get_with
    return self._get_values(key)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/pandas/core/series.py", line 985, in _get_values
    self._data.get_slice(indexer), fastpath=True
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/pandas/core/internals/managers.py", line 1542, in get_slice
    return type(self)(self._block._slice(slobj), self.index[slobj], fastpath=True,)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/pandas/core/indexes/range.py", line 708, in __getitem__
    return super().__getitem__(key)
  File "/gpfs/slac/atlas/fs1/d/rafaeltl/public/conda/miniconda3/envs/py3/lib/python3.7/site-packages/pandas/core/indexes/base.py", line 3940, in __getitem__
    result = getitem(key)
KeyboardInterrupt

------------------------------------------------------------
Sender: LSF System <lsf@kiso0041>
Subject: Job 60384: <data16_periodK_000028_SM_2b_odd> in cluster <slac> Exited

Job <data16_periodK_000028_SM_2b_odd> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Thu Aug  6 15:03:13 2020
Job was executed on host(s) <kiso0041>, in queue <short>, as user <nhartman> in cluster <slac> at Thu Aug  6 15:43:28 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b/GraphNN> was used as the working directory.
Started at Thu Aug  6 15:43:28 2020
Terminated at Thu Aug  6 15:44:22 2020
Results reported at Thu Aug  6 15:44:22 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python processData.py --physicsSample data16 --prodTag FEB20 --nSelectedJets 5 --pTcut 40 --filename ../../public/hh4b/data/user.mswiatlo.HH4B.periodK.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664145._000028.MiniNTuple.root --ntag 3 --pairAGraphConfig SM_2b_odd
------------------------------------------------------------

TERM_OWNER: job killed by owner.
Exited with exit code 1.

Resource usage summary:

    CPU time :                                   14.09 sec.
    Max Memory :                                 450 MB
    Average Memory :                             323.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                4
    Run time :                                   54 sec.
    Turnaround time :                            2469 sec.

The output (if any) is above this job summary.

