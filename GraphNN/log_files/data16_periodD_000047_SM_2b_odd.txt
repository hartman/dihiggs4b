
Opening ../../public/hh4b/data/user.mswiatlo.HH4B.periodD.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664137._000047.MiniNTuple.root
Loading in the event df
Loading in the jet array

  Jet sfs for evts with 1 selected jets out of 4 resolved jets
  Jet sfs for evts with 2 selected jets out of 4 resolved jets
  Jet sfs for evts with 3 selected jets out of 4 resolved jets
  Jet sfs for evts with 4 selected jets out of 4 resolved jets

  Jet sfs for evts with 1 selected jets out of 5 resolved jets
  Jet sfs for evts with 2 selected jets out of 5 resolved jets
  Jet sfs for evts with 3 selected jets out of 5 resolved jets
  Jet sfs for evts with 4 selected jets out of 5 resolved jets
  Jet sfs for evts with 5 selected jets out of 5 resolved jets

  Jet sfs for evts with 1 selected jets out of 6 resolved jets
  Jet sfs for evts with 2 selected jets out of 6 resolved jets
  Jet sfs for evts with 3 selected jets out of 6 resolved jets
  Jet sfs for evts with 4 selected jets out of 6 resolved jets
  Jet sfs for evts with 5 selected jets out of 6 resolved jets
  Jet sfs for evts with 6 selected jets out of 6 resolved jets

  Jet sfs for evts with 1 selected jets out of 7 resolved jets
  Jet sfs for evts with 2 selected jets out of 7 resolved jets
  Jet sfs for evts with 3 selected jets out of 7 resolved jets
  Jet sfs for evts with 4 selected jets out of 7 resolved jets
  Jet sfs for evts with 5 selected jets out of 7 resolved jets
  Jet sfs for evts with 6 selected jets out of 7 resolved jets
  Jet sfs for evts with 7 selected jets out of 7 resolved jets

  Jet sfs for evts with 1 selected jets out of 8 resolved jets
  Jet sfs for evts with 2 selected jets out of 8 resolved jets
  Jet sfs for evts with 3 selected jets out of 8 resolved jets
  Jet sfs for evts with 4 selected jets out of 8 resolved jets
  Jet sfs for evts with 5 selected jets out of 8 resolved jets
  Jet sfs for evts with 6 selected jets out of 8 resolved jets
  Jet sfs for evts with 7 selected jets out of 8 resolved jets
  Jet sfs for evts with 8 selected jets out of 8 resolved jets

  Jet sfs for evts with 3 selected jets out of 9 resolved jets
  Jet sfs for evts with 4 selected jets out of 9 resolved jets
  Jet sfs for evts with 5 selected jets out of 9 resolved jets
  Jet sfs for evts with 6 selected jets out of 9 resolved jets
  Jet sfs for evts with 7 selected jets out of 9 resolved jets
  Jet sfs for evts with 8 selected jets out of 9 resolved jets
  Jet sfs for evts with 9 selected jets out of 9 resolved jets

  Jet sfs for evts with 3 selected jets out of 10 resolved jets
  Jet sfs for evts with 4 selected jets out of 10 resolved jets
  Jet sfs for evts with 5 selected jets out of 10 resolved jets
  Jet sfs for evts with 6 selected jets out of 10 resolved jets
  Jet sfs for evts with 7 selected jets out of 10 resolved jets
  Jet sfs for evts with 8 selected jets out of 10 resolved jets
  Jet sfs for evts with 9 selected jets out of 10 resolved jets
  Jet sfs for evts with 10 selected jets out of 10 resolved jets

  Jet sfs for evts with 3 selected jets out of 11 resolved jets
  Jet sfs for evts with 4 selected jets out of 11 resolved jets
  Jet sfs for evts with 5 selected jets out of 11 resolved jets
  Jet sfs for evts with 6 selected jets out of 11 resolved jets
  Jet sfs for evts with 7 selected jets out of 11 resolved jets
  Jet sfs for evts with 8 selected jets out of 11 resolved jets

  Jet sfs for evts with 3 selected jets out of 12 resolved jets
  Jet sfs for evts with 4 selected jets out of 12 resolved jets
  Jet sfs for evts with 5 selected jets out of 12 resolved jets
  Jet sfs for evts with 6 selected jets out of 12 resolved jets
  Jet sfs for evts with 7 selected jets out of 12 resolved jets
  Jet sfs for evts with 9 selected jets out of 12 resolved jets

  Jet sfs for evts with 4 selected jets out of 13 resolved jets
  Jet sfs for evts with 6 selected jets out of 13 resolved jets

  Jet sfs for evts with 4 selected jets out of 14 resolved jets
  Jet sfs for evts with 5 selected jets out of 14 resolved jets
  Jet sfs for evts with 8 selected jets out of 14 resolved jets
  Jet sfs for evts with 9 selected jets out of 14 resolved jets

  Jet sfs for evts with 1 selected jets out of 15 resolved jets
Applying 2016 triggers
../data/data16_PFlow-FEB20-5jets/files/df_periodD_000047.h5

Scaling pt.
Scaling E.
Saving  ../data/data16_PFlow-FEB20-5jets/files/df_periodD_000047_scaledInputs.h5
max(njets) 5
Getting jet pts
Getting jet etas
Getting jet phis
Getting jet Es
Calculating Xwt
Calculating Xhh
Saving ../data/data16_PFlow-FEB20-5jets/files/df_periodD_000047_SM_2b_odd_2b_even.h5

------------------------------------------------------------
Sender: LSF System <lsf@kiso0018>
Subject: Job 413270: <data16_periodD_000047_SM_2b_odd> in cluster <slac> Done

Job <data16_periodD_000047_SM_2b_odd> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Sat Jul 25 15:58:12 2020
Job was executed on host(s) <kiso0018>, in queue <short>, as user <nhartman> in cluster <slac> at Sat Jul 25 16:00:34 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b/GraphNN> was used as the working directory.
Started at Sat Jul 25 16:00:34 2020
Terminated at Sat Jul 25 16:00:41 2020
Results reported at Sat Jul 25 16:00:41 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python processData.py --physicsSample data16 --prodTag FEB20 --nSelectedJets 5 --pTcut 40 --filename ../../public/hh4b/data/user.mswiatlo.HH4B.periodD.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664137._000047.MiniNTuple.root --ntag 2 --pairAGraphConfig SM_2b_odd
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   5.24 sec.
    Max Memory :                                 119 MB
    Average Memory :                             117.67 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                4
    Run time :                                   7 sec.
    Turnaround time :                            149 sec.

The output (if any) is above this job summary.

