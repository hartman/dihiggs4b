  File "processData.py", line 39
    fout = f"{fDir}files/df_period{period}_f_{f}{tag}{}.h5"
          ^
SyntaxError: f-string: empty expression not allowed

------------------------------------------------------------
Sender: LSF System <lsf@atlprf02>
Subject: Job 849247: <data_16_PFlow-MAY2019-5jets_periodA_-1b_all> in cluster <slac> Exited

Job <data_16_PFlow-MAY2019-5jets_periodA_-1b_all> was submitted from host <rhel6-64d> by user <nhartman> in cluster <slac> at Mon Jan 13 06:50:23 2020
Job was executed on host(s) <atlprf02>, in queue <atlas-t3>, as user <nhartman> in cluster <slac> at Mon Jan 13 06:50:28 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b> was used as the working directory.
Started at Mon Jan 13 06:50:28 2020
Terminated at Mon Jan 13 06:50:31 2020
Results reported at Mon Jan 13 06:50:31 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
singularity exec docker://gitlab-registry.cern.ch/hartman/ml-gpu/ml-gpu:latest ./tmp_data_16_PFlow-MAY2019-5jets_periodA_-1b_all.sh
------------------------------------------------------------

Exited with exit code 1.

Resource usage summary:

    CPU time :                                   0.88 sec.
    Max Memory :                                 -
    Average Memory :                             -
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              -
    Max Threads :                                -
    Run time :                                   3 sec.
    Turnaround time :                            8 sec.

The output (if any) is above this job summary.

