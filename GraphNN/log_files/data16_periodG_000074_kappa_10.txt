
Opening ../../public/hh4b/data/user.mswiatlo.HH4B.periodG.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664143._000074.MiniNTuple.root
Loading in the event df
Loading in the jet array

  Jet sfs for evts with 1 selected jets out of 4 resolved jets
  Jet sfs for evts with 2 selected jets out of 4 resolved jets
  Jet sfs for evts with 3 selected jets out of 4 resolved jets
  Jet sfs for evts with 4 selected jets out of 4 resolved jets

  Jet sfs for evts with 1 selected jets out of 5 resolved jets
  Jet sfs for evts with 2 selected jets out of 5 resolved jets
  Jet sfs for evts with 3 selected jets out of 5 resolved jets
  Jet sfs for evts with 4 selected jets out of 5 resolved jets
  Jet sfs for evts with 5 selected jets out of 5 resolved jets

  Jet sfs for evts with 1 selected jets out of 6 resolved jets
  Jet sfs for evts with 2 selected jets out of 6 resolved jets
  Jet sfs for evts with 3 selected jets out of 6 resolved jets
  Jet sfs for evts with 4 selected jets out of 6 resolved jets
  Jet sfs for evts with 5 selected jets out of 6 resolved jets
  Jet sfs for evts with 6 selected jets out of 6 resolved jets

  Jet sfs for evts with 1 selected jets out of 7 resolved jets
  Jet sfs for evts with 2 selected jets out of 7 resolved jets
  Jet sfs for evts with 3 selected jets out of 7 resolved jets
  Jet sfs for evts with 4 selected jets out of 7 resolved jets
  Jet sfs for evts with 5 selected jets out of 7 resolved jets
  Jet sfs for evts with 6 selected jets out of 7 resolved jets
  Jet sfs for evts with 7 selected jets out of 7 resolved jets

  Jet sfs for evts with 1 selected jets out of 8 resolved jets
  Jet sfs for evts with 2 selected jets out of 8 resolved jets
  Jet sfs for evts with 3 selected jets out of 8 resolved jets
  Jet sfs for evts with 4 selected jets out of 8 resolved jets
  Jet sfs for evts with 5 selected jets out of 8 resolved jets
  Jet sfs for evts with 6 selected jets out of 8 resolved jets
  Jet sfs for evts with 7 selected jets out of 8 resolved jets
  Jet sfs for evts with 8 selected jets out of 8 resolved jets

  Jet sfs for evts with 2 selected jets out of 9 resolved jets
  Jet sfs for evts with 3 selected jets out of 9 resolved jets
  Jet sfs for evts with 4 selected jets out of 9 resolved jets
  Jet sfs for evts with 5 selected jets out of 9 resolved jets
  Jet sfs for evts with 6 selected jets out of 9 resolved jets
  Jet sfs for evts with 7 selected jets out of 9 resolved jets
  Jet sfs for evts with 8 selected jets out of 9 resolved jets
  Jet sfs for evts with 9 selected jets out of 9 resolved jets

  Jet sfs for evts with 2 selected jets out of 10 resolved jets
  Jet sfs for evts with 3 selected jets out of 10 resolved jets
  Jet sfs for evts with 4 selected jets out of 10 resolved jets
  Jet sfs for evts with 5 selected jets out of 10 resolved jets
  Jet sfs for evts with 6 selected jets out of 10 resolved jets
  Jet sfs for evts with 7 selected jets out of 10 resolved jets
  Jet sfs for evts with 8 selected jets out of 10 resolved jets
  Jet sfs for evts with 9 selected jets out of 10 resolved jets

  Jet sfs for evts with 4 selected jets out of 11 resolved jets
  Jet sfs for evts with 5 selected jets out of 11 resolved jets
  Jet sfs for evts with 6 selected jets out of 11 resolved jets
  Jet sfs for evts with 7 selected jets out of 11 resolved jets
  Jet sfs for evts with 8 selected jets out of 11 resolved jets
  Jet sfs for evts with 9 selected jets out of 11 resolved jets
  Jet sfs for evts with 10 selected jets out of 11 resolved jets

  Jet sfs for evts with 3 selected jets out of 12 resolved jets
  Jet sfs for evts with 4 selected jets out of 12 resolved jets
  Jet sfs for evts with 6 selected jets out of 12 resolved jets
  Jet sfs for evts with 8 selected jets out of 12 resolved jets
  Jet sfs for evts with 9 selected jets out of 12 resolved jets

  Jet sfs for evts with 3 selected jets out of 13 resolved jets
  Jet sfs for evts with 4 selected jets out of 13 resolved jets
  Jet sfs for evts with 6 selected jets out of 13 resolved jets
  Jet sfs for evts with 8 selected jets out of 13 resolved jets

  Jet sfs for evts with 5 selected jets out of 14 resolved jets
  Jet sfs for evts with 8 selected jets out of 14 resolved jets

  Jet sfs for evts with 9 selected jets out of 15 resolved jets
Applying 2016 triggers
../data/data16_PFlow-FEB20-5jets/files/df_periodG_000074.h5

Scaling pt.
Scaling E.
Saving  ../data/data16_PFlow-FEB20-5jets/files/df_periodG_000074_scaledInputs.h5
max(njets) 5
Getting jet pts
Getting jet etas
Getting jet phis
Getting jet Es
Calculating Xwt
Calculating Xhh
Saving ../data/data16_PFlow-FEB20-5jets/files/df_periodG_000074_kappa_10_2b.h5

------------------------------------------------------------
Sender: LSF System <lsf@kiso0062>
Subject: Job 629949: <data16_periodG_000074_kappa_10> in cluster <slac> Done

Job <data16_periodG_000074_kappa_10> was submitted from host <cent7d> by user <nhartman> in cluster <slac> at Thu Jul 23 14:41:08 2020
Job was executed on host(s) <kiso0062>, in queue <short>, as user <nhartman> in cluster <slac> at Thu Jul 23 14:41:30 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b/GraphNN> was used as the working directory.
Started at Thu Jul 23 14:41:30 2020
Terminated at Thu Jul 23 14:41:39 2020
Results reported at Thu Jul 23 14:41:39 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python processData.py --physicsSample data16 --prodTag FEB20 --nSelectedJets 5 --pTcut 40 --filename ../../public/hh4b/data/user.mswiatlo.HH4B.periodG.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/user.mswiatlo.20664143._000074.MiniNTuple.root --ntag 2 --pairAGraphConfig kappa_10
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   6.50 sec.
    Max Memory :                                 121 MB
    Average Memory :                             121.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                4
    Run time :                                   9 sec.
    Turnaround time :                            31 sec.

The output (if any) is above this job summary.

