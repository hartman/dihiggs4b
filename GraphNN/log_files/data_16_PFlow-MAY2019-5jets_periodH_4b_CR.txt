df_f_0.22_2b.h5
df_f_0.22_2b_SB_rw_vars.h5
df_f_0.22_3b.h5
df_f_0.22_4b_SB_rw_vars.h5
df_f_0.22_rw_vars.h5
files
old_files
0it [00:00, ?it/s]0it [00:00, ?it/s]

------------------------------------------------------------
Sender: LSF System <lsf@atlprf12>
Subject: Job 816348: <data_16_PFlow-MAY2019-5jets_periodH_4b_CR> in cluster <slac> Done

Job <data_16_PFlow-MAY2019-5jets_periodH_4b_CR> was submitted from host <rhel6-64d> by user <nhartman> in cluster <slac> at Mon Jan 13 04:43:11 2020
Job was executed on host(s) <atlprf12>, in queue <atlas-t3>, as user <nhartman> in cluster <slac> at Mon Jan 13 04:43:38 2020
</u/ki/nhartman> was used as the home directory.
</u/ki/nhartman/gpfs/diHiggs4b> was used as the working directory.
Started at Mon Jan 13 04:43:38 2020
Terminated at Mon Jan 13 04:43:47 2020
Results reported at Mon Jan 13 04:43:47 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
singularity exec docker://gitlab-registry.cern.ch/hartman/ml-gpu/ml-gpu:latest ./tmp_data_16_PFlow-MAY2019-5jets_periodH_4b_CR.sh
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   5.12 sec.
    Max Memory :                                 22 MB
    Average Memory :                             22.00 MB
    Total Requested Memory :                     -
    Delta Memory :                               -
    Max Swap :                                   489 MB
    Max Processes :                              5
    Max Threads :                                14
    Run time :                                   8 sec.
    Turnaround time :                            36 sec.

The output (if any) is above this job summary.

