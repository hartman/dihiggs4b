'''
ML preprocessing for the pytorch implementation of the transformers that I'm
using for the pairing studies.

Winter 2020
'''
import numpy as np
import pandas as pd
import json
import matplotlib.pyplot as plt
from itertools import combinations, product
from scipy.special import comb
from glob import glob

import uproot
from uproot_methods.classes.TLorentzVector import TLorentzVectorArray
from uproot_methods.classes.TVector3 import TVector3Array

import torch
from torch.utils.data import Dataset, DataLoader, TensorDataset
from torch.utils.data.sampler import SubsetRandomSampler, SequentialSampler
import torch.nn.functional as F

import os
os.sys.path.append('../code')
from analysis import fileDir, getXwt, getXhh, triggers

import pyarrow as pa
import pyarrow.parquet as pq

with open("../code/hashMap.json", 'r') as varfile:
    hashMap = json.load(varfile)
triggerHashes = { year : [ hashMap[ti] for ti in triggers[year] ] for year in [2015,2016,2017,2018] }

jetVars = ['pt','eta','phi','E','Db']

mcToYr = {'mc16a': 2016,
          'mc16d': 2017,
          'mc16e': 2018}


def processDf(filename,treename="XhhMiniNtuple",nJetsMax=6,year=2016,
              pT_min=30,eta_max=2.5,sort='pt',mc=True,truth=True, entrystart=None,entrystop=None):
    '''
    Given an input MNT, get the relevant jet features and apply the ml preprocessing
    
    Inputs:
    - filename:
    - treename:
    - nJetsMax:
    - year: Year to apply the corresponding triggers for (default 2016)
    - pT_min:
    - eta_max:
    - mc: Flag for whether filename is mc
    - truth: flag for if truth info is stored in the MNT
    Returns:
    
    '''
    
    # Open the file
    print(f"\nOpening {filename}")
    f = uproot.open(filename)
    tree = f[treename]

    print('Loading in the event df')
    evt_vars = ['eventNumber', 'nresolvedJets','weight_pileup']
    if mc:
        print('Running on mc')
        evt_vars += ['mcEventWeight','rand_run_nr']
    else:
        evt_vars += ['runNumber']
    df = tree.pandas.df(evt_vars,entrystart=entrystart,entrystop=entrystop)
    nEvents = len(df)
    
    # Get the jets
    jet_vars = ['pt','eta','phi','E','is_DL1r_FixedCutBEff_77','Quantile_DL1r_Continuous']
    # Note - jet_vars has to be in this order for accessing the X_wt function
    jVars = ['pt','eta','phi','E','Db']
    if mc:
        jet_vars += ['SF_DL1r_Continuous']
        jVars += ['sf']
    pre = 'resolvedJets'
    jet_vars = [f'{pre}_{v}' for v in jet_vars]
    myCols = [f'j{i}_{v}' for i in range(nJetsMax) for v in jVars] + ['njets']
    for c in myCols:
        df[c] = 0
    
    print('Loading in the jet array')
    arr = tree.arrays(jet_vars,entrystart=entrystart,entrystop=entrystop)
    
    ks = [v.encode() for v in jet_vars[:5]]
    df['X_wt']  = -1
    df['njets'] = -1
    # vars for Lucas to do his trigger opt
    df['HT_all']  = -1
    df['lead_pt']  = -1
    df['lead_tag']  = False 
    
    for nresJets in range(4,np.max(df.nresolvedJets)+1):

        mask = (df['nresolvedJets'] == nresJets)
        if np.sum(mask) == 0: continue

        # Use the jet pt and eta functions to define masks
        jpts  = arr[b'resolvedJets_pt'][mask].flatten().reshape(-1, nresJets)
        jetas = arr[b'resolvedJets_eta'][mask].flatten().reshape(-1,nresJets)
        btags = arr[b'resolvedJets_is_DL1r_FixedCutBEff_77'][mask].flatten().reshape(-1,nresJets).astype(bool)

        jmask = (jpts > pT_min) & (np.abs(jetas) < eta_max)

        njets = np.sum(jmask,axis = 1)
        df.loc[mask,'njets'] = njets

        bmask = jmask & btags
        df.loc[mask,'ntag_all'] = np.sum(bmask,axis=1)

        # Save HT_all for Lucas 
        df.loc[mask,'HT_all'] = np.sum(jpts,axis=1)
        df.loc[mask,'lead_pt'] = np.max(jpts,axis=1)
        i_lead = np.argmax(jpts,axis=1)
        df.loc[mask,'lead_tag'] = btags[ np.arange(np.sum(mask)), i_lead ]

        for nj in range(np.min(njets),nresJets+1):

            m = mask.copy()
            m[mask] = (njets == nj)
            N = min(nj,nJetsMax)

            if (np.sum(m) == 0) or (np.sum(jmask[njets == nj]) == 0): continue
            
            nEvts = np.sum(m)
            if sort != 'pt':
                k = f'resolvedJets_{sort}'
                v_sort = arr[k.encode()][m].flatten().reshape(-1,nresJets)[jmask[njets == nj]].reshape(-1,nj)
                
                # Note - this sorts by decreasing variable value
                idx_sort = np.argsort(v_sort[:,::-1],axis=1) 
            
            for v,w in zip(jVars[:5], jVars[:4]+['Quantile_DL1r_Continuous']):
                k = f'resolvedJets_{w}'.encode()
                jarr = arr[k][m].flatten().reshape(-1,nresJets)[jmask[njets == nj]].reshape(-1,nj)
                
                if sort != 'pt':
                    jarr = jarr[:,::-1][np.arange(nEvts)[:,None],idx_sort][:,::-1]
                
                df.loc[m,[f'j{i}_{v}' for i in range(N)]] = jarr if N < nJetsMax else jarr[:,:N]

            print(f'  Jet sfs for evts with {nj} selected jets out of {nresJets} resolved jets')
            if mc:
                sf_arr = np.array([[ci[0] for ci in c] for c in arr[f'{pre}_SF_DL1r_Continuous'.encode()][m]])[jmask[njets == nj]].reshape(-1,nj)
                df.loc[m,[f'j{i}_sf'  for i in range(N)]] = sf_arr if N < nJetsMax else sf_arr[:,:N] 

                if sort != 'pt':
                    df.loc[m,'mc_sf'] = np.prod(sf_arr, axis=1)

            if nj >= 4:
                assert b'resolvedJets_is_DL1r_FixedCutBEff_77' in ks # needed for Xwt calc
                arr_3d = np.dstack([arr[k][m].flatten().reshape(-1,nresJets)[jmask[njets == nj]].reshape(-1,nj) 
                                    for k in ks])
                df.loc[m,'X_wt'] = parallelXwt(arr_3d)

    # Get the analysis trigger decision
    print(f'Applying {year} triggers')
    #passedTriggerHashes = tree.array(b'passedTriggerHashes',entrystart=entrystart,entrystop=entrystop)

    # When running over the JZ slices, I was having some issues with not being 
    # able to do the sum operation all in one step b/c the array couldn't fit 
    # onto memory - so I'm just going to do a little for loop to chunk the operation
    # for each trigger decision
    chunk_size = 10000
    chunks = np.arange(0,nEvents+chunk_size,chunk_size)

    for ti in triggers[year]:
        
        trig_out = np.zeros(nEvents).astype(bool)
        
        for i_min, i_max in zip(chunks[:-1],chunks[1:]):
            
            #ja = passedTriggerHashes[i_min:i_max]
            ja = tree.array(b'passedTriggerHashes',entrystart=i_min,entrystop=i_max)
            trig_out[i_min:i_max] = (ja==hashMap[ti]).sum().astype(bool)
        
        df[ti] = trig_out

        # Implemetnation that wasn't working for some JZ files
        #df[ti] = (passedTriggerHashes == hashMap[ti]).sum().astype(bool)

    df['trigger'] = df[triggers[year]].sum(axis=1).astype(bool)

    if mc:
        if sort == 'pt':
            # Calculate the mcEvent Weight over the 5 jets "considered" for the analysis
            sfs = df[[f'j{i}_sf' for i in range(nJetsMax)]].values
            sfs = np.where(sfs==0, 1, sfs)
            sfs = np.prod(sfs,axis=1)
        else:
            sfs = df['mc_sf']
        
        df['mc_sf'] = df['mcEventWeight'] * df['weight_pileup'] * sfs
        df['run_number'] = df['rand_run_nr']
    else:
        df['mc_sf'] = 1

    df['ntag']  = np.sum(df[[f'j{i}_Db' for i in range(nJetsMax)]] >= 3, axis=1)
    df['GNNJets'] = np.sum(df[[f'j{i}_pt' for i in range(nJetsMax)]] != 0, axis=1)
    
    # Also save the truth info
    if mc and truth:
        truthInfo(tree, df, entrystart=entrystart, entrystop=entrystop)
        parallelTruthMatchJets(df, nJetsMax)
        parallelGetCorrectPair(df, nJetsMax)
    
    return df
    # Apply 4 jets, b-tagger cuts
#     preSel = df['trigger'] & (df['nresolvedJets'] >= 4) & (df['ntag'] >= 4)
#     return df[preSel]


def process4bs(filename,treename="XhhMiniNtuple",year=2016,
               pT_min=30,eta_max=2.5,mc=True,truth=True, 
               entrystart=None,entrystop=None):
    '''
    Analogous to processDf, except this just returns a df w/ 4 b-jets @ the 77% WP
    and uses the SFs appropriate for 77% (instead of PC).
    
    Ultimately I might want to combine more of the code block with processDf, but 
    since I was unclear what the "baseline analysis" was, I decided to do it this 
    was as a safety precaution to make sure I'm applying stuff correctly
    
    '''

    # Open the file
    print(f"\nOpening {filename}")
    f = uproot.open(filename)
    tree = f[treename]
    
    print('Loading in the event df')
    evt_vars = ['eventNumber', 'nresolvedJets','weight_pileup']
    #evt_vars += [f'resolved_{e}' for e in ['nBTags','nValidPairings','isSR','isCR','isSB','nGoodJets']]
    if mc:
        print('Running on mc')
        evt_vars += ['mcEventWeight','rand_run_nr']
    else:
        evt_vars += ['runNumber']
    df = tree.pandas.df(evt_vars,entrystart=entrystart,entrystop=entrystop)
    nEvents = len(df)
        
    # Note - jet_vars has to be in this order for accessing the X_wt function
    jet_vars = ['pt','eta','phi','E','is_DL1r_FixedCutBEff_77','Quantile_DL1r_Continuous']
    jVars = ['pt','eta','phi','E','Db']
    if mc:
        jet_vars += ['SF_DL1r_FixedCutBEff_77']
    pre = 'resolvedJets'
    jet_vars = [f'{pre}_{v}' for v in jet_vars]
    myCols = [f'j{i}_{v}' for i in range(4) for v in jVars] + ['njets']
    for c in myCols:
        df[c] = 0
                
    print('Loading in the jet array')
    arr = tree.arrays(jet_vars,entrystart=entrystart,entrystop=entrystop)
    ks = [v.encode() for v in jet_vars[:5]]
    df['X_wt']  = -1
    df['njets'] = -1
    df['mc_sf'] = 1

    # vars for Lucas to do his trigger opt
    df['HT_all']  = -1
    df['lead_pt']  = -1
    df['lead_tag']  = False 

    # Get the analysis trigger decision
    print(f'Applying {year} triggers')
    #passedTriggerHashes = tree.array(b'passedTriggerHashes',entrystart=entrystart,entrystop=entrystop)
    
    chunk_size = 10000
    chunks = np.arange(0,nEvents+chunk_size,chunk_size)

    for ti in triggers[year]:
        
        trig_out = np.zeros(nEvents).astype(bool)
        
        for i_min, i_max in zip(chunks[:-1],chunks[1:]):
            
            # ja = passedTriggerHashes[i_min:i_max]
            ja = tree.array(b'passedTriggerHashes',entrystart=i_min,entrystop=i_max)
            trig_out[i_min:i_max] = (ja==hashMap[ti]).sum().astype(bool)
        
        df[ti] = trig_out

    df['trigger'] = df[triggers[year]].sum(axis=1).astype(bool)
    

    print('len(df)',len(df))

    for nresJets in range(4,np.max(df.nresolvedJets)+1):

        mask = (df['nresolvedJets'] == nresJets)
        if np.sum(mask) == 0: continue
        
        # Use the jet pt and eta functions to define masks
        jpts  = arr[b'resolvedJets_pt'][mask].flatten().reshape(-1, nresJets)
        jetas = arr[b'resolvedJets_eta'][mask].flatten().reshape(-1,nresJets)
        btags = arr[b'resolvedJets_is_DL1r_FixedCutBEff_77'][mask].flatten().reshape(-1,nresJets).astype(bool)
        
        jmask = (jpts > pT_min) & (np.abs(jetas) < eta_max) 
        
        njets = np.sum(jmask,axis = 1)
        df.loc[mask,'njets'] = njets
        
        bmask = jmask & btags
        not_b = jmask & ~btags
        df.loc[mask,'ntag_all'] = np.sum(bmask,axis=1)
        
        # Save HT_all for Lucas 
        df.loc[mask,'HT_all'] = np.sum(jpts,axis=1)
        df.loc[mask,'lead_pt'] = np.max(jpts,axis=1)
        i_lead = np.argmax(jpts,axis=1)
        df.loc[mask,'lead_tag'] = btags[ np.arange(np.sum(mask)), i_lead ]
        
        for nj in range(4,nresJets+1):
            
            m = mask.copy()
            m[mask] = (njets == nj)
            
            if (np.sum(m) == 0) or (np.sum(jmask[njets == nj]) == 0): continue
            
            if mc:
                
                # Only keep calculating the SF up until we have 4 b-jets in the event, so need the array
                # for the tagging decisions
                k = b'resolvedJets_is_DL1r_FixedCutBEff_77'
                is_77 = arr[k][m].flatten().reshape(-1,nresJets)[jmask[njets == nj]].reshape(-1,nj)
                
                # Need to access the first el of the array, b/c the others are for the FTAG eigenvariations
                sf_77 = np.array([[ci[0] for ci in c] for c in arr[f'{pre}_SF_DL1r_FixedCutBEff_77'.encode()][m]])[jmask[njets == nj]].reshape(-1,nj)
                
                # However, we want to do the jet selection based on corrected jet pT, so reorder is_77 and sf_77
                # based on the jet pT
                pt = arr[b'resolvedJets_pt'][m].flatten().reshape(-1,nresJets)[jmask[njets == nj]].reshape(-1,nj)
                idx_sort = np.argsort(pt,axis=1)[:,::-1]
                
                N = np.sum(m)
                is_77 = np.vstack([is_77[np.arange(np.sum(N)),idx_sort[:,i]] for i in range(nj)]).T
                sf_77 = np.vstack([sf_77[np.arange(np.sum(N)),idx_sort[:,i]] for i in range(nj)]).T

                # Sort by b-tag decision (there's some index trickery b/c the argsort sorts in descending order,
                # and when b-tag decision didn't discriminate b/w jets I wanted to take the leading one)
                rev_idx = np.arange(nj)[::-1]
                idx_b4 = rev_idx[np.argsort(is_77[:,::-1],axis=1)[:,-4]]
                
                # This logic is only valid for 4b rn, so only consider events where the 4th b is the 4th el
                # of the array
                sf_evt = np.ones_like(idx_b4).astype(float)
                for i in range(3,nj):
                    sf_evt[idx_b4==i] = np.prod(sf_77[idx_b4==i,:i+1],axis=1)
                df.loc[m,'mc_sf'] = sf_evt
                
                # W/ fewer than 4 b-tags, multiply all the SFs 
                df.loc[m & (df.ntag_all < 4),'mc_sf'] = np.prod( sf_77[np.sum(is_77,axis=1)<4], axis=1)
                
            # Xwt        
            #if nj >= 4:
            assert b'resolvedJets_is_DL1r_FixedCutBEff_77' in ks # needed for Xwt calc
            arr_3d = np.dstack([arr[k][m].flatten().reshape(-1,nresJets)[jmask[njets == nj]].reshape(-1,nj) 
                                for k in ks])
            df.loc[m,'X_wt'] = parallelXwt(arr_3d)
                                    
            '''
            And now fill w/ jet-level info
            '''
            #for ib in range(4,nj+1):
            for ib in range(1,nj+1):
                
                # First sort by b-tag decision - then by jet pT
                jpt_all = arr[b'resolvedJets_pt'][m & (df.ntag_all==ib)].flatten().reshape(-1,nresJets)
                
                mi = (njets==nj ) & (df.loc[mask,'ntag_all']==ib)
                Ni = np.sum(mi)
                
                pt_tag = jpt_all[ bmask[mi] ].reshape(-1,ib)
                i_tag = np.argsort(pt_tag,axis=1)[:,::-1]
                
                if nj > ib:
                    pt_not = jpt_all[ not_b[mi] ].reshape(-1,nj-ib) 
                    i_not = np.argsort(pt_not,axis=1)[:,::-1]
                    
                for v,w in zip(jVars[:5], jVars[:4]+['Quantile_DL1r_Continuous']):
                    k = f'resolvedJets_{w}'.encode()
                        
                    total = arr[k][m& (df.ntag_all==ib)].flatten().reshape(-1,nresJets)
                    v_tag = total[ bmask[mi] ].reshape(-1,ib)
                    v_tag_sort = np.vstack([v_tag[np.arange(Ni),i_tag[:,iit]] for iit in range(ib)]).T
                        
                    if nj > ib:
                        v_not = total[ not_b[mi] ].reshape(-1,nj-ib)
                        v_not_sort = np.vstack([v_not[np.arange(Ni),i_not[:,iin]] 
                                                for iin in range(nj-ib)]).T
                        jarr = np.concatenate([v_tag_sort ,v_not_sort],axis=1)
                    else:
                        jarr = v_tag_sort
                            
                    #if nj <= 4:
                    #    df.loc[m & (df.ntag_all==ib),[f'j{i}_{v}' for i in range(nj)]] = jarr     
                    #else:
                    df.loc[m & (df.ntag_all==ib),[f'j{i}_{v}' for i in range(4)]] = jarr[:,:4]    
                
            print(f'  Jet sfs for evts with {nj} selected jets out of {nresJets} resolved jets')

    print('len(df)',len(df))
        
    if mc:
        df['mc_sf'] *= (df['mcEventWeight'] * df['weight_pileup']) 
        df['run_number'] = df['rand_run_nr']
        
        # Also save the truth info
        if truth:
            truthInfo(tree, df,entrystart=entrystart,entrystop=entrystop)
            parallelTruthMatchJets(df, 4)
            parallelGetCorrectPair(df, 4)
        
    df['ntag']  = np.sum(df[[f'j{i}_Db'   for i in range(4)]] >= 3, axis=1)
    df['GNNJets'] = np.sum(df[[f'j{i}_pt' for i in range(4)]] != 0, axis=1)
    
    return df


def parallelXwt(arr_3d):
    '''
    Input: 
    - arr_3d: An array of shape nEvents,nJets,nFeatures where the features are assumed
              to be (pt, eta, phi, E, b-tag boolean)
    '''
    
    nEvt, nj, nFeat = arr_3d.shape
    
    ps = [ TLorentzVectorArray.from_ptetaphie(*(arr_3d[:,i,:4].T)) for i in range(nj) ]
    
    Xwt_arr = np.zeros((nEvt,nj,int(comb(nj-1,2))))

    for ib, bjet  in enumerate(ps):

        # Get the jets valid for reconstructing the W-candidate
        other = [pi for i,pi in enumerate(ps) if i != ib]
        for j, (wjet1, wjet2) in enumerate(combinations(other,2)):

            W = wjet1 + wjet2

            Xwt = getXhh(W, bjet+W, mlead_mean=80.4, msubl_mean=172.5) 
            Xwt_arr[:,ib,j] = Xwt


    # Only consider b-tagged jets as the b from the top decay 
    mask = arr_3d[:,:,-1].astype(bool)
    #mask[mask.sum(axis=1)>4,-1] = False
    mask = mask.reshape(nEvt,nj,1)
    
    Xwt_arr = np.where(~mask, np.inf, Xwt_arr) 

    return np.min(Xwt_arr.reshape(nEvt,-1),axis=1)



'''
Goal: Implement the jet -> np step and the truth label assignment
'''
def truthInfo(tree, df, entrystart=None, entrystop=None):

    print('Retriving truth id')
    pdg     = np.vstack(tree.array('truth_pdgId',  entrystart=entrystart,entrystop=entrystop))
    barcode = np.vstack(tree.array('truth_barcode',entrystart=entrystart,entrystop=entrystop))
    pbarc   = tree.array('truth_parent_barcode',   entrystart=entrystart,entrystop=entrystop)
    
    h_barcodes = barcode[pdg == 25].reshape(-1,2)
    
    print('Getting the b-quark parent\'s barcodes')
    b_parent_barcodes = np.vstack([[x[0] for pdg,x in zip(pdgs,ps) if np.abs(pdg) == 5] \
                                for pdgs,ps in zip(pdg,pbarc) ])
    
    print('b-quark kinematics')
    pt  = np.vstack(tree.array('truth_pt', entrystart=entrystart,entrystop=entrystop))
    eta = np.vstack(tree.array('truth_eta',entrystart=entrystart,entrystop=entrystop))
    phi = np.vstack(tree.array('truth_phi',entrystart=entrystart,entrystop=entrystop))
    E   = np.vstack(tree.array('truth_E',  entrystart=entrystart,entrystop=entrystop))
    
    h_pt =  pt[pdg == 25].reshape(-1,2)
    h_eta = eta[pdg == 25].reshape(-1,2)
    h_phi = phi[pdg == 25].reshape(-1,2)
    h_E   = E[pdg == 25].reshape(-1,2)
    
    b_pt =  pt[np.abs(pdg) == 5].reshape(-1,4)
    b_eta = eta[np.abs(pdg) == 5].reshape(-1,4)
    b_phi = phi[np.abs(pdg) == 5].reshape(-1,4)
    b_E   = E[np.abs(pdg) == 5].reshape(-1,4)
    
    cols =  [f'h{i}_{v}' for i in range(2) for v in ['pt','eta','phi','E','barcode']]
    cols += [f'b{i}_{v}' for i in range(4) for v in ['pt','eta','phi','E','parent_barcode']]
    for c in cols: df[c] = 0
        
    # Ok - maybe I just need the b-quark eta, phi and parent parcodes?
    print('Saving info to the dfs')
    df[[f'h{i}_pt'      for i in range(2)]] = h_pt
    df[[f'h{i}_eta'     for i in range(2)]] = h_eta
    df[[f'h{i}_phi'     for i in range(2)]] = h_phi
    df[[f'h{i}_E'       for i in range(2)]] = h_E
    df[[f'h{i}_barcode' for i in range(2)]] = h_barcodes

    df[[f'b{i}_pt'  for i in range(4)]] = b_pt
    df[[f'b{i}_eta' for i in range(4)]] = b_eta
    df[[f'b{i}_phi' for i in range(4)]] = b_phi
    df[[f'b{i}_E'   for i in range(4)]] = b_E
    df[[f'b{i}_parent_barcode' for i in range(4)]] = b_parent_barcodes
    
    # For reweighting later save truth m_HH as well - Is there a way to speed this up?
    print('Calculating the truth m_hh')
    b0 = TLorentzVectorArray.from_ptetaphi(b_pt[:,0], b_eta[:,0], b_phi[:,0], b_E[:,0])
    b1 = TLorentzVectorArray.from_ptetaphi(b_pt[:,1], b_eta[:,1], b_phi[:,1], b_E[:,1])
    b2 = TLorentzVectorArray.from_ptetaphi(b_pt[:,2], b_eta[:,2], b_phi[:,2], b_E[:,2])
    b3 = TLorentzVectorArray.from_ptetaphi(b_pt[:,3], b_eta[:,3], b_phi[:,3], b_E[:,3])

    df['truth_mhh'] = (b0+b1+b2+b3).mass
    df['truth_pthh'] = (b0+b1+b2+b3).pt
    
def parallelTruthMatchJets(df,nJetsMax=6):
    '''
    '''
    
    cols = [f'b{i}_{v}'  for i in range(4) for v in ['jidx','drMatch']]
    cols += [f'j{i}_{v}' for i in range(nJetsMax) for v in ['bidx']]
    for c in cols: df[c] = -1
    for c in [f'j{i}_drMatch' for i in range(nJetsMax)]: df[c] = 10
        
    for njets in range(4,nJetsMax+1):
    
        print('njets = ',njets)
        if njets == nJetsMax:
            mask = (df.njets >= njets)
        else:
            mask = (df.njets == njets)
    
        betas = df.loc[mask,[f'b{i}_eta' for i in range(4)]].values.reshape(-1,4,1)
        jetas = df.loc[mask,[f'j{i}_eta' for i in range(njets)]].values.reshape(-1,1,njets)
        
        bphis = df.loc[mask,[f'b{i}_phi' for i in range(4)]].values.reshape(-1,4,1)
        jphis = df.loc[mask,[f'j{i}_phi' for i in range(njets)]].values.reshape(-1,1,njets)
    
        deta = betas-jetas
        dphi = np.arccos(np.cos(bphis-jphis))

        D = np.sqrt(deta**2 + dphi**2)
        
        idx = np.argmin(D,axis=-1)
        dr_match = np.min(D,axis=-1)

        df.loc[mask,[f'b{i}_jidx' for i in range(4)]] = idx
        df.loc[mask,[f'b{i}_drMatch' for i in range(4)]] = dr_match
        
        # Save the bidx for each jet,
        # where -1 will correspond to no truth matched jets - and perhaps
        for i in range(njets):
    
            matched = np.sum(idx==i,axis=-1).astype(bool)
        
            matchedMask = np.zeros_like(mask).astype(bool)
            matchedMask[mask] = matched
            
            df.loc[matchedMask,f'j{i}_bidx'] = np.argmax(idx[matched]==i,axis=1)

    '''
    Assign the jet the dr of the closest matched b-quark
    '''
    idx = df[[f'b{i}_jidx' for i in range(4)]].values
    dr_match = df[[f'b{i}_drMatch' for i in range(4)]].values
    for nj in range(nJetsMax):    
        for nm in range(1,np.max(np.sum(idx==nj,axis=1))+1):
            nMatches = (np.sum(idx==nj,axis=1)==nm)
            y = dr_match[(idx==nj) & nMatches.reshape(-1,1)]
            #ji_drMatch[nMatches,nj] = y if nm ==1 else np.min(y.reshape(-1,nm),axis=1)
            df.loc[nMatches,f'j{nj}_drMatch'] = y if nm ==1 else np.min(y.reshape(-1,nm),axis=1)
            
def goodJets(df,Rmatch=0.3):
    '''
    For a df, get a column for the # of unique jets with dRmatch < 0.3.
    '''
    
    bi_jidx = df[[f'b{i}_jidx' for i in range(4)]].values
    bi_drMatch = df[[f'b{i}_drMatch' for i in range(4)]].values
    
    unique = np.ones_like(df.index).astype(bool)

    b_parent_barcodes = df[[f'b{i}_parent_barcode' for i in range(4)]].values
    N = b_parent_barcodes.shape[0]

    df['sameParent'] = False

    # Using np.unique didn't work - so I just ended up needing to check the
    # combinations manually
    for i,j in combinations(range(4),2):
        
        mask = bi_jidx[:,i] == bi_jidx[:,j]
        unique[mask] = False

        # If two b-quarks were matched to the same jet, check if they came from the same HC
        mask = mask & (b_parent_barcodes[:,i] == b_parent_barcodes[:,j])
        df.loc[mask,'sameParent'] = True

    dRmatch = ~ np.sum(bi_drMatch > Rmatch, axis=1).astype(bool)
    
    df['unique']   = unique
    df['dRmatch']  = dRmatch
    df['goodJets'] = dRmatch & unique
    
        
def parallelGetCorrectPair(df,nJetsMax=6):
    
    df['correctPair'] = -1
    goodJets(df)

    b_parent_barcodes = df[[f'b{i}_parent_barcode' for i in range(4)]].values
    N = b_parent_barcodes.shape[0]

    for njets in range(4,nJetsMax+1):

        # Get the mask for when we actually have njets
        if njets == nJetsMax:
            mask = (df.njets >= njets)
        else:
            mask = (df.njets == njets)
        mask = mask & (df.goodJets)
        
        i = 0
        
        for i0,i1,i2,i3 in combinations(range(njets),4):
            
            # Get parent b-quark barcodes
            j0_parent = b_parent_barcodes[np.arange(N),df[f'j{i0}_bidx'].values]
            j1_parent = b_parent_barcodes[np.arange(N),df[f'j{i1}_bidx'].values]
            j2_parent = b_parent_barcodes[np.arange(N),df[f'j{i2}_bidx'].values]
            j3_parent = b_parent_barcodes[np.arange(N),df[f'j{i3}_bidx'].values]
            
            # Also - make sure all of the i0,i1,i2,i3 correspond to *matched* jets (i.e, not -1)
            matched = np.all(df[[f'j{ii}_bidx' for ii in [i0,i1,i2,i3]]].values>=0,axis=1)
            
            # Loop through the pairings
            df.loc[mask & matched & (j0_parent == j1_parent) & (j2_parent == j3_parent), 'correctPair'] = i
            df.loc[mask & matched & (j0_parent == j2_parent) & (j1_parent == j3_parent), 'correctPair'] = i+1
            df.loc[mask & matched & (j0_parent == j3_parent) & (j1_parent == j2_parent), 'correctPair'] = i+2
            
            i += 3

def getNumPairs(n):
    '''
    Get the number of valid pairs given n jets
    '''
    return int( np.prod( np.arange(n, n-4, -1) ) / 8 )

def takeLog(X,mask,featureIndices=[0,3]):
    '''
    Take the log of the (unmaked) features indicated by featureIndices
    
    Inputs:
    - X: Data in the form of (events, jets, features)
    - mask: Indicates the unmasked values of X
    - featureIndices: the indices indicating which vars to take the log of
    
    Modifies the data in place
    '''

    for vi in featureIndices:
        X[:,:,vi][mask] = np.log(X[:,:,vi][mask])

        # Sometime the mass goes slightly negative - so we just need to correct this
        X[:,:,vi] = np.nan_to_num(X[:,:,vi])
        X[:,:,vi][mask] = np.where(X[:,:,vi][mask]>0,X[:,:,vi][mask], 1e-8)

def scale(X, mask, featureIndices=[0,3],var_names=None, filename='test.json',savevars=True):
    '''
    Inputs:
    - X: Data in the form of (events, jets, features)
    - mask: Indicates the unmasked values of X
    - featureIndices: the indices indicating which vars to take the log of
    - var_names:
    - fileame: Name of the scaling file to write the output to
    - savevars:

    Modifies the data in place

    Reference: Taken from Micky's dataprocessing.py file in
    https://github.com/mickypaganini/RNNIP
    '''

    scale = {}

    if var_names is None:
        var_names = [jetVars[i] for i in featureIndices]

    if savevars:

        for vi,v in zip(featureIndices,var_names):
            print(f'Scaling {v}.')
            f = X[:, :, vi]
            slc = f[mask]
            m, s = slc.mean(), slc.std()
            slc -= m
            slc /= s
            X[:, :, vi][mask] = slc.astype('float32')
            scale[v] = {'mean' : float(m), 'sd' : float(s)}

        with open(filename, 'w') as varfile:
            json.dump(scale, varfile)

    else:
        with open(filename, 'r') as varfile:
            varinfo = json.load(varfile)

        for vi,v in zip(featureIndices,var_names):
            print(f'Scaling {v}.')
            f = X[:, :, vi]
            slc = f[mask]
            m = varinfo[v]['mean']
            s = varinfo[v]['sd']
            slc -= m
            slc /= s
            X[:, :, vi][mask] = slc.astype('float32')

def transformData(inputFile='', scalingFile='scale_SMNR_PFlow-FEB2019-5jets.json',
                  df=None, nSelectedJets=5, log_cutoff=40, PT_2b="Db_njets_4b_CR.json"):
    '''
    Apply the scaling for the jet variables, and return the df.
    '''
    if len(inputFile) == 0:
        print("Error: Cannot scale an empty input set: Returning")
        return
    
    # Load in the df 
    if df is None: df = pd.read_hdf(inputFile,key='df')

    X_all = np.dstack([df[[f'j{i}_{v}' for i in range(nSelectedJets)]].values for v in jetVars])
    m_all =  ~ np.all(X_all==0, axis=-1)

    #df['GNNjets'] = np.sum(m_all,axis=-1) 
    
    # pT and E normalization
    for vi in [0,3]:
        X_all[:,:,vi][m_all] = np.log(X_all[:,:,vi][m_all]-log_cutoff)
    scale(X_all,m_all,filename=scalingFile,savevars=False)

    # Center Db
    if 'Db' in jetVars:
        X_all[:,:,-1][m_all] -= 3 # shift Db bins

    # save the output columns
    for vi,v in enumerate(jetVars):
        for i in range(nSelectedJets):
            df[f'ml_j{i}_{v}'] = X_all[:,i,vi]

    '''
    This is just for 2b (I think)
    # But - I'm going to want to pseudo-tag the jets in 2b events using the 
    # 4 tag template that I made!
    Db_cols = [f'ml_j{i}_Db' for i in range(nSelectedJets)]
    # Load in the json file
    with open(PT_2b, 'r') as varfile:
        Db_4b = json.load(varfile)
    njetsRange = [int(k) for k in Db_4b.keys()]
    for k,v in Db_4b.items():
        nj = int(k)
        if nj == njetsRange[-1]:
            njmask = (df.njets >= nj)
        else:
            njmask = (df.njets == nj)
        evtMask = njmask & (df.ntag == 2)
        for stop in range(4,nSelectedJets+1):
            N = np.sum(evtMask & (df.njets==stop))
            for c in Db_cols[2:stop]:
                df.loc[evtMask & (df.njets==stop),c] = np.random.choice(np.arange(-1.5,3.5), N, p=v)
    '''

    outputFile = inputFile[:-3] + "_scaledInputs.h5"
    print("Saving ",outputFile)
    df.to_hdf(outputFile, key='df', mode='w')

    return df

def visualizeInputs(X, X_norm, ws, jetVars, subDir):
    '''
    Visualize the normal and scaled inputs
    
    Inputs:
    - X:
    - X_norm:
    - ws: The mc_sf event weight.
    - jetVars: The input features to plot
    '''
    
    fig,axes = plt.subplots(2,len(jetVars),figsize=(3.5*len(jetVars),7))

    mask = ~ np.all(X==0, axis=-1)

    myRanges = [(0,500),(-2.5,2.5),(-np.pi,np.pi),(0,600),[0.5,5.5]]
    myBins = [25]*4 + [5]
    for vi,(v,ax,r,n) in enumerate(zip(jetVars,axes[0],myRanges,myBins)):
        ax.hist(X[:,:,vi][mask], n, range=r, histtype='step', weights=ws[mask])
        ax.set_xlabel('jet '+v)

    for vi,(v,ax,r,n) in enumerate(zip(jetVars,axes[1],[(-3,3)]+myRanges[1:3]+[(-3,3),(-2.5,2.5)],myBins)): 
        ax.hist(X_norm[:,:,vi][mask], n, range=r, histtype='step', weights=ws[mask])
    
        if (v == 'pt') or (v == 'E'): 
            xlabel='normalized log '
        elif v == 'Db':
            xlabel = 'shifted '
        else:
            xlabel = ''
        xlabel += f"jet {v}"
        ax.set_xlabel(xlabel)   

    inptStr = "_".join(jetVars)
    plt.subplots_adjust()
    plt.savefig(f'figures/{subDir}/inputs_{inptStr}.pdf',bbox_inches='tight')
    

def prepareData(nSelectedJets=5, jetVars = ['pt','eta','phi','E','Db'],
                physicsSample='SMNR',mcs=['mc16a','mc16d','mc16e'],prodTag='AUG2019',
                pTcut=40,ntag=3,trainingEvents='odd',split='eventNumber'):
    '''
    Goal: Get the dataframes in a format where it's easy to load them into
    a single Dataset.
    
    1. For each df, split it into a train + test set
    2. Concatenating all the sets together and normalize the inputs
    3. Save these xformed datasets for reference as well
    '''
    
    # Step 1: Load in the DataFrame
    dfs = []
    for mc in mcs:

        key = f'{physicsSample}_{mc}_PFlow-{prodTag}'
        fDir = f'../data/{key}'
        if nSelectedJets != 4:
            fDir += f'-{nSelectedJets}jets'
        if pTcut != 40:
            fDir += f'-{pTcut}GeV'
        
        mc_filename = f"{fDir}/df_{ntag}b.h5"
        if not os.path.exists(mc_filename):
            
            # Process the MNTs and *save*
            print(mc_filename, 'does not exist: calling processDf()')

            # Get the MNT name
            dis = []
            for filename in glob(fileDir[key]+'*.root'):
                # Process the df
                di = processDf(filename,nJetsMax=nSelectedJets,pT_min=pTcut,year=mcToYr[mc])
                dis.append( di )

            # Save the files from a single mc campaign
            dfi = pd.concat(dis)

            # And save for future use!
            dfi[(dfi.njets >=4) & (dfi.ntag >= ntag)].to_hdf(mc_filename,key='df')

        else:
            dfi = pd.read_hdf(mc_filename,key='df')
            
        # Concat onto the list of dfs corresponding to the mc campaigns
        dfs.append(dfi)
        
    df = pd.concat(dfs)
        
    X = np.dstack([df.loc[df.correctPair!=-1,['j{}_{}'.format(i,v) for i in range(nSelectedJets)]].values for v in jetVars])
    y = df.loc[df.correctPair!=-1, 'correctPair'].values
    w = df.loc[df.correctPair!=-1, 'mc_sf'].values

    # Step 2: Normalize the features (classic ml preprocessing)
    mask = ~ np.all(X==0, axis=-1)
    X_norm = X.copy()

    # pT and E: Take the log before normalizing
    #takeLog(X_norm,mask,featureIndices=[0,3])
    for vi in [0,3]:
        X_norm[:,:,vi][mask] = np.log(X_norm[:,:,vi][mask]-pTcut)

    mc = 'mc16' + ''.join([m[-1] for m in mcs])
    trainDir = f'{physicsSample}_{mc}_PFlow-{prodTag}-{nSelectedJets}jets'
    scalingFile = f"scale_{trainDir}.json"
    scale(X_norm,mask,filename=scalingFile,savevars=True)

    # Db: Just shift st the bins are centered at 0
    X_norm[:,:,-1][mask] -= 3

    # Step 3: Visualize the raw features and the normalized ones
    ws = df.loc[df.correctPair != -1,['mc_sf']*nSelectedJets].values
    visualizeInputs(X,X_norm,ws,jetVars,trainDir)
    
    df = df[df.correctPair != -1]
    for vi,v in enumerate(jetVars):
        for i in range(nSelectedJets):
            df[f'ml_j{i}_{v}'] = X_norm[:,i,vi]
    
    # Step 4: Save the training dataset 
    fDir = f'../data/{trainDir}/'
    #outputFile = f"{fDir}/df_{ntag}b_scaledInputs_train.h5"
    outputFile = f"{fDir}/df_{ntag}b_scaledInputs_{trainingEvents}.h5"
    print('Saving',outputFile)
    if trainingEvents == 'odd':
        df[df[split]%2 == 1].to_hdf(outputFile, key='df', mode='w')
        #df[df.index%2 == 1].to_hdf(outputFile, key='df', mode='w')
    elif trainingEvents == 'even':
        df[df[split]%2 == 0].to_hdf(outputFile, key='df', mode='w')
        #df[df.index%2 == 0].to_hdf(outputFile, key='df', mode='w')
    else:
        df.to_hdf(outputFile, key='df', mode='w')
    
    # Step 5: Save the rest of the dfs with the ml scaled inputs separately as well
    for mc in mcs:
        fDir = f'../data/{physicsSample}_{mc}_PFlow-{prodTag}-{nSelectedJets}jets/'
        _ = transformData(fDir+f"df_{ntag}b.h5", scalingFile,nSelectedJets=nSelectedJets)
    

def getDataLoaders(batch_size=1024,trainFrac=.8):
    '''
    Return the DataLoaders for my pytorch function.
    
    Input: 
    - myFile: The name of the hdf5 file with the pandas Dataframe
    - cols: The input variables of interest
        -batch_size: default 128
    -N: Number of events to load in, the default value of -1  
 
    Returns: loader_train, loader_val, loader_test
        DataLoaders for the train, val, and test sets
    '''
        
    dset = rwDataset(df_2b,df_3b)
     
    N = len(dset)
    trainEvts = int(.8 * len(dset))
    
    train,val = torch.utils.data.random_split(dset, [trainEvts,N-trainEvts])

    loader_train = DataLoader(train, batch_size=batch_size, shuffle=True)
    loader_val   = DataLoader(val, batch_size=batch_size, shuffle=True)
    
    return loader_train, loader_val

def getHCs(df,model,nSelectedJets,device='cpu',masked=True):
    '''
    Goal: Get HCs from the pairing alg
    '''
    
    scoreToIdx =  {njets:[[[h11,h12], [h21, h22]] for i0,i1,i2,i3 in combinations(range(njets),4) \
                   for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)])] \
                   for njets in range(4,nSelectedJets+1)}

    X_test = np.dstack([df[['ml_j{}_{}'.format(i,v) for i in range(nSelectedJets)]].values for v in jetVars])
    njets = np.sum(~ np.all(X_test==0, axis=-1),axis=1) 

    '''
    Modify the 2b data in X_test *if* a Db 4b templates have been passed!
    '''

    X_test = torch.tensor(X_test).float().to(device)
    model.to(device)
    model.eval()
    
    if masked:
        
        y_pred = torch.zeros(len(df.index)).long()
        max_prob = torch.zeros(len(df.index)).float()
        print('max(njets)',df.GNNJets.max())        

        for nj in range(4,nSelectedJets+1):
            m = (df.GNNJets == nj).values
            logits, _ = model(X_test[m,:nj])
            probs = F.softmax(logits,1)
            max_prob[m], y_pred[m] = torch.max(probs,1)
        df['max_prob'] = max_prob.detach().numpy() 
    else:

        # Get the mask for the scores
        ttlPairs = getNumPairs(nSelectedJets)
        scoreMask = np.ones((X_test.shape[0],ttlPairs)).astype(bool)
        
        for ji in range(4,nSelectedJets):
            nPairs = getNumPairs(ji)
            scoreMask[njets==ji] *= np.array([True]*nPairs + [False]*(ttlPairs-nPairs))
        
        # Make them pytorch tensors
        scoreMask = torch.tensor(scoreMask).to(device)
        
        logits, _ = model(X_test)
        logits[~scoreMask] =  float('-inf') #0
        _, y_pred = torch.max(logits,1)

    y_pred = y_pred.cpu().numpy()
    df['predPair'] = y_pred

    for c in [f'idx_HC{hci}_j{ji}' for hci in ['a','b'] for ji in range(2)]: 
        df[c] = 0

    indices = np.array([scoreToIdx[nj][yi] for nj, yi in zip(njets,y_pred)])
    df[[f'idx_HCa_j{ji}' for ji in range(2)]] = indices[:,0]
    df[[f'idx_HCb_j{ji}' for ji in range(2)]] = indices[:,1]

    pts  = df[[f'j{i}_pt'  for i in range(nSelectedJets)]].values
    etas = df[[f'j{i}_eta' for i in range(nSelectedJets)]].values
    phis = df[[f'j{i}_phi' for i in range(nSelectedJets)]].values
    Es   = df[[f'j{i}_E'   for i in range(nSelectedJets)]].values

    print('Getting jet pts')
    ja0_pt = [vs[i] for i, vs in zip(df.idx_HCa_j0, pts)]
    ja1_pt = [vs[i] for i, vs in zip(df.idx_HCa_j1, pts)]
    jb0_pt = [vs[i] for i, vs in zip(df.idx_HCb_j0, pts)]
    jb1_pt = [vs[i] for i, vs in zip(df.idx_HCb_j1, pts)]
    
    print('Getting jet etas')
    ja0_eta = [vs[i] for i, vs in zip(df.idx_HCa_j0, etas)]
    ja1_eta = [vs[i] for i, vs in zip(df.idx_HCa_j1, etas)]
    jb0_eta = [vs[i] for i, vs in zip(df.idx_HCb_j0, etas)]
    jb1_eta = [vs[i] for i, vs in zip(df.idx_HCb_j1, etas)]
    
    print('Getting jet phis')
    ja0_phi = [vs[i] for i, vs in zip(df.idx_HCa_j0, phis)]
    ja1_phi = [vs[i] for i, vs in zip(df.idx_HCa_j1, phis)]
    jb0_phi = [vs[i] for i, vs in zip(df.idx_HCb_j0, phis)]
    jb1_phi = [vs[i] for i, vs in zip(df.idx_HCb_j1, phis)]
    
    print('Getting jet Es')
    ja0_E = [vs[i] for i, vs in zip(df.idx_HCa_j0, Es)]
    ja1_E = [vs[i] for i, vs in zip(df.idx_HCa_j1, Es)]
    jb0_E = [vs[i] for i, vs in zip(df.idx_HCb_j0, Es)]
    jb1_E = [vs[i] for i, vs in zip(df.idx_HCb_j1, Es)]

    ja0 = TLorentzVectorArray.from_ptetaphi(ja0_pt, ja0_eta, ja0_phi, ja0_E)
    ja1 = TLorentzVectorArray.from_ptetaphi(ja1_pt, ja1_eta, ja1_phi, ja1_E)

    jb0 = TLorentzVectorArray.from_ptetaphi(jb0_pt, jb0_eta, jb0_phi, jb0_E)
    jb1 = TLorentzVectorArray.from_ptetaphi(jb1_pt, jb1_eta, jb1_phi, jb1_E)
    
    HCa = ja0 + ja1
    HCb = jb0 + jb1

    return HCa, HCb, ja0, ja1, jb0, jb1

def lead_dRjj_cut(dRjj, m4j):
    '''
    Given the opening angle of the constituents of the leading HC
    (ordered by the SS of the constituents), return true or false
    for whether or not the opening angle is within the envelope 
    given by the 4 jet invariant mass.

    Inputs:
    - dRjj: A np array of shape (nEvts,)
    - m4j: A np array of shape (nEvs,) 
    '''

    mask = np.ones_like(dRjj).astype(bool)
    
    # low mass cut
    mask[(m4j < 1250) & (dRjj < 360 / m4j - 0.5)] = False
    mask[(m4j < 1250) & (dRjj > 652.863 / m4j + 0.474449)] = False

    # high mass cut
    mask[(m4j > 1250) & (dRjj < 0)] = False
    mask[(m4j > 1250) & (dRjj > 0.9967394)] = False

    return mask

def subl_dRjj_cut(dRjj, m4j):
    '''
    dR cut for the subleading HC
    '''

    mask = np.ones_like(dRjj).astype(bool)

    # low mass cut
    mask[(m4j < 1250) & (dRjj < 235.242 / m4j + 0.0162996)] = False
    mask[(m4j < 1250) & (dRjj > 874.890 / m4j + 0.347137)] = False

    # high mass cut
    mask[(m4j > 1250) & (dRjj < 0)] = False
    mask[(m4j > 1250) & (dRjj > 1.047049)] = False

    return mask

def MDR_cut(lead_dR_arr, subl_dR_arr, m4j):
    '''
    Determine which of the pairings satisfy the MDR cut

    Inputs:
    - lead_dRarr: Array of the opening angle between the jets of the leading HC
    - subl_dRarr: Array of the opening angle between the jets of the subleading HC
    - m4j: The invariant mass of the 4-jets

    Output:
    - mask: A boolean np array for
    '''
    lead_mask = lead_dRjj_cut(lead_dR_arr, m4j)
    subl_mask = subl_dRjj_cut(subl_dR_arr, m4j)
    mask = lead_mask & subl_mask
    
    return mask

def MDpT_cut(df):
    '''
    Apply the pT sort and derive the corresponding MDpT mask
    '''

    df['lead_HC_pt'] = np.where(df.pT_h1>df.pT_h2, df.pT_h1, df.pT_h2)
    df['subl_HC_pt'] = np.where(df.pT_h1>df.pT_h2, df.pT_h2, df.pT_h1)
    
    lead_pT_cut = 0.513333 * df.m_hh - 103.3333
    subl_pT_cut = 0.33333  * df.m_hh -  73.3333

    df['MDpT'] = (df.lead_HC_pt > lead_pT_cut) & (df.subl_HC_pt > subl_pT_cut) 


def getHelicityAngles(df,lab_hc1,lab_hc2,lab_j11,lab_j12,lab_j21,lab_j22,pTOrdering):
    '''
    Calculate cos(theta*), cos(theta1), cos(theta2), (should I get the others as 
    well)? Given the ...
    
    Note: I haven't decided yet how I want to order the HCs for these helicity angles.
    
    '''

    lab_HH = lab_hc1 + lab_hc2

    boost = - lab_HH.boostp3

    rest_hc1 = lab_hc1._to_cartesian().boost(boost)
    rest_hc2 = lab_hc2._to_cartesian().boost(boost)

    # Theta* (or maybe each possible permutation?)
    df['cosThetaStar'] = np.cos(rest_hc1.theta)

    '''
    Calculate theta1 by booosting hc2 and j11 into the rest frame of hc1
    '''
    boost1 = - lab_hc1.boostp3
    
    hc2s = lab_hc2._to_cartesian().boost(boost1).p3
    j11s = lab_j11._to_cartesian().boost(boost1).p3
    j12s = lab_j12._to_cartesian().boost(boost1).p3
    df['cosTheta1'] = - hc2s.dot(j11s) / (hc2s.mag * j11s.mag)
    df['cosTheta12'] = - hc2s.dot(j12s) / (hc2s.mag * j12s.mag)
    
    '''
    Calculate theta2 by booosting hc2 and j11 into the rest frame of hc1
    '''
    boost2 = - lab_hc2.boostp3
    
    hc1s = lab_hc1._to_cartesian().boost(boost2).p3
    j21s = lab_j21._to_cartesian().boost(boost2).p3
    df['cosTheta2'] = - hc1s.dot(j21s) / (hc1s.mag * j21s.mag)

    '''
    Determine which jet to HC combination is correct, and recalculate 
    the correct theta
    '''
    mask = (~pTOrdering)
    
    df.loc[mask,'cosTheta1'] = df.loc[mask,'cosTheta2']
    df.loc[mask,'cosTheta2'] = (- hc2s.dot(j11s) / (hc2s.mag * j11s.mag))[mask]

    '''
    Finally: Phi and Phi1
    '''
    # Step 1: Get the jets in the hh rest frame
    rest_j11 = lab_j11._to_cartesian().boost(boost) 
    rest_j12 = lab_j12._to_cartesian().boost(boost)

    rest_j21 = lab_j21._to_cartesian().boost(boost)
    rest_j22 = lab_j22._to_cartesian().boost(boost)

    rest_j11 = TVector3Array(rest_j11.x, rest_j11.y, rest_j11.z)
    rest_j12 = TVector3Array(rest_j12.x, rest_j12.y, rest_j12.z)

    rest_j21 = TVector3Array(rest_j21.x, rest_j21.y, rest_j21.z)
    rest_j22 = TVector3Array(rest_j22.x, rest_j22.y, rest_j22.z)

    ebeam = 7000 # GeV
    p1 = TLorentzVectorArray([0],[0],[ebeam],[ebeam])
    p2 = TLorentzVectorArray([0],[0],[-ebeam],[ebeam])

    rest_p1 = p1.boost(boost).p3
    rest_p2 = p2.boost(boost).p3 
    
    z = TVector3Array(rest_p1.x, rest_p1.y, rest_p1.z)
    rest_hc1 = TVector3Array(rest_hc1.x, rest_hc1.y, rest_hc1.z)
    
    # Step 2: Get the normal vectors for the 3 planes
    n1  = rest_j11.cross(rest_j12) / rest_j11.cross(rest_j12).mag 
    n2  = rest_j21.cross(rest_j22) / rest_j21.cross(rest_j22).mag 
    nsc = z.cross(rest_hc1) / z.cross(rest_hc1).mag 

    # Step 3: Get the angles
    Phi  = np.arccos(- n1.dot(n2))
    Phi *= rest_hc1.dot(n1.cross(n2)) / np.abs(rest_hc1.dot(n1.cross(n2)))
    df['Phi'] = Phi
    
    Phi1  =  np.arccos(n1.dot(nsc))
    Phi1 *=  rest_hc1.dot(n1.cross(nsc)) / np.abs(rest_hc1.dot(n1.cross(nsc)))
    df['Phi1'] = Phi1

    '''
    Take into account the HC ordering
    '''
    Phi1  =  np.arccos(n2.dot(nsc))
    Phi1 *=  rest_hc1.dot(n2.cross(nsc)) / np.abs(rest_hc1.dot(n2.cross(nsc)))
    df.loc[mask,'Phi1'] = Phi1[mask]

def getRegions(df,HC1,HC2,mask=None):
    '''
    This code block was common for whether or not I considered the jet selection
    along w/ the pairing.
    '''
    # SR, CR, SB
    print('Calculating Xhh')
    if mask is None:
        df['Xhh'] = np.array([getXhh(hc1,hc2) for hc1,hc2 in zip(HC1,HC2)])
        df['kinematic_region'] = -1
        df.loc[df.Xhh < 1.6,'kinematic_region'] = 0 
        CR_mask = (np.sqrt((df.m_h1 - 124)**2 + (df.m_h2-113)**2) < 30) & (df.kinematic_region != 0)
        df.loc[CR_mask,'kinematic_region'] = 1
        SB_mask = (np.sqrt((df.m_h1 - 126)**2 + (df.m_h2-116)**2) < 45) & (df.kinematic_region != 1) & (df.kinematic_region != 0)
        df.loc[SB_mask,'kinematic_region'] = 2

    else:
        df.loc[mask,'Xhh'] = np.array([getXhh(hc1,hc2) for hc1,hc2 in zip(HC1[mask],HC2[mask])])
        df['kinematic_region'] = -1
        df.loc[(df.Xhh < 1.6) & mask,'kinematic_region'] = 0 
        CR_mask = (np.sqrt((df.m_h1 - 124)**2 + (df.m_h2-113)**2) < 30) & (df.kinematic_region != 0)
        df.loc[CR_mask & mask,'kinematic_region'] = 1
        SB_mask = (np.sqrt((df.m_h1 - 126)**2 + (df.m_h2-116)**2) < 45) & (df.kinematic_region != 1) & (df.kinematic_region != 0)
        df.loc[SB_mask & mask,'kinematic_region'] = 2

    
def applyCuts(df, model, nSelectedJets, HC_ordering='scalar_pt'):
    '''
    Goal: Given a trained GNN, and df with the ml scaling applied for the 
    rest of the jet inputs, do the pairing, and get masks for the rest of the 
    analysis cuts. 
    
    Note, in future, I would like to make this function configurable since I want
    to optimize more of the analysis cuts using Bayesian methods.
    
    I also would like to calculate the helicity angles and try to use them for
    S vs B discrimination as well.
    
    '''

    # Get the HCs 
    HCa, HCb, ja0, ja1, jb0, jb1 = getHCs(df,model,nSelectedJets)

    df['m_hh'] = (HCa + HCb).mass
    df['pt_hh'] = (HCa + HCb).pt
    df["m_hh_cor"] = (HCa * 125 / HCa.mass + HCb * 125 / HCb.mass).mass 

    if HC_ordering == 'scalar_pt':
        pTOrdering = (ja0.pt + ja1.pt) > (jb0.pt + jb1.pt)
    elif HC_ordering == 'pt':
        pTOrdering = HCa.pt > HCb.pt
    else:
        print(f"Error: HC_ordering {HC_ordering} not recognized")

    # Save the HC 4-vector information 
    HC1 = TLorentzVectorArray.from_ptetaphim(np.where(pTOrdering,HCa.pt,  HCb.pt),
                                             np.where(pTOrdering,HCa.eta, HCb.eta),
                                             np.where(pTOrdering,HCa.phi, HCb.phi),
                                             np.where(pTOrdering,HCa.mass,HCb.mass))

    HC2 = TLorentzVectorArray.from_ptetaphim(np.where(pTOrdering,HCb.pt,  HCa.pt),
                                             np.where(pTOrdering,HCb.eta, HCa.eta),
                                             np.where(pTOrdering,HCb.phi, HCa.phi),
                                             np.where(pTOrdering,HCb.mass,HCa.mass))

    df['pT_h1']  = HC1.pt 
    df['eta_h1'] = HC1.eta
    df['phi_h1'] = HC1.phi
    df['m_h1']   = HC1.mass

    df['pT_h2']  = HC2.pt 
    df['eta_h2'] = HC2.eta
    df['phi_h2'] = HC2.phi
    df['m_h2']   = HC2.mass

    # Also save the indices for the jets
    idx_hca_j0 = np.where(ja0.pt > ja1.pt, df.idx_HCa_j0, df.idx_HCa_j1)
    idx_hca_j1 = np.where(ja0.pt < ja1.pt, df.idx_HCa_j0, df.idx_HCa_j1) 

    idx_hcb_j0 = np.where(jb0.pt > jb1.pt, df.idx_HCb_j0, df.idx_HCb_j1)
    idx_hcb_j1 = np.where(jb0.pt < jb1.pt, df.idx_HCb_j0, df.idx_HCb_j1) 

    df['idx_HC0_j0'] = np.where( pTOrdering, idx_hca_j0, idx_hcb_j0)
    df['idx_HC0_j1'] = np.where( pTOrdering, idx_hca_j1, idx_hcb_j1)
    df['idx_HC1_j0'] = np.where(~pTOrdering, idx_hca_j0, idx_hcb_j0)
    df['idx_HC1_j1'] = np.where(~pTOrdering, idx_hca_j1, idx_hcb_j1)

    # MDR 
    HCa_dRjj = ja0.delta_r(ja1) 
    HCb_dRjj = jb0.delta_r(jb1)
    df['dRjj_h1'] = np.where(pTOrdering, HCa_dRjj, HCb_dRjj)
    df['dRjj_h2'] = np.where(pTOrdering, HCb_dRjj, HCa_dRjj)

    HCa_dPhi = np.arccos(np.cos(ja0.phi - ja1.phi))
    HCb_dPhi = np.arccos(np.cos(jb0.phi - jb1.phi))
    df['dPhi_h1'] = np.where(pTOrdering, HCa_dPhi, HCb_dPhi)
    df['dPhi_h2'] = np.where(pTOrdering, HCb_dPhi, HCa_dPhi)

    df['MDR'] = MDR_cut(df.dRjj_h1, df.dRjj_h2, df.m_hh)

    # MDpT
    MDpT_cut(df)
    #df['lead_HC_pt'] = np.where(HCa.pt>HCb.pt, HCa.pt, HCb.pt)
    #df['subl_HC_pt'] = np.where(HCa.pt>HCb.pt, HCb.pt, HCa.pt)
    #
    #lead_pT_cut = 0.513333 * df.m_hh - 103.3333
    #subl_pT_cut = 0.33333  * df.m_hh -  73.3333
    #df['MDpT'] = (df.lead_HC_pt > lead_pT_cut) & (df.subl_HC_pt > subl_pT_cut) 
    
    # deta 
    df['abs_deta_hh'] = np.abs(HCa.eta - HCb.eta)
    df['cut_deta_hh'] = (df.abs_deta_hh < 1.5) 
    
    # Xwt
    df['cut_Xwt'] = (df.X_wt > 1.5) 
    
    getRegions(df,HC1,HC2)

    '''
    Add the reweighting cols as well
    '''
    # pt2 and pt4
    pts = np.vstack([ja0.pt,ja1.pt,jb0.pt,jb1.pt]).T
    pts = np.sort(pts,axis=-1)[:,::-1]
    df['pT_2'] = pts[:,1]
    df['pT_4'] = pts[:,3]

    df['eta_i'] = (np.abs(ja0.eta)+np.abs(ja1.eta)+np.abs(jb0.eta)+np.abs(jb1.eta)) / 4

    # Get all the pairs of dR comnbinations
    for pi, j11, j12, j21, j22 in zip(range(3), [ja0,ja0,ja0], [ja1,jb0,jb1],
                                      [jb0,ja1,ja1], [jb1,jb1,jb0]):
                            
        df[f'dRjj_h1_pair{pi}'] = j11.delta_r(j12)
        df[f'dRjj_h2_pair{pi}'] = j21.delta_r(j22)
    
    # Now choose the pair which minimizes the dR
    dR_h1s = df[[f'dRjj_h1_pair{pi}' for pi in range(3)]].values
    dR_h2s = df[[f'dRjj_h2_pair{pi}' for pi in range(3)]].values
    dR_mins  = np.where(dR_h1s < dR_h2s, dR_h1s, dR_h2s)
    dR_other = np.where(dR_h1s < dR_h2s, dR_h2s, dR_h1s)

    i_min = np.argmin(dR_mins,axis=1)

    df['dRjj_1'] = dR_mins[ np.arange(len(df.index)), i_min ] 
    df['dRjj_2'] = dR_other[np.arange(len(df.index)), i_min ]

    # Calculate the helicity angles as well
    getHelicityAngles(df, HCa, HCb, ja0, ja1, jb0, jb1, pTOrdering)

    # Add HT for getting the systematic
    df['HT'] = ja0.pt + ja1.pt + jb0.pt + jb1.pt

    # Change the naming convention for some of the vars
    df['event_number'] = df['eventNumber']
    if 'runNumber' in df.columns:
        df['run_number'] = df['runNumber']


def pairAndProcess(inputFile: str, ntag=3,
                   GNNParams: dict = {'physicsSample': 'SMNR',
                                      'mc': 'mc16ade',
                                      'prodTag': 'AUG2019',
                                      'nSelectedJets': 5,
                                      'nLayers': 1,
                                      'embed_dim': 20, 
                                      'ff_dim': 20,
                                      'nHeads': 4,
                                      'dpt': 0.3,
                                      'lr': 0.01,
                                      'epoch': 19},
                   cols: list = ["run_number","event_number","mc_sf","ntag","ntag_all","njets",
                                 "kinematic_region","m_hh","pt_hh","X_wt","m_hh_cor",
                                 "pT_h1","eta_h1","phi_h1","m_h1","dRjj_h1","dPhi_h1",
                                 "pT_h2", "eta_h2", "phi_h2","m_h2","dRjj_h2","dPhi_h2",
                                 "pT_2", "pT_4", "eta_i", "dRjj_1", "dRjj_2",
                                 "cosThetaStar", "cosTheta1", "cosTheta2", "Phi", "Phi1",
                                 'ml_j0_Db','ml_j1_Db','ml_j2_Db','ml_j3_Db','ml_j4_Db',
                                 'idx_HC0_j0','idx_HC0_j1','idx_HC1_j0','idx_HC1_j1',
                                 "HT", "MDR",  "MDpT", "max_prob", "abs_deta_hh",
                                 "HT_all","lead_pt","lead_tag"],
                   dataSample=None,dataProdTag=None,year=None,
                   save=False,fileTag='',truth=False):
    '''
    Given a MNT, select the desired jets, do the GNN preprocessing, and apply 
    the analysis cuts and return a df with a given # of btags in a specified 
    kinematic region with only a subset of the columns.
    '''    
    from trainNet import pairAGraph

    # Step 0: Apply the jet selection
    parts = inputFile.split('.')
    periodTag = parts[7]
    fileNumTag = parts[-3]
    
    if dataSample is None: dataSample = parts[8]
    if dataProdTag is None: dataProdTag = inputFile.split('-')[1]
    
    if year is None: year = 2000+int(dataSample[-2:])
    print('dataSample',dataSample)
    is_mc = False if ('data' in dataSample) else True
    print('is_mc',is_mc)
    df = processDf(inputFile, nJetsMax=GNNParams['nSelectedJets'], pT_min=40, year=year,
                   mc=is_mc,truth=truth)
    
    # Apply 4-jets, trigger and b-tagging cuts
    if ntag == 2:
        bmask = (df.ntag == ntag) 
    else:
        bmask = (df.ntag >= ntag) 
    df = df[(df.njets>=4) & bmask & df.trigger]

    # Step 1: xform the data 
    physicsSample, mc = GNNParams['physicsSample'], GNNParams['mc']
    prodTag, nSelectedJets = GNNParams['prodTag'], GNNParams['nSelectedJets']
    trainingEvents = GNNParams['trainingEvents']
    trainTag = f'_{trainingEvents}' if trainingEvents != 'odd' else ''
    subDir = f"{physicsSample}_{mc}_PFlow-{prodTag}-{nSelectedJets}jets"
    scalingFile = f"scale_{physicsSample}_{mc}_PFlow-{prodTag}-{nSelectedJets}jets.json"
    
    print('periodTag',periodTag)
    if is_mc:
        outputFile = f'../data/{dataSample}_PFlow-{dataProdTag}-{nSelectedJets}jets/files/df{fileNumTag}{fileTag}.h5'
    else:
        outputFile = f'../data/{dataSample}_PFlow-{dataProdTag}-{nSelectedJets}jets/files/df_{periodTag}{fileNumTag}{fileTag}.h5'
    print(outputFile)
    df = transformData(outputFile,scalingFile,df)
    
    if trainingEvents == 'odd':
        df = df[df.index % 2 == 0]
        #fileTag += '_even'
    elif trainingEvents == 'even':
        df = df[df.index % 2 == 1]
        #fileTag += '_odd'
    
    # Sometimes, we could not have any entries left 
    if len(df.index) == 0:
        return None

    # Step 2: Apply the GNN
    nLayers, embed_dim, ff_dim = GNNParams['nLayers'], GNNParams['embed_dim'], GNNParams['ff_dim']
    nHeads, dpt, lr, epoch = GNNParams['nHeads'], GNNParams['dpt'], GNNParams['lr'], GNNParams['epoch']
    model = pairAGraph(inpt_dim=len(jetVars),embed_dim=embed_dim,ff_dim=ff_dim,
                       nAttnBlocks=1,nHeads=nHeads,p=dpt,njets=nSelectedJets)

    # Load the trained weights
    epochTag = f'_epoch{epoch}' if epoch != -1 else ''
    tag = GNNParams['tag']
    modelDir = f"models/{subDir}/xformer_{nLayers}layers_dim{embed_dim}_ff{ff_dim}_{nHeads}heads_dpt{dpt}_jetCompatibility_lr{lr}_batch2048{trainTag}_mask{tag}"

    model.load_state_dict(torch.load(f'{modelDir}/model{epochTag}.pt',map_location='cpu'))
    model.eval()

    # Step 3: Apply the analysis cuts
    applyCuts(df,model,nSelectedJets)

    for c in df.columns:
        print(c)
    
    # Also save the individual triggers that fired for Lucas
    cols += triggers[year]
        
    # Step 4: Save and return the df
    if save:
        #fout = outputFile[:-3]+fileTag+'.h5'
        print("Saving",outputFile)
        df[cols].to_hdf(outputFile,key='df',mode='w',format='table',data_columns=True)
        
        print('Also save the parquet file')
        table  = pa.Table.from_pandas(df[cols])
        pq.write_table(table,outputFile[:-3]+'.parquet')
        
    return df[cols]

def allPairsParallel(df,HC_ordering='scalar_pt'):
    '''
    Given four jets, get the 3 combinations and decide which one is correct
    '''

    jvars = ['pt','eta','phi','E']
    
    for pi, (i0,i1,i2,i3) in enumerate(zip([0,0,0], [1,2,3], [2,1,1], [3,3,2])):
    
        print(f'pair {pi}:',(i0,i1),(i2,i3))
        
        #Step 1: Get jet 4-vectors
        ja0 = TLorentzVectorArray.from_ptetaphi(*df[[f'j{i0}_{v}' for v in jvars]].values.T)
        ja1 = TLorentzVectorArray.from_ptetaphi(*df[[f'j{i1}_{v}' for v in jvars]].values.T)
        
        jb0 = TLorentzVectorArray.from_ptetaphi(*df[[f'j{i2}_{v}' for v in jvars]].values.T)
        jb1 = TLorentzVectorArray.from_ptetaphi(*df[[f'j{i3}_{v}' for v in jvars]].values.T)
    
        # Step 2: Get HCs
        HCa = ja0+ja1
        HCb = jb0+jb1
   
        if HC_ordering == 'scalar_pt':
            pTOrdering = (ja0.pt + ja1.pt) > (jb0.pt + jb1.pt)
        elif HC_ordering == 'pt':
            pTOrdering = HCa.pt > HCb.pt
        else:
            print(f"Error: HC_ordering {HC_ordering} not recognized")

        # Save the HC 4-vector information 
        HC1 = TLorentzVectorArray.from_ptetaphim(np.where(pTOrdering,HCa.pt,  HCb.pt),
                                                 np.where(pTOrdering,HCa.eta, HCb.eta),
                                                 np.where(pTOrdering,HCa.phi, HCb.phi),
                                                 np.where(pTOrdering,HCa.mass,HCb.mass))

        HC2 = TLorentzVectorArray.from_ptetaphim(np.where(pTOrdering,HCb.pt,  HCa.pt),
                                                 np.where(pTOrdering,HCb.eta, HCa.eta),
                                                 np.where(pTOrdering,HCb.phi, HCa.phi),
                                                 np.where(pTOrdering,HCb.mass,HCa.mass))

        df[f'pT_h1_pair{pi}']  = HC1.pt 
        df[f'eta_h1_pair{pi}'] = HC1.eta
        df[f'phi_h1_pair{pi}'] = HC1.phi
        df[f'm_h1_pair{pi}']   = HC1.mass

        df[f'pT_h2_pair{pi}']  = HC2.pt 
        df[f'eta_h2_pair{pi}'] = HC2.eta
        df[f'phi_h2_pair{pi}'] = HC2.phi
        df[f'm_h2_pair{pi}']   = HC2.mass
        
        # dRjjs in each HC
        HCa_dRjj = ja0.delta_r(ja1) 
        HCb_dRjj = jb0.delta_r(jb1)
        df[f'dRjj_h1_pair{pi}'] = np.where(pTOrdering, HCa_dRjj, HCb_dRjj)
        df[f'dRjj_h2_pair{pi}'] = np.where(pTOrdering, HCb_dRjj, HCa_dRjj)

        HCa_dPhi = np.arccos(np.cos(ja0.phi - ja1.phi))
        HCb_dPhi = np.arccos(np.cos(jb0.phi - jb1.phi))
        df[f'dPhi_h1_pair{pi}'] = np.where(pTOrdering, HCa_dPhi, HCb_dPhi)
        df[f'dPhi_h2_pair{pi}'] = np.where(pTOrdering, HCb_dPhi, HCa_dPhi)

    # And get m4j since it's also needed for the pairing
    df['m_hh']  = (ja0+ja1+jb0+jb1).mass
    df['pt_hh'] = (ja0+ja1+jb0+jb1).pt


def MDR_minDhh(df):
    '''
    Plan: Loop of the # valid pairings options and save the HCs 
    '''
    
    # MDR cuts
    dRjj_h1s = df[[f'dRjj_h1_pair{i}' for i in range(3)]].values
    dRjj_h2s = df[[f'dRjj_h2_pair{i}' for i in range(3)]].values
    valid_mask = MDR_cut(dRjj_h1s, dRjj_h2s, df.m_hh.values.reshape(-1,1))

    df['nValidPairs'] = np.sum(valid_mask,axis=1)
    df['MDR'] = (df.nValidPairs > 0)

    # If nValidPairs is 0, fill HCs w/ 0 (j take care of as initialization step)
    df['chosenPair'] = -1
    for hi,v in product([1,2],['pT','eta','phi','m','dRjj']):
        df[f'{v}_h{hi}'] = 0
    
    # If nValidPairs is 1, no min necessary
    m = df.nValidPairs==1
    for hi,v in product([1,2],['pT','eta','phi','m','dRjj','dPhi']):
        k = f'{v}_h{hi}'
        df.loc[m,k] = df.loc[m,[f'{k}_pair{i}' for i in range(3)]].values[valid_mask[m]]
    
    # Save the chosen pair 
    vp_idx = np.vstack([i * np.ones(np.sum(m)) for i in range(3)]).T
    df.loc[m,'chosenPair'] = vp_idx[valid_mask[m]]
    
    # Otherwise minimize over Dhh - so first we need to *define* Dhh!!
    s = 120 / 110
    for i in range(3):
        df[f'Dhh_pair{i}'] = np.abs(df[f'm_h1_pair{i}'] - s * df[f'm_h2_pair{i}']) / np.sqrt(1+s**2)

    for vp in [2,3]:

        m = df.nValidPairs==vp
        N = np.sum(m)
        
        vp_idx = np.vstack([i * np.ones(N) for i in range(3)]).T
        i_min = np.argmin(df.loc[m,[f'Dhh_pair{i}' for i in range(3)]].values[valid_mask[m]].reshape(-1,vp),axis=1)
        
        df.loc[m,'chosenPair'] = vp_idx[valid_mask[m]].reshape(-1,vp)[np.arange(N),i_min]
        
        # And now save all the other (relevant) HC variables
        for hi,v in product([1,2],['pT','eta','phi','m','dRjj','dPhi']):
            k = f'{v}_h{hi}'
            vals = df.loc[m,[f'{k}_pair{i}' for i in range(3)]].values[valid_mask[m]].reshape(-1,vp)
            df.loc[m,k] = vals[np.arange(N),i_min]
    
def min_dR1(df):
    '''
    Choose the pair which minimizes the dR b/w the jets of the leading HC (where 
    leading has already been defined in the HC ordering in allPairsParallel)
    '''

    # Choose the pair which minimizes dRjj_h1
    dR_h1s = df[[f'dRjj_h1_pair{pi}' for pi in range(3)]].values
    i_min = np.argmin(dR_h1s,axis=1)

    df['chosenPair'] = i_min
    df['nValidPairs'] = 1 
    df['MDR'] = True 

    # Save the relevant HC info
    for hi,v in product([1,2],['pT','eta','phi','m','dRjj','dPhi']):
        k = f'{v}_h{hi}'
        vals = df[[f'{k}_pair{i}' for i in range(3)]].values
        df[k] = vals[np.arange(len(df.index)),i_min]
    

def applyCuts4jets(df):
    '''
    '''
    
    vs = ['pT','eta','phi','m']
    HC1 = TLorentzVectorArray.from_ptetaphim(*df[[f'{v}_h1' for v in vs]].values.T)
    HC2 = TLorentzVectorArray.from_ptetaphim(*df[[f'{v}_h2' for v in vs]].values.T)

    # MDpT
    MDpT_cut(df)
    # df['lead_HC_pt'] = np.where(HC1.pt>HC2.pt, HC1.pt, HC2.pt)
    # df['subl_HC_pt'] = np.where(HC1.pt>HC2.pt, HC2.pt, HC1.pt)
    # 
    # lead_pT_cut = 0.513333 * df.m_hh - 103.3333
    # subl_pT_cut = 0.33333  * df.m_hh -  73.3333
    # 
    # df['MDpT'] = (df.lead_HC_pt > lead_pT_cut) & (df.subl_HC_pt > subl_pT_cut) 
    
    # deta 
    df['abs_deta_hh'] = np.abs(HC1.eta - HC2.eta)
    df['cut_deta_hh'] = (df.abs_deta_hh < 1.5) 
    
    # Xwt
    df['cut_Xwt'] = (df.X_wt > 1.5) 

    # Get the kinematic_regions
    getRegions(df, HC1, HC2, df.MDR)
    
    # Add the reweighting variables as well
    pts = df[[f'j{ji}_pt' for ji in range(4)]].values
    pts = np.sort(pts,axis=-1)[:,::-1]
    df['pT_2'] = pts[:,1]
    df['pT_4'] = pts[:,3]

    df['eta_i'] = np.sum(np.abs(df[[f'j{ji}_eta' for ji in range(4)]].values),axis=1) / 4

    dR_h1s = df[[f'dRjj_h1_pair{pi}' for pi in range(3)]].values
    dR_h2s = df[[f'dRjj_h2_pair{pi}' for pi in range(3)]].values
    dR_mins  = np.where(dR_h1s < dR_h2s, dR_h1s, dR_h2s)
    dR_other = np.where(dR_h1s < dR_h2s, dR_h2s, dR_h1s)

    i_min = np.argmin(dR_mins,axis=1)

    df['dRjj_1'] = dR_mins[ np.arange(len(df.index)), i_min ] 
    df['dRjj_2'] = dR_other[np.arange(len(df.index)), i_min ]

    # cos(theta*) 
    lab_HH = HC1 + HC2
    boost = - lab_HH.boostp3
    rest_hc1 = HC1._to_cartesian().boost(boost)

    df['cosThetaStar'] = np.cos(rest_hc1.theta)

    # corrected m_hh
    df["m_hh_cor"] = (HC1 * 125 / HC1.mass + HC2 * 125 / HC2.mass).mass 
    df['HT'] = pts.sum(axis=1)
    
    # Change the naming convention for some of the vars
    df['event_number'] = df['eventNumber']
    if 'runNumber' in df.columns:
        df['run_number'] = df['runNumber']



def pair4jets(inputFile: str, applyCuts=False, ntag=-1,
              config: dict = {}, cols: list = [],
              pairing='MDR',HC_ordering='scalar_pt',
              dataSample=None,dataProdTag=None,year=None,
              save=False,fileTag='',truth=False, 
              entrystart=None, entrystop=None):
    '''
    '''
    
    parts = inputFile.split('.')
    periodTag = parts[7]
    fileNumTag = parts[-3]
    
    if dataSample is None: dataSample = parts[8]
    if dataProdTag is None: dataProdTag = inputFile.split('-')[1]
    if year is None: year = 2000+int(dataSample[-2:])

    # Step 1: jet selection 
    is_mc = False if ('data' in dataSample) else True
    df = process4bs(inputFile, year=year, pT_min=40,mc=is_mc,truth=truth,
                    entrystart=entrystart,entrystop=entrystop)
    print('after process4bs: len(df)',len(df))
    print('ntag==2',np.sum(df.ntag==2))
    print('ntag_all==2',np.sum(df.ntag_all==2))
    
    # 4 jets + trigger cuts
    if ntag == -1:
        bmask = np.ones_like(df.index).astype(bool)
    elif ntag == 2:
        bmask = (df.ntag == 2) 
    else:    
        bmask = (df.ntag >= ntag)
        
    if applyCuts: 
        df = df[(df.njets>=4) & df.trigger & bmask]
    
    # Step 2: Pairing 
    assert (HC_ordering == 'scalar_pt') or (HC_ordering == 'pt')
    allPairsParallel(df,HC_ordering=HC_ordering)
    
    assert pairing =='MDR' or (pairing == 'min_dR1')
    if pairing == 'MDR':
        MDR_minDhh(df)
    elif pairing == 'min_dR1':
        min_dR1(df)
    
    # Step 3: Analysis cuts 
    applyCuts4jets(df)

    for c in df.columns:
        print(c)

    # Step 4: save 
    if is_mc:
        outputFile = f'../data/{dataSample}_PFlow-{dataProdTag}/files/df{fileNumTag}{fileTag}.h5'
    else:
        print('periodTag',periodTag)
        outputFile = f'../data/{dataSample}_PFlow-{dataProdTag}/files/df_{periodTag}{fileNumTag}{fileTag}.h5'
    print(outputFile)
    print('len(df)',len(df))
    
    if len(df) == 0:
        print('Return w/o saving')
        return 
        
    if save:
        print("Saving",outputFile)
        if len(cols) > 0:
            df = df[cols]
        df.to_hdf(outputFile,key='df',mode='w',format='table',data_columns=True)

        print('Also save the parquet file')
        table  = pa.Table.from_pandas(df)
        pq.write_table(table,outputFile[:-3]+'.parquet')
        
    return df
