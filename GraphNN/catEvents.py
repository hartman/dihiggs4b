'''
Script to print out files for a given event.
'''
import pandas as pd

split = 'even'
in_dir = '../data/data16_PFlow-FEB20-5jets'

dfi = pd.read_hdf(f'{in_dir}/df_SM_2b_{split}_2b.h5', 'df', mode='r')
dfi['abs_deta_hh'] = (dfi['eta_h1'] - dfi['eta_h2']).abs()

mask = (dfi.abs_deta_hh < 1.5) & (dfi.kinematic_region==1)
print('2b deta_cut VR',np.sum(mask))

dfi[mask].to_hdf(f'test_{split}.h5',key='df')

