'''
processData.py

For running over the data with my GNN, the dfs get too large when I run over all 
of them at once, so instead I'm parallelizing this as much as possible.

'''

import numpy as np
import pandas as pd
from glob import glob
from tqdm import tqdm
import itertools
from preprocess import pairAndProcess, pair4jets
import json
import uproot 

import os
os.sys.path.append('../code')
from analysis import fileDir, normalizeWeight
from AnalysisUtils import get_xsec 

import warnings
warnings.simplefilter("ignore")

# Some useful dictionary mappings
physToDSID = {#'SMNR':          450000, 
              'semilep_ttbar': 410470,
              'allhad_ttbar':  410471,
              'SMNR':600043,
              'k10': 600044,
              'JZ2': 800285,
              'JZ3': 800286,
              'JZ4': 800287
             }

L = {
        15  : 3.2,
        16  : 24.6,
        17  : 43.65,
        18  : 58.45,
        #
        2015: 3.2,
        2016: 24.6,
        2017: 43.65,
        2018: 58.45
    }

mcToYr = {'mc16a': 2016,
          'mc16d': 2017,
          'mc16e': 2018}

stdCols = ["run_number","event_number","mc_sf","ntag","njets",
           "kinematic_region","m_hh","pt_hh","X_wt","m_hh_cor",
           "pT_h1","eta_h1","phi_h1","m_h1","dRjj_h1","dPhi_h1",
           "pT_h2", "eta_h2", "phi_h2","m_h2","dRjj_h2","dPhi_h2",
           "pT_2", "pT_4", "eta_i", "dRjj_1", "dRjj_2",
           "cosThetaStar", "cosTheta1", "cosTheta2", "Phi", "Phi1",
           'ml_j0_Db','ml_j1_Db','ml_j2_Db','ml_j3_Db','ml_j4_Db',
           'idx_HC0_j0','idx_HC0_j1','idx_HC1_j0','idx_HC1_j1',
           "HT", "MDR",  "MDpT", "max_prob", "abs_deta_hh","HT_all","lead_pt","lead_tag" ]
#pagCols = []

truthCols = ['eventNumber', 'nresolvedJets','mcEventWeight','weight_pileup','rand_run_nr',
             'j0_pt','j0_eta','j0_phi','j0_E','j0_Db','j0_sf',
             'j1_pt','j1_eta','j1_phi','j1_E','j1_Db','j1_sf',
             'j2_pt','j2_eta','j2_phi','j2_E','j2_Db','j2_sf',
             'j3_pt','j3_eta','j3_phi','j3_E','j3_Db','j3_sf',
             'j4_pt','j4_eta','j4_phi','j4_E','j4_Db','j4_sf',
             #'HLT_2j35_bmv2c2060_split_2j35_L14J15.0ETA25','HLT_j100_2j55_bmv2c2060_split','HLT_j225_bmv2c2060_split',
             'njets','trigger','mc_sf','ntag','truth_mhh','truth_pthh',
             'h0_pt','h0_eta','h0_phi','h0_barcode',
             'h1_pt','h1_eta','h1_phi','h1_barcode',
             'b0_pt','b0_eta','b0_phi','b0_parent_barcode',
             'b1_pt','b1_eta','b1_phi','b1_parent_barcode',
             'b2_pt','b2_eta','b2_phi','b2_parent_barcode',
             'b3_pt','b3_eta','b3_phi','b3_parent_barcode',
             'b0_jidx','b0_drMatch','b1_jidx','b1_drMatch','b2_jidx','b2_drMatch','b3_jidx','b3_drMatch',
             'j0_bidx','j1_bidx','j2_bidx','j3_bidx','j4_bidx',
             'j0_drMatch','j1_drMatch','j2_drMatch','j3_drMatch','j4_drMatch',
             'correctPair','sameParent','unique','dRmatch','goodJets',
             'ml_j0_pt','ml_j1_pt','ml_j2_pt','ml_j3_pt','ml_j4_pt',
             'ml_j0_eta','ml_j1_eta','ml_j2_eta','ml_j3_eta','ml_j4_eta',
             'ml_j0_phi','ml_j1_phi','ml_j2_phi','ml_j3_phi','ml_j4_phi',
             'ml_j0_E','ml_j1_E','ml_j2_E','ml_j3_E','ml_j4_E',
             'ml_j0_Db','ml_j1_Db','ml_j2_Db','ml_j3_Db','ml_j4_Db',
             'max_prob','predPair','idx_HCa_j0','idx_HCa_j1','idx_HCb_j0','idx_HCb_j1',
             'm_hh','pt_hh','m_hh_cor',
             'pT_h1','eta_h1','phi_h1','m_h1',
             'pT_h2','eta_h2','phi_h2','m_h2',
             'idx_HC0_j0','idx_HC0_j1','idx_HC1_j0','idx_HC1_j1',
             'dRjj_h1','dRjj_h2','MDR',
             'lead_HC_pt','subl_HC_pt','MDpT',
             'abs_deta_hh','cut_deta_hh','X_wt','cut_Xwt','Xhh','kinematic_region',
             'pT_2','pT_4','eta_i','dRjj_1','dRjj_2',
             'cosThetaStar','cosTheta1','cosTheta12','cosTheta2','Phi','Phi1',
             'HT','event_number','dPhi_h1','dPhi_h2', "HT_all","lead_pt","lead_tag"]

gb_split = 3

def getLambdaWeights(smnr,
                     lambdaFile='../data/weight-mHH-from-cHHHp01d0-to-cHHHpx_10GeV_Jul28.root',
                     w_sm='mc_sf'):
    '''
    Add weights corresponding to the different lambda variations to the df
    - smnr: pandas df
    - lambdaFile: The file storing the reweighting histogram for truth_mhh 
    '''

    f = uproot.open(lambdaFile)

    for kl in np.arange(-20,21):

        if kl == 1: # no need to rw
            continue

        neg = 'n' if kl < 0 else ''
        histName = f'reweight_mHH_1p0_to_{neg}{np.abs(kl)}p0'
        rw = f[histName].allvalues
        edg = f[histName].edges

        idx = np.digitize(smnr['truth_mhh'],edg)

        #smnr[f'w_k{kl}'] = get_xsec(kl)/get_xsec(1) * rw[idx] * smnr['mc_sf']
        smnr[f'w_k{kl}'] = get_xsec(kl)/31.05 * rw[idx] * smnr[w_sm]

def concatDfs(subDir,key,ntag,pconfig,pairing,physicsSample,year):
    '''
    Goal: Conatenate the dfs in each year and save the corresponding file
    '''

    # Check that all the batch jobs for this pairAGraph configuration succeeded
    MNT_list = glob(fileDir[key]+'*.root')
    numMNTs = len(MNT_list)
    tag = f'_{ntag}b' if ntag != -1 else ''
    if pairing == 'pag':
        fname = f"../data/{subDir}/files/df_00*_{pconfig}{tag}.h5"
    else:
        fname = f"../data/{subDir}/files/df_00*_{pairing}{tag}.h5"
    print('fname',fname)
    numH5 = len(glob(fname))

    # But also - I have some logic added which splits in 2 any MNTs that are larger
    # than 3 GB - so I'll need to check that here too (I guess)
    #num_split = sum([(os.stat(f).st_size / 1e9) > gb_split for f in MNT_list])
    #if numMNTs + num_split != numH5:
    #    print(f'Error: {numMNTs} MNT, should have split into {numMNTs+num_split}, but only {numH5} processed... returning')
    #    return

    if numMNTs != numH5:
        print(f'Error: {numMNTs} MNT, but only {numH5} processed... returning')
        return

    dfs = [pd.read_hdf(f,key='df') for f in tqdm(glob(fname))]

    if pairing == 'pag':
        filename = f"../data/{subDir}/df_{pconfig}{tag}.h5"
    else:
        filename = f"../data/{subDir}/df_{pairing}{tag}.h5"
    df = pd.concat(dfs)
    
    # Normalize to the corresponding year's luminosity 
    print(key)
    normalizeWeight(df,key,L[year],physToDSID[physicsSample])
    
    # Step 4: kappa lambda reweighting
    if 'SMNR' in physicsSample:
        getLambdaWeights(df)

    df.to_hdf(filename,key='df',format='table',data_columns=True)


if __name__ == '__main__':

    from argparse import ArgumentParser

    # Start off just using the same OptionParser arguments that Zihao used.
    p = ArgumentParser()
    p.add_argument('--physicsSample', type=str, default='SMNR',
                   help="The physics sample to run over (default SMNR)")

    p.add_argument('--prodTag', type=str, default='APR2020',
                   help="The production tag for the miniNtuple (default APR2020).")

    p.add_argument('--batch', action='store_true',
                   help="Submit the individual jobs on the (slac) batch cluster")

    p.add_argument('--nSelectedJets',type=int,default=5,help="Number of jets for GNN")
    p.add_argument('--pTcut',type=int,default=40,help="pT cut to use for the jets (default 40 GeV)")

    p.add_argument('--mc', type=str, default='mc16a', help="The mc campaign to consider")
    p.add_argument('--ntag',type=int,default=3)

    p.add_argument('--filename',type=str,default='')
    p.add_argument('--pairAGraphConfig','--pconfig',dest='pairAGraphConfig',
                   type=str,default='last_hp',
                   help='the config file to use for pairAGraph, default last_hp')

    p.add_argument('--concat',action='store_true',help='concatenate the individual files')

    # p.add_argument('--process4bs',action='store_true',help='revert to the baseline 4 b-jet sel')
    # I'm not sure if I want this rn
    
    p.add_argument('--pairing',type=str,default='pag',
                   help="Pairing algo to use, one of pag (default), MDR, min_dR1")
    
    p.add_argument('--start',type=int,default=-1,help='whether to use a non-default entry_start')
    p.add_argument('--stop', type=int,default=-1,help='whether to use a non-default entry_stop for the MNT')
    
    args = p.parse_args()

    physicsSample, prodTag = args.physicsSample, args.prodTag 
    nSelectedJets, pTcut, batch = args.nSelectedJets, args.pTcut, args.batch
    filename, mc, ntag = args.filename, args.mc, args.ntag
    start, stop = args.start, args.stop

    pairing = args.pairing
    assert (pairing == 'pag') or (pairing == 'MDR') or (pairing == 'min_dR1')

    subDir = f'{physicsSample}_{mc}_PFlow-{prodTag}'
    key = subDir
    if nSelectedJets != 4:
        subDir += f"-{nSelectedJets}jets"
    if pTcut != 40:
        subDir += f"-{pTcut}GeV"
    pconfig = args.pairAGraphConfig
    
    if not os.path.exists(f'../data/{subDir}'):
        os.mkdir(f'../data/{subDir}')
        os.mkdir(f'../data/{subDir}/files')
    
    if len(args.filename) > 0:

        kwargs = {'dataSample':f'{physicsSample}_{mc}', 'dataProdTag':prodTag,
                  'year':mcToYr[mc], 'save':True}

        if pairing == 'pag':
            
            # Load in the pairAGraph config file
            with open(f"configs/{pconfig}.json", 'r') as varfile:
                GNNParams = json.load(varfile)
                
            tag = f'_{pconfig}'
            if ntag != -1: tag += f'_{ntag}b'
                
            kwargs['fileTag'] = tag
                    
            # The mc MNTs that I have all non-minimal, except for ttbar
            if not (('ttbar' in physicsSample) or ('JZ' in physicsSample)):
                kwargs['cols'] = truthCols
                kwargs['truth'] = True

            pairAndProcess(args.filename, ntag, GNNParams, **kwargs)

        else: 
            
            kwargs['fileTag'] = f'_{pairing}'
            if ntag != -1: kwargs['fileTag'] += f'_{ntag}b'
            if (start != -1) or (stop != -1): 
                kwargs['fileTag'] = f'_{start}_{stop}_' + kwargs['fileTag']
                kwargs['entrystart'] = start
                kwargs['entrystop']  = stop
                
            kwargs['truth'] = False if (('ttbar' in physicsSample) or ('JZ' in physicsSample)) else True
            kwargs['pairing'] = f'{pairing}'
            
            if pairing == 'MDR':
                kwargs['HC_ordering'] = 'scalar_pt'
                pair4jets(args.filename, applyCuts=True, ntag=args.ntag, **kwargs)

            elif pairing == 'min_dR1':
                kwargs['HC_ordering'] = 'pt'
                pair4jets(args.filename, applyCuts=True, ntag=args.ntag, **kwargs)
            
            else:
                print('pairing arg',pairing,'not implemented yet')
                raise NotImplementedError

    elif args.concat:
        concatDfs(subDir,key,ntag,pconfig,pairing,physicsSample,mcToYr[mc])

    else:

        wildFile = fileDir[key]+'*.root'
        print(wildFile)
        print(len(glob(wildFile)))
        for filename in glob(wildFile):

            pythonCmd =  f"python processMC.py --physicsSample {physicsSample} --mc {mc} "
            pythonCmd += f"--prodTag {prodTag} --nSelectedJets {nSelectedJets} --pTcut {pTcut} "
            pythonCmd += f"--filename {filename} --ntag {ntag} --pairAGraphConfig {pconfig} "
            pythonCmd += f"--pairing {pairing} "
                
            if args.batch:
                parts = filename.split('.')
                fileNumTag = parts[-3]
                print('Running over',fileNumTag)
                    
                if pairing == 'pag':
                    job = f'{physicsSample}_{mc}{fileNumTag}_{pconfig}_{ntag}b'
                else:
                    job = f'{physicsSample}_{mc}{fileNumTag}_{pairing}_{ntag}b'
                bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0026]\' -W 4:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
                        
                outputFile = f'../data/{subDir}/files/df_{mc}{fileNumTag}_{pconfig}_{ntag}b.parquet'
                if os.path.exists(outputFile):
                    print(f'Not submitting: {outputFile} already esists')
                else:
                    os.system(bsubCmd)
            else:
                os.system(pythonCmd)
                    
            '''
            # Check the size of the file 
            t = uproot.open(filename)['XhhMiniNtuple']
            gb = os.stat(filename).st_size / 1e9 # filesize in GigaBytes
            if gb > gb_split:
                starts = [0,len(t)//2]
                stops  = [len(t)//2,len(t)]
            else: 
                starts = [-1]
                stops  = [-1]
                
            for start, stop in zip(starts, stops):
        
                pythonCmd =  f"python processMC.py --physicsSample {physicsSample} --mc {mc} "
                pythonCmd += f"--prodTag {prodTag} --nSelectedJets {nSelectedJets} --pTcut {pTcut} "
                pythonCmd += f"--filename {filename} --ntag {ntag} --pairAGraphConfig {pconfig} "
                pythonCmd += f"--pairing {pairing} --start {start} --stop {stop}"
                
                if args.batch:
                    parts = filename.split('.')
                    fileNumTag = parts[-3]
                    print('Running over',fileNumTag)
                    
                    if pairing == 'pag':
                        job = f'{physicsSample}_{mc}{fileNumTag}_{pconfig}_{ntag}b'
                    else:
                        job = f'{physicsSample}_{mc}{fileNumTag}_{pairing}_{ntag}b'
                    bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0026]\' -W 4:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
                        
                    os.system(bsubCmd)
                else:
                    os.system(pythonCmd)
            '''
