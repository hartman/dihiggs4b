'''
Goal:  Take the training dataset that I've prepared combining the mc campaigns,
and play w/ the hyperparameters to optimize the performance.

I was hoping that I would be able to get > 90% accurate on the 4b dataset, b/c
this is what we're getting w/ the baseline algorithm, but I have my worries that
this model might be a bit complicated for what I'm trying to solve b/c it tends 
to overfit pretty easily.
'''

from argparse import ArgumentParser
from random import choice
import os

if __name__ == '__main__':
    p = ArgumentParser()

    # Parameters of the model
    p.add_argument('--batch', action='store_true', help="Submit the individual jobs on the (slac) batch cluster")
    p.add_argument('--nIter', type=int, default=1, help="# of iterations to keep drawing random samples for")

    args = p.parse_args()

    nIter = args.nIter

    hyperparams = {
        'embed_dim' : [10,20,40], 
        'ff_dim' : [5,10,20,30], 
        'nHeads' : [2,4,5],
        'nLayers' : [1], #[1,2],
        'dropout' : [.1,.2,.3,.4],
        'lr' : [1e-3,2e-3,5e-3,1e-2],
        #'lr' : [1e-3,5e-3,.01],
        } 
        
    for i in range(nIter):

        print(f'Iter {i+1} / {nIter}')
        cmd = "python trainNet.py --nMaxEpochs 200 "
        for key, options in hyperparams.items():
            vi = choice(options)
            cmd += f"--{key} {vi} "

        os.system(cmd)
