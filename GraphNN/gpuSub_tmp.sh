#!/bin/bash -l
#SBATCH --account=shared
#SBATCH --partition=shared
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=5
#SBATCH --mem-per-cpu=2g
#SBATCH --time=15:00:00
#SBATCH --gpus 1
#SBATCH --job-name=sec4.3.1_DIPS_pt_500_d0_3.5_z0_5,qoverp_iter4
#SBATCH --output=output/sec4.3.1_DIPS_pt_500_d0_3.5_z0_5,qoverp_iter4.log
#SBATCH --error=output/sec4.3.1_DIPS_pt_500_d0_3.5_z0_5,qoverp_iter4.log
singularity exec --nv /gpfs/slac/atlas/fs1/d/nhartman/py3_pytorch1.simg python3 trainNet.py --mc mc16d --jetCollection PFlow --nJets 3000000 --timestamp _BTagging201903 --mode train --model DIPS --physicsSample ttbar --nTrks 25 --nClasses 3  --batch_norm --sortFlag sd0_rev --trkSelection pt_500_d0_3.5_z0_5 --dense_sizes 100,100  --jointNormVars nPixHits,nSCTHits,ip3d_d0,ip3d_z0,phi,theta,qoverp --iter 4
