'''
processData.py

For running over the data with my GNN, the dfs get too large when I run over all 
of them at once, so instead I'm parallelizing this as much as possible.

'''

import os
import numpy as np
import pandas as pd
from glob import glob
from tqdm import tqdm
import itertools
from preprocess import pairAndProcess, pair4jets
import json
import uproot 

import os
os.sys.path.append('../code')
from analysis import fileDir

cols = ["run_number","event_number","mc_sf","ntag","ntag_all","njets","trigger",
       "kinematic_region","m_hh","pt_hh","X_wt","m_hh_cor",
       "pT_h1","eta_h1","phi_h1","m_h1","dRjj_h1","dPhi_h1",
       "pT_h2", "eta_h2", "phi_h2","m_h2","dRjj_h2","dPhi_h2",
       "pT_2", "pT_4", "eta_i", "dRjj_1", "dRjj_2",
       "cosThetaStar", 'j0_Db','j1_Db','j2_Db','j3_Db',
       "HT", "MDR",  "MDpT", "abs_deta_hh", "nValidPairs", 
       "HT_all","lead_pt","lead_tag"]

def concatDfs(subDir,key,ntag,pconfig,pairing='pag',deta_cut=False,regions=False):
    '''
    Goal: Conatenate the dfs in each year and save the corresponding file
    '''

    print("subDir",subDir)
    print("key",key)
    print("deta_cut",deta_cut)
    print("regions",regions)

    # Check that all the batch jobs for this pairAGraph configuration succeeded
    numMNTs = len(glob(fileDir[key]+'*.root'))
    detaTag = '_detaCut' if deta_cut else ''
    
    tag = f'_{pconfig}' if pairing == 'pag' else f'_{pairing}'
    if ntag != -1: tag += f'_{ntag}b'
    
    fname = f"../data/{subDir}/files/df_period?_00*{tag}.h5"
    numH5 = len(glob(fname))

    if numMNTs != numH5:
        print(f'Warning: {numMNTs} MNT, but only {numH5} processed.')
    else:
        print(f'{numMNTs} MNTs and h5 files')

    # Iterate over globbed files 
    for f in glob(fileDir[key]+'*.root'):
        
        # Retrieve the period tag and the fileNum 
        parts = f.split('.')
        periodTag = parts[3]
        fileNumTag = parts[-3]

        outputFile = f'../data/{subDir}/files/df_{periodTag}{fileNumTag}{tag}.h5'
        logFile = f"log_files/{key.split('_')[0]}_{periodTag}{fileNumTag}{tag}.txt"

        # If the file doesn't exist, check that the corresponding _scaledInputs file exists 
        if not os.path.exists(outputFile):
            
            # Set this variable to true, but then manually iterate over the cases that 
            # could have caused the error that are deemed "reasonable" 
            error = True
            
            intFile = outputFile[:-3] + '_scaledInputs.h5'
            if os.path.exists(intFile):
                # Open the corresponding df and check that it's empty (i.e, none of the 
                #events in this MNT passed the corresponding trigger and 4 good jets cuts)
                dfi = pd.read_hdf(intFile)
                if len(dfi)==0:
                    print(f'File for {key}, {periodTag} file{fileNumTag} didn\'t have any events passing triggers + 4 good jets cuts')
                    error = False
                else:
                    print(f'Error, LSF job for file for {key}, {periodTag} file{fileNumTag} prob failed midway')
                    print(f'_scaledInputs file has {len(dfi)} events... returning so debugging can be done')
                    #return
            
            elif os.path.exists(logFile):
                
                print(f'Looking at {logFile}')
                lf = open(logFile)

                len_df = []
                for l in lf:
                    if 'len(df)' in l:
                        len_df.append(l.strip('\n'))
                
                if len(len_df) > 0:        
                    postCuts = int(len_df[-1].split(' ')[-1])
                    
                    if postCuts == 0:
                        print(f'File for {key}, {periodTag} file{fileNumTag} didn\'t have any events passing triggers + 4 good jets cuts')
                        error = False
                else:
                
                    # Check if the MNT file had the tree 
                    fi = uproot.open(f)
                    if not (b'XhhMiniNtuple;1' in fi.keys()):
                        print(f'MNT for {key}, {periodTag} file{fileNumTag} didn\'t have the XhhMiniNtuple tree')
                        error = False
                    else:
                        print(f'Error, {logFile} exists, but says the df has {postCuts} entries, smth must have gone wrong - returning so debugging can happen')
                        #return
            else:
                
                # Check if the MNT file had the tree 
                fi = uproot.open(f)
                if not (b'XhhMiniNtuple;1' in fi.keys()):
                    print(f'MNT for {key}, {periodTag} file{fileNumTag} didn\'t have the XhhMiniNtuple tree')
                    error = False
                else:
                    print(f'Error, no output file for {key}, df_{periodTag}{fileNumTag}{tag}.h5')
                    #return
    
            # If we haven't been able to explain the absense of this file, return 
            # w/o concatenating everything
            if error:
                print('Unable to explain missing file',f)
                print(f'returning w/o concatenating {key}')
                return 
                
    if regions:

        krs = ['CR','VR','SR']
        dfs = {k: [] for k in krs}

        for f in tqdm(glob(fname)):
            dfi = pd.read_hdf(f, key='df')
            
            for r,k in zip([2,1,0],krs):
            
                mask = (dfi.kinematic_region == r)
                if deta_cut:
                    mask = mask & (np.abs(dfi.eta_h1 - dfi.eta_h2) <= 1.5)
                    if pairing == 'MDR':
                        mask = mask & dfi.MDR & dfi.MDpT
                dfs[k].append( dfi[ mask  ] )
        
        # Save the output
        for k in krs:
            filename = f'../data/{subDir}/df{tag}{detaTag}_{k}.h5'
            df = pd.concat(dfs[k])
            df.to_hdf(filename,key='df',format='table',data_columns=True)
    else:
        if deta_cut:
            dfs = []
            for f in tqdm(glob(fname)):
                dfi = pd.read_hdf(f, key='df')
                mask = np.abs(dfi.eta_h1 - dfi.eta_h2) < 1.5
                if pairing == 'MDR':
                        mask = mask & dfi.MDR & dfi.MDpT
                dfs.append( dfi[ mask ] )
        else:
            dfs = [pd.read_hdf(f,key='df') for f in tqdm(glob(fname))]

        print(dfs[0].columns)
        filename = f'../data/{subDir}/df{tag}{detaTag}.h5'
        df = pd.concat(dfs)
        print(df.columns)
        df.to_hdf(filename,key='df',format='table',data_columns=True)


def concatPeriod(fDir,period,nbtags,colConfig=''):
    #Given a filedirectory, concatenate the files with a given # of b-tags 
    #and in a specified kinematic region

    fName = f"files/df_period{period}_00*entry_*000.h5"
    dfs = []

    assert len(colConfig) != 0

    # Load in the columns 
    with open("../"+colConfig, 'r') as varfile:
        c = json.load(varfile)
    colTag = c["ID"]
    cols = c["cols"]
    
    for myFile in tqdm(glob(fDir + fName)):
        dfi = pairAndProcess(myFile,cols=cols)
                
        if dfi is not None:
            #for c in dfi.columns:
            #    print(c)
            dfs.append(dfi)

    if len(dfs) > 0:
        df = pd.concat(dfs,ignore_index=True)

        tag = ''
        if nbtags != -1:
            tag += f"_{nbtags}b"
        
        fout = f"{fDir}files/df_period{period}_{tag}{colTag}.h5"
        print("Saving",fout)
        df.to_hdf(fout,key='df',mode='w')

if __name__ == '__main__':

    from argparse import ArgumentParser

    # Start off just using the same OptionParser arguments that Zihao used.
    p = ArgumentParser()
    p.add_argument('--physicsSample', type=str, default='data16',
                   help="The physics sample to run over (default data16)")

    p.add_argument('--prodTag', type=str, default='FEB20',
                   help="The production tag for the miniNtuple (default FEB20).")

    p.add_argument('--batch', action='store_true',
                   help="Submit the individual jobs on the (slac) batch cluster")

    p.add_argument('--nSelectedJets',type=int,default=5,help="Number of jets for GNN")
    p.add_argument('--pTcut',type=int,default=40,help="pT cut to use for the jets (default 40 GeV)")
    #p.add_argument('--sort',type=int,default='Db',help="Sort to use for the jets")

    p.add_argument('--mc', type=str, default='mc16a', help="The mc campaign to consider")
    #p.add_argument('--period', type=str, default='',
    #               help="The period for data taking, should only be passed for data, the default is an empty string.")
    p.add_argument('--ntag',type=int,default=3)

    p.add_argument('--filename',type=str,default='')

    p.add_argument('--colConfig',type=str,default='',help='If not an empty string, '\
                    +'pass the name of a .json file that has the list of the columns saved')
    p.add_argument('--pairAGraphConfig','--pconfig',dest='pairAGraphConfig',
                   type=str,default='last_hp',
                   help='the config file to use for pairAGraph, default last_hp')
    p.add_argument('--pairing',type=str,default='pag',
                   help="Pairing algo to use, one of pag (default), MDR, min_dR")

    p.add_argument('--deta_cut',action='store_true')
    p.add_argument('--regions',action='store_true')
    
    p.add_argument('--period',type=str,default='all')
    #p.add_argument('--concat',action='store_true',help='concatenate the individual files')

    #p.add_argument('--start',type=int,default=-1,help='whether to use a non-default entry_start')
    #p.add_argument('--stop', type=int,default=-1,help='whether to use a non-default entry_stop for the MNT')
    
    args = p.parse_args()

    physicsSample, prodTag = args.physicsSample, args.prodTag 
    nSelectedJets, pTcut, batch = args.nSelectedJets, args.pTcut, args.batch
    filename, period, ntag = args.filename, args.period, args.ntag

    colConfig = args.colConfig

    subDir = f'{physicsSample}_PFlow-{prodTag}'
    key = subDir
    if nSelectedJets != 4:
        subDir += f"-{nSelectedJets}jets"
    if pTcut != 40:
        subDir += f"-{pTcut}GeV"
        
    if not os.path.exists(f'../data/{subDir}'):
        os.mkdir(f'../data/{subDir}')
    if not os.path.exists(f'../data/{subDir}/files'):
        os.mkdir(f'../data/{subDir}/files')
        
    pairing, pconfig = args.pairing, args.pairAGraphConfig
    assert (pairing == 'pag') or (pairing == 'dhh') or (pairing == 'min_dR1')

    tag = f'_{pconfig}' if pairing == 'pag' else f'_{pairing}'
    if ntag != -1: tag += f'_{ntag}b'

    # Load in the pairAGraph config file
    with open(f"configs/{pconfig}.json", 'r') as varfile:
        GNNParams = json.load(varfile)

    
    if len(args.filename) > 0:

        if pairing == 'pag':
            pairAndProcess(args.filename, ntag, GNNParams, save=True, fileTag=tag)
            
        elif pairing == 'MDR':
            print('Ordering HCs by scalar sum of jet pTs')
            pair4jets(args.filename, applyCuts=True, ntag=ntag, cols=cols, 
                      pairing=pairing,HC_ordering='scalar_pt',save=True, fileTag=tag)
                      
        elif pairing == 'min_dR1':
            print('Ordering by HC pT')
            pair4jets(args.filename, applyCuts=True, ntag=ntag, cols=cols, 
                      pairing=pairing,HC_ordering='pt',save=True, fileTag=tag)
                
        else:
            print('pairing arg',pairing,'not implemented yet')
            raise NotImplementedError


    elif len(period) > 0:
        if period == 'all':
            configInfo = f'{pconfig} config' if pairing == 'pag' else f'{pairing} pairing'
            print(f"\n\nConcatenating files for {key} and {nSelectedJets}-jets with {ntag}b and {configInfo}.")
            concatDfs(subDir,key,ntag,pconfig,pairing,args.deta_cut,args.regions)
        else:
            print("I haven't finished the individaul period concatenation yet.")
            #fDir = f"/u/ki/nhartman/gpfs/diHiggs4b/data/{subDir}/"
            #concatPeriod(fDir,subDir,period,ntag,pairAGraphConfig=colConfig)

    else:

        wildFile = f'../../public/hh4b/data/user.mswiatlo.HH4B.period*.{physicsSample}..AB21.2.91-{prodTag}-0.pflow_vr_MiniNTuple.root/user.mswiatlo.*._00*.MiniNTuple.root'
        print(wildFile)
        print(len(glob(wildFile)))
        for filename in glob(wildFile):
        
            pythonCmd =  f"python processData.py --physicsSample {physicsSample} "
            pythonCmd += f"--prodTag {prodTag} --nSelectedJets {nSelectedJets} --pTcut {pTcut} "
            pythonCmd += f"--filename {filename} --ntag {ntag} --pairAGraphConfig {pconfig} --pairing {pairing}"
            if args.batch:
                parts = filename.split('.')
                fileNumTag = parts[-3]
                periodTag = parts[7]
                
                job = f'{physicsSample}_{periodTag}{fileNumTag}{tag}'
                bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -W 30 -oo log_files/{job}.txt -J {job} {pythonCmd}"

                outputFile = f'../data/{subDir}/files/df_{periodTag}{fileNumTag}{tag}.h5'
                if os.path.exists(outputFile):
                    print(f'Not submitting: {outputFile} already exists')
                else:
                    os.system(bsubCmd)
            else:
                os.system(pythonCmd)

