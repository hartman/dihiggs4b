'''
Loop over the options for processMC.py
'''

import os

import warnings
warnings.simplefilter("ignore")

for mc in ['mc16a','mc16d','mc16e']: 
	for physicsSample in ['JZ2','JZ3','JZ4']:
		for ntag in [2,3]:
			print(mc,physicsSample,ntag)
			pythonCmd = f'python processMC.py --physicsSample {physicsSample} --prodTag HLLHC'
			pythonCmd+= f' --mc {mc} --pairing MDR --ntag {ntag} --nSelectedJets 4 --batch' # 
			print(pythonCmd)
			os.system(pythonCmd)

'''
# And now for pairAGraph
for mc in ['mc16a','mc16d','mc16e']: 
	for physicsSample in ['JZ2','JZ3','JZ4']:
		for ntag in [2,3]:
			print(mc,physicsSample,ntag)
			pythonCmd = f'python processMC.py --physicsSample {physicsSample} --prodTag HLLHC'
			pythonCmd+= f' --mc {mc} --pairing pag --pconfig SM_2b --ntag {ntag} --nSelectedJets 5 --batch' # 
			print(pythonCmd)
			os.system(pythonCmd)
for mc in ['mc16a','mc16d','mc16e']: 
	for physicsSample in ['SMNR','k10']:
		if mc == 'mc16e' and physicsSample == 'k10':
			continue
	
		for pairing in ['MDR', 'min_dR1']:#
			for ntag in [2,3]:
				
				pythonCmd = f'python processMC.py --physicsSample {physicsSample} --prodTag APR2020'
				pythonCmd+= f' --mc {mc} --pairing {pairing} --ntag {ntag} --nSelectedJets 4 --concat' # 
				print(pythonCmd)
				os.system(pythonCmd)

for mc in ['mc16a','mc16d','mc16e']: # 
	for physicsSample in ['SMNR','k10']:
		if physicsSample == 'k10' and mc == 'mc16e':
			continue
		
		for pconfig in ['SM_2b_even','SM_2b_odd','kappa_10_even','kappa_10_odd']: 
			for ntag in [2,3]:
				pythonCmd = f'python processMC.py --physicsSample {physicsSample} --prodTag APR2020'
				pythonCmd+= f' --mc {mc} --pairAGraphConfig {pconfig} --ntag {ntag} --concat' # 
				print(pythonCmd)
				os.system(pythonCmd)

'''
