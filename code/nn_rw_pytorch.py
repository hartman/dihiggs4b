'''
nn_rw_pytorch.py

Goal: Starting from Rafael's nb, let's implement the NN rw
in pytorch so that it plays nicely with my pairAGraph sw
setup.
'''

import numpy as np
import pandas as pd 
from sklearn.preprocessing import StandardScaler

import json
import os

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader

class rwDataset(Dataset):
  
    def __init__(self, df, kinematic_region=2, 
                 rw_columns=['njets','eta_i','pT_2_log','pT_4_log','dRjj_1_log','dRjj_2_log','pt_hh_log'],
                 baseline_cuts=False, apply_Xwt=False,modelDir=""):
        '''
        Goal: Load in the jet level variables into a pytorch dataframe to be ready
        for training, and do the necessary ml pre-processing.
        '''

        super(rwDataset, self).__init__()
      
        #rw_columns = ['njets','eta_i','pT_2','pT_4','dRjj_1','dRjj_2','pt_hh']

        
      
        # We reweight in the CR after the analysis cuts
        mask_2b = (df.ntag == 2) & (df.kinematic_region==kinematic_region)
        mask_4b = (df.ntag >= 4) & (df.kinematic_region==kinematic_region)
    
        if baseline_cuts:
            mask_2b = mask_2b & df.MDR & df.MDpT & df.cut_deta_hh
            mask_4b = mask_4b & df.MDR & df.MDpT & df.cut_deta_hh

        if apply_Xwt:
            mask_2b = mask_2b & (df.X_wt > 1.5)
            mask_4b = mask_4b & (df.X_wt > 1.5)

        original = df.loc[mask_2b,rw_columns]
        target   = df.loc[mask_4b,rw_columns]
      
        X = pd.concat((original, target), ignore_index=True).values
        #X[:,2:] = np.log(X[:,2:])
      
        y = []
        for _df, ID in [(original,1), (target, 0)]:
            y.extend([ID] * _df.shape[0])
        y = np.array(y)

        scaler = StandardScaler()
        X_scaled = scaler.fit_transform(X)

        self.X = torch.from_numpy(X_scaled).float()
        self.y = torch.from_numpy(y).float()
      
        # Save the scaling file for evaluation (later)
        d = {}
        d['mean'] = list(scaler.mean_)
        d['scale'] = list(scaler.scale_)
        d['var'] = list(scaler.var_)
        d['n_samples_seen'] = int(scaler.n_samples_seen_)

        Xwt_tag = "postXwt" if apply_Xwt else "preXwt"
        fout = f"{modelDir}/rw_scale.json"
        with open(fout, 'w') as varfile:
            json.dump(d, varfile)
      
    def __len__(self):
        return self.X.shape[0] 

    def __getitem__(self, idx):
        return self.X[idx], self.y[idx]


def getDataLoaders(df,kinematic_region=2,
                   rw_columns=['njets','eta_i','pT_2_log','pT_4_log','dRjj_1_log','dRjj_2_log','pt_hh_log'],
                   baseline_cuts=False, apply_Xwt=False,
                   modelDir="",batch_size=1024,trainFrac=.8):
    '''
    Return the DataLoaders for my pytorch function.
    
    Input: 
    - myFile: The name of the hdf5 file with the pandas Dataframe
    - cols: The input variables of interest
        -batch_size: default 128
    -N: Number of events to load in, the default value of -1  
 
    Returns: loader_train, loader_val, loader_test
        DataLoaders for the train, val, and test sets
    '''
        
    dset = rwDataset(df,kinematic_region,rw_columns,baseline_cuts,apply_Xwt,modelDir)
     
    N = len(dset)
    trainEvts = int(.8 * len(dset))
    
    print("Dataset size",N)
    
    train,val = torch.utils.data.random_split(dset, [trainEvts,N-trainEvts])

    loader_train = DataLoader(train, batch_size=batch_size, shuffle=True)
    loader_val   = DataLoader(val, batch_size=batch_size, shuffle=True)
    
    return loader_train, loader_val


class reweighter(nn.Module):

    def __init__(self,inpt_dim=7,hidden_dim=20,nLayers=3):
        '''
        Inputs:
        - inpt_dim: The number of features used for the reweighting
            Default 7 for njets, pT_2, pT_4, eta_i, dRjj_1, dRjj_2, pt_hh
        - hidden_dim: The dimension for the hidden layers
        '''

        super(reweighter, self).__init__()

        self.h1  = nn.Linear(inpt_dim,  hidden_dim)
        self.h2  = nn.Linear(hidden_dim,hidden_dim)
        self.h3  = nn.Linear(hidden_dim,hidden_dim)
        self.out = nn.Linear(hidden_dim,1)

    def forward(self, x):
    
        h = self.h1(x)
        
        for linear in [self.h2,self.h3,self.out]:
            h = nn.ReLU()(h)
            h = linear(h)
            
        return h
        
def louppe_loss(y_true, y_pred):
    #return torch.mean(y_true * (torch.sqrt(torch.exp(y_pred))) + 
    #        (1.0 - y_true) * (1.0 / torch.sqrt(torch.exp(y_pred))))
    return torch.mean(y_true * torch.exp(0.5*y_pred) + (1.0 - y_true) * torch.exp(-0.5*y_pred))


def check_loss(loader,model,device='cpu'): 
    '''
    Calculate the loss
    '''
    model.eval()
    L = 0
    
    model = model.to(device=device)
    
    with torch.no_grad():
        for xi,yi in loader:
            
            xi = xi.to(device=device)
            yi = yi.to(device=device)

            pred = model(xi)
            loss = louppe_loss(yi, pred)
            L += loss.item()
            
    return L
    
def train(rw,loader_train,loader_val,nEpochs=50,patience=50,lr=1e-3,modelDir="",device='cpu'):
    
    optimizer = torch.optim.Adam(rw.parameters(), lr=lr)#.001)

    metrics = {k:[] for k in [f"{s}_loss" for s in ['train','val']]}

    rw.to(device=device)

    bestEpoch = 0 
    bestValLoss = 10000

    for epoch in range(nEpochs):

        L = 0
        rw.train()

        for xi,yi in loader_train:

            xi = xi.to(device=device)
            yi = yi.to(device=device)

            optimizer.zero_grad()

            pred = rw(xi)
            loss = louppe_loss(yi, pred)
                    
            loss.backward()
            optimizer.step()

            L += loss.item()

        metrics['train_loss'].append(L)
        metrics['val_loss'].append(check_loss(loader_val,rw,device))

        torch.save(rw.state_dict(), f'{modelDir}/model_epoch{epoch}.pt')
        
        myVars = (epoch, L, metrics['val_loss'][-1]*4)
        print("Epoch {:2d} | Train loss {: .3f} | Val loss {: .3f}".format(*myVars))
        
        # Check if the validation loss improved
        if metrics['val_loss'][-1] < bestValLoss:
            bestValLoss = metrics['val_loss'][-1]
            bestEpoch = epoch

        # Save the model at each epoch
        torch.save(rw.state_dict(),f'{modelDir}/model_epoch{epoch}.pt')

        # See if it's time to break out of the training loop 
        if epoch - bestEpoch > patience:
            break

    filename = f"{modelDir}/loss_acc.json"
    with open(filename, 'w') as varfile:
        json.dump(metrics, varfile)

    # Also save the model w/ the best validation loss 
    rw.load_state_dict(torch.load(f'{modelDir}/model_epoch{bestEpoch}.pt'))
    torch.save(rw.state_dict(), f'{modelDir}/model.pt')

    return metrics

def getNNweights(df, kinematic_region=2, train_model=True, yr=16,
                 baseline_cuts=False, apply_Xwt=False, tag="",device='cpu',
                 nEpochs=50,patience=50,lr=1e-3,
                 rw_columns=['njets','eta_i','pT_2_log','pT_4_log','dRjj_1_log','dRjj_2_log','pt_hh_log']):
    '''
    Get the weights from the reweighting NN
    '''

    # Load in the model architecture 
    rw = reweighter(inpt_dim=len(rw_columns))

    # Either load in the saved weights, or save the pre-trained weights
    region = {0: 'SR', 1: 'VR', 2: 'CR'}
    Xwt_tag = "postXwt" if apply_Xwt else "preXwt"
    baseline_tag = "_baselineCuts" if baseline_cuts else ""
    modelDir = f"nn_rw/data_{yr}{baseline_tag}_{Xwt_tag}{tag}_{region[kinematic_region]}"

    print("modelDir",modelDir)

    for c in rw_columns:
        if (c[-4:]=='_log') and not (c in df.columns):
            print(f"Adding {c} to the df")
            df[c] = np.log(df[c[:-4]])

    if not os.path.exists(modelDir):
        os.mkdir(modelDir)
    
    if train_model:
        
        print("Getting in the data loaders")
        # Load in the training dataset
        tr, val = getDataLoaders(df, kinematic_region=kinematic_region, 
                                 rw_columns=rw_columns,
                                 baseline_cuts=baseline_cuts, apply_Xwt=apply_Xwt,
                                 modelDir=modelDir)

        print("About to train the model")
        # Train the model
        metrics = train(rw,tr,val,nEpochs=nEpochs,patience=patience,
                        modelDir=modelDir,device=device,lr=lr)
        
    else:
        # Just load in the previously saved weights
        rw.load_state_dict(torch.load(f'{modelDir}/model.pt'))#map_location=torch.device('cpu')

    # Get the normalization in the region where you trained 
    rw.eval()

    X = df[rw_columns].values

    fin = f"{modelDir}/rw_scale.json"
    with open(fin, 'r') as varfile:
        varinfo = json.load(varfile)

    scaler        = StandardScaler(copy=False) # scale the data in place
    scaler.mean_  = varinfo['mean']
    scaler.scale_ = varinfo['scale'] 
    scaler.var_   = varinfo['var'] 
    scaler.n_samples_seen_ = varinfo['n_samples_seen'] 
    X = scaler.transform(X)

    Xpt = torch.tensor(X).float().to(device)

    rw.eval()
    R = torch.exp(rw(Xpt)).cpu().detach().numpy()

    mask_2b = (df.ntag == 2) & (df.kinematic_region==kinematic_region)
    mask_4b = (df.ntag >= 4) & (df.kinematic_region==kinematic_region)
    
    if baseline_cuts:
        mask_2b = mask_2b & df.MDR & df.MDpT & df.cut_deta_hh
        mask_4b = mask_4b & df.MDR & df.MDpT & df.cut_deta_hh

    if apply_Xwt:
        mask_2b = mask_2b & (df.X_wt > 1.5)
        mask_4b = mask_4b & (df.X_wt > 1.5)

    N_2b = np.sum(R[mask_2b]) 
    N_4b = np.sum(mask_4b) 

    # Save the weights in the df
    key = 'w_2b' if (kinematic_region == 2) else f'w_2b_{region[kinematic_region]}'
    df[key] = R * N_4b / N_2b

