'''
Functions to make event displays using the DataFrames I make from the miniNtuples.
'''

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import transforms

def drawCircle(eta, phi, R=0.4):
    '''
    Get the locations for the circle of a jet.

    Inputs:
    - eta: jet's eta
    - phi: jet's phi
    - R: The radius of the jet, default 0.4

    Outputs:
    - etas, phis: nparrays for the (eta, phi) coordinates for a circle
                  representing the jet
    '''

    # Get the upper half of the hemisphere
    centeredEta = np.linspace(-R, R)
    centeredPhi = np.sqrt(R**2 - centeredEta**2)

    # Concatenate with the solution for the lower hemisphere
    etas = np.concatenate((eta+centeredEta, (eta+centeredEta)[::-1]))
    phis = np.concatenate((phi+centeredPhi, (phi-centeredPhi)[::-1]))

    return etas,phis

def rainbow_text(x,y,ls,lc,yoffset=.75,ax=None,**kw):
    '''
    Take a list of strings ``ls`` and colors ``lc`` and place them next to each
    other, with text ls[i] being shown in color lc[i].

    This example shows how to do both vertical and horizontal text, and will
    pass all keyword arguments to plt.text, so you can set the font size,
    family, etc.
    '''

    if ax is None:
        ax=plt.gca()

    t = ax.transData
    fig = plt.gcf()

    #horizontal version
    for s,c in zip(ls,lc):
        text = plt.text(x,y," "+s,color=c, transform=t, **kw)
        text.draw(fig.canvas.get_renderer())
        ex = text.get_window_extent()
        t = transforms.offset_copy(text._transform, y=-yoffset*ex.height, units='dots')



def truthJetMatch(ievt,df,drawHCs=False,figDir='',triggerKey=None):
    '''
    Goal: Look at the event display to show where the b-quarks and truth higgses
    are, and the dR matched reco jets.
    '''

    # Setup the figure
    plt.figure(figsize=(8,2*np.pi))

    plt.xlim(-4,4)
    plt.ylim(-np.pi,np.pi)
    plt.grid()

    plt.xlabel('$\eta$',fontsize=18)
    plt.ylabel('$\phi$',fontsize=18)
    plt.title('Event {}'.format(ievt),fontsize=18)

    ls = ["Truth higgses:\n"]

    # Step 1: Draw the truth higgses
    hpts  = df.loc[ievt,['h{}_pt'.format(i) for i in range(2)]]
    hetas = df.loc[ievt,['h{}_eta'.format(i) for i in range(2)]]
    hphis = df.loc[ievt,['h{}_phi'.format(i) for i in range(2)]]
    plt.scatter([hetas],[hphis],81,color='k',label='truth higgses')

    ls += ["  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}\n".format(pt,eta,phi) \
            for pt, eta, phi in zip(hpts, hetas, hphis)]

    # Step 2: Draw the truth b-quarks
    colors = ['C0','C1','C2','C3']
    bpts = [df.loc[ievt,'b{}_pt'.format(i)] for i in range(4)]
    betas = [df.loc[ievt,'b{}_eta'.format(i)] for i in range(4)]
    bphis = [df.loc[ievt,'b{}_phi'.format(i)] for i in range(4)]
    plt.scatter(betas,bphis,200,color=colors,marker="*")

    #text = "Truth b-quarks:\n"
    ls += ["Truth b-quarks:\n"]
    lc = ['k'] * len(ls)

    ls += ["  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}\n".format(pt,eta,phi) \
            for pt, eta, phi in zip(bpts, betas, bphis)]
    lc += colors

    # If desired - draw the truth HCs
    if drawHCs:
        HC_pts = df.loc[ievt,['pair{}_HC{}_pT'.format(i,j) for i in range(3) for j in [1,2] ]].values
        HC_etas = df.loc[ievt,['pair{}_HC{}_eta'.format(i,j) for i in range(3) for j in [1,2] ]].values
        HC_phis = df.loc[ievt,['pair{}_HC{}_phi'.format(i,j) for i in range(3) for j in [1,2] ]].values
        cs = [ci for ci in ['navy','maroon','darkgreen'] for j in range(2)]

        plt.scatter(HC_etas,HC_phis,color=cs,marker='s')#,label='HCs')

        ls += ["Reco HSs:\n"]
        lc += ['k'] + cs

        ls += ["  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}\n".format(pt,eta,phi) \
                for pt, eta, phi in zip(HC_pts, HC_etas, HC_phis)]


    # Step 3: Draw the reco jets
    ls += ["Reco Jets:\n"]
    lc += ['k']
    for pt, eta, phi, btag in zip(df.loc[ievt,'resolvedJets_pt'],
                                  df.loc[ievt,'resolvedJets_eta'],
                                  df.loc[ievt,'resolvedJets_phi'],
                                  df.loc[ievt,'resolvedJets_is_MV2c10_FixedCutBEff_70']):
        etas, phis = drawCircle(eta,phi)

        c = 'k' if btag else 'grey'
        plt.plot(etas,phis,c)

        lc.append(c)
        ls.append("  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}\n".format(pt,eta,phi))

    if triggerKey is not None:
        ls += ["Passed trigger: {}\n".format(df.loc[ievt,triggerKey])]
        lc += ['k']

    rainbow_text(-4,-4,ls,lc)

    if len(figDir) > 0:
        plt.savefig('{}/evt_{}.pdf'.format(figDir,ievt),bbox_inches='tight')

    plt.show()



def twoValidPairings(ievt,df,figDir=''):
    '''
    Goal: Look at the event display to show where the b-quarks and truth higgses
    are, and the reconstructed HCs and which jet pairings are valid from the MDR
    cut.
    '''

    # Setup the figure
    fig,ax = plt.subplots(figsize=(7.5,3*np.pi))

    plt.xlabel('$\eta$',fontsize=18)
    plt.ylabel('$\phi$',fontsize=18)
    plt.title('Event {} with two valid pairings'.format(ievt),fontsize=18)

    plt.xlim(-2.5,2.5)
    plt.ylim(-np.pi,np.pi)
    plt.grid()

    pt_norm = df.loc[ievt,'resolvedJets_pt'][0]
    pow = 3
    R = 0.4

    markerSize = 169

    # Step 1: Draw the *valid* HCs
    pair_mask = df.loc[ievt,['pair{}_mask'.format(i) for i in range(3) for HC_idx in [1,2]]].values.astype(bool)

    HC_pts  = df.loc[ievt,['pair{}_HC{}_pT'.format(i, HC_idx) for i in range(3) for HC_idx in [1,2]]].values
    HC_etas = df.loc[ievt,['pair{}_HC{}_eta'.format(i,HC_idx) for i in range(3) for HC_idx in [1,2]]].values
    HC_phis = df.loc[ievt,['pair{}_HC{}_phi'.format(i,HC_idx) for i in range(3) for HC_idx in [1,2]]].values

    HC_pts = HC_pts[pair_mask]
    HC_etas = HC_etas[pair_mask]
    HC_phis = HC_phis[pair_mask]

    hc_colors = ['C4','C2','C6','C1']

    plt.scatter(HC_etas,HC_phis,markerSize,color=hc_colors,marker='s',zorder=3,
                linewidth=1,edgecolor='k')#,label='HCs')

    ls = ["Pair 1 HCs:\n"]
    lc = ['k']
    ls += ["  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}\n".format(pt,eta,phi) \
            for pt, eta, phi in zip(HC_pts[:2], HC_etas[:2], HC_phis[:2])]
    lc += hc_colors[:2]

    ls += ["Pair 2 HCs:\n"]
    lc += ['k']
    ls += ["  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}\n".format(pt,eta,phi) \
            for pt, eta, phi in zip(HC_pts[2:], HC_etas[2:], HC_phis[2:])]
    lc += hc_colors[2:]

    # Make a dictionary of lists: circleColors to keep track of which of the analysis
    # jets should be included
    i = 0
    hc1_colors = [hc_colors[0], hc_colors[2]]
    hc2_colors = [hc_colors[1], hc_colors[3]]

    circleColors = {k:[] for k in range(4)}

    for pi, idx1, idx2 in zip(range(3), [(0,1),(0,2),(0,3)], [(2,3),(1,3),(1,2)]):
        if df.loc[ievt,'pair{}_mask'.format(pi)]:

            ss1 = np.sum(df.loc[ievt,['j{}_pT'.format(i1) for i1 in idx1]])
            ss2 = np.sum(df.loc[ievt,['j{}_pT'.format(i2) for i2 in idx2]])

            if ss1 > ss2:

                circleColors[idx1[0]].append(hc1_colors[i])
                circleColors[idx1[1]].append(hc1_colors[i])

                circleColors[idx2[0]].append(hc2_colors[i])
                circleColors[idx2[1]].append(hc2_colors[i])

            else:

                circleColors[idx1[0]].append(hc2_colors[i])
                circleColors[idx1[1]].append(hc2_colors[i])

                circleColors[idx2[0]].append(hc1_colors[i])
                circleColors[idx2[1]].append(hc1_colors[i])
            i += 1

    # Step 2: Draw analysis reco jets (with the muon-in-jet correction)
    ls += ["Reco Jets:\n"]
    lc += ['k']

    # Start off by plotting the analysis jets
    for i in range(4):

        pt, eta, phi = df.loc[ievt,['j{}_{}'.format(i,v) for v in ['pT','eta','phi']]]

        kwargs = {'facecolor':circleColors[i][0],'alpha':1-np.power(1-pt/pt_norm,pow),
                  'hatch':"x",'linewidth':0,'edgecolor':circleColors[i][1],'zorder':2}

        circle = plt.Circle((eta,phi),0.4,**kwargs)
        ax.add_artist(circle)

        if phi-R < np.pi:
            circle = plt.Circle((eta,phi+2*np.pi),R,**kwargs)
            ax.add_artist(circle)
        if phi+R > np.pi:
            circle = plt.Circle((eta,phi-2*np.pi),R,**kwargs)
            ax.add_artist(circle)

        ls += ["  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}\n".format(pt,eta,phi)]
    lc += ['k'] * 4

    # Then plot the non-analysis jets
    for i, pt, eta, phi, mv2, btag in zip(range(df.loc[ievt,'nresolvedJets']),
                                           df.loc[ievt,'resolvedJets_pt'],
                                           df.loc[ievt,'resolvedJets_eta'],
                                           df.loc[ievt,'resolvedJets_phi'],
                                           df.loc[ievt,'resolvedJets_MV2c10'],
                                           df.loc[ievt,'resolvedJets_is_MV2c10_FixedCutBEff_70']):

        if i in list(df.loc[ievt,['b{}_jidx'.format(bi) for bi in range(4)]]):
            continue

        kwargs = {'facecolor':'grey','alpha':1-np.power(1-pt/pt_norm,pow),
                  'linewidth': (2.5 if btag else 0), 'edgecolor':'k',
                  'zorder':1}

        circle = plt.Circle((eta,phi),R,**kwargs)
        ax.add_artist(circle)

        if phi-R < np.pi:
            circle = plt.Circle((eta,phi+2*np.pi),R,**kwargs)
            ax.add_artist(circle)
        if phi+R > np.pi:
            circle = plt.Circle((eta,phi-2*np.pi),R,**kwargs)
            ax.add_artist(circle)

        ls += ["  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}, mv2 = {:.2f}\n".format(pt,eta,phi,mv2)]
    lc += ['grey'] * (df.loc[ievt,'nresolvedJets'].astype(int) - 4)

    ls += ["Truth higgses:\n"]

    # Step 3: Draw the truth higgses
    correctPair = df.loc[ievt,'correctPair'].astype(int)
    HC1_idx = df.loc[ievt,['pair{}_HC1_hidx'.format(i) for i in range(3)]].values[correctPair].astype(int)
    HC2_idx = df.loc[ievt,['pair{}_HC2_hidx'.format(i) for i in range(3)]].values[correctPair].astype(int)

    if correctPair == 0:
        if df.loc[ievt,'pair0_mask']:
            correctHC1_color = "C4"
            correctHC2_color = "C2"
        else:
            print("Error: Do not know how to assign edge color b/c the correctPair is not valid.")
            correctHC1_color = "grey"
            correctHC2_color = "grey"
    elif correctPair == 1:
        if df.loc[ievt,'pair0_mask'] & df.loc[ievt,'pair1_mask']:
            correctHC1_color = "C6"
            correctHC2_color = "C1"
        elif ~df.loc[ievt,'pair0_mask'] & df.loc[ievt,'pair1_mask']:
            correctHC1_color = "C4"
            correctHC2_color = "C2"
        else:
            print("Error: Do not know how to assign edge color b/c the correctPair is not valid.")
            correctHC1_color = "grey"
            correctHC2_color = "grey"
    elif correctPair == 2:
        if df.loc[ievt,'pair2_mask']:
            correctHC1_color = "C6"
            correctHC2_color = "C1"
        else:
            print("Error: Do not know how to assign edge color b/c the correctPair is not valid.")
            correctHC1_color = "grey"
            correctHC2_color = "grey"
    else:
        print("Error: correctPair {} is not a valid idx.".format(correctPair))
        correctHC1_color = "grey"
        correctHC2_color = "grey"

    if HC1_idx == 0:
        hcolors = [correctHC1_color, correctHC2_color]
    else:
        hcolors = [correctHC2_color, correctHC1_color]

    hpts  = df.loc[ievt,['h{}_pt'.format(i) for i in range(2)]]
    hetas = df.loc[ievt,['h{}_eta'.format(i) for i in range(2)]]
    hphis = df.loc[ievt,['h{}_phi'.format(i) for i in range(2)]]
    plt.scatter([hetas],[hphis],markerSize,color=hcolors,label='truth higgses',
                linewidth=1,edgecolor='k',zorder=4)

    ls += ["  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}\n".format(pt,eta,phi) \
            for pt, eta, phi in zip(hpts, hetas, hphis)]
    lc += ['k'] + hcolors

    # Step 2: Draw the truth b-quarks
    bcolors = [hcolors[hidx] for hidx in df.loc[ievt,['b{}_hidx'.format(bi) for bi in range(4)]]]
    bpts = [df.loc[ievt,'b{}_pt'.format(i)] for i in range(4)]
    betas = [df.loc[ievt,'b{}_eta'.format(i)] for i in range(4)]
    bphis = [df.loc[ievt,'b{}_phi'.format(i)] for i in range(4)]
    plt.scatter(betas,bphis,200,color=bcolors,marker="*",linewidth=1,edgecolor='k',zorder=5)

    ls += ["Truth b-quarks:\n"]
    ls += ["  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}\n".format(pt,eta,phi) \
            for pt, eta, phi in zip(bpts, betas, bphis)]
    lc += ['k'] + bcolors

    # Also - while I'm at it, I'm going to draw an edge color around the circle
    # for which is the *correct* match
    for jidx, ci in zip(df.loc[ievt,['b{}_selJet_jidx'.format(bi) for bi in range(4)]],bcolors):

        pt, eta, phi = df.loc[ievt,['j{}_{}'.format(jidx,v) for v in ['pT','eta','phi']]]

        kwargs = {'color':ci,'alpha':1-np.power(1-pt/pt_norm,pow),
                  'fill':False,'linewidth':2.5,'zorder':2}
        circle = plt.Circle((eta,phi),R,**kwargs)
        ax.add_artist(circle)

        if phi-R < np.pi:
            circle = plt.Circle((eta,phi+2*np.pi),R,**kwargs)
            ax.add_artist(circle)
        if phi+R > np.pi:
            circle = plt.Circle((eta,phi-2*np.pi),R,**kwargs)
            ax.add_artist(circle)

    rainbow_text(2.75,3,ls,lc,fontsize=12)

    #plt.xlabel('$\eta$',fontsize=18)
    #plt.ylabel('$\phi$',fontsize=18)
    #plt.title('Event {} with 2 valid pairings'.format(ievt),fontsize=18)

    if len(figDir) > 0:
        plt.savefig('{}/evt_{}.pdf'.format(figDir,ievt),bbox_inches='tight')


    plt.show()

def truthDisplay(ievt, df, figDir='', triggerKey=None, mode=0,
                 jdf=None, mdf=None, tagger='MV2c10', WP=70, quantile=False,
                 text='',nSelectedJets=-1,title='',tag=''):
    '''
    What's special about this event display is that it focuses on the color
    matching b/w the higgses and the b-quarks, and just overlays the jets for
    illustration purposes
    
    ievt:
    df:
    figDir: If not an empty string - the directory to save the figure to 
    triggerKey:
    mode: Indicates the way that the b-quark barcode information was saved
        - 0: The b parent to higgs barcode matching has already been done 
        - 1: We still need to the barcode matching
        
    
    '''

    # Setup the figure
    plt.figure(figsize=(7.5,3*np.pi))

    plt.xlim(-2.5,2.5)
    plt.ylim(-np.pi,np.pi)
    plt.grid()

    plt.xlabel('$\eta$',fontsize=18)
    plt.ylabel('$\phi$',fontsize=18)
    title = f'Event {ievt}' if len(title) == 0 else title
    if len(text) > 0:
        ax = plt.gca()
        plt.text(0,1,text,ha='left',va='bottom',transform=ax.transAxes)
        plt.title(title, fontsize=18, loc='right')
    else:
        plt.title(title, fontsize=18)

    ls = ["Truth $m_{hh}$: " + f"{df.loc[ievt,'truth_mhh']:.0f} GeV\n"]
    ls += ["Truth higgses:\n"]

    # Step 1: Draw the truth higgses
    hpts  = df.loc[ievt,['h{}_pt'.format(i) for i in range(2)]]
    hetas = df.loc[ievt,['h{}_eta'.format(i) for i in range(2)]]
    hphis = df.loc[ievt,['h{}_phi'.format(i) for i in range(2)]]
    hcolors = ['navy','C3']
    plt.scatter([hetas],[hphis],81,color=hcolors)

    ls += ["  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}\n".format(pt,eta,phi) \
            for pt, eta, phi in zip(hpts, hetas, hphis)]
    lc = ['k']*2 + hcolors

    # Step 2: Draw the truth b-quarks
    bpts  = [df.loc[ievt,'b{}_pt'.format(i)]  for i in range(4)]
    betas = [df.loc[ievt,'b{}_eta'.format(i)] for i in range(4)]
    bphis = [df.loc[ievt,'b{}_phi'.format(i)] for i in range(4)]

    if mode == 0:
        bhidxs = [df.loc[ievt,'b{}_hidx'.format(i)] for i in range(4)]
        colors = [hcolors[i] for i in bhidxs]
    elif mode == 1:
        b_parent_barcodes = df.loc[ievt,[f'b{i}_parent_barcode' for i in range(4)]].values
        h_barcodes = df.loc[ievt,[f'h{i}_barcode' for i in range(2)]].values
        colors = [hcolors[0] if b_pb == h_barcodes[0] else hcolors[1] if b_pb == h_barcodes[1] else 'k' 
                  for b_pb in b_parent_barcodes] 
    else:
        print(f'Error mode = {mode} not implemented returning')
        return 

    plt.scatter(betas,bphis,200,color=colors,marker="*")

    ls += ["Truth b-quarks:\n"]
    ls += ["  $p_T$ = {:.2f} GeV, $\eta$ = {:.2f}, $\phi$ = {:.2f}\n".format(pt,eta,phi) \
            for pt, eta, phi in zip(bpts, betas, bphis)]
    lc += ['k'] + colors

    # Step 3: Draw the reco jets
    ls += ["Reco Jets:\n"]
    lc += ['k']
    df2 = df if jdf is None else jdf
    
    if nSelectedJets == -1:
        pdg_to_label = {0:0, 4:1, 5:2, 15:3}
        df2['resolvedJets_label'] = df2['resolvedJets_HadronConeExclTruthLabelID'].map(pdg_to_label)
        
        for pt, eta, phi, mv2, btag, l in zip(df2.loc[ievt,'resolvedJets_pt'],
                                              df2.loc[ievt,'resolvedJets_eta'],
                                              df2.loc[ievt,'resolvedJets_phi'],
                                              df2.loc[ievt,f'resolvedJets_Quantile_{tagger}_Continuous'] if quantile else df.loc[ievt,f'resolvedJets_{tagger}'],
                                              df2.loc[ievt,f'resolvedJets_is_{tagger}_FixedCutBEff_{WP}'],
                                              df2.loc[ievt,'resolvedJets_label']):
            etas, phis = drawCircle(eta,phi)
            c = 'k' if btag else 'grey'
            plt.plot(etas,phis,f'C{l}',zorder=1)
            plt.plot(etas,phis,c,linestyle='--',zorder=2)
            if phi > np.pi - 0.4:
                plt.plot(etas,phis-2*np.pi,f'C{l}',zorder=1)
                plt.plot(etas,phis-2*np.pi,c,linestyle='--',zorder=2)
            elif phi < -np.pi + 0.4:
                plt.plot(etas,phis+2*np.pi,f'C{l}',zorder=1)
                plt.plot(etas,phis+2*np.pi,c,linestyle='--',zorder=2)

            lc.append(c)
            ls.append(f"  $p_T$ = {pt:.2f} GeV, $\eta$ = {eta:.2f}, $\phi$ = {phi:.2f}, {tagger}{' quantile' if quantile else ''} = {mv2:.2f}\n")

    else:
        vs = ['pt','eta','phi','Db']
        Xi = df2.loc[ievt,[f'j{i}_{v}' for i in range(nSelectedJets) for v in vs]].values
        for pt, eta, phi, Q in Xi.reshape(nSelectedJets,len(vs)):
            
            if sum([pt, eta, phi, Q])==0:
                break
            
            etas, phis = drawCircle(eta,phi)
            c = 'k' if Q >=3 else 'grey'
            plt.plot(etas,phis,c,zorder=2)
            if phi > np.pi - 0.4:
                plt.plot(etas,phis-2*np.pi,c,zorder=2)
            elif phi < -np.pi + 0.4:
                plt.plot(etas,phis+2*np.pi,c,zorder=2)

            lc.append(c)
            ls.append(f"  $p_T$ = {pt:.2f} GeV, $\eta$ = {eta:.2f}, $\phi$ = {phi:.2f}, {tagger} quantile = {Q}\n")

    # Optionally - can print the muon information as well!!
    if not (mdf is None):

        ls += ["Muons:\n"]
        lc += ['k']
        c = 'rebeccapurple'
        for pt, eta, phi in zip(mdf.loc[ievt,'muon_pt'], mdf.loc[ievt,'muon_eta'], mdf.loc[ievt,'muon_phi']):

            plt.scatter([eta],[phi],200,color=c,marker='x')

            lc.append(c)
            ls.append(f"  $p_T$ = {pt:.2f} GeV, $\eta$ = {eta:.2f}, $\phi$ = {phi:.2f}\n")



    if triggerKey is not None:
        ls += ["Passed trigger: {}\n".format(df.loc[ievt,'trigger'])]
        lc += ['k']

    ls += ["mc evt weight: {:.2g}\n".format(df.loc[ievt,'mcEventWeight'])]
    lc += ['k']

    rainbow_text(2.75,3,ls,lc)


    if len(figDir) > 0:
        plt.savefig('{}/evt_{}{}.pdf'.format(figDir,ievt,tag),bbox_inches='tight')

    plt.show()
