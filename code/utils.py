'''
Useful functions for pre-processing for these GNN and looking at the truth
pairing studies.
'''

import numpy as np
#from skhep.math.vectors import LorentzVector, Vector3D
from uproot_methods.classes.TLorentzVector import TLorentzVectorArray
import uproot
import pickle

def getTruth4Vectors(tpts, tetas, tphis, tEs, tpdgs, pdg, ievt, miniNtuple):
    '''
    Given lists of the truth particles in the event, return a list of the
    TLorentzVector for the particles matching the requested pdg.

    Inputs:
    - tpts: List of the truth pts in the event
    - tetas: List of the truth etas in the event
    - tphis: List of the truth phis in the event
    - tEs: List of the truth Es in the event
    - tpdgs: List of the truth pdgs in the event
    - pdg: The pdg of the particles we're selecting

    Outputs:
    - tp4s: A list of TLorentzVectors

    '''

    mask = (np.abs(tpdgs) == pdg)
    tp4s = TLorentzVectorArray.from_ptetaphim(tpts,tetas,tphis,tEs)[mask]

    # Save the corresponding output information
    pcles = {5 : 'b', 25 : 'h'}
    vars = ['pt','eta','phi','E','pdg']
    vals = [tp4s.pt, tp4s.eta, tp4s.phi, tp4s.E, tpdgs[mask]]
    for var, val in zip(vars,vals):
    	bcols = ['{}{}_{}'.format(pcles[pdg],i,var) for i in range(np.sum(mask))]
    	miniNtuple.loc[ievt,bcols] = val

    return tp4s

def barcodeMatch(ievt, miniNtuple):
    '''
    Determine which truth higgs each of the b-quarks came from using a barcode
    match

    '''
    tbarcodes = miniNtuple.loc[ievt,'truth_barcode']
    tParentBarcodes = np.array([barc[0] if len(barc) > 0 else -1 for barc in miniNtuple.loc[ievt,'truth_parent_barcode']])
    tpdgs = miniNtuple.loc[ievt,'truth_pdgId']

    hbarcodes = tbarcodes[tpdgs == 25]

    for bi, parentBarcode in enumerate(tParentBarcodes[np.abs(tpdgs) == 5]):
        miniNtuple.loc[ievt,'b{}_hidx'.format(bi)] = 0 if parentBarcode == hbarcodes[0] else 1 if parentBarcode == hbarcodes[1] else -1



def calcMinDr(vs, ws, R=0.4,verbose=False):
    '''
    Given two lists of 4-vectors, return the min distances
    between each particle in vs and the closest particle in ws,
    and the indices these particles in ws.

    Outputs:
    - dr_match: An np.array with the same shape as vs with the distance to the closest particle in ws
    - idx: The index in ws of this closest particle in vs

    '''

    njets = len(vs)

    # Calculate the matrix of dRs for all possible combinations of vs and ws,
    # where R is a matrix of shape (len(vs), len(ws)), st R(iv,iw) is the dR
    # b/w particle vs[iv] and ws[iw]
    D = np.array([[v.delta_r(w) for iw, w in enumerate(ws)] for iv, v in enumerate(vs)])

    # Calculate the indices corresponding to which element of w has the min dr to
    # each element of v, and what this min dR for the match is as well
    idx = np.argmin(D,axis=-1)
    dr_match = np.min(D,axis=-1)

    if verbose:
        # If the indices of the matching are not unique, make sure that
        # one of the drs are greater than R as well, the R for the matching
        # Note: This is a first-pass check, I'm not checking whether one
        # of the non-unique indices matches to this dr_match value greater
        # than R... it's a possible follow-up.
        if np.unique(idx).size != njets:
            if np.max(dr_match) < R:
                print('Waring: Multiple rows are dr matched to the same col')
                print('D',D)
                print('dr_match',dr_match)
                print('idx',idx)
                print('Should maybe consider setting dr_match to an unphysically large value.\n')

    # Return the distances of the matched particles and the indices of the match
    return dr_match, idx
