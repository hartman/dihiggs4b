'''
Some useful functions for processing the analysis after
I have nanoNtuples.

Nicole Hartman
Oct 2019
'''

import numpy as np
import pandas as pd
import uproot
from uproot_methods.classes.TLorentzVector import TLorentzVectorArray
from uproot_methods.classes.TVector3 import TVector3Array
from analysis import fileDir

import os
os.sys.path.append('../../hh4b-background-estimation/hep_ml')
from hep_ml import reweight
import pickle
from glob import glob

import pyhf

def get_xsec(kl):
    '''
    Taken from lhcxswg twiki
    '''
    return 70.3874-50.4111*kl+11.0595*(kl**2)

def nanoToDf(filename,ttree='sig',branches=None):

    print(ttree)
    f = uproot.open(filename)
    df = f[ttree].pandas.df()
    
    # When Sean trains BDTs / NNs, he saves the normalization 
    # parameter separately, so just multiply it to get w_2b and w_2b_VR
    if 'NN_d24_weight_16' in df.columns:
        df['w_2b'] = f['NN_norm_16']._fVal * df['NN_d24_weight_16'] 

    if 'NN_d24_weight_CRderiv_16' in df.columns:
        df['w_2b_VR'] = f['NN_norm_CRderiv_16']._fVal * df['NN_d24_weight_CRderiv_16'] 
    
    return df 

def data2bSigDf(filename,branches=[],yr=15):
    '''
    Get the 2b signal df
    '''

    f = uproot.open(filename)
    t = f['fullmassplane']
    
    arrays = t.arrays(branches)
    
    sig_mask = (arrays[b'kinematic_region']==0) & (arrays[b'ntag']==2)
    
    df = pd.DataFrame()
    
    for k,v in arrays.items():
        if (k == b'kinematic_region') or (k == b'ntag'):
            continue
             
        df[k.decode()] = v[sig_mask]

    # Additionally, get w_2b and w_2b_VR for the bootstrapped bkg estimate
    df['w_2b'] = f[f'NN_norm_bstrap_med_{yr}']._fVal * df[f'NN_d24_weight_bstrap_med_{yr}'] 
    df['w_2b_VR'] = f[f'NN_norm_CRderiv_bstrap_med_{yr}']._fVal * df[f'NN_d24_weight_bstrap_med_{yr}'] 
    
    return df 

def getBDTWeights(data,BDT_fname, trainBDT=False, kinematic_region=2):
    '''
    Add the BDT weights to the dataframe

    Note: I should add functionality for training in the SB before the cuts,
    but since min_dRjj_hc1 isn't looking promising, I'll do this later.

    Inputs:
    - data: pd DataFrame for the data
    - BDT_fname: Name of the pickle file to either save the BDT to or read it from
    - trainBDT: If true, retrain the BDT
    - kinematic_region: The region to use for the kinematic reweighting
                        * 2: SB (default)
                        * 1: CR (for the background systematic)

    '''

    mask_2b = (data.ntag == 2)&(data.kinematic_region == kinematic_region)
    mask_4b = (data.ntag >= 4)&(data.kinematic_region == kinematic_region)

    sort_rw_cols = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2', 'njets']

    if trainBDT:

        BDT_params = {
            'n_estimators' : 50,
            'learning_rate' : 0.1,
            'max_depth' : 3,
            'min_samples_leaf' : 125,
            'gb_args' : {'subsample': 0.4}
            }

        reweighter = reweight.GBReweighter(n_estimators=BDT_params['n_estimators'],
                                           learning_rate=BDT_params['learning_rate'],
                                           max_depth=BDT_params['max_depth'],
                                           min_samples_leaf=BDT_params['min_samples_leaf'],
                                           gb_args=BDT_params['gb_args'])

        original = data.loc[mask_2b,sort_rw_cols]
        target  = data.loc[mask_4b,sort_rw_cols]

        print("Training on columns:", sort_rw_cols)
        reweighter.fit(original, target)

        pickle.dump(reweighter, open( BDT_fname, "wb" ))

    else:

        reweighter = pickle.load(open( BDT_fname, "rb" ))


    N4bTo2b = np.sum(mask_4b) / np.sum(mask_2b)
    key = "w_2b"
    if kinematic_region == 1:
        key += "_CR"
    data[key] = N4bTo2b * reweighter.predict_weights(data[sort_rw_cols])


def simple(signal_data,bkg_data,batch_size=None):
    spec = {
        'channels':[
            {
                'name': 'singlechannel',
                'samples' : [
                    {
                        'name': 'signal',
                        'data': signal_data,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data':bkg_data,
                        'modifiers':[
                        ],
                    },
                ],
            }
        ]
    }
    return pyhf.Model(spec, batch_size=batch_size)


def getExpectedBand(s,b,mus,alpha=0.05):
    '''
    Given histograms for signal and a background and a grid of mu values to test,
    return a list for the -2 sigma, -1 sigma, exp mu, +1 sigma, +2 sigma limits.
    '''

    # Define the pyhf model
    mi = simple(list(s), list(b))

    # Get the scan over mu
    hypo_tests = [pyhf.utils.hypotest(mu, mi.expected_data([0]), mi,
                                      0.5, [(0,np.max(mus))],
                                      return_expected_set=True,
                                      return_test_statistics=True,
                                      qtilde=False)
                  for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    # Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
            for y_vals in cls_exp]

    return band

def bkgSyst(signal_data,bkg_data,var,batch_size=None):
    '''

    '''

    spec = {
        'channels':[
            {
                'name': 'singlechannel',
                'samples' : [
                    {
                        'name': 'signal',
                        'data': signal_data,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data':bkg_data,
                        'modifiers':[
                            {'name': 'low_HT',  'type':'histosys', "data":{"lo_data":var['Low_HT'][0], "hi_data":var['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', "data":{"lo_data":var['High_HT'][0],"hi_data":var['High_HT'][1]}},
                            {'name': 'bkg_norm','type':'normsys',  "data":{"lo":var['norm'][0],"hi":var['norm'][1]}}
                        ],
                    },
                ],
            }
        ]
    }
    return pyhf.Model(spec, batch_size=batch_size)

def getBkgSysBand(s,b,vars,mus,alpha=0.05):
    '''
    Given histograms for signal, background, the high and low HT variations 
    and a grid of mu values to test,return a list for the 
    -2 sigma, -1 sigma, exp mu, +1 sigma, +2 sigma limits.
    
    
    Inputs:
    - s: numpy array for the signal histogram
    - b: numpy array for the background histogram
    - var:  dictionary with keys low_HT, high_HT where the values for the keys 
            are a list with the VR weighted and VR inverted variation histograms
    
    '''

    # Define the pyhf model
    mi = bkgSyst(list(s), list(b), vars)

    # Get the scan over mu
    hypo_tests = [pyhf.utils.hypotest(mu, mi.expected_data([0,0,0]), mi,
                                      [0.5,0,0], [(0,np.max(mus)),(-5,5),(-5,5)],
                                      return_expected_set=True,
                                      return_test_statistics=True,
                                      qtilde=True)
                  for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    # Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
            for y_vals in cls_exp]

    return band

def getLambdaWeights(smnr,L=1):
    '''
    Add weights corresponding to the different lambda variations
    to the dataframe
    '''

    lambdaFile = "../data/LambdaWeightFile.root"
    f = uproot.open(lambdaFile)

    for ttree in f.keys():

        tree = uproot.open(lambdaFile)[ttree]

        idx = np.digitize(smnr['m_hh_cor'],tree.edges)

        l = ttree.decode()[13:][:-2]
        smnr['w_lambda{}'.format(int(l))] = tree.allvalues[idx] * L * smnr['mc_sf']

def hackHistogram(n0,mask_value=-999):
    '''
    Zero out the negative values of the histogram and interpolate b/w 
    neighboring bins for bins w/o any entries
    '''
    
    n = np.where(n0<0,mask_value,n0)
    interp = (n==mask_value)
    
    n[1:-1][interp[1:-1]] = 0.5 * (n[:-2][interp[1:-1]] + n[2:][interp[1:-1]])

    return n

'''
fct in analysis.py more up-to-date
def normalizeWeight(df,physicsSample,lumi):

    sum_weights_initial = 0
    treeName = "MetaData_EventCount_XhhMiniNtuple"

    for myFile in glob(fileDir[physicsSample] + "user.*.MiniNTuple.root"):
        metadata = uproot.open(myFile)[treeName]
        sum_weights_initial += metadata.allvalues[3]

    print("sum_weights_initial",sum_weights_initial)

    assert 'SMNR' in physicsSample # I've hard-coded the mc channel # rn
    db_entry = read_tsv()
    i_mc = 450000

    sample_weight = lumi
    sample_weight *= db_entry.loc[i_mc,"xsec"]
    sample_weight *= db_entry.loc[i_mc,"k_factor"]
    sample_weight *= db_entry.loc[i_mc,"gen_filter_eff"]
    sample_weight /= sum_weights_initial

    print("xsec",db_entry.loc[i_mc,"xsec"])
    print("k_factor",db_entry.loc[i_mc,"k_factor"])
    print("gen_filter_eff",db_entry.loc[i_mc,"gen_filter_eff"])
    print("sample_weight",sample_weight)

    df['mc_sf'] *= sample_weight
'''

def getHelicityAnglesRR(df):
    '''
    Get the helicity angles for the df made from RR trees
    '''
    
    '''
    HC 4 vectors
    '''
    lab_hc1 = TLorentzVectorArray.from_ptetaphim(df.pT_h1.values, 
                                                 df.eta_h1.values, 
                                                 df.phi_h1.values, 
                                                 df.m_h1.values)
    lab_hc2 = TLorentzVectorArray.from_ptetaphim(df.pT_h2.values, 
                                                 df.eta_h2.values, 
                                                 df.phi_h2.values, 
                                                 df.m_h2.values)

    '''
    Jet 4 vectors
    '''
    lab_j11 = TLorentzVectorArray.from_ptetaphim(df.pT_h1_j1.values, 
                                                 df.eta_h1_j1.values, 
                                                 df.phi_h1_j1.values, 
                                                 df.m_h1_j1.values)
    lab_j12 = TLorentzVectorArray.from_ptetaphim(df.pT_h1_j2.values, 
                                                 df.eta_h1_j2.values, 
                                                 df.phi_h1_j2.values, 
                                                 df.m_h1_j2.values)
    
    lab_j21 = TLorentzVectorArray.from_ptetaphim(df.pT_h2_j1.values, 
                                                 df.eta_h2_j1.values, 
                                                 df.phi_h2_j1.values, 
                                                 df.m_h2_j1.values)
    lab_j22 = TLorentzVectorArray.from_ptetaphim(df.pT_h2_j2.values, 
                                                 df.eta_h2_j2.values, 
                                                 df.phi_h2_j2.values, 
                                                 df.m_h2_j2.values)
    
    
    lab_HH = lab_hc1 + lab_hc2

    boost = - lab_HH.boostp3

    rest_hc1 = lab_hc1._to_cartesian().boost(boost)
    rest_hc2 = lab_hc1._to_cartesian().boost(boost)
    df['cosThetaStar'] = np.cos(rest_hc1.theta)

    '''
    Calculate theta1 by booosting hc2 and j11 into the rest frame of hc1
    '''
    boost1 = - lab_hc1.boostp3
    
    hc2s = lab_hc2._to_cartesian().boost(boost1).p3
    j11s = lab_j11._to_cartesian().boost(boost1).p3
    j12s = lab_j12._to_cartesian().boost(boost1).p3
    df['cosTheta1'] = - hc2s.dot(j11s) / (hc2s.mag * j11s.mag)
    
    '''
    Calculate theta2 by booosting hc2 and j11 into the rest frame of hc1
    '''
    boost2 = - lab_hc2.boostp3
    
    hc1s = lab_hc1._to_cartesian().boost(boost2).p3
    j21s = lab_j21._to_cartesian().boost(boost2).p3
    df['cosTheta2'] = - hc1s.dot(j21s) / (hc1s.mag * j21s.mag)
    
    '''
    Finally: Phi and Phi1
    '''
    # Step 1: Get the jets in the hh rest frame
    rest_j11 = lab_j11._to_cartesian().boost(boost) 
    rest_j12 = lab_j12._to_cartesian().boost(boost)

    rest_j21 = lab_j21._to_cartesian().boost(boost)
    rest_j22 = lab_j22._to_cartesian().boost(boost)

    rest_j11 = TVector3Array(rest_j11.x, rest_j11.y, rest_j11.z)
    rest_j12 = TVector3Array(rest_j12.x, rest_j12.y, rest_j12.z)

    rest_j21 = TVector3Array(rest_j21.x, rest_j21.y, rest_j21.z)
    rest_j22 = TVector3Array(rest_j22.x, rest_j22.y, rest_j22.z)

    ebeam = 7000 # GeV
    p1 = TLorentzVectorArray(np.array([0]),np.array([0]),np.array([ebeam]),np.array([ebeam]))
    rest_p1 = p1.boost(boost).p3
    
    z = TVector3Array(rest_p1.x, rest_p1.y, rest_p1.z)
    rest_hc1 = TVector3Array(rest_hc1.x, rest_hc1.y, rest_hc1.z)
    
    # Step 2: Get the normal vectors for the 3 planes
    n1  = rest_j11.cross(rest_j12) / rest_j11.cross(rest_j12).mag 
    n2  = rest_j21.cross(rest_j22) / rest_j21.cross(rest_j22).mag 
    nsc = z.cross(rest_hc1) / z.cross(rest_hc1).mag 

    # Step 3: Get the angles
    Phi  = np.arccos(- n1.dot(n2))
    Phi *= rest_hc1.dot(n1.cross(n2)) / np.abs(rest_hc1.dot(n1.cross(n2)))
    df['Phi'] = Phi
    
    Phi1  =  np.arccos(n1.dot(nsc))
    Phi1 *=  rest_hc1.dot(n1.cross(nsc)) / np.abs(rest_hc1.dot(n1.cross(nsc)))
    df['Phi1'] = Phi1
    
    
