'''
preprocess.py

Goal: I want to be able to run the analysis quickly and analyze results at each stage of
the cutflow, so this collection of functions allows me to submit jobs in parallel over
individual miniNtuples and thn concatenate them at the end after all of the jobs have
finished.

Author Nicole Hartman
Spring 2018

'''

try:
    from root_numpy import root2array
    import ROOT
    from ROOT import TLorentzVector, TVector3
except ImportError:
    print("Running with a conda env: don't import root packages.")

import pandas as pd
import numpy as np
import glob
from utils import calcMinDr

import os
import re


# Make a dictionary to store where all the miniNtuples live.
# This will be different when I'm running on lxplus vs slac
fileDir = {
	'SM_NR': '/eos/user/h/hartman/hh4b/SMNR/user.bstanisl.HH4B.450000.SM_HH.MC16a-2015-2016.AB21.2.61-FEB2019-Prod.pflow_vr-systs-resolved_MiniNTuple.root/',
	'data_15': '/eos/user/m/mproffit/public/hh4b/user.bstanisl.HH4B.2018-04-30T0908Z.data15-rel21_MiniNTuple.root/',
}

# Store the 2015 and 2016 triggers that were used for the previous iteration
# of the analysis
triggers = {
	2015 : ["HLT_2j35_btight_2j35_L14J15.0ETA25",
			"HLT_j100_2j55_bmedium",
			"HLT_j225_bloose"],
	2016 : ["HLT_2j35_bmv2c2060_split_2j35_L14J15.0ETA25",
			"HLT_j100_2j55_bmv2c2060_split",
			"HLT_j225_bmv2c2060_split"]
}

# Parsing years
yr_short = {2015: 15, 15: 15, 2016: 16, 16: 16}

def loadData(filename):
	'''
	Given the filename, load the root file into a pandas DataFrame
	with the desired branches.
	'''

	# Infer from the filename whether or not this sample is mc
	mc = not ('data' in filename)

	# Set up a df with the relevant branches
	branches = ['nresolvedJets',
            	'resolvedJets_E',
            	'resolvedJets_pt',
            	'resolvedJets_phi',
            	'resolvedJets_eta',
            	'resolvedJets_MV2c10',
            	'resolvedJets_HadronConeExclTruthLabelID',
            	'resolvedJets_is_MV2c10_FixedCutBEff_70',
            	'nmuon','muon_pt','muon_eta',
            	'muon_phi','muon_m','muon_EnergyLoss',
            	'passedTriggers']

	mc_branches = ['resolvedJets_SF_MV2c10_FixedCutBEff_70','mcChannelNumber','mcEventWeight']

	if mc: branches += mc_branches

	treeName = "XhhMiniNtuple"
	evtNum = root2array(filename, treeName,branches='eventNumber')#,selection='nresolvedJets >= 4')
	arr = root2array(filename, treeName,branches=branches)#,selection='nresolvedJets >= 4')
	miniNtuple = pd.DataFrame(arr,index=evtNum,columns=branches)

	# Instantiate the extra branches that we want to add
	miniNtuple['m4j'] = 0
	miniNtuple['mc_sf'] = 0 if mc else 1

	for pair in [0,1,2]:
		for HC in [1,2]:
			for var in ['m','dRjj']:
				key = 'pair{}_HC{}_{}'.format(pair,HC,var)
				miniNtuple[key] = 0
		for pvar in ['Dhh','mask']:
			key = 'pair{}_{}'.format(pair,pvar)
			miniNtuple[key] = 0

	miniNtuple['trigger'] = 0
	miniNtuple['nValidPairs'] = 0
	miniNtuple['HC1_vecSum_pT'] = 0
	miniNtuple['HC2_vecSum_pT'] = 0
	miniNtuple['deta_hh'] = 0
	miniNtuple['Xhh'] = 0
	miniNtuple['Xwt'] = 0

	return miniNtuple

# For data, set the mc branches to 1 since these are branches that you don't need to 1
	if not mc:
		for b in mc_branches:
			miniNtuple[b] = 1

			return miniNtuple

def muonInJet(ps,nmu,mu_pts,mu_etas,mu_phis,mu_ms,mu_eloss,Rmatch=0.4):
    '''
    Do the muon in jet correction to add the muon energy back to the jet.

    Inputs:
    - ps: A list of the TLorentzVectors for the jets
	- nmu: The number of muons in the jet
    - mu_pts: An array for the muon pts
    - mu_etas: An array for the muon etas
    - mu_phis: An array for the muon phis
    - mu_ms: An array for the muon ms
    - mu_eloss: The expected energy loss from the muon in the calorimeter
    - Rmatch: The opening angle for a muon to be matched to a jet, default 0.4
              for the small R jets that we use in the analysis.
    Since jet centers can be just over R apart, this means we might be able to
    have multiple jets that are dR matched to the same muon. This will not throw
    an error, but it will print a warning message.

    Modified the list of TLorentzVectors for the jets in place.

    '''

    if nmu == 0:
        return

    # Do the muon in jet correction
    mus = [TLorentzVector() for i in range(nmu)]
    for i, (pt, eta, phi, m) in enumerate(zip(mu_pts,mu_etas,mu_phis,mu_ms)):
        mus[i].SetPtEtaPhiM(pt, eta, phi, m)

    # Do the matching of muons to jets
    # dr_mus distance between the jet and it's closest muon
    # imus indexes this closest muon in the list
    dr_mus, imus = calcMinDr(ps,mus,R=Rmatch)

    # Add the matched muons to the jet 4-vectors
    njets = len(ps)
    for ij, dr, imu in zip(range(njets),dr_mus,imus):
        if dr < Rmatch:

            # Get the energy loss in the calorimeter
            eloss = mu_eloss[imu] / 1000. # It looks like the XhhCommon has this in MeV
            theta, phi = mus[imu].Theta(), mus[imu].Phi()

            v3 = TVector3()
            v3.SetMagThetaPhi(eloss, theta, phi)

            # Add the muon, and subtract off the expected energy loss
            ps[ij] = ps[ij] + mus[imu] - TLorentzVector(v3,eloss)


def lead_dRjj_cut(dRjj, m4j):
    '''
    Given the opening angle of the constituents of the leading HC
    (ordered by the SS of the constituents), return true or false
    for whether or not the pairing is valid, given the 4 jet
    invariant mass.

    Inputs:
    - dRjj: A np array of shape (nEvts,) # nPairings)
    - m4j: A np array of shape (nEvs,) for broadcasting
    '''

    validPairs = np.ones_like(dRjj).astype(bool)

    # low mass cut
    validPairs[(m4j < 1250) & (dRjj < 360 / m4j - 0.5)] = False
    validPairs[(m4j < 1250) & (dRjj > 652.863 / m4j + 0.474449)] = False

    # high mass cut
    validPairs[(m4j > 1250) & (dRjj < 0)] = False
    validPairs[(m4j > 1250) & (dRjj > 0.9967394)] = False

    return validPairs

def subl_dRjj_cut(dRjj, m4j):
    '''
    dR cut for the subleading HC
    '''

    validPairs = np.ones_like(dRjj).astype(bool)

    # low mass cut
    validPairs[(m4j < 1250) & (dRjj < 235.242 / m4j + 0.0162996)] = False
    validPairs[(m4j < 1250) & (dRjj > 874.890 / m4j + 0.347137)] = False

    # high mass cut
    validPairs[(m4j > 1250) & (dRjj < 0)] = False
    validPairs[(m4j > 1250) & (dRjj > 1.047049)] = False

    return validPairs

def allPairs(ps, ievt, miniNtuple):
    '''
    Create HCs for the three combinations for the selected jets.

    Inputs:
    - ps: The 4 vectors for the selected jets
    - ievt: The event we're considering
    - miniNtuple: A reference to the df we're iterating over, for filling the
                  pair?_HC?_{m,dRjj} columns as we creat the pairs.

    Outputs:
    - hc1s: List of the 4-vectors for the HC1 candidates
    '''

    hc1s, hc2s = [], []

    # For 4 jets, we have 3 pairings
    # At this stage, the HCs are sorted by the scalar sum of the pT of the constituents
    for pi, idx1, idx2 in zip(range(3), [(0,1),(0,2),(0,3)], [(2,3),(1,3),(1,2)]):

        # Calculate the scalar sum of the two HCs
        ss1 = ps[idx1[0]].Pt() + ps[idx1[1]].Pt()
        ss2 = ps[idx2[0]].Pt() + ps[idx2[1]].Pt()

        # Order the pairs by the SS of the constituents
        if ss1 > ss2:
            j11, j12 = ps[idx1[0]], ps[idx1[1]]
            j21, j22 = ps[idx2[0]], ps[idx2[1]]

        else:
            j11, j12 = ps[idx2[0]], ps[idx2[1]]
            j21, j22 = ps[idx1[0]], ps[idx1[1]]

        HC1 = j11 + j12
        HC2 = j21 + j22

        hc1s.append(HC1)
        hc2s.append(HC2)

        # Another alternative: if I don't want to pass miniNutple and fill these
        # vectors in the function, I could just return the constituent jets outside
        # of the function and do this inside of the for loop of the run_one function?

        # Calculate the invariant mass
        miniNtuple.loc[ievt,'pair{}_HC1_m'.format(pi)] = HC1.M()
        miniNtuple.loc[ievt,'pair{}_HC2_m'.format(pi)] = HC2.M()

        # Calculate the opening angle between the consituents
        miniNtuple.loc[ievt,'pair{}_HC1_dRjj'.format(pi)] = j11.DeltaR(j12)
        miniNtuple.loc[ievt,'pair{}_HC2_dRjj'.format(pi)] = j21.DeltaR(j22)

    return hc1s, hc2s

def MDR(lead_dR_arr, subl_dR_arr, ievt, miniNtuple):
	'''
	Determine which of the pairings satisfy the MDR cut

	Inputs:
	- lead_dRarr: Array of the opening angle between the jets of the leading HC
	- subl_dRarr: Array of the opening angle between the jets of the subleading HC
	- m4j: The invariant mass of the 4-jets

	Output:
	- pair_mask: A boolean np array with shape 3 for the valid pairings
	'''

	m4j = miniNtuple.loc[ievt,'m4j']
	lead_pair_mask = lead_dRjj_cut(lead_dR_arr, m4j)
	subl_pair_mask = subl_dRjj_cut(subl_dR_arr, m4j)
	pair_mask = lead_pair_mask & subl_pair_mask
	nValidPairs = pair_mask.sum()

	miniNtuple.loc[ievt,'nValidPairs'] = nValidPairs

	return pair_mask


def Dhh(hc1s, hc2s, pair_mask, ievt, miniNtuple):
	'''
	Calculate Dhh for the valid pairs in the event

	Inputs:
	- hc1s, hc2s: Lists of the possible pairings for the HCs
	- pair_mask: Boolean mask of whether or not each pairing passed the MDR cut
	- ievt: The evt we're considering
	- miniNtuple: A reference to the df we're modifying

	Outputs:
	- HC1, HC2: The selected Higgs candidates
	'''

	s = 120 / 100

	Dhh_keys = ['pair{}_Dhh'.format(pair) for pair in range(3)]
	mask_keys = ['pair{}_mask'.format(pair) for pair in range(3)]
	Dhhs = [ np.abs(hc1.M() - s * hc2.M()) / np.sqrt(1 + s**2) for hc1,hc2 in zip(hc1s,hc2s)]

	# Save the Dhh values for future reference
	miniNtuple.loc[ievt,Dhh_keys] = Dhhs
	miniNtuple.loc[ievt,mask_keys] = pair_mask

	# Find the index of the minimum element in the jet that also minimizes this pairing
	pi = min([(Dhh,i) for Dhh, i, valid in zip(Dhhs,range(3),pair_mask) if valid])[1]

	return hc1s[pi], hc2s[pi]


def getXhh(HC1, HC2, mlead_mean=120, msubl_mean=110, mres=0.1):
	'''
	Return the Xhh value for the event

	Inputs:
	- HC1, HC2: The 4-vectors for the leading and subleading HCs in the event
	- mlead_mean, msubl_mean: The center for the ellipse in the (lead,subl) HC mass plane
	- mres: The radius of the elipse, which will be multiplied by the HC mass
	Output:
	- Xhh: The Xhh value for the event, eq (3) from the int note

	'''
	Xhh = 0
	for mi, mean in zip([HC1.M(),HC2.M()],[mlead_mean,msubl_mean]):
		Xhh += ( (mi - mean) / (mres * mi) )**2
	return np.sqrt(Xhh)


def getXwt(ps, nSelectedJets=4):
	'''
	Calculate Xwt for the event by looking at all the jets with pT > 40 GeV and
	|eta| < 2.5, and looking at all possible triplets, choosing the HC-jet leading
	in b-tag disc score as the b-jet, and the other two as the W-jet

	Inputs:
	- ps: A list of the 4-vectors of the jets in the event with pT > 40 GeV and
		  |eta| < 2.5, which have been sorted by b-tag disc. Note, the first 4
		  jets in the list are the ones used for the HCs.

	Output:
	- The min Xwt in this event

	'''

	Xwts = []
	njets = len(ps)

	for ib, bjet in enumerate(ps[:nSelectedJets]):
		for iw1, wjet1 in zip(range(ib+1,njets), ps[ib+1:]):
			for iw2, wjet2 in zip(range(iw1+1,njets), ps[iw1+1:]):

				w_cand = wjet1 + wjet2
				t_cand = bjet + w_cand

				# My getXhh function has enough generality that it's just calculating
				# distances from the center of an elipse in a massplane, so I can
				# use it to find Xwt as well by letting the W act as a proxy for HC1
				# and the t replace HC2, filling in the corresponding values for
				# the center of the ellipse from eq (2) of the int note.
				Xwt = getXhh(w_cand,t_cand,mlead_mean=80.4, msubl_mean=172.5)
				Xwts.append(Xwt)

	return min(Xwts)

def saveVars(ievt, miniNtuple):
	'''
	I wasn't sure if it would help clean up my code if I put all the saving of
	the miniNtuple variables in one spot, but I'm going to forego this for now.
	'''
	pass

def run_one(filename, outputfile, year):
    '''
    Run the analysis over a single miniNtuple

    Inputs:
    - filename: The name of the input file to run over
    - outputDir: A (relative) path for where to save the output data file to

    '''

    mc = not ('data' in filename)
    miniNtuple = loadData(filename)

    # Loop through the events
    Rmatch = 0.4
    nSelectedJets = 4
    cols = ['nresolvedJets','resolvedJets_pt', 'resolvedJets_eta', 'resolvedJets_phi', 'resolvedJets_E',
            'resolvedJets_MV2c10', 'resolvedJets_is_MV2c10_FixedCutBEff_70',
            'resolvedJets_SF_MV2c10_FixedCutBEff_70','mcEventWeight',
            'nmuon','muon_pt','muon_eta','muon_phi','muon_m','muon_EnergyLoss']

    print(miniNtuple.columns)

    counter = 100

    for ievt, (njets, jpts ,jetas, jphis, jEs, jmv2s, jtags, jsfs, mcEvtWt,
               nmu, mu_pts, mu_etas, mu_phis, mu_ms, mu_eloss) in miniNtuple[cols].iterrows():

        if counter == 500:
            break

        if counter % 100 == 0:
            print("Evt",counter)
        counter += 1

        # Trigger
        nTriggersPassed = [ (ti in triggers[year])  for ti in miniNtuple.loc[ievt,'passedTriggers']]
        miniNtuple.loc[ievt,'trigger'] = (sum(nTriggersPassed) > 0)

        # 4 b-tagged jets w/ pT > 40 GeV, |eta| < 2.5
        mask = (jpts > 40) & (np.abs(jetas) < 2.5) # (jmv2s > 0.83)
        nGoodJets = np.sum(mask)
        if nGoodJets < nSelectedJets:
            continue

        # Calculate the mc sf for the event. To do this, we need to consider all of the taggable jets
        if mc:
            miniNtuple.loc[ievt,'mc_sf'] = mcEvtWt #* sample_weight
            for jsf in jsfs[mask]:
                miniNtuple.loc[ievt,'mc_sf'] *= jsf[0]

        # Sort the good jets by mv2
        idx  = np.argsort(jmv2s[mask])[::-1]

        sort_pts = jpts[mask][idx]
        sort_etas = jetas[mask][idx]
        sort_phis = jphis[mask][idx]
        sort_Es = jEs[mask][idx]

        # With 4 jets, we can find the invariant mass
        ps = [TLorentzVector() for i in range(nGoodJets)]
        for i, (pt, eta, phi, E) in enumerate(zip(sort_pts,sort_etas,sort_phis,sort_Es)):
            ps[i].SetPtEtaPhiE(pt, eta, phi, E)

        muonInJet(ps,nmu,mu_pts,mu_etas,mu_phis,mu_ms,mu_eloss,Rmatch)

        # Calculate the di-higgs 4-vector
        p4j = TLorentzVector()
        for i in range(nSelectedJets):
            p4j += ps[i]
        m4j = p4j.M()
        miniNtuple.loc[ievt,'m4j'] = m4j

        hc1s, hc2s = allPairs(ps[:nSelectedJets], ievt, miniNtuple)

        # Select the valid pairings using the mass dependent R cut
        hc1_cols = ['pair{}_HC1_dRjj'.format(i) for i in range(3)]
        hc2_cols = ['pair{}_HC2_dRjj'.format(i) for i in range(3)]

        lead_dR_arr = miniNtuple.loc[ievt,hc1_cols].values
        subl_dR_arr = miniNtuple.loc[ievt,hc2_cols].values

        pair_mask = MDR(lead_dR_arr,subl_dR_arr,ievt,miniNtuple)
        if pair_mask.sum() == 0:
            continue

        HC1, HC2 = Dhh(hc1s, hc2s, pair_mask, ievt,miniNtuple)

        # Resort the HCs by the vector sum of the pT to get the mass dependent pT cut
        miniNtuple.loc[ievt,'HC1_vecSum_pT'] = max(HC1.Pt(),HC2.Pt())
        miniNtuple.loc[ievt,'HC2_vecSum_pT'] = min(HC1.Pt(),HC2.Pt())
        miniNtuple.loc[ievt,'deta_hh'] = np.abs(HC1.Eta() - HC2.Eta())
        miniNtuple.loc[ievt,'Xhh'] = getXhh(HC1, HC2)
        miniNtuple.loc[ievt,'Xwt'] = getXwt(ps,nSelectedJets)


    # Make the masks that define the anlaysis chain
    miniNtuple['fourGoodJets'] = (miniNtuple['m4j'] != 0)

    miniNtuple['MDR'] = (miniNtuple['nValidPairs'] > 0) & miniNtuple['fourGoodJets']

    lead_pT_cut = 0.5  * miniNtuple['m4j'] - 103
    subl_pT_cut = 0.33 * miniNtuple['m4j'] -  73
    miniNtuple['MDpT'] = (miniNtuple['HC1_vecSum_pT'] > lead_pT_cut) & (miniNtuple['HC2_vecSum_pT'] > subl_pT_cut) & miniNtuple['MDR']

    miniNtuple['cut_deta_hh'] = (miniNtuple['deta_hh'] < 1.5) & miniNtuple['MDpT']

    miniNtuple['cut_Xwt'] = (miniNtuple['Xwt'] < 1.6) & miniNtuple['cut_deta_hh']
    miniNtuple['cut_Xhh'] = (miniNtuple['Xhh'] < 1.5) & miniNtuple['cut_Xwt']

    # Save the miniNtuple
    miniNtuple.to_hdf(outputfile, key='df',mode='w')

def read_tsv():
	'''
	Read  the tsv file (this function assumes that my dihiggs repo is parallel
	to the hh-resolved-reconstruction repo), and it returns a pandas DataFrame
	with the rows corresponding to the DISD numbers, and the columns to the
	relevant physics information that we want to extract.
	'''
	tsvFile = "../../hh4b-resolved-reconstruction/mc-config/xsec.tsv"

	# To figure out the format of the .tsv file, I looked at Beojean's MCConfig.cpp file.
	db_cols = ['physics_short','xsec','gen_filter_eff','k_factor','rel_uncert_up','rel_uncert_down','generator']
	db_entry = pd.read_csv(tsvFile,sep='\t \t|\t\t',index_col=0,engine='python',names=db_cols)

	db_entry['xsec'] *= 1000

	return db_entry

def concat_dfs():

	# Get the sample weights for all of the files that you were running over
	sample_weight = 0
	pass


if __name__ == '__main__':

	from argparse import ArgumentParser

	# Start off just using the same OptionParser arguments that Zihao used.
	p = ArgumentParser()
	p.add_argument('--physicsSample', type=str, default='SM_NR',
				   help="The physics sample to run over: rn either SM_NR or data")

	p.add_argument('--year', type=int, default=2015,
                   help="The year to apply the triggers for. Right now only 2015 and 2016 are supported.")

	p.add_argument('--filename', type=str, default='',
               	help="The .root file for the miniNtuple you want to run over.")

	p.add_argument('--batch', action='store_true',
               	help="Submit the individual jobs on the (slac) batch cluster")

	# I don't have all the files downloaded to do this study yet
	p.add_argument('--jetCollection', type=str, default='PFlow',
               	help="The jet collection to do the analysis over")

	p.add_argument('--lumi', type=float, default=3.2,
               	help='Luminosity to normalize the mc signal to.')

	args = p.parse_args()

	print('Running over the {} sample'.format(args.physicsSample))
	print('Using triggers from {}'.format(args.year))
	print('Look at the {} jet collection'.format(args.jetCollection))

	subDir = '{}_{}_{}'.format(args.physicsSample,yr_short[args.year],args.jetCollection)

	# Create the new directory if it doesn't exist
	outputDir = '../data/{}'.format(subDir)
	if not os.path.exists(outputDir):
		print('Creating new directory',outputDir)
		os.makedirs(outputDir)

	if len(args.filename) > 0:

		outputDir += '/files'
		if not os.path.exists(outputDir):
			print('Creating new directory',outputDir)
			os.makedirs(outputDir)

		fileNum = int(re.findall(r'\d+', args.filename)[-1])
		fileTag = '_{:06d}'.format(fileNum)
		outputfile = '{}/df_{}.h5'.format(outputDir,fileTag)

		run_one(args.filename, outputfile, args.year)

    # Otherwise, loop over all of the individual files and submit the jobs
	else:
		print('Still need to implement this file concatenation')
