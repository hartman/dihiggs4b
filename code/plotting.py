'''
Module with functions to create some standard plots to share across notebooks.
'''
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm, Normalize
from matplotlib import gridspec
from matplotlib.offsetbox import TextArea, VPacker, AnnotationBbox
import numpy as np
import pandas as pd
from scipy.stats import entropy, ks_2samp, chisquare
try:
    from ROOT import TH1D, TH2D
    from root_numpy import fill_hist
except ImportError:
    print("Running w/ conda env: don't import root packages.")

from eventDisplays import rainbow_text

def JS(x,y):
    '''
    Calculate the Jensen-Shannon divergence b/w two normalized dists x and y.
    '''
    m = 0.5 * (x + y )
    return 0.5 * (entropy(x,m) + entropy(y,m))

'''
Since I use these same circles for all of the plots, I'm just going to make
them global variables.
'''
SR_x = np.linspace(120 / 1.16, 120 / 0.840000000001)
alpha = np.power(10*(SR_x - 120)/SR_x,2)
SR_y1 = 110 / (1 + 0.1 * np.sqrt(np.power(1.6,2) - alpha))
SR_y2 = 110 / (1 - 0.1 * np.sqrt(np.power(1.6,2) - alpha))

CR_x = np.linspace(124-30,124+30)
beta = np.sqrt(30**2 - np.power(CR_x-124, 2))
CR_y1 = 113 + beta
CR_y2 = 113 - beta

SB_x = np.linspace(126-45,126+45)
gamma = np.sqrt(45**2 - np.power(SB_x-126, 2))
SB_y1 = 116 + gamma
SB_y2 = 116 - gamma

def varEvolution(df, key, masks, colors, labels=[], wkey='mc_sf',
                 nBins=50,var_range=None, density=False,
                 xlabel='', title='',
                 figDir='', baseFigName='', tag='',
                 text='',text_loc=None, ratio=False, rlabel=''):
    '''

    Plot the evolution of the variable throughout an anlaysis chain.

    Inputs:
    - key: The key for the variable that we want to plot
    - masks: A list of the masks to use for the variable while plotting
    - colors: The colors to use for each histogram
    - labels: List of lables for each mask for the legend. This list
              does not passed if there is only one mask in the list
    - wkey: The key in the df to use for the weights for the hist
    - nBins: The number of bins to use for the histogram
    - var_range: The range to use for the histogram
    - density: If True, normalize the histograms (default False)
    - title: A title to put on the figure. The default is no title.
    - figDir: The directory to save the figure to. If no argument is
              passed, the default is the cwd.
    - baseFigName: Tag for the beginning of the figure name.
                   The full figure name will be
                   baseFigName + '_' + key + tag + '.pdf'
                   If no argument is passed, the fig will not be saved
     - tag: Extra tag to append to the figure name (if passed)
     - text: If a non-empty string, text to include on the figure
     - text_loc: The location to put the text on the figure (not supported yet)
     - ratio: Whether to make the plot a ratio
     - rlabel: The label for the ratio subplot

    '''

    assert len(masks) == len(colors)

    # Set up the figure
    if ratio:
        fig = plt.figure(figsize=(6,5))
        gs = gridspec.GridSpec(5,1)
        ax1 = fig.add_subplot(gs[:4,0])
        ax2 = fig.add_subplot(gs[4:,0],sharex=ax1)
    else:
        fig, ax1 = plt.subplots()

    hists = [df.loc[m,key] for m in masks]
    ws = [df.loc[m,wkey] for m in masks]

    my_labels = ['' for c in colors] if len(labels) == 0 else labels

    #fig,ax = plt.subplots()
    for i, (hist, c, w, l) in enumerate(zip(hists,colors,ws,my_labels)):

        n, edg, _ = ax1.hist(hist, bins=nBins,range=var_range,label=l,
                             histtype='step',color=c, weights=w,density=density)

        if ratio:
            if i == 0:
                # Save the baseline for subsequent iterations
                x = 0.5 * (edg[:-1]+edg[1:])
                n0 = n
            else:
                # Plot the ratio
                ax2.scatter(x, n / n0, color=c) #, marker=marker, label=label)

    # Set the labels
    ax1.set_title(title)
    ax1.set_ylabel("Normalized entries" if density else "Entries")
    if ratio:
        ax2.set_xlabel(xlabel if len(xlabel) > 0 else key, fontsize=12)
        ax2.set_ylabel(rlabel if len(rlabel) > 0 else '1 / {}'.format(labels[0]) if len(labels) > 0 else 'ratio')

        # Take out the white space between the figures
        plt.subplots_adjust(hspace=0)

        # Take out the largest ytick on ax2 so that it doesn't overlap with ax1
        yticks = [0,0.25,0.5,0.75]
        ax2.set_yticks(yticks)
        ax2.set_yticklabels([str(yi) for yi in yticks])
        ax2.set_ylim(0,1)

    else:
        ax1.set_xlabel(xlabel if len(xlabel) > 0 else key, fontsize=12)


    if len(labels) > 0:
        ax1.legend()

    if len(text) > 0:
        _,ymax = ax1.get_ylim()
        ax1.text(0,ymax,text)
        ax1.set_ylim(0,1.1*ymax)

    # Also, take out the 0 from the yticks on ax1 if we're using the ratio mode
    if ratio:
        yticks = ax1.get_yticks()
        ax1.set_yticks(yticks[1:])
        ax1.set_yticklabels(["{:2g}".format(yi) for yi in yticks[1:]])
        if len(text) > 0: ax1.set_ylim(0,1.1*ymax)

    if len(baseFigName) > 0:
        plt.savefig('{}/{}_{}{}{}.pdf'.format(figDir,baseFigName,key,tag,'_ratio' if ratio else ''))
    plt.show()

def massPlane(m1s, m2s, ws, cmap, nBins=20, varRange=(0,200), title='',
              figDir='',baseFigName='',tag='',text=''):
    '''

    Make the mass plane plot.

    Inputs:
    - m1s: HC1 masses
    - m2s: HC2 masses
    - ws: The associated weights
    - cmap: The color map to use
    - title: The title for the figure
    - figDir: The directory to save the figure to. If no argument is
              passed, the default is the cwd.
    - baseFigName: Tag for the beginning of the figure name.
                   If no baseFigName is passed, the fig will not be saved
    - tag: Additional tag to add to the figure name
           The full figure name will be
           baseFigName + '_massplane' + tag + '.pdf'
     - text: If a non-empty string, text to include on the figure

    '''

    for logTag,normObj in zip(['','_log'],[Normalize(),LogNorm()]):

        plt.figure()

        plt.hist2d(m1s, m2s, bins=nBins, range=[varRange]*2,
                   cmap=cmap,weights=ws,norm=normObj)
        plt.colorbar()

        plt.xlabel('$m_{HC1}$ [GeV]',fontsize=14)
        plt.ylabel('$m_{HC2}$ [GeV]',fontsize=14)
        plt.title(title)

        plt.plot(SR_x, SR_y1,'hotpink')
        plt.plot(SR_x, SR_y2,'hotpink')

        plt.plot(CR_x, CR_y1,'darkorange')
        plt.plot(CR_x, CR_y2,'darkorange')

        plt.plot(SB_x, SB_y1,'navy')
        plt.plot(SB_x, SB_y2,'navy')

        if len(text) > 0:
            ax = plt.gca()
            plt.text(.05,.9,text,ha='left',va='top',transform=ax.transAxes)
            #plt.text(.05*varRange[1],.9*varRange[1],text)

        if len(baseFigName) > 0:
            plt.savefig('{}/{}_massplane{}{}.pdf'.format(figDir,baseFigName,tag,logTag),bbox_inches='tight')

    plt.show()

def normHist2d(c, xe, ye, normCol=True, xlabel='', ylabel='', title='',
               figName='', figDir='../figures', cmap=None, returnCounts=False):
    '''
    Given the output from a 2d histogram, display the result either normalized
    by rows or columns

    Inputs:
    - c: a 2d np.array for the counts
    - xe: the x edges
    - ye: the x edges
    - normCol: True if you normalize by the col, pass False to normalize by row
    - xlabel: Label for the x-axis of the histogram
    - ylabel: Label for the y-axis of the histogram
    - title: Title for the figure
    - figName: Base name to save the figure to (will append rowNorm or colNorm
               as well to this name depending on normCol)
    - subDir: Subdirectory inside ../figures to save the image to
    - cmap: The color map to use for the plot
    - returnCounts: If True, return the histogram for the normalized counts that
                    is being plotted

    Note: If I divide by 0 because one of the cols (or rows),  don't have any
    entried, it will print an error message, but the output of the function
    is still fine since I just replace NaNs with 0s.

    '''

    normAxis = 1 if normCol else 0

    col_sum = np.sum(c, axis=normAxis, keepdims=True)
    c_new = c / col_sum
    c_new = np.nan_to_num(c_new)

    plt.figure()
    plt.imshow(c_new.T, origin="lower", aspect='auto',
               extent=[xe[0],xe[-1],ye[0],ye[-1]],
               cmap=cmap,norm=LogNorm())
    plt.colorbar()
    plt.xlabel(xlabel, fontsize=14)
    plt.ylabel(ylabel, fontsize=14)
    plt.title(title)

    if len(figName) > 0:
        normTag = 'colNorm' if normCol else 'rowNorm'
        plt.savefig('{}/{}_{}.pdf'.format(figDir,figName,normTag),bbox_inches='tight')

    plt.show()

    if returnCounts:
        return c_new

def cf_2b_same_HC(df, nbi = 25, mask=None, dataText='', title='',
                  figDir='', tag='SB',stats=False):
    '''
    Ok, so I've been doing some checks comparing the 2b to 4b distributions
    when the 2b events are in the same HC, so I'm turning this into a function
    so that I can look at this plot across all the notebooks since it's
    something that I want to check for each of the pairing algorithms.

    Note: This notebook has the functionality to fix the ordering to be by HC
    pT if the sorting is by scalar sum of the jet constituents, as it is in
    the baseline analysis.

    Inputs:
    - df: DataFrame made from the nano Ntuples
    - nbi: The # of bins to use for each of the 2d histograms, default 25
    - mask: An additional mask to plot for an additional selection
    - title: Title for the figures
    - figDir: If passed, the directory to save the figure to
    - tag: Tag to put after the variable name when saving the figure
    - stats: If true, include the chi^2 / ndf and KS tests in the figure as well
    '''

    # Make a dummy mask that is always true if no mask is being passed
    mi = np.ones_like(df.pT_h1).astype(bool) if mask is None else mask

    for c,r,xlabel,textLoc in zip(['m_','E_','pT_','eta_','phi_'],
                          [(45,210),(0,1000),(0,400),(-2.5,2.5),(-np.pi,np.pi)]*2,
                          ['$m$ [GeV]','$E$ [GeV]', '$p_T$ [GeV]','$|\eta|$','$\phi$'],
                          [(165,.55),(700,.55),(300,.5),(1,.85),(0,.1)]):

        fig = plt.figure(figsize=(6,5))
        gs = gridspec.GridSpec(5,1)
        ax1 = fig.add_subplot(gs[:4,0])
        ax2 = fig.add_subplot(gs[4:,0],sharex=ax1)

        m = (df.pT_h1 > df.pT_h2)

        v1 = np.where(m, df[c+'h1'], df[c+'h2'])
        tags1 = np.where(m, df['tag_h1_j1'].astype(int)+df['tag_h1_j2'].astype(int),
                            df['tag_h2_j1'].astype(int)+df['tag_h2_j2'].astype(int))
        N_h1_2b = np.sum((df.ntag == 2) & (tags1 == 2)  & mi)

        hc1_2b,edg,_ = ax1.hist(v1[(df.ntag == 2) & (tags1 == 2) & mi], nbi, r,
                                weights=np.ones(N_h1_2b) / N_h1_2b,
                                color='orange', linestyle='--', histtype='step',
                                label='2b in leading HC')

        v2 = np.where(m, df[c+'h2'],df[c+'h1'])
        tags2 = np.where(m, df['tag_h2_j1'].astype(int)+df['tag_h2_j2'].astype(int),
                            df['tag_h1_j1'].astype(int)+df['tag_h1_j2'].astype(int))
        N_h2_2b = np.sum((df.ntag == 2) & (tags2 == 2)  & mi)

        hc2_2b,edg,_ = ax1.hist(v2[(df.ntag == 2) & (tags2 ==2)  & mi], nbi, r,
                                weights=np.ones(N_h2_2b) / N_h2_2b,
                                color='royalblue', linestyle='--', histtype='step',
                                label='2b in subleading HC')

        N_4b = np.sum((df.ntag >= 4) & mi)
        hc1_4b,edg = np.histogram(v1[(df.ntag >= 4) & mi], nbi, r, weights=np.ones(N_4b) / N_4b)
        hc2_4b,edg = np.histogram(v2[(df.ntag >= 4) & mi], nbi, r, weights=np.ones(N_4b) / N_4b)

        N_2b = np.sum((df.ntag == 2) & mi)

        if c=="m_":
            print("2b in HC1 = {}".format(N_2b))
            print("2b in HC1 = {}, % 2b = {:.2f}".format(N_h1_2b,100*N_h1_2b/N_2b))
            print("2b in HC2 = {}, % 2b = {:.2f}".format(N_h2_2b,100*N_h2_2b/N_2b))
            print("4b in HC1 = {}".format(N_4b))


        # Add Poissonian error bars for 4b
        x = 0.5*(edg[1:] + edg[:-1])

        ax1.errorbar(x, hc1_4b, yerr=np.sqrt(hc1_4b / N_4b),
                     color='orange', label='4b: leading HC',
                     markersize=0, drawstyle='steps-mid', linewidth=1)
        ax1.errorbar(x, hc2_4b, yerr=np.sqrt(hc2_4b / N_4b),
                     color='royalblue', label='4b: subleading HC',
                     markersize=0, drawstyle='steps-mid', linewidth=1)

        # Add the ratio below
        ax2.errorbar(x, hc1_4b / hc1_2b, np.sqrt(hc1_4b / N_4b) / hc1_2b,
                     fmt='.',color='orange')
        ax2.errorbar(x, hc2_4b / hc2_2b, np.sqrt(hc2_4b / N_4b)/ hc2_2b,
                     fmt='.',color='royalblue')

        ax2.plot([edg[0], edg[-1]],[1]*2,'k--')

        # Add error bars on the ratio plot as well, assuming no errors for the rw 2b
        if len(dataText) != 0:
            ax1.text(0,1,dataText,ha='left',va='bottom',transform=ax1.transAxes)
            ax1.set_title(title, loc='right', fontsize=14)
        else:
            ax1.set_title(title)

        ax1.legend()
        ax2.set_xlabel(xlabel,fontsize=14)
        ax1.set_ylabel('Normalized Entries',fontsize=14)
        ax2.set_ylabel('4b / 2b',fontsize=12)
        ax2.set_ylim(0.5,2)

        # Include some comparison stats as well
        if stats:

            ls=[]
            lc=['orange','royalblue']

            v2bs = [v1[(df.ntag == 2) & (tags1 == 2) & mi],
                    v2[(df.ntag == 2) & (tags2 ==2)  & mi]]
            v4bs = [v1[(df.ntag >= 4) & mi],
                    v2[(df.ntag >= 4) & mi]]

            for HCi,v2b,v4b in zip(["1","2"],v2bs,v4bs):

                h_2b = TH1D(c+HCi+"_2b", c+HCi+"_2b", nbi, *r)
                h_4b = TH1D(c+HCi+"_4b", c+HCi+"_4b", nbi, *r)

                fill_hist(h_2b, v2b)
                fill_hist(h_4b, v4b)

                chi2 = h_4b.Chi2Test(h_2b,"CHI2/NDF")
                #KS_pval = h_4b.KolmogorovTest(h_2b)
                #text = "$\chi^2$/ndf = {:.2f}, KS $p$-val = {:.2g}\n".format(chi2, KS_pval)
                text = "$\chi^2$/ndf = {:.2f}\n".format(chi2)
                ls.append(text)

            _,ymax = ax1.get_ylim()
            plt.sca(ax1)
            rainbow_text(textLoc[0],textLoc[1]*ymax,ls,lc,yoffset=0.55)

        #ax1.text(1.025,1,text,ha='left',va='top',transform=ax1.transAxes)
        #else:
        #    ax1.text(textLoc[0],textLoc[1]*ymax,text,ha='left',va='top')


        if len(figDir) > 0:
            plt.savefig('{}/{}{}.pdf'.format(figDir,c,tag),bbox_inches='tight')

        plt.show()

def sig_bkg_massplanes(data, smnr, nb=33, mhRanges=[(45,210)]*2,
                       loc=(50,190),text='',w_data='mc_sf',
                       data_title='2b 2016 data',sig_title='SM NR signal',
                       blinded=False,bkg_cmap="YlGnBu",sig_cmap="BuPu",
                       figDir='',tag='sig_2b2016'):
    '''
    Goal: Compare the signal and background massplanes and save them in a
    single figure. Note, the default nb and mhRanges correspond to what were
    the bins from the paper.

    Inputs:
    - data: Dataframe for the 2b data (I could modify this to look at the 2b rw data)
    - smnr: Dataframe for the signal
    - nb: The number of bins for the mass planes
    - mhRanges: The ranges to supply the 2d histogram
    - loc: The location for the text, if a text string is passed
    - text: The text string to add to the figures
    - w_data: The weight to apply for the 2b data: mc_sf is just 2b data and
              corresponds to no reweighting, while I normally will use w_2b
              for a 2b reweighting
    - data_title: The title for the data plot
    - sm_title: The title for the signal plot
    - figDir: If passed, the directory to save the plot to
    - tag: The tag to postpend to the figure name

    '''

    fig, axes = plt.subplots(1,2,figsize=(6,3))

    # 2b data
    bkg_mask = (data.ntag == 2)
    axes[0].hist2d(data.loc[bkg_mask,"m_h1"],data.loc[bkg_mask,"m_h2"],
                   nb,mhRanges,cmap=bkg_cmap,weights=data.loc[bkg_mask,"mc_sf"])

    axes[0].set_xlabel('$m_{HC1}$ [GeV]')
    axes[0].set_ylabel('$m_{HC2}$ [GeV]')

    # 4b signal
    sig_mask = (smnr.ntag >= 4)
    if blinded:
        sig_mask = sig_mask & (smnr.kinematic_region != 0)
    axes[1].hist2d(smnr.loc[sig_mask,"m_h1"],smnr.loc[sig_mask,"m_h2"],
                   nb,mhRanges,cmap=sig_cmap,weights=smnr.loc[sig_mask,"mc_sf"])
    axes[1].set_xlabel('$m_{HC1}$ [GeV]')

    for ax in axes:
        ax.plot(SR_x, SR_y1,'hotpink')
        ax.plot(SR_x, SR_y2,'hotpink')

        ax.plot(CR_x, CR_y1,'darkorange')
        ax.plot(CR_x, CR_y2,'darkorange')

        ax.plot(SB_x, SB_y1,'navy')
        ax.plot(SB_x, SB_y2,'navy')

        if len(text) > 0:
            ax.text(*loc, text)

    axes[0].set_title(data_title)
    axes[1].set_title(sig_title)

    if len(figDir) > 0:
        plt.savefig('{}/massplane_{}.pdf'.format(figDir,tag),bbox_inches='tight')

    plt.show()

def bkg_2bsameHC_massplanes(data, nb=33, mhRanges=[(45,210)]*2,cmap="YlGnBu",
                            loc=(50,190), text='',title='2016 data',
                            blinded=True, figDir='',tag='2016',chi2Option="UU"):
    '''
    Goal: Compare the signal and background massplanes and save them in a
    single figure. Note, the default nb and mhRanges correspond to what were
    the bins from the paper.

    Inputs:
    - data: Dataframe for the 2b data (I could modify this to look at the 2b rw data)
    - nb: The number of bins for the mass planes
    - mhRanges: The ranges to supply the 2d histogram
    - loc: The location for the text, if a text string is passed
    - text: The text string to add to the figures
    - blinded: Whether or not our 4b data should be blinded
    - figDir: If passed, the directory to save the plot to
    - tag: The tag to postpend to the figure name

    '''

    # Define the histograms
    h2b = TH2D("mp_2b","mp_2b",nb,*mhRanges[0],nb,*mhRanges[1])
    h2b_sameHC = TH2D("mp_2b_hc","mp_2b_hc",nb,*mhRanges[0],nb,*mhRanges[1])
    h4b = TH2D("mp_4b","mp_4b",nb,*mhRanges[0],nb,*mhRanges[1])

    h2b.Sumw2()
    h2b_sameHC.Sumw2()
    h4b.Sumw2()

    fig, axes = plt.subplots(1,3,figsize=(12,3.4))

    # 2b data
    mask = (data.ntag == 2)
    axes[0].hist2d(data.loc[mask,"m_h1"],data.loc[mask,"m_h2"],
                   nb,mhRanges,cmap=cmap,
                   weights=None if chi2Option=="UU" else data.loc[mask,"mc_sf"])
    if blinded:
        mask = mask & (data.kinematic_region != 0)
    fill_hist(h2b,np.vstack((data.loc[mask,"m_h1"],data.loc[mask,"m_h2"])).T)

    axes[0].set_xlabel('$m_{HC1}$ [GeV]')
    axes[0].set_ylabel('$m_{HC2}$ [GeV]')

    '''
    2b where the bs are required to come from the *same* HC
    '''
    HC1_2b = data.tag_h1_j1 & data.tag_h1_j2
    HC2_2b = data.tag_h2_j1 & data.tag_h2_j2
    mask = (data.ntag == 2) & (HC1_2b | HC2_2b)
    axes[1].hist2d(data.loc[mask,"m_h1"], data.loc[mask,"m_h2"],
                   nb,mhRanges,cmap=cmap,
                   weights=None if chi2Option=="UU" else data.loc[mask,"mc_sf"])
    if blinded:
        mask = mask & (data.kinematic_region != 0)
    fill_hist(h2b_sameHC,np.vstack((data.loc[mask,"m_h1"],data.loc[mask,"m_h2"])).T)

    axes[1].set_xlabel('$m_{HC1}$ [GeV]')

    # 4b signal
    mask = (data.ntag >= 4)
    if blinded:
        mask = mask & (data.kinematic_region != 0)
    axes[2].hist2d(data.loc[mask,"m_h1"],data.loc[mask,"m_h2"],
                   nb,mhRanges,cmap=cmap,
                   weights=None if chi2Option=="UU" else data.loc[mask,"mc_sf"])
    fill_hist(h4b,np.vstack((data.loc[mask,"m_h1"],data.loc[mask,"m_h2"])).T)

    axes[2].set_xlabel('$m_{HC1}$ [GeV]')


    for h,t in zip([h2b,h2b_sameHC],['2b','2b same HC']):

        # Print the metrics
        chi2 = h4b.Chi2Test(h,chi2Option+";CHI2/NDF")
        KS_pval = h4b.KolmogorovTest(h)
        print(t)
        print("$\chi^2$/ndf = {:.2f}, KS $p$-val = {:.2g}\n".format(chi2, KS_pval))



    '''
    Draw the SR, CR, SB on the histograms
    '''
    for ax in axes:
        ax.plot(SR_x, SR_y1,'hotpink')
        ax.plot(SR_x, SR_y2,'hotpink')

        ax.plot(CR_x, CR_y1,'darkorange')
        ax.plot(CR_x, CR_y2,'darkorange')

        ax.plot(SB_x, SB_y1,'navy')
        ax.plot(SB_x, SB_y2,'navy')

        if len(text) > 0:
            ax.text(*loc, text)

    axes[0].set_title("2b "+title)
    axes[1].set_title("2b SAME HC "+title)
    axes[2].set_title("4b "+title)

    if len(figDir) > 0:
        plt.savefig('{}/massplane_{}.pdf'.format(figDir,tag),bbox_inches='tight')

    plt.show()

def bkg_2b_rw_massplanes(data, nb=33, mhRanges=[(45,210)]*2,
                         loc=(50,190), text='',
                         blinded=True, figDir='',tag='bkgRwt'):
    '''
    Goal: Compare the signal and background massplanes and save them in a
    single figure. Note, the default nb and mhRanges correspond to what were
    the bins from the paper.

    Inputs:
    - data: Dataframe for the 2b data (I could modify this to look at the 2b rw data)
    - nb: The number of bins for the mass planes
    - mhRanges: The ranges to supply the 2d histogram
    - loc: The location for the text, if a text string is passed
    - text: The text string to add to the figures
    - blinded: Whether or not our 4b data should be blinded
    - figDir: If passed, the directory to save the plot to
    - tag: The tag to postpend to the figure name

    '''

    fig, axes = plt.subplots(1,3,figsize=(12,3.4))

    # 2b data
    mask = (data.ntag == 2)
    axes[0].hist2d(data.loc[mask,"m_h1"],data.loc[mask,"m_h2"],
                   nb,mhRanges,cmap="YlGnBu")

    axes[0].set_xlabel('$m_{HC1}$ [GeV]')
    axes[0].set_ylabel('$m_{HC2}$ [GeV]')

    '''
    2b reweighted
    '''
    axes[1].hist2d(data.loc[mask,"m_h1"], data.loc[mask,"m_h2"],
                   nb,mhRanges,cmap="YlGnBu",weights=data.loc[mask,"w_2b"])

    axes[1].set_xlabel('$m_{HC1}$ [GeV]')

    # 4b signal
    mask = (data.ntag >= 4)
    if blinded:
        mask = mask & (data.kinematic_region != 0)
    axes[2].hist2d(data.loc[mask,"m_h1"],data.loc[mask,"m_h2"],
                   nb,mhRanges,cmap="YlGnBu")

    axes[2].set_xlabel('$m_{HC1}$ [GeV]')

    '''
    Draw the SR, CR, SB on the histograms
    '''
    for ax in axes:
        ax.plot(SR_x, SR_y1,'hotpink')
        ax.plot(SR_x, SR_y2,'hotpink')

        ax.plot(CR_x, CR_y1,'darkorange')
        ax.plot(CR_x, CR_y2,'darkorange')

        ax.plot(SB_x, SB_y1,'navy')
        ax.plot(SB_x, SB_y2,'navy')

        if len(text) > 0:
            ax.text(*loc, text)

    axes[0].set_title("2b 2016 data")
    axes[1].set_title("2b reweighted 2016 data")
    axes[2].set_title("4b 2016 data")

    if len(figDir) > 0:
        plt.savefig('{}/massplane_{}.pdf'.format(figDir,tag),bbox_inches='tight')

    plt.show()

def cf_1d_dists(data, drawUnweighted=True, sameHC=False,
                w2b='w_2b',extra_w=None, mask=None,
                title='sideband', dataText='',label='2b reweighted',extra_label='',
                nbi=50, color_2b="C9",color_2bsameHC="C1",extra_color="C1",
                figDir='',tag='_CR',stats=False,chi2Only=True,notRootChi2=False):
    '''
    Goal: When I'm evaluating the performance of the pairing algorithm with the
    different categories of the analysis, I'm comparing all of the 1d
    distributions and looking at the agreement using ratio plots and maybe some
    similarity metrics

    Inputs:
    - data: DataFrame from the nano Ntuple
    - drawUnweighted: Whether to also include the unnormalized distributions on
                      the plot.
    - sameHC: Whether to include the 2b same HC (reweighted) distributions
    - w2b: The key to retrieve the 2b weights with
    - extra_w: The key to retrieve another set of weights for the 2b histogram
    - mask: Extra mask to apply to the data
    - title: Title for the figures
    - dataText: Text to include in upper left of the plot
    - label: Label for the histogram with the w2b weights
    - extra_label: Label for the histogram with the extra_w weights
    - nbi: Number of bins to use for the continuous variables
    - color_2b: Color to use for 2b data
    - color_2bsameHC: Color to use for 2b same HC data (only drawn if sameHC=True)
    - extra_color: Color to use for 2b data with extra_w weights
    - figDir: If passed, the directory to save the plot to
    - tag: The tag to postpend to the figure name
    - stats: Whether or not to include stats for the similarity metrics on the plot
    - chi2Only: Just add chi^2 / ndf to the plot, color coded w/ the 2b colors

    '''

    cols = ['njets', 'm_hh','pt_hh','X_wt','m_hh_cor',
            'm_h1','E_h1','pT_h1','eta_h1','phi_h1','dRjj_h1',
            'm_h2','E_h2','pT_h2','eta_h2','phi_h2','dRjj_h2',
            #'m_h1_j1','E_h1_j1','pT_h1_j1','eta_h1_j1','phi_h1_j1','tag_h1_j1',
            #'m_h1_j2','E_h1_j2','pT_h1_j2','eta_h1_j2','phi_h1_j2','tag_h1_j2',
            #'m_h2_j1','E_h2_j1','pT_h2_j1','eta_h2_j1','phi_h2_j1','tag_h2_j1',
            #'m_h2_j2','E_h2_j2','pT_h2_j2','eta_h2_j2','phi_h2_j2','tag_h2_j2',
            'pT_4','pT_2','eta_i','dRjj_1','dRjj_2']


    nBins = [8,nbi,nbi,nbi,nbi,
             nbi,nbi,nbi,nbi,nbi,nbi,
             nbi,nbi,nbi,nbi,nbi,nbi,
             #nbi,nbi,nbi,nbi,nbi,4,
             #nbi,nbi,nbi,nbi,nbi,4,
             #nbi,nbi,nbi,nbi,nbi,4,
             #nbi,nbi,nbi,nbi,nbi,4,
             nbi,nbi,nbi,nbi,nbi]

    myRanges = [(2.5,10.5),(0,600),(0,150),(0,8),(0,1200),
                (40,180),(0,1000),(0,400),(-2.5,2.5),(-np.pi,np.pi),(0,2*np.pi),
                (40,180),(0,1000),(0,400),(-2.5,2.5),(-np.pi,np.pi),(0,2*np.pi),
                #(0,40),(0,500),(0,250),(-2.5,2.5),(-np.pi,np.pi),(-1.5,2.5),
                #(0,40),(0,500),(0,250),(-2.5,2.5),(-np.pi,np.pi),(-1.5,2.5),
                #(0,40),(0,500),(0,250),(-2.5,2.5),(-np.pi,np.pi),(-1.5,2.5),
                #(0,40),(0,500),(0,250),(-2.5,2.5),(-np.pi,np.pi),(-1.5,2.5),
                (0,125),(0,200),(0,2.5),(0,np.pi),(0,np.pi)]

    textLocs = [(7,.75), (0,.75), (20,.3),(1.5,.3),(700,.75),
                (80,.3),(600,.75),(225,.75),(-1,.5),(0,.3),(3.5,.75),
                (80,.3),(600,.75),(225,.75),(-1,.5),(0,.3),(3.5,.75),
                #(20,.75),(275,.75), (150,.75),(-1,.5),(0,.3),(-1.5,.75),
                #(20,.75),(275,.75), (150,.75),(-1,.5),(0,.3),(-1.5,.75),
                #(20,.75),(275,.75), (150,.75),(-1,.5),(0,.3),(-1.5,.75),
                #(20,.75),(275,.75), (150,.75),(-1,.5),(0,.3),(-1.5,.75),
                (70,.75),(125,.75),(1.5,.75),(1.75,.75),(0,.975)]

    if mask is None:
        mask = np.ones_like(data.ntag).astype(bool)

    if sameHC:
        HC1_2b = data.tag_h1_j1 & data.tag_h1_j2
        HC2_2b = data.tag_h2_j1 & data.tag_h2_j2
        hc_2b = HC1_2b | HC2_2b

    mask_2b = (data.ntag == 2) & mask
    mask_4b = (data.ntag >= 4) & mask

    N_4b = np.sum(mask_4b)
    N_2b = np.sum(mask_2b)

    print(N_2b)

    for c, nb, myRange, textLoc in zip(cols,nBins,myRanges, textLocs):

        if c not in data.columns:
            continue


        fig = plt.figure(figsize=(6,5))
        gs = gridspec.GridSpec(5,1)
        ax1 = fig.add_subplot(gs[:4,0])
        ax2 = fig.add_subplot(gs[4:,0],sharex=ax1)

        kargs = {'bins': nb, 'range': myRange}

        if drawUnweighted:
            n_2b_orig,_,_ = ax1.hist(data.loc[mask_2b,c],color='C1',label='2b',
                                     histtype='step', **kargs,
                                     weights=data.loc[mask_2b,'mc_sf'] / N_2b)
        n_2b,edg,_ = ax1.hist(data.loc[mask_2b,c],color=color_2b, label=label,
                              histtype='step', **kargs,
                              weights=data.loc[mask_2b,w2b] / np.sum(data.loc[mask_2b,w2b]))

        if sameHC:
            n_2b_sameHC,edg,_ = ax1.hist(data.loc[mask_2b & hc_2b,c],color=color_2bsameHC, label='2b rw same HC',
                                  histtype='step', **kargs,
                                  weights=data.loc[mask_2b & hc_2b,'w_2bsameHC'] / np.sum(data.loc[mask_2b & hc_2b,'w_2bsameHC']))

        if extra_w is not None:
            n_2b_extra,edg,_ = ax1.hist(data.loc[mask_2b,c],color=extra_color, label=extra_label,
                                        histtype='step', **kargs,
                                        weights=data.loc[mask_2b,extra_w] / np.sum(data.loc[mask_2b,extra_w]))

        # Use Poissonian error bars for the 4b data
        x = 0.5 * (edg[:-1] + edg[1:])
        n_4b,edg = np.histogram(data.loc[mask_4b,c], **kargs)
        ax1.errorbar(x, n_4b / N_4b, yerr=np.sqrt(n_4b) / N_4b, color='k',label='4b '+tag[1:3],
                     markersize=0,drawstyle='steps-mid',linewidth=1)

        # Plot the ratio of 4b / 2b prediction, with error bars for 4b
        ax2.errorbar(x, n_4b / (N_4b * n_2b), yerr=np.sqrt(n_4b) / (N_4b * n_2b), fmt='.', color='k')
        if sameHC:
            ax2.errorbar(x, n_4b / (N_4b * n_2b_sameHC),
                         yerr=np.sqrt(n_4b) / (N_4b * n_2b_sameHC),
                         fmt='.', color=color_2bsameHC)
        if extra_w is not None:
            ax2.errorbar(x, n_4b / (N_4b * n_2b_extra),
                         yerr=np.sqrt(n_4b) / (N_4b * n_2b_extra),
                         fmt='.', color=extra_color)


        ax2.plot([edg[0],edg[-1]], [1]*2, color=color_2b, linestyle='--')

        ax2.set_xlabel(c,fontsize=14)
        ax2.set_ylabel('4b / rw 2b',fontsize=12)
        ax1.set_ylabel('Normalized entries',fontsize=14)
        ax2.set_ylim(0.75,1.25)
        loc = 'upper right' if c == 'X_wt' else None
        ax1.legend(fontsize=12,loc=loc)

        ymin,ymax=ax1.get_ylim()
        if len(dataText) != 0:
            ax1.text(0,1,dataText,ha='left',va='bottom',transform=ax1.transAxes)
            ax1.set_title(title, loc='right',fontsize=14)
        else:
            ax1.set_title(title)

        if stats or chi2Only:
            # For the chi^2 fct, I need to use a root histogram
            h_2b = TH1D(c+"_2b", c+"_2b", nb, *myRange)
            h_4b = TH1D(c+"_4b", c+"_4b", nb, *myRange)

            h_2b.Sumw2()

            fill_hist(h_2b, data.loc[mask_2b,c], weights=data.loc[mask_2b,"w_2b"])
            fill_hist(h_4b, data.loc[mask_4b,c])

            stat, pval = h_4b.Chi2Test(h_2b,"UW;CHI2/NDF"), h_4b.Chi2Test(h_2b,"UW")

            if chi2Only:
                text = "$\chi^2$/ndf = {:.2f}\n".format(stat)

                ls = [text]
                lc = [color_2b]

                if sameHC:
                    h_2bsameHC = TH1D(c+"_2b", c+"_2b", nb, *myRange)
                    fill_hist(h_2bsameHC, data.loc[mask_2b & hc_2b,c],
                              weights=data.loc[mask_2b & hc_2b,"w_2bsameHC"])
                    stat = h_4b.Chi2Test(h_2bsameHC,"UW;CHI2/NDF")

                    ls.append("$\chi^2$/ndf = {:.2f}".format(stat))
                    lc.append(color_2bsameHC)

                plt.sca(ax1)
                rainbow_text(textLoc[0],textLoc[1]*0.75*ymax,ls,lc,yoffset=0.1)

            else:

                text = "$\chi^2$/ndf = {:.2f}, $p$ = {:.2f}\n".format(stat, pval)

                stat, pval = h_4b.KolmogorovTest(h_2b,"M"), h_4b.KolmogorovTest(h_2b)
                text += "KS stat = {:.2g}, $p$ = {:.2g}\n".format(stat, pval)

                text += "KL div = {:.2g}\n".format(entropy(n_4b, n_2b))
                text += "JS div = {:.2g}".format(JS(n_4b / N_4b, n_2b))

                if drawUnweighted:
                    ax1.text(1.025,1,text,ha='left',va='top',transform=ax1.transAxes)
                else:
                    ax1.text(textLoc[0],textLoc[1]*ymax,text,ha='left',va='top')

        if notRootChi2:

            mask=(n_4b != 0) & (n_2b != 0)

            text = "$\chi^2$/ndf = {:.2f}\n".format(chisquare(n_4b[mask],n_2b[mask])[0]/(nb-1))

            ls = [text]
            lc = [color_2b]

            if extra_w is not None:

                text = "$\chi^2$/ndf = {:.2f}\n".format(chisquare(n_4b,n_2b_extra)[0]/(nb-1))

                ls.append(text)
                lc.append(extra_color)

            print(ls)
            plt.sca(ax1)
            rainbow_text(textLoc[0],textLoc[1]*0.75*ymax,ls,lc,yoffset=0.1)

        # break
        if len(figDir) > 0:
            plt.savefig('{}/{}{}.pdf'.format(figDir,c,tag),bbox_inches='tight')

        plt.show()
# def cf_hists(df1, df2, c, tag, nb=10, myRange=None):
#     
#     '''
#     Goal: Compare the 4b signal, 2b reweighted estimates (with the weights
#     derived in the CR), and the 4b data (if we're *not* including 
#     the SR in the plot).
#     '''
# 
#     # Infer the mask from the tag variable that gets passed
#     mask1_2b = (df1.ntag == 2)
#     mask1_4b = (df1.ntag >= 4)
# 
#     mask2_2b = (df2.ntag == 2)
#     mask2_4b = (df2.ntag >= 4)
#     
#     if tag == '':
#         unblind = False
#         title = 'full mass plane'
#         
#     elif tag == '_SR':
#         
#         unblind = False
#         
#         mask1_2b = mask1_2b & (df1.kinematic_region == 0)
#         mask1_4b = mask1_4b & (df1.kinematic_region == 0)
# 
#         mask2_2b = mask2_2b & (df2.kinematic_region == 0)
#         mask2_4b = mask2_4b & (df2.kinematic_region == 0)
#         
#         title = 'signal region'
#         
#     elif tag == '_VR':
#           
#         unblind = True
#             
#         mask1_2b = mask1_2b & (df1.kinematic_region == 1)
#         mask1_4b = mask1_4b & (df1.kinematic_region == 1)
# 
#         mask2_2b = mask2_2b & (df2.kinematic_region == 1)
#         mask2_4b = mask2_4b & (df2.kinematic_region == 1)
#         
#         title = 'control region'
#         
#     elif tag == '_CR':
#        
#         unblind = True
#     
#         mask1_2b = mask1_2b & (df1.kinematic_region == 2)
#         mask1_4b = mask1_4b & (df1.kinematic_region == 2)
# 
#         mask2_2b = mask2_2b & (df2.kinematic_region == 2)
#         mask2_4b = mask2_4b & (df2.kinematic_region == 2)
#     
#         title = 'sideband'
#     
#     else:
#         print("Error in cf_hists: Unrecognized argument {} for argument `tag`".format(tag))
#         return
#     
#     fig = plt.figure(figsize=(6,5))
#     gs = gridspec.GridSpec(5,1)
#     ax1 = fig.add_subplot(gs[:4,0])
#     ax2 = fig.add_subplot(gs[4:,0],sharex=ax1)
# 
#     kargs = {'bins': nb, 'range': myRange}
#     
#     # 2b rw background histograms
#     ts,edg,_ = ax1.hist(df1.loc[mask1_2b,c],color="C9", label='Topo:  reweighted 2b',
#                         histtype='step', **kargs,
#                         weights=df1.loc[mask1_2b,'w_2b'])
#     
#     ps,edg,_ = ax1.hist(df2.loc[mask2_2b,c],color="C9", label='PFlow: reweighted 2b',
#                         histtype='step', **kargs,linestyle='--',
#                         weights=df2.loc[mask2_2b,'w_2b'])
# 
#     x = 0.5 * (edg[:-1] + edg[1:])
#     ax2.plot(x,ps/ts,marker='.', linewidth=0, color='C9')
#     
#     # 4b background histograms
#     if unblind:
#         
#         topo_N4b = np.sum(topo_4b_bkg)
#         pflow_N4b = np.sum(pflow_4b_bkg)
#         
#         ts,edg,_ = ax1.hist(topo_data16.loc[topo_4b_bkg,c],color="k", label='Topo:  4b data',
#                         histtype='step', **kargs)
#     
#         ps,edg,_ = ax1.hist(pflow_data16.loc[mask2_4b,c],color="k", label='PFlow: 4b data',
#                             histtype='step', **kargs,linestyle='--')
# 
#         ax2.plot(x,ps/ts,marker='.', linewidth=0, color='k')
#     
#     ax2.plot([edg[0],edg[-1]], [1]*2,color='grey', linestyle='--')
#     ax2.set_ylim(0.9,1.1)
#     
#     ax2.set_xlabel(c,fontsize=14)
#     ax2.set_ylabel('PFlow / Topo',fontsize=10)
#     ax1.set_ylabel('Normalized entries',fontsize=14)
#     ax1.legend(fontsize=12)
# 
#     ymin,ymax=ax1.get_ylim()
#     ax1.text(0,1,dataText,ha='left',va='bottom',transform=ax1.transAxes)
#     ax1.set_title(title, loc='right',fontsize=14)
# 
#     plt.savefig('figures/kinematics/{}{}.pdf'.format(c,tag),bbox_inches='tight')
# 
#     plt.show()



def plot_results(ax, test_mus, cls_obs, cls_exp, test_size=0.05,unblind=False):

    if unblind:
        ax.plot(test_mus, cls_obs, c = 'k')
    for i,c in zip(range(5),['k','k','k','k','k']):
        ax.plot(test_mus, cls_exp[i], c = c, linestyle = 'dotted' if i!=2 else 'dashed')
    ax.fill_between(test_mus,cls_exp[0],cls_exp[-1], facecolor = 'yellow')
    ax.fill_between(test_mus,cls_exp[1],cls_exp[-2], facecolor = 'lime')
    ax.plot(test_mus,[test_size]*len(test_mus), c = 'r')
    ax.set_ylim(0,1)

def invert_interval(test_mus, cls_obs, cls_exp, test_size = 0.05):
    point05cross = {'exp':[],'obs':None}
    for cls_exp_sigma in cls_exp:
        y_vals = cls_exp_sigma
        point05cross['exp'].append(np.interp(test_size, list(reversed(y_vals)), list(reversed(test_mus))))
    y_vals = cls_obs
    point05cross['obs'] = np.interp(test_size, list(reversed(y_vals)), list(reversed(test_mus)))
    return point05cross
    
    
'''
Some cut opt plots that I've been looking at
'''
def plot_MDpT(df,nBins=30,histRange=[[200,1250],[0,400]],
              cmap='RdPu',text='',title='',figDir='',tag='',
              a_pT1=0.5,  b_pT1=-103,
              a_pT2=0.33, b_pT2=-73,
              labels=[],linestyles=[]
             ):
    '''
    Draw the 2d histogram plot of dRjj vs m4j for the leading and subleading HC
    that we cut on for our MDR cuts.
    
    
    Inputs:
    - df:
    - nBins:
    - range:
    - cmap:
    - text:
    - title:
    - figDir:
    - tag:
    - a_dRjj1_min, b_dRjj1_dmin
    - a_dRjj1_max, b_dRjj1_max
    - a_dRjj2_min, b_dRjj2_min
    - a_dRjj2_max, b_dRjj2_max

    
    '''
    
    fig, axes = plt.subplots(1,2,figsize=(8,4))
    
    m4j = np.linspace(*histRange[0])
    
    for ax,col,ylabel,a,b,c in zip(axes,['lead_HC_pt','subl_HC_pt'],
                                   ['$p_T^{HC1}$','$p_T^{HC2}$'],
                                   [a_pT1,a_pT2],[b_pT1,b_pT2],
                                   ['rebeccapurple','navy']):
    
        n,xx,yy = np.histogram2d(df['m_hh'],df[col],nBins,histRange,weights=df['mc_sf'])
        pcm = ax.pcolormesh(xx,yy,n.T, cmap=cmap)
        fig.colorbar(pcm, ax=ax)
        ax.set_xlabel('$m_{4j}$ [GeV]',fontsize=12)
        ax.set_ylabel(ylabel+' [GeV]',fontsize=12)
    
        # Overlay the proposed cuts
        if type(a) == list:
            for ai, bi, l, s in zip(a,b,labels,linestyles):
                ax.plot(m4j, ai * m4j + bi, c, label=l, linestyle=s)
            ax.legend(loc='lower right', fontsize=12)
        else:
            ax.plot(m4j, a * m4j + b, c)
        ax.set_ylim(*histRange[1])
    
    # Add text and a title(?)
    axes[0].text(0,1,text,ha='left',va='bottom',transform=axes[0].transAxes)
    axes[1].set_title(title,loc='right',fontsize=16)
    plt.tight_layout()
    
    # Save the figure
    if len(figDir) > 0:
        plt.savefig(f'{figDir}/pT_hi_vs_m4j{tag}.pdf',bbox_inches='tight')
    
    plt.show()


def plotSigBkg(sig_df,bkg_df,col,w2b='mc_sf',nBins=100,histRange=None,abs=True,
               sig_c='C4',bkg_c='k',bkg_label='2b 2016 data',
               text='',title='',figDir='',tag='',
               cut=None,xlabel=None,
              ):
    '''
    Overlay the histograms for signal and background.
    
    Note: It would also be nice to overlay the proposed categories if that's
    something we're going to try to do.
    
    Inputs:
    - sig_df:
    - bkg_df:
    - nBins:
    - range:
    - cmap:
    - text:
    - title:
    - figDir:
    - tag:
    '''
    
    plt.hist(np.abs(sig_df[col]) if abs else sig_df[col],
             nBins,histRange,label='4b SM NR',color=sig_c,histtype='step',
             weights=sig_df['mc_sf']/np.sum(sig_df['mc_sf']))
    plt.hist(np.abs(bkg_df[col]) if abs else bkg_df[col],
             nBins,histRange,label=bkg_label,color=bkg_c,histtype='step',
             weights=bkg_df[w2b]/np.sum(bkg_df[w2b]))

    if xlabel is None:
        plt.xlabel(col,fontsize=14)
    else:
        plt.xlabel(xlabel,fontsize=14)
    plt.ylabel('Normalized entries',fontsize=14)
    
    ax = plt.gca()
    plt.text(0,1,text,ha='left',va='bottom',transform=ax.transAxes)
    plt.title(title,loc='right',fontsize=16)
    plt.legend()
    
    if len(figDir) > 0:
        plt.savefig(f'{figDir}/{col}{tag}.pdf',bbox_inches='tight')
    
    plt.show()

def plot2dSig(sig_df,bkg_df,col1='m_hh',col2='deta_hh',
              nBins=[100,100],histRange=[[0,1250],[0,2.5]],N2bTo4b=1,w2b='w_2b',
              ylabel='$|\Delta \eta_{hh}|$',smText='',dataText='',
              figDir='',tag=''):
    '''
    For a given signal and background, get the 2d significance and plot it!
    '''
    
    fig = plt.figure(figsize=(8, 9))
    gs = gridspec.GridSpec(3,2)
    ax1 = fig.add_subplot(gs[0,0])
    ax2 = fig.add_subplot(gs[0,1])
    ax3 = fig.add_subplot(gs[1:,:])

    s,xx,yy = np.histogram2d(sig_df[col1],np.abs(sig_df[col2]),
                           nBins,histRange,weights=sig_df['mc_sf'])
    ps = ax1.pcolormesh(xx,yy,s.T,cmap='Reds')
    fig.colorbar(ps, ax=ax1)

    ax1.text(0,1,smText,ha='left',va='bottom',transform=ax1.transAxes)

    b,xx,yy = np.histogram2d(bkg_df[col1],np.abs(bkg_df[col2]),
                           nBins,histRange,
                           #weights=N2bTo4b*np.ones_like(bkg_df.index))
                           weights=bkg_df[w2b])
    pb = ax2.pcolormesh(xx,yy,b.T,cmap='Blues')
    fig.colorbar(pb, ax=ax2)

    ax2.text(0,1,dataText,ha='left',va='bottom',transform=ax2.transAxes)

    sig = s/np.sqrt(b)
    pcm = ax3.pcolormesh(xx,yy,sig.T,cmap='Purples')
    fig.colorbar(pcm, ax=ax3)

    sigText = '$\sqrt{\sum_i{s_i^2 / b_i}}$ = '
    sigText += f'{np.sqrt(np.sum(sig[(b!=0)&(s>0)]**2)):.3f}'  

    Texts = []
    for t,fs in zip(['$s / \sqrt{b}$',sigText],[24,16]):
        Texts.append(TextArea(t,textprops=dict(fontsize=fs)))
    texts_vbox = VPacker(children=Texts,pad=0,sep=10,align='center')
    ann = AnnotationBbox(texts_vbox,(.75,.8),xycoords=ax3.transAxes,
                         bboxprops=dict(facecolor='white',alpha=0.5))
    ann.set_figure(fig)
    fig.artists.append(ann)

    '''
    Maybe I should also add some text in the lower right for what step of the 
    analysis we're calculating the significance in?
    '''
    
    for ax,fs in zip([ax1,ax2,ax3],[10,10,16]):
        ax.set_xlabel('$m_{hh}$ [GeV]',fontsize=fs)
        ax.set_ylabel(ylabel,fontsize=fs)

    fig.tight_layout()
    if len(figDir) > 0:
        plt.savefig(f'{figDir}/sig2d_{col1}_{col2}{tag}.pdf',bbox_inches='tight')
    fig.show()
        
    print(np.sum(s),np.sum(b))

    return np.where(s>0,s,0), np.where(b>0,b,0)
    
    
def recreateCutflowRR(sm,k10,ntag=4):
    '''
    Recreate the parts of the cutflow tables that I can w/ the $m_{hh}$ reweighting
    '''

    idx = ['everything',f'{ntag}b','$|\Delta \eta_{hh}|<1.5$','$X_{wt}>1.5$','CR','VR','SR']
    cols = ['$\kappa_\lambda$ = 1', '$\kappa_\lambda$ = 10', 'rw $\kappa_\lambda$ = 10']
    cols += ['err '+c for c in cols]
    
    df = pd.DataFrame(0,idx,cols)
    
    # everything
    df.loc['everything',   '$\kappa_\lambda$ = 1' ] = np.sum( sm.mc_sf)
    df.loc['everything',   '$\kappa_\lambda$ = 10'] = np.sum(k10.mc_sf)
    df.loc['everything','rw $\kappa_\lambda$ = 10'] = np.sum( sm.w_k10)
    
    df.loc['everything','err $\kappa_\lambda$ = 1']     = np.sum( sm.mc_sf**2)
    df.loc['everything','err $\kappa_\lambda$ = 10']    = np.sum(k10.mc_sf**2)
    df.loc['everything','err rw $\kappa_\lambda$ = 10'] = np.sum( sm.w_k10**2)
    
    
    # ntag
    if ntag == 4:
        m_sm = (sm.ntag  >= 4)
        m_10 = (k10.ntag >= 4)
    else:
        m_sm = (sm.ntag  == ntag)
        m_10 = (k10.ntag == ntag)

    df.loc[f'{ntag}b',   '$\kappa_\lambda$ = 1' ] = np.sum( sm.loc[m_sm,'mc_sf'])
    df.loc[f'{ntag}b',   '$\kappa_\lambda$ = 10'] = np.sum(k10.loc[m_10,'mc_sf'])
    df.loc[f'{ntag}b','rw $\kappa_\lambda$ = 10'] = np.sum( sm.loc[m_sm,'w_k10'])
    
    df.loc[f'{ntag}b','err $\kappa_\lambda$ = 1']     = np.sqrt(np.sum( sm.loc[m_sm,'mc_sf']**2))
    df.loc[f'{ntag}b','err $\kappa_\lambda$ = 10']    = np.sqrt(np.sum(k10.loc[m_10,'mc_sf']**2))
    df.loc[f'{ntag}b','err rw $\kappa_\lambda$ = 10'] = np.sqrt(np.sum( sm.loc[m_sm,'w_k10']**2))
    
    
    # deta_hh
    if not ('dEta_hh' in  sm.columns):  sm['dEta_hh'] =  sm['deta_hh']
    if not ('dEta_hh' in k10.columns): k10['dEta_hh'] = k10['deta_hh']
    
    assert np.min( sm.dEta_hh) > 0 
    assert np.min(k10.dEta_hh) > 0 
    
    m_sm = m_sm & (sm.dEta_hh  < 1.5)
    m_10 = m_10 & (k10.dEta_hh < 1.5)
    
    df.loc['$|\Delta \eta_{hh}|<1.5$',   '$\kappa_\lambda$ = 1' ] = np.sum( sm.loc[m_sm,'mc_sf'])
    df.loc['$|\Delta \eta_{hh}|<1.5$',   '$\kappa_\lambda$ = 10'] = np.sum(k10.loc[m_10,'mc_sf'])
    df.loc['$|\Delta \eta_{hh}|<1.5$','rw $\kappa_\lambda$ = 10'] = np.sum( sm.loc[m_sm,'w_k10'])    

    df.loc['$|\Delta \eta_{hh}|<1.5$','err $\kappa_\lambda$ = 1']     = np.sqrt(np.sum( sm.loc[m_sm,'mc_sf']**2))
    df.loc['$|\Delta \eta_{hh}|<1.5$','err $\kappa_\lambda$ = 10']    = np.sqrt(np.sum(k10.loc[m_10,'mc_sf']**2))
    df.loc['$|\Delta \eta_{hh}|<1.5$','err rw $\kappa_\lambda$ = 10'] = np.sqrt(np.sum( sm.loc[m_sm,'w_k10']**2))
    
    
    # Xwt 
    m_sm = m_sm & (sm.X_wt > 1.5)
    m_10 = m_10 & (k10.X_wt > 1.5)
    
    df.loc['$X_{wt}>1.5$',   '$\kappa_\lambda$ = 1' ] = np.sum( sm.loc[m_sm,'mc_sf'])
    df.loc['$X_{wt}>1.5$',   '$\kappa_\lambda$ = 10'] = np.sum(k10.loc[m_10,'mc_sf'])
    df.loc['$X_{wt}>1.5$','rw $\kappa_\lambda$ = 10'] = np.sum( sm.loc[m_sm,'w_k10'])
    
    df.loc['$X_{wt}>1.5$','err $\kappa_\lambda$ = 1']     = np.sqrt(np.sum( sm.loc[m_sm,'mc_sf']**2))
    df.loc['$X_{wt}>1.5$','err $\kappa_\lambda$ = 10']    = np.sqrt(np.sum(k10.loc[m_10,'mc_sf']**2))
    df.loc['$X_{wt}>1.5$','err rw $\kappa_\lambda$ = 10'] = np.sqrt(np.sum( sm.loc[m_sm,'w_k10']**2))
    
    # final bkg rw + signal regions 
    for r,region in zip([2,1,0],['CR','VR','SR']):
        
        df.loc[region,   '$\kappa_\lambda$ = 1' ] = np.sum(sm.loc[ m_sm & ( sm.kinematic_region==r),'mc_sf'])
        df.loc[region,   '$\kappa_\lambda$ = 10'] = np.sum(k10.loc[m_10 & (k10.kinematic_region==r),'mc_sf'])
        df.loc[region,'rw $\kappa_\lambda$ = 10'] = np.sum(sm.loc[ m_sm & ( sm.kinematic_region==r),'w_k10'])
    
        df.loc[region,'err $\kappa_\lambda$ = 1']     = \
            np.sqrt(np.sum( sm.loc[m_sm & ( sm.kinematic_region==r),'mc_sf']**2))
        df.loc[region,'err $\kappa_\lambda$ = 10']    = \
            np.sqrt(np.sum(k10.loc[m_10 & (k10.kinematic_region==r),'mc_sf']**2))
        df.loc[region,'err rw $\kappa_\lambda$ = 10'] = \
            np.sqrt(np.sum( sm.loc[m_sm & ( sm.kinematic_region==r),'w_k10']**2))

    return df
    
    
    
def compareRW(sm,k10,title,figname='',col='truth_mhh',nb=100,r=(0,1000),
              xlabel='truth $m_{hh}$ [GeV]',norm=False,ylim=(0.5,1.5)):

    fig = plt.figure(figsize = (6,6))
    gs = gridspec.GridSpec(2,1, height_ratios =[3,1])
    ax0 = plt.subplot(gs[0])
    ax1 = plt.subplot(gs[1])

    edg = np.linspace(*r,nb+1)
    xmid = 0.5*(edg[1:]+edg[:-1]) 

    '''
    Get the histograms and error bars
    '''
    n_sm     = np.histogram(sm[col],edg,weights=sm['mc_sf'])[0]
    n_sm_err = np.sqrt(np.histogram(sm[col],edg,weights=sm['mc_sf']**2)[0])
    
    n_rw     = np.histogram(sm[col],edg,weights=sm['w_k10'])[0]
    n_rw_err = np.sqrt(np.histogram(sm[col],nb,r,weights=sm['w_k10']**2)[0])
    
    n_10     = np.histogram(k10[col],edg,weights=k10['mc_sf'])[0]
    n_10_err = np.sqrt(np.histogram(k10[col],edg,weights=k10['mc_sf']**2)[0])
    
    kwargs = {'lw':0, 'elinewidth':2, 'marker':'.'}
    
    if norm:
        
        # Overlay the histograms
        ax0.errorbar(xmid, n_sm/np.sum(n_sm), n_sm_err/np.sum(n_sm), 
                     color='gold', label='$\kappa_\lambda$=1',**kwargs)
        ax0.errorbar(xmid, n_10/np.sum(n_10), n_10_err/np.sum(n_10), 
                     color='deeppink', 
                     label='$\kappa_\lambda$=10',**kwargs)

        ax0.errorbar(xmid, n_rw/np.sum(n_rw), n_rw_err/np.sum(n_rw), color='turquoise',
                     label='$\kappa_\lambda$=1 rw to $\kappa_\lambda$=10', **kwargs)
    
        # Plot the ratio
        ax1.errorbar(xmid,(n_rw/np.sum(n_rw))/(n_10/np.sum(n_10)),
                     (n_rw_err/np.sum(n_rw))/(n_10/np.sum(n_10)),
                     color='turquoise',**kwargs)
    else:
        
        # Overlay the histograms
        ax0.errorbar(xmid, n_sm, n_sm_err, color='gold', 
                     label='$\kappa_\lambda$=1',**kwargs)
        ax0.errorbar(xmid, n_10, n_10_err, color='deeppink', 
                     label='$\kappa_\lambda$=10',**kwargs)

        ax0.errorbar(xmid, n_rw, n_rw_err, color='turquoise',
                     label='$\kappa_\lambda$=1 rw to $\kappa_\lambda$=10', 
                     **kwargs)
    
        # Plot the ratio
        ax1.errorbar(xmid,n_rw/n_10, n_rw_err/n_10,
                     color='turquoise',**kwargs)
    
    ax1.set_xlabel(xlabel,fontsize=15)
    ax0.set_ylabel('Normalized entries' if norm else 'Entries', fontsize=15)
    ax1.set_ylabel('ratio',fontsize=15)
    ax0.legend(fontsize=12)
    ax0.set_title(title,fontsize=15,loc='left')

    ax1.set_ylim(ylim)
    ax1.plot(edg,np.ones_like(edg),color='deeppink',ls='--')

    if len(figname) > 0:
        if norm:
            plt.savefig(figname[:-4]+'_norm.pdf',bbox_inches='tight')
        else:
            plt.savefig(figname,bbox_inches='tight')
    plt.show()
    
    
def compareRWCat(sm,k10,title,figname='',col='truth_mhh',nb=100,r=(0,1000),
                 xlabel='truth $m_{hh}$ [GeV]',csb = np.linspace(0,1,6),
                 figsize=(15,3.5),ylim=(0.5,1.5)):

    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(2,len(csb)-1, height_ratios =[3,1])

    edg = np.linspace(*r,nb+1)
    xmid = 0.5*(edg[1:]+edg[:-1]) 
    kwargs = {'lw':0, 'elinewidth':2, 'marker':'.'}
    
    for i, (cs_min,cs_max) in enumerate(zip(csb[:-1],csb[1:])):
        ax0 = plt.subplot(gs[0,i])
        ax1 = plt.subplot(gs[1,i])

        # Retrieve the appropriate mask
        mi_sm = (sm.absCosThetaStar  > cs_min) & (sm.absCosThetaStar  < cs_max)
        mi_10 = (k10.absCosThetaStar > cs_min) & (k10.absCosThetaStar < cs_max)
        
        '''
        Get the histograms and error bars
        '''
        n_sm     = np.histogram(sm.loc[mi_sm,col],edg,weights=sm.loc[mi_sm,'mc_sf'])[0]
        n_sm_err = np.sqrt(np.histogram(sm.loc[mi_sm,col],edg,
                                        weights=sm.loc[mi_sm,'mc_sf']**2)[0])

        n_rw     = np.histogram(sm.loc[mi_sm,col],edg,weights=sm.loc[mi_sm,'w_k10'])[0]
        n_rw_err = np.sqrt(np.histogram(sm.loc[mi_sm,col],nb,r,
                                        weights=sm.loc[mi_sm,'w_k10']**2)[0])

        n_10     = np.histogram(k10.loc[mi_10,col],edg,weights=k10.loc[mi_10,'mc_sf'])[0]
        n_10_err = np.sqrt(np.histogram(k10.loc[mi_10,col],edg,
                                        weights=k10.loc[mi_10,'mc_sf']**2)[0])

        # Overlay the histograms
        ax0.errorbar(xmid, n_sm, n_sm_err, color='gold', 
                     label='$\kappa_\lambda$=1',**kwargs)
        ax0.errorbar(xmid, n_10, n_10_err, color='deeppink', 
                     label='$\kappa_\lambda$=10',**kwargs)

        ax0.errorbar(xmid, n_rw, n_rw_err, color='turquoise',
                     label='rw $\kappa_\lambda$=10', 
                     **kwargs)

        # Plot the ratio
        ax1.errorbar(xmid,n_rw/n_10, n_rw_err/n_10,
                     color='turquoise',**kwargs)
        
        ax0.set_title(f'{cs_min:.1f}$<|\cos \Theta^*|<${cs_max:.1f}')
        ax1.set_xlabel(xlabel,fontsize=14)
        ax1.set_ylim(0,2)
        ax1.plot(xmid,np.ones_like(xmid),color='deeppink',ls='--')
        
        if i==0:
            ax0.set_ylabel('Entries',fontsize=14)
            ax1.set_ylabel('ratio',  fontsize=14)
            
    ax0.legend()
    fig.suptitle(title,fontsize=15,va='bottom')

    if len(figname) > 0:
        plt.savefig(figname,bbox_inches='tight')

def hist2dRW(sm,k10,cx,cy,rx,ry,nb=25,title='',figname=''):
    '''
    Goal: Also look at some of the correlations w/ the reweighting.
    '''
    
    fig, (ax1,ax2,ax3) = plt.subplots(1,3,figsize=(14,3))

    # sm
    Z1,ex,ey = np.histogram2d(sm[cx], sm[cy], nb, [rx,ry], weights=sm['mc_sf'])
    im_edg = np.linspace(-0.5,nb-0.5,nb+1)
    
    xticks = [f'{x:.0f}' for x in ex[::5]]
    yticks = [f'{x:.1f}' for x in ey[::5]]
    
    im1 = ax1.imshow(Z1.T, cmap='YlOrBr',origin='lower')
    fig.colorbar(im1,ax=ax1)
    ax1.set_xlabel(cx,fontsize=12)
    ax1.set_ylabel(cy,fontsize=12)
    ax1.set_title('$\kappa_\lambda = 1$',fontsize=12)
      
    ax1.set_xticks(im_edg[::5])
    ax1.set_xticklabels(xticks)
    ax1.set_yticks(im_edg[::5])
    ax1.set_yticklabels(yticks)
        
        
    # k10
    Z2 = np.histogram2d(k10[cx], k10[cy], nb, [rx,ry], weights=k10['mc_sf'])[0]  
    im2 = ax2.imshow(Z2.T, cmap='PuRd',origin='lower')
    fig.colorbar(im2,ax=ax2)
    ax2.set_xlabel(cx,fontsize=12)
    ax2.set_ylabel(cy,fontsize=12)
    ax2.set_title('$\kappa_\lambda = 10$',fontsize=12)
    
    ax2.set_xticks(im_edg[::5])
    ax2.set_xticklabels(xticks)
    ax2.set_yticks(im_edg[::5])
    ax2.set_yticklabels(yticks)
    
    # rw k10
    Z3 = np.histogram2d(sm[cx], sm[cy], nb,[rx,ry], weights=sm['w_k10'])[0]  
    im3 = ax3.imshow(Z3.T, cmap='GnBu',origin='lower')
    fig.colorbar(im3,ax=ax3)
    ax3.set_xlabel(cx,fontsize=12)
    ax3.set_ylabel(cy,fontsize=12)
    ax3.set_title('$\kappa_\lambda = 1$ rw to $\kappa_\lambda = 10$',fontsize=12)
    
    ax3.set_xticks(im_edg[::5])
    ax3.set_xticklabels(xticks)
    ax3.set_yticks(im_edg[::5])
    ax3.set_yticklabels(yticks)
    
    fig.suptitle(title,va='bottom',fontsize=15)
    
    if len(figname) > 0:
        plt.savefig(figname,bbox_inches='tight')
    
    