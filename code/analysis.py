'''
analysis.py

Goal: I want to be able to run the analysis quickly and analyze results at each stage of
the cutflow, so this collection of functions allows me to submit jobs in parallel over
individual miniNtuples and then concatenate them at the end after all of the jobs have
finished.

Author: Nicole Hartman
Spring 2018

'''

import pandas as pd
import numpy as np
from glob import glob
from utils import calcMinDr, getTruth4Vectors, barcodeMatch
import uproot
from uproot_methods.classes.TLorentzVector import TLorentzVectorArray
from uproot_methods import TVector3
import json
import os
import re

# Make a dictionary to store where all the miniNtuples live.
fileDir = {
	'SMNR_mc16a_PFlow-FEB2019': '/u/ki/nhartman/gpfs/public/hh4b/SMNR/user.bstanisl.HH4B.450000.SM_HH.MC16a-2015-2016.AB21.2.61-FEB2019-Prod.pflow_vr-systs-resolved_MiniNTuple.root/',
	'SMNR_mc16a_Topo-FEB2019': '/u/ki/nhartman/gpfs/public/hh4b/SMNR/user.saparede.HH4B.450000.SM_HH.MC16a-2015-2016.AB21.2.61-FEB2019-Prod.topo-systs-boosted_MiniNTuple.root/',
	'SMNR_mc16a_PFlow-MAR2019': '/u/ki/nhartman/gpfs/public/hh4b/SMNR/user.bstanisl.HH4B.450000.SM_HH.MC16a-2015-2016.AB21.2.61-MAR2019-Prod.pflow_vr_MiniNTuple.root/',
	'SMNR_mc16a_PFlow-MAY2019': '/u/ki/nhartman/gpfs/public/hh4b/SMNR/user.saparede.HH4B.450000.SM_HH.MC16a-2015-2016.AB21.2.72-MAY2019_VRFIX.pflow_vr_min-systs-both_MiniNTuple.root/',
	'SMNR_mc16a_PFlow-AUG2019': '/u/ki/nhartman/gpfs/public/hh4b/SMNR/user.tseiss.HH4B.450000.SM_HH.MC16a-2015-2016.AB21.2.72-AUG2019-AFII.pflow_MiniNTuple.root/',
	'SMNR_mc16d_PFlow-AUG2019': '/u/ki/nhartman/gpfs/public/hh4b/SMNR/user.tseiss.HH4B.450000.SM_HH.MC16d-2017.AB21.2.72-AUG2019-AFII.pflow_MiniNTuple.root/',
	'SMNR_mc16e_PFlow-AUG2019': '/u/ki/nhartman/gpfs/public/hh4b/SMNR/user.tseiss.HH4B.450000.SM_HH.MC16e-2018.AB21.2.72-AUG2019-AFII.pflow_MiniNTuple.root/',
	# new b-taggers
	'SMNR_mc16a_PFlow-MAR2020': '/u/ki/nhartman/gpfs/public/hh4b/SMNR/user.jagrundy.HH4B.450000.SM_HH.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'SMNR_mc16d_PFlow-MAR2020': '/u/ki/nhartman/gpfs/public/hh4b/SMNR/user.jagrundy.HH4B.450000.SM_HH.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'SMNR_mc16e_PFlow-MAR2020': '/u/ki/nhartman/gpfs/public/hh4b/SMNR/user.jagrundy.HH4B.450000.SM_HH.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',

 	'data_15_Topo-FEB2019': '/u/ki/nhartman/gpfs/public/hh4b/data/user.mswiatlo.HH4B.period*.data15..AB21.2.61-FEB2019-Prod.topo_MiniNTuple.root/',
	'data_15_PFlow-FEB2019' : '/u/ki/nhartman/gpfs/public/hh4b/data/user.mswiatlo.HH4B.period*.data15..AB21.2.61-FEB2019-Prod.pflow_vr_MiniNTuple.root/',
	'data_15_PFlow-MAY2019':'/u/ki/nhartman/gpfs/public/hh4b/data/user.dabbott.HH4B.period*.data15..AB21.2.72-MAY2019_VRFIX.pflow_vr_min_MiniNTuple.root/',

 	'data_16_Topo-FEB2019': '/u/ki/nhartman/gpfs/public/hh4b/data/user.mswiatlo.HH4B.period*.data16..AB21.2.61-FEB2019-Prod.topo_MiniNTuple.root/',
	'data_16_PFlow-MAR2019' : '/u/ki/nhartman/gpfs/public/hh4b/data/user.mswiatlo.HH4B.period*.data16..AB21.2.61-MAR2019-Prod.pflow_vr_MiniNTuple.root/',
	'data_16_PFlow-MAY2019':'/u/ki/nhartman/gpfs/public/hh4b/data/user.dabbott.HH4B.period*.data16..AB21.2.72-MAY2019_VRFIX.pflow_vr_min_MiniNTuple.root/',
	'data16_PFlow-FEB20':'/u/ki/nhartman/gpfs/public/hh4b/data/user.mswiatlo.HH4B.period*.data16..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/',

	'data17_PFlow-FEB20':'/u/ki/nhartman/gpfs/public/hh4b/data/user.mswiatlo.HH4B.period*.data17..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/',
	'data18_PFlow-FEB20':'/u/ki/nhartman/gpfs/public/hh4b/data/user.mswiatlo.HH4B.period*.data18..AB21.2.91-FEB20-0.pflow_vr_MiniNTuple.root/',

	'graviton300_mc16a_Topo-APR2019': '/u/ki/nhartman/gpfs/public/hh4b/gravitons/X300/user.wbalunas.HH4B.301488.RS_graviton.MC16a-2015-2016.AB21.2.61-APR2019-SemiMerged.topo_inter_MiniNTuple.root/',
	'scalar300_mc16a_PFlow-AUG2019': '/u/ki/nhartman/gpfs/public/hh4b/scalars/X300/user.tseiss.HH4B.450251.scalar_af2.MC16a-2015-2016.AB21.2.72-AUG2019-AFII.pflow_MiniNTuple.root/',

	# NLO with FT
	'SMNR_mc16a_PFlow-APR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/kappa_lambda/user.valentem.HH4B.600043.HH_NLO.MC16a-2015-2016.AB21.2.91-APR20-1.full_MiniNTuple.root/',
	'SMNR_mc16d_PFlow-APR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/kappa_lambda/user.valentem.HH4B.600043.HH_NLO.MC16d-2017.AB21.2.91-APR20-1.full_MiniNTuple.root/',
	'SMNR_mc16e_PFlow-APR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/kappa_lambda/user.valentem.HH4B.600043.HH_NLO.MC16e-2018.AB21.2.91-APR20-1.full_MiniNTuple.root/',
	'k10_mc16a_PFlow-APR2020' : '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/kappa_lambda/user.valentem.HH4B.600044.HH_NLO.MC16a-2015-2016.AB21.2.91-APR20-1.full_MiniNTuple.root/',
	'k10_mc16d_PFlow-APR2020' : '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/kappa_lambda/user.valentem.HH4B.600044.HH_NLO.MC16d-2017.AB21.2.91-APR20-1.full_MiniNTuple.root/',

	'SMNR_mc16a_PFlow-JUN2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/kappa_lambda/user.valentem.HH4B.600043.HH_FullTop.MC16a-2015-2016.AB21.2.91-JUN20-1.full_MiniNTuple.root/',
	'SMNR_mc16d_PFlow-JUN2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/kappa_lambda/user.valentem.HH4B.600043.HH_FullTop.MC16d-2017.AB21.2.91-JUN20-1.full_MiniNTuple.root/',
	'SMNR_mc16e_PFlow-JUN2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/kappa_lambda/user.valentem.HH4B.600043.HH_FullTop.MC16e-2018.AB21.2.91-JUN20-1.full_MiniNTuple.root/',
	'k10_mc16a_PFlow-JUN2020' : '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/kappa_lambda/user.valentem.HH4B.600044.HH_FullTop.MC16a-2015-2016.AB21.2.91-JUN20-1.full_MiniNTuple.root/',
	'k10_mc16d_PFlow-JUN2020' : '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/kappa_lambda/user.valentem.HH4B.600044.HH_FullTop.MC16d-2017.AB21.2.91-JUN20-1.full_MiniNTuple.root/',
	'k10_mc16e_PFlow-JUN2020' : '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/kappa_lambda/user.valentem.HH4B.600044.HH_FullTop.MC16e-2018.AB21.2.91-JUN20-1.full_MiniNTuple.root/',

	# scalars - ATLAS fast sim
	'scalar251_mc16a_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450248.scalar_af2.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar260_mc16a_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450249.scalar_af2.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar280_mc16a_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450250.scalar_af2.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar300_mc16a_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450251.scalar_af2.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar350_mc16a_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450252.scalar_af2.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar400_mc16a_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450253.scalar_af2.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar500_mc16a_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450254.scalar_af2.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar600_mc16a_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450255.scalar_af2.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar700_mc16a_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450256.scalar_af2.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar800_mc16a_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450257.scalar_af2.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar900_mc16a_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450258.scalar_af2.MC16a-2015-2016.AB21.2.91-MAR20-0.full_MiniNTuple.root/',

	'scalar251_mc16d_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450248.scalar_af2.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/', 
	'scalar260_mc16d_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450249.scalar_af2.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar280_mc16d_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450250.scalar_af2.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar300_mc16d_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450251.scalar_af2.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar350_mc16d_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450252.scalar_af2.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar400_mc16d_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450253.scalar_af2.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar500_mc16d_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450254.scalar_af2.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar600_mc16d_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450255.scalar_af2.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar700_mc16d_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450256.scalar_af2.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar800_mc16d_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450257.scalar_af2.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar900_mc16d_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450258.scalar_af2.MC16d-2017.AB21.2.91-MAR20-0.full_MiniNTuple.root/',

	'scalar251_mc16e_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450248.scalar_af2.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar260_mc16e_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450249.scalar_af2.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar280_mc16e_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450250.scalar_af2.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar300_mc16e_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450251.scalar_af2.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar350_mc16e_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450252.scalar_af2.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar400_mc16e_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450253.scalar_af2.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar500_mc16e_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450254.scalar_af2.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar600_mc16e_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450255.scalar_af2.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar700_mc16e_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450256.scalar_af2.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar800_mc16e_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450257.scalar_af2.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',
	'scalar900_mc16e_PFlow-MAR2020': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/scalars_af2/user.bstanisl.HH4B.450258.scalar_af2.MC16e-2018.AB21.2.91-MAR20-0.full_MiniNTuple.root/',

	# all had ttbar 
	'semilep_ttbar_mc16a_PFlow-MAR2020' : '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/ttbar/user.bstanisl.HH4B.410470.ttbar.MC16a-2015-2016.AB21.2.91-MAR20-0.min_MiniNTuple.root/',
	'allhad_ttbar_mc16a_PFlow-MAR2020' : '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/ttbar/user.bstanisl.HH4B.410471.ttbar.MC16a-2015-2016.AB21.2.91-MAR20-0.min_MiniNTuple.root/',

	# pythia QCD samples
	'JZ2_mc16a_PFlow-HLLHC': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/qcd_mc/user.mswiatlo.HH4B.800285.dijets_b_new.MC16a-2015-2016.AB21.2.91.HLLHC.asymmtriggers.yby.full_MiniNTuple.root/',
	'JZ3_mc16a_PFlow-HLLHC': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/qcd_mc/user.mswiatlo.HH4B.800286.dijets_b_new.MC16a-2015-2016.AB21.2.91.HLLHC.asymmtriggers.yby.full_MiniNTuple.root/',
	'JZ4_mc16a_PFlow-HLLHC': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/qcd_mc/user.mswiatlo.HH4B.800287.dijets_b_new.MC16a-2015-2016.AB21.2.91.HLLHC.asymmtriggers.yby.full_MiniNTuple.root/',

	'JZ2_mc16d_PFlow-HLLHC': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/qcd_mc/user.mswiatlo.HH4B.800285.dijets_b_new.MC16d-2017.AB21.2.91.HLLHC.asymmtriggers.yby.full_MiniNTuple.root/',
	'JZ3_mc16d_PFlow-HLLHC': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/qcd_mc/user.mswiatlo.HH4B.800286.dijets_b_new.MC16d-2017.AB21.2.91.HLLHC.asymmtriggers.yby.full_MiniNTuple.root/',
	'JZ4_mc16d_PFlow-HLLHC': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/qcd_mc/user.mswiatlo.HH4B.800287.dijets_b_new.MC16d-2017.AB21.2.91.HLLHC.asymmtriggers.yby.full_MiniNTuple.root/',
	
	'JZ2_mc16e_PFlow-HLLHC': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/qcd_mc/user.mswiatlo.HH4B.800285.dijets_b_new.MC16e-2018.AB21.2.91.HLLHC.asymmtriggers.yby.full_MiniNTuple.root/',
	'JZ3_mc16e_PFlow-HLLHC': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/qcd_mc/user.mswiatlo.HH4B.800286.dijets_b_new.MC16e-2018.AB21.2.91.HLLHC.asymmtriggers.yby.full_MiniNTuple.root/',
	'JZ4_mc16e_PFlow-HLLHC': '/gpfs/slac/atlas/fs1/d/nhartman/public/hh4b/qcd_mc/user.mswiatlo.HH4B.800287.dijets_b_new.MC16e-2018.AB21.2.91.HLLHC.asymmtriggers.yby.full_MiniNTuple.root/',
	
}

# Store the 2015 and 2016 triggers that were used for the previous iteration
# of the analysis
triggers = {
	2015 : ["HLT_2j35_btight_2j35_L13J25.0ETA23",
			"HLT_2j35_btight_2j35_L14J15.0ETA25",
			"HLT_j100_2j55_bmedium",
			"HLT_j225_bloose"],
	2016 : ["HLT_2j35_bmv2c2060_split_2j35_L14J15.0ETA25",
			"HLT_j100_2j55_bmv2c2060_split",
			"HLT_j225_bmv2c2060_split"],
	2017 : ["HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15.0ETA25",
			"HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21",
			"HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30",
			"HLT_j225_gsc300_bmv2c1070_split"],
			#"HLT_j35_gsc55_bmv2c1050_split_ht700_L1HT190-J15s5.ETA21"
	2018 : ["HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21",
			"HLT_2j35_bmv2c1060_split_2j35_L14J15.0ETA25",
			"HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30",
			"HLT_j225_gsc300_bmv2c1070_split"]
}

# Parsing years
yr_short = {2015: 15, 15: 15, 2016: 16, 16: 16, 2017: 17, 17: 17, 2018: 18, 18: 18}

def loadData(filename, truth=False, entry_start=-1, entry_stop=-1, nSelectedJets=4):
	'''
	Given the filename, load the root file into a pandas DataFrame
	with the desired branches.
	'''

	# Infer from the filename whether or not this sample is mc
	mc = not ('data' in filename)

	evt_vars = ['eventNumber', 'nresolvedJets', 'nmuon']
	'''
	In the FEB production, the miniNtuples saved strings for the names of the
	triggers, whereas in MAR (and probably later) they used hashes
	'''
	if 'FEB' in filename:
		evt_vars += ['passedTriggers']
	else: # if 'MAR' in filename:
		evt_vars += ['passedTriggerHashes']

	jet_vars = ['resolvedJets_'+v for v in ['E','pt','phi','eta','MV2c10','is_MV2c10_FixedCutBEff_70']] # 'JetVertexCharge_discriminant','HadronConeExclTruthLabelID',
	mu_vars = ['muon_'+v for v in ['pt','eta', 'phi','m','EnergyLoss']]

	# Set up a df with the relevant branches
	branches = evt_vars + jet_vars + mu_vars

	mc_branches = ['resolvedJets_SF_MV2c10_FixedCutBEff_70','mcChannelNumber',
				   'mcEventWeight','weight_pileup','rand_run_nr']
	data_branches = ['rand_run_nr']
	x = ['truth_'+v for v in ['pt','eta','phi','E',
		 'pdgId','barcode','child_barcode','parent_barcode']]
	if truth: mc_branches += x

	if mc:
		branches += mc_branches
	else:
		branches += data_branches

	treeName = "XhhMiniNtuple"
	if entry_start != -1 and entry_stop != -1:
		miniNtuple = next(uproot.pandas.iterate(filename, treeName, branches, flatten=False,
							entrysteps=[(entry_start, entry_stop)] ))
	else:
		myTree = uproot.open(filename)[treeName]
		miniNtuple = myTree.pandas.df(branches, flatten=False)

	# Instantiate the extra branches that we want to add
	miniNtuple['mc_sf'] = 0 if mc else 1

	extraCols = ['pair{}_HC{}_{}'.format(pair,HC,v) for pair in [0,1,2] \
				 for HC in [1,2] for v in ['m','dRjj','pt','eta','phi']]
	extraCols += ['pair{}_{}'.format(pair,v) for pair in [0,1,2] for v in ['Dhh']]
	extraCols += ['HC{}_{}'.format(HC,v) for HC in [1,2] for v in ['pt','eta','phi','m','ntags']]
	extraCols += ["j{}_{}".format(jetID,v) for jetID in range(nSelectedJets) for v in ['pt','eta','phi','m','E','idx','jvc','Db']]
	extraCols += ['m4j','nbtags','chosenPair','nValidPairs','HC1_vecSum_pt','HC2_vecSum_pt','deta_hh','Xhh','Xwt']

	# These should really be mc_branches, but they deleted these branches in the MAY-Prod (or maybe earlier)
	# so I need to set them to a default value
	if not truth: extraCols += x

	if mc:
		extraCols += ['hh_{}'.format(v)  for v in ['m','pt','dr','angle']]
		extraCols += ["{}_{}".format(pcle,v) for pcle in ['h0','h1','b0','b1','b2','b3'] \
					  for v in ['pt','eta','phi','E','pdg','drMatch','jidx','selJet_drMatch','selJet_jidx','hidx'] \
					  if not ('h' in pcle and ('idx' in v or 'drMatch' in v))]
		extraCols += data_branches
	else:
		extraCols += mc_branches

	for c in extraCols:
		miniNtuple[c] = 0

	boolCols = ['pair{}_{}'.format(pair,v) for pair in [0,1,2] for v in ['mask']]
	boolCols += [ti for year in triggers.keys() for ti in triggers[year]]
	boolCols += ['{}_triggers'.format(year) for year in triggers.keys()]
	for b in boolCols:
		miniNtuple[b] = False

	return miniNtuple


def truthMatchJets(ps, bs, ievt, miniNtuple,prefix=""):
	'''
	Match the truth quarks to the reco jets
	'''

	# Truth study: match (4) b-quarks to b-jets
	dr_match, jidx = calcMinDr(bs,ps,R=0.3)

	# Save the relevant b-quark info
	vars = ['drMatch','jidx']
	vals = [dr_match, jidx]

	for var, val in zip(vars,vals):
		bcols = ['b{}_{}{}'.format(i,prefix,var) for i in range(4)]
		miniNtuple.loc[ievt,bcols] = val


def muonInJet(ps,mus,mu_eloss,Rmatch=0.4):
	'''
	Do the muon in jet correction to add the muon energy back to the jet.

	Inputs:
	- ps: A list of the TLorentzVectors for the jets
	- mus: A list of the TLorentzVectors for the jets
	- mu_eloss: The expected energy loss from the muon in the calorimeter (in MeV)
	- Rmatch: The opening angle for a muon to be matched to a jet, default 0.4
			  for the small R jets that we use in the analysis.
			  Since jet centers can be just over R apart, this means we might be able to
			  have multiple jets that are dR matched to the same muon. This will not throw
			  an error, but it will print a warning message.

	'''

	# Do the matching of muons to jets
	# dr_mus distance between the jet and it's closest muon
	# imus indexes this closest muon in the list
	dr_mus, imus = calcMinDr(ps,mus,R=Rmatch)

	# Get the expected energy loss in the calorimeter
	elosses = TLorentzVectorArray.from_spherical(mu_eloss*1e-3,mus.theta,mus.phi,mu_eloss*1e-3)

	rmask = (dr_mus < Rmatch)
	muVec = mus[imus][rmask] - elosses[imus][rmask]

	x,y,z,t = np.zeros((4,len(ps)))
	x[rmask] = muVec.x
	y[rmask] = muVec.y
	z[rmask] = muVec.z
	t[rmask] = muVec.t

	delta_mu = TLorentzVectorArray.from_cartesian(x,y,z,t)
	return ps + delta_mu


def dRjj_cut(dRjj, m4j, a,b,c,d,e,f):
    '''
    Given the opening angle of the constituents of the leading HC
    (ordered by the SS of the constituents), return true or false
    for whether or not the pairing is valid, given the 4 jet
    invariant mass.

    Inputs:
    - dRjj: A np array of shape (nEvts,) # nPairings)
    - m4j: A np array of shape (nEvs,) for broadcasting
	- a,b,c,d,e,f: Constants specifying the dRjj cuts
    '''

    validPairs = np.ones_like(dRjj).astype(bool)

    if m4j < 1250:
        # low mass cut
        validPairs[dRjj < a / m4j + b] = False
        validPairs[dRjj > c / m4j + d] = False
    else:
        # high mass cut
        validPairs[(dRjj < e) | (dRjj > f)] = False

    return validPairs


def allPairs(ps, ievt, miniNtuple):
    '''
    Create HCs for the three combinations for the selected jets.

    Inputs:
    - ps: The 4 vectors for the selected jets
    - ievt: The event we're considering
    - miniNtuple: A reference to the df we're iterating over, for filling the
                  pair?_HC?_{m,dRjj} columns as we creat the pairs.

    Outputs:
    - hc1s: List of the 4-vectors for the HC1 candidates
    '''

    hc1s, hc2s = [], []

    # For 4 jets, we have 3 pairings
    # At this stage, the HCs are sorted by the scalar sum of the pT of the constituents
    for pi, idx1, idx2 in zip(range(3), [(0,1),(0,2),(0,3)], [(2,3),(1,3),(1,2)]):

        # Calculate the scalar sum of the two HCs
        ss1 = ps[idx1[0]].pt + ps[idx1[1]].pt
        ss2 = ps[idx2[0]].pt + ps[idx2[1]].pt

        # Order the pairs by the SS of the constituents
        if ss1 > ss2:
            j11, j12 = ps[idx1[0]], ps[idx1[1]]
            j21, j22 = ps[idx2[0]], ps[idx2[1]]

        else:
            j11, j12 = ps[idx2[0]], ps[idx2[1]]
            j21, j22 = ps[idx1[0]], ps[idx1[1]]

        HC1 = j11 + j12
        HC2 = j21 + j22

        hc1s.append(HC1)
        hc2s.append(HC2)

        # Another alternative: if I don't want to pass miniNutple and fill these
        # vectors in the function, I could just return the constituent jets outside
        # of the function and do this inside of the for loop of the run_one function?

        # Calculate the invariant mass
        for HC, name in zip([HC1,HC2], ["HC1","HC2"]):
            miniNtuple.loc[ievt,'pair{}_{}_pt'.format(pi,name)] = HC.pt
            miniNtuple.loc[ievt,'pair{}_{}_eta'.format(pi,name)] = HC.eta
            miniNtuple.loc[ievt,'pair{}_{}_phi'.format(pi,name)] = HC.phi
            miniNtuple.loc[ievt,'pair{}_{}_m'.format(pi,name)] = HC.mass

        # Calculate the opening angle between the consituents
        miniNtuple.loc[ievt,'pair{}_HC1_dRjj'.format(pi)] = j11.delta_r(j12)
        miniNtuple.loc[ievt,'pair{}_HC2_dRjj'.format(pi)] = j21.delta_r(j22)

    return hc1s, hc2s


def MDR(lead_dR_arr, subl_dR_arr, ievt, miniNtuple):
	'''
	Determine which of the pairings satisfy the MDR cut

	Inputs:
	- lead_dRarr: Array of the opening angle between the jets of the leading HC
	- subl_dRarr: Array of the opening angle between the jets of the subleading HC
	- m4j: The invariant mass of the 4-jets

	Output:
	- pair_mask: A boolean np array with shape 3 for the valid pairings
	'''

	m4j = miniNtuple.loc[ievt,'m4j']

	leadR_cuts = (360 ,-0.5, 652.863, 0.474449, 0, 0.9967394)
	sublR_cuts = (235.242, 0.0162996, 874.890, 0.347137, 0, 1.047049)

	lead_pair_mask = dRjj_cut(lead_dR_arr, m4j, *leadR_cuts)
	subl_pair_mask = dRjj_cut(subl_dR_arr, m4j, *sublR_cuts)
	pair_mask = lead_pair_mask & subl_pair_mask
	nValidPairs = np.sum(pair_mask.astype(int))

	miniNtuple.loc[ievt,'nValidPairs'] = nValidPairs

	return pair_mask


def Dhh(hc1s, hc2s, pair_mask, ievt, miniNtuple):
	'''
	Calculate Dhh for the valid pairs in the event

	Inputs:
	- hc1s, hc2s: Lists of the possible pairings for the HCs
	- pair_mask: Boolean mask of whether or not each pairing passed the MDR cut
	- ievt: The evt we're considering
	- miniNtuple: A reference to the df we're modifying

	Outputs:
	- HC1, HC2: The selected Higgs candidates
	'''
	s = 120 / 110

	Dhh_keys = ['pair{}_Dhh'.format(pair) for pair in range(3)]
	mask_keys = ['pair{}_mask'.format(pair) for pair in range(3)]
	Dhhs = [ np.abs(hc1.mass - s * hc2.mass) / np.sqrt(1 + s**2) for hc1,hc2 in zip(hc1s,hc2s)]

	# Save the Dhh values for future reference
	miniNtuple.loc[ievt,Dhh_keys] = Dhhs
	miniNtuple.loc[ievt,mask_keys] = pair_mask

	# Find the index of the minimum element in the jet that also minimizes this pairing
	pi = min([(Dhh,i) for Dhh, i, valid in zip(Dhhs,range(3),pair_mask) if valid])[1]

	return hc1s[pi], hc2s[pi], pi


def getXhh(HC1, HC2, mlead_mean=120, msubl_mean=110, mres=0.1):
	'''
	Return the Xhh value for the event

	Inputs:
	- HC1, HC2: The 4-vectors for the leading and subleading HCs in the event
	- mlead_mean, msubl_mean: The center for the ellipse in the (lead,subl) HC mass plane
	- mres: The radius of the elipse, which will be multiplied by the HC mass
	Output:
	- Xhh: The Xhh value for the event, eq (3) from the int note

	'''

	Xhh = 0 
	for mi, mean in zip([HC1.mass,HC2.mass], [mlead_mean,msubl_mean]):
		Xhh += ( (mi - mean) / (mres * mi) )**2
	return np.sqrt(Xhh)


def getXwt(ps, nSelectedJets=4):
	'''
	Calculate Xwt for the event by looking at all the jets with pT > 40 GeV and
	|eta| < 2.5, and looking at all possible triplets, choosing the HC-jet leading
	in b-tag disc score as the b-jet, and the other two as the W-jet

	Inputs:
	- ps: A list of the 4-vectors of the jets in the event with pT > 40 GeV and
		  |eta| < 2.5, which have been sorted by b-tag disc. Note, the first 4
		  jets in the list are the ones used for the HCs.

	Output:
	- The min Xwt in this event

	'''

	# My getXhh function has enough generality that it's just calculating
	# distances from the center of an elipse in a massplane, so I can
	# use it to find Xwt as well by letting the W act as a proxy for HC1
	# and the t replace HC2, filling in the corresponding values for
	# the center of the ellipse from eq (2) of the int note.
	Xwts = [getXhh(ps[iw1]+ps[iw2], bjet+ps[iw1]+ps[iw2], mlead_mean=80.4, msubl_mean=172.5) \
			for ib, bjet in enumerate(ps[:nSelectedJets]) \
			for iw1 in range(nSelectedJets) if (iw1 != ib) \
			for iw2 in range(iw1+1,nSelectedJets) if (iw2 != ib)]

	# In the 2015/16 analysis, the jets were sorted by b-tag score, so the first
	# jet in the sequence could be assumed to be the b-jet. This wasn't FTAG safe 
	# though, so this is why an alternate definition is implemented above.
	#njets = len(ps)
	# Xwts = [getXhh(wjet1+wjet2, bjet+wjet1+wjet2, mlead_mean=80.4, msubl_mean=172.5) \
	# 		for ib, bjet in enumerate(ps[:nSelectedJets]) \
	# 		for iw1, wjet1 in zip(range(ib+1,njets), ps[ib+1:]) \
	# 		for iw2, wjet2 in zip(range(iw1+1,njets), ps[iw1+1:]) ]

	return min(Xwts)

def getCorrectPair(miniNtuple,prefix='selJet_',nSelectedJets=4):
    '''
    Goal: For the mc, save which event is the correct one by using whether
    the truth matched b-jets correspond to the pairing from the truth record.
    '''

    # Step 1: See which b-jets matched to which quarks
    dr_max = np.max(miniNtuple[['b{}_{}drMatch'.format(i,prefix) for i in range(4)]].values,axis=1)
    Rmatch = 0.3

    jetMatches = np.array([ np.max(np.unique(indices.values,return_counts=True)[1]) \
                          for ievt, indices in miniNtuple[[f'b{i}_{prefix}jidx' for i in range(4)]].iterrows()])

    # Step 2: See which event has all four of its jets truth matched to b-quarks
    badJets = (dr_max > Rmatch) | (jetMatches > 1)

    # Step 3: Determine which pairing is correct
    key = 'correctPair' if prefix == 'selJet_' else prefix + 'correctPair'
    miniNtuple[key] = -1

    myCols = ['njets'] + ['b{}_hidx'.format(i) for i in range(4)]
    myCols += ['b{}_{}jidx'.format(i,prefix) for i in range(4)]

    # Note - I could probably speed up this loop itertuples instead
    #for ievt, rows in miniNtuple.loc[~badJets,myCols].iterrows():
    for ievt, nj, b0_hidx,b1_hidx,b2_hidx,b3_hidx, b0_jidx,b1_jidx,b2_jidx,b3_jidx \
        in miniNtuple.loc[~badJets,myCols].itertuples():

        njets = min(int(nj), nSelectedJets) 

        h_jidx = {0:[], 1:[]}
        for bi_hidx, bi_jidx in zip([b0_hidx,b1_hidx,b2_hidx,b3_hidx], 
                                    [b0_jidx,b1_jidx,b2_jidx,b3_jidx]):
            h_jidx[bi_hidx].append(bi_jidx)

        bpairs = [set(h_jidx[hidx]) for hidx in range(2)]

        i = 0
        for i0 in range(njets-3):
            for i1 in range(i0+1,njets-2):
                for i2 in range(i1+1,njets-1):
                    for i3 in range(i2+1,njets):

                        if (set([i0,i1]) in bpairs) and (set([i2,i3]) in bpairs):
                            miniNtuple.loc[ievt,key] = i
                        elif (set([i0,i2]) in bpairs) and (set([i1,i3]) in bpairs):
                            miniNtuple.loc[ievt,key] = i + 1
                        elif (set([i0,i3]) in bpairs) and (set([i1,i2]) in bpairs):
                            miniNtuple.loc[ievt,key] = i + 2
                        i += 3

    miniNtuple['badJets'] = pd.Series(badJets,index=miniNtuple.index)


def saveVars(ievt, miniNtuple):
	'''
	I wasn't sure if it would help clean up my code if I put all the saving of
	the miniNtuple variables in one spot, but I'm going to forego this for now.
	'''
	pass

def run_one(filename, outputfile, f, truth=False, entry_start=-1, entry_stop=-1,
			nSelectedJets=4, pTcut=40, btagAlg="MV2c10"):
	'''
	Run the analysis over a single miniNtuple

	Inputs:
	- filename: The name of the input file to run over
	- outputDir: A (relative) path for where to save the output data file to
	- f: The pseudo-tag rate (for 2b)
	- entry_start: The first entry in the miniNtuple to load
	- entry_stop: The entry in the miniNtuple to stop loading at
	- nSelectedJets: # of jets to save (if != 1, will retrun after the jet selection)
	- pTcut: The pT cut to apply on the jets.

	'''

	mc = not ('data' in filename)
	miniNtuple = loadData(filename, truth, entry_start, entry_stop, nSelectedJets)

	# Check that a miniNtuple was actually returned
	if miniNtuple is None:
		return

	# Loop through the events
	Rmatch = 0.4
	cols = ['nresolvedJets','resolvedJets_pt', 'resolvedJets_eta', 'resolvedJets_phi',
			'resolvedJets_E',#'resolvedJets_JetVertexCharge_discriminant',
			f'resolvedJets_{btagAlg}', 'resolvedJets_is_MV2c10_FixedCutBEff_70',
			'resolvedJets_SF_MV2c10_FixedCutBEff_70','mcEventWeight','weight_pileup',
			'nmuon','muon_pt','muon_eta','muon_phi','muon_m','muon_EnergyLoss',
			'truth_pt','truth_eta','truth_phi','truth_E',
			'truth_pdgId','truth_barcode','truth_child_barcode']

	print(miniNtuple.columns)

	with open("hashMap.json", 'r') as varfile:
		hashMap = json.load(varfile)
	triggerHashes = { year : [ hashMap[ti] for ti in triggers[year] ] for year in [2015,2016,2017,2018] }

	if not truth:
		print("We don't have info to access the truth hs and bs")

	for ievt, (nresolvedJets, jpts ,jetas, jphis, jEs, jmv2s, jtags, jsfs, mcEvtWt, wPU,
			   nmu, mu_pts, mu_etas, mu_phis, mu_ms, mu_eloss,
			   tpts, tetas, tphis, tEs, tpdgs, tbarcodes, tChildBarcodes) \
		in miniNtuple[cols].iterrows():

		if ievt % 500 == 0:
			print("Evt",ievt)

		# Trigger
		if 'passedTriggers' in miniNtuple.columns:
			for year in [2015,2016,2017,2018]:
				nTriggersPassed = [ (ti.decode() in triggers[year])  for ti in miniNtuple.loc[ievt,'passedTriggers']]
				miniNtuple.loc[ievt,'{}_triggers'.format(year)] = (sum(nTriggersPassed) > 0)

				for ti in triggers[year]:
					miniNtuple.loc[ievt,ti] = (ti.encode() in miniNtuple.loc[ievt,'passedTriggers'])

		else:
			for year in [2015,2016,2017,2018]:
				nTriggersPassed = [ (ti in triggerHashes[year])  for ti in miniNtuple.loc[ievt,'passedTriggerHashes']]
				miniNtuple.loc[ievt,'{}_triggers'.format(year)] = (sum(nTriggersPassed) > 0)

				for ti in triggers[year]:
					miniNtuple.loc[ievt,ti] = (hashMap[ti] in miniNtuple.loc[ievt,'passedTriggerHashes'])

		# 4 b-tagged jets w/ pT > 40 GeV, |eta| < 2.5 - although for future studies
		# I also wanted to loosen the pT cut.
		mask = (jpts > pTcut) & (np.abs(jetas) < 2.5)
		miniNtuple.loc[ievt,'nbtags'] = np.sum(mask & jtags)

		njets = np.sum(mask)
		miniNtuple.loc[ievt,'njets'] = njets

		# Need at least 4 jets to continue
		if njets < 4:
			continue

		# Calculate the mc sf for the event. To do this, we need to consider all of the taggable jets
		if mc:
			miniNtuple.loc[ievt,'mc_sf'] = mcEvtWt * wPU
			for jsf in np.array(jsfs)[mask]:
				miniNtuple.loc[ievt,'mc_sf'] *= jsf[0]

			# Get the LorentzVectors for the reco jets in the event
			js = TLorentzVectorArray.from_ptetaphim(jpts,jetas,jphis,jEs)

			# Get 4-vectors for the truth bs and hs
			if truth:
				bs = getTruth4Vectors(tpts, tetas, tphis, tEs, tpdgs, 5, ievt, miniNtuple)
				hs = getTruth4Vectors(tpts, tetas, tphis, tEs, tpdgs, 25, ievt, miniNtuple)

				# Save the hh info
				miniNtuple.loc[ievt,'hh_m'] = (hs[0] + hs[1]).mass
				miniNtuple.loc[ievt,'hh_pt'] = (hs[0] + hs[1]).pt
				miniNtuple.loc[ievt,'hh_dr'] = hs[0].delta_r(hs[1])
				miniNtuple.loc[ievt,'hh_angle'] = TVector3(hs[0].x,hs[0].y,hs[0].z).angle(TVector3(hs[1].x,hs[1].y,hs[1].z))

				# Match the truth quarks to reco-jets
				truthMatchJets(js, bs, ievt, miniNtuple)

				# Save which truth higgs each of the quarks came from
				barcodeMatch(ievt,miniNtuple)

		if np.sum(mask & jtags) == 2:
			# Use the pseudotag rate for the 2b region
			myTag = np.random.binomial(1, f, size=nresolvedJets).astype(bool)

			# Make sure the real b-tags stay b-tagged
			myTag[jtags] = True

			# Update the mask for 2b events
			mask = (mask & myTag).astype(bool)

			if np.sum(mask) < 4:
				continue

		# Sort the good jets by mv2
		#idx  = np.argsort(jmv2s[bmask])[::-1]
		idx  = np.argsort(jmv2s[mask])[::-1]

		ps = TLorentzVectorArray.from_ptetaphi(jpts,jetas,jphis,jEs)
		#ps = ps[bmask][idx]
		ps = ps[mask][idx]

		if nmu > 0:
			mus = TLorentzVectorArray.from_ptetaphim(mu_pts, mu_etas, mu_phis, mu_ms)
			ps = muonInJet(ps,mus,mu_eloss)

		# Calculate the di-higgs 4-vector
		p4j = ps[:4].sum()
		miniNtuple.loc[ievt,'m4j'] = p4j.mass # Note - I need to update this for GNNs
		miniNtuple.loc[ievt,'HCs_pt'] = p4j.pt

		# Save the jet kinematics - need to worry about padding + truncating as well
		if nSelectedJets < len(ps):
			miniNtuple.loc[ievt,['j{}_pt'.format(ji)  for ji in range(nSelectedJets)]]  = ps[:nSelectedJets].pt
			miniNtuple.loc[ievt,['j{}_eta'.format(ji) for ji in range(nSelectedJets)]] = ps[:nSelectedJets].eta
			miniNtuple.loc[ievt,['j{}_phi'.format(ji) for ji in range(nSelectedJets)]] = ps[:nSelectedJets].phi
			miniNtuple.loc[ievt,['j{}_m'.format(ji)   for ji in range(nSelectedJets)]]   = ps[:nSelectedJets].mass
			miniNtuple.loc[ievt,['j{}_E'.format(ji)   for ji in range(nSelectedJets)]]   = ps[:nSelectedJets].E
			#miniNtuple.loc[ievt,['j{}_jvc'.format(ji) for ji in range(nSelectedJets)]] = jvcs[mask][idx][:nSelectedJets]
			miniNtuple.loc[ievt,['j{}_Db'.format(ji) for ji in range(nSelectedJets)]] = jmv2s[mask][idx][:nSelectedJets]

			miniNtuple.loc[ievt,['j{}_idx'.format(ji) for ji in range(nSelectedJets)]] = np.arange(nresolvedJets)[idx][:nSelectedJets]

		else:
			miniNtuple.loc[ievt,['j{}_pt'.format(ji)  for ji in range(len(ps))]]  = ps.pt
			miniNtuple.loc[ievt,['j{}_eta'.format(ji) for ji in range(len(ps))]] = ps.eta
			miniNtuple.loc[ievt,['j{}_phi'.format(ji) for ji in range(len(ps))]] = ps.phi
			miniNtuple.loc[ievt,['j{}_m'.format(ji)   for ji in range(len(ps))]]   = ps.mass
			miniNtuple.loc[ievt,['j{}_E'.format(ji)   for ji in range(len(ps))]]   = ps.E
			#miniNtuple.loc[ievt,['j{}_jvc'.format(ji) for ji in range(len(ps))]] = jvcs[mask][idx]
			miniNtuple.loc[ievt,['j{}_Db'.format(ji)  for ji in range(len(ps))]] = jmv2s[mask][idx]

			miniNtuple.loc[ievt,['j{}_idx'.format(ji) for ji in range(len(ps))]] = np.arange(nresolvedJets)[idx]

		# Now that I have all the possible HCs defined, I can decide which one
		# corresponds to the true HC match
		if truth:

			# Also - after I've done the muon-in-jet correction, I also want to
			# find the truth matches to the selected jets
			truthMatchJets(ps[:nSelectedJets], bs, ievt, miniNtuple,prefix='selJet_')

		# Ok, we'll have to rethink the events below for cases where we have
		# more than 4 jets, i.e, by loading in the GNN
		if nSelectedJets != 4:
			continue

		hc1s, hc2s = allPairs(ps[:nSelectedJets], ievt, miniNtuple)

		# Select the valid pairings using the mass dependent R cut
		hc1_cols = ['pair{}_HC1_dRjj'.format(i) for i in range(3)]
		hc2_cols = ['pair{}_HC2_dRjj'.format(i) for i in range(3)]

		lead_dR_arr = miniNtuple.loc[ievt,hc1_cols].values
		subl_dR_arr = miniNtuple.loc[ievt,hc2_cols].values

		pair_mask = MDR(lead_dR_arr,subl_dR_arr,ievt,miniNtuple)
		if np.sum(pair_mask.astype(int)) == 0:
			continue

		HC1, HC2, pi = Dhh(hc1s, hc2s, pair_mask, ievt,miniNtuple)
		miniNtuple.loc[ievt,'chosenPair'] = pi

		# To unravel this cut and save the # of b-tags, save which jet indices
		# correspond to this (...)
		jetPairs = [{'HC1': [0,1], 'HC2': [2,3]},
					{'HC1': [0,2], 'HC2': [1,3]},
					{'HC1': [0,3], 'HC2': [1,2]}]

		# Save some of the kinematics info
		for p4, name in zip([HC1,HC2],['HC1','HC2']):
			miniNtuple.loc[ievt,'{}_pt'.format(name)] = p4.pt
			miniNtuple.loc[ievt,'{}_eta'.format(name)] = p4.eta
			miniNtuple.loc[ievt,'{}_phi'.format(name)] = p4.phi
			miniNtuple.loc[ievt,'{}_m'.format(name)] = p4.mass
			#miniNtuple.loc[ievt,'{}_ntags'.format(name)] = sum([jtags[bmask][idx][i] for i in jetPairs[pi][name]])
			miniNtuple.loc[ievt,'{}_ntags'.format(name)] = sum([jtags[mask][idx][i] for i in jetPairs[pi][name]])

		# Save the opening angles between the HCs
		miniNtuple.loc[ievt,'HCs_dr'] = HC1.delta_r(HC2)
		miniNtuple.loc[ievt,'hh_angle'] = TVector3(HC1.x,HC1.y,HC1.z).angle(TVector3(HC2.x,HC2.y,HC2.z))

		# Resort the HCs by the vector sum of the pT to get the mass dependent pT cut
		miniNtuple.loc[ievt,'HC1_vecSum_pt'] = max(HC1.pt,HC2.pt)
		miniNtuple.loc[ievt,'HC2_vecSum_pt'] = min(HC1.pt,HC2.pt)
		miniNtuple.loc[ievt,'deta_hh'] = np.abs(HC1.eta - HC2.eta)
		miniNtuple.loc[ievt,'Xhh'] = getXhh(HC1, HC2)
		miniNtuple.loc[ievt,'Xwt'] = getXwt(ps,nSelectedJets)

	# Make the masks that define the anlaysis chain
	miniNtuple['fourGoodJets'] = (miniNtuple['m4j'] != 0)

	miniNtuple['MDR'] = (miniNtuple['nValidPairs'] > 0) & miniNtuple['fourGoodJets']

	lead_pT_cut = 0.513333 * miniNtuple['m4j'] - 103.3333
	subl_pT_cut = 0.33333 * miniNtuple['m4j'] -  73.3333
	miniNtuple['MDpT'] = (miniNtuple['HC1_vecSum_pt'] > lead_pT_cut) & (miniNtuple['HC2_vecSum_pt'] > subl_pT_cut) & miniNtuple['MDR']

	miniNtuple['cut_deta_hh'] = (miniNtuple['deta_hh'] < 1.5) & miniNtuple['MDpT']

	miniNtuple['cut_Xwt'] = (miniNtuple['Xwt'] > 1.5) & miniNtuple['cut_deta_hh']
	miniNtuple['cut_Xhh'] = (miniNtuple['Xhh'] < 1.6) & miniNtuple['cut_Xwt']

	if truth:
		getCorrectPair(miniNtuple,nSelectedJets=nSelectedJets)
	else:
		print("Skipping the accessing of the correct pairing b/c we don't have truth info")

	# Save the miniNtuple
	miniNtuple.to_hdf(outputfile, key='df',mode='w')

def read_tsv():
	'''
	Read  the tsv file (this function assumes that my dihiggs repo is parallel
	to the hh-resolved-reconstruction repo), and it returns a pandas DataFrame
	with the rows corresponding to the DISD numbers, and the columns to the
	relevant physics information that we want to extract.
	'''
	tsvFile = "../data/xsec.tsv"

	# To figure out the format of the .tsv file, I looked at Beojean's MCConfig.cpp file.
	db_cols = ['physics_short','xsec','gen_filter_eff','k_factor','rel_uncert_up','rel_uncert_down','generator']
	db_entry = pd.read_csv(tsvFile,sep='\t \t|\t\t',index_col=0,engine='python',names=db_cols)

	db_entry['xsec'] *= 1000

	return db_entry

def normalizeWeight(df,physicsSample,lumi,i_mc=450000):
    '''
    Load in the MNT to get the relevant normalization from the sample_weight and lumi
    Inputs:
    - df: If a df is passed: normalize to mc_sf (in place). 
          If `None`, Otherwise, the sample_weight will be returned (as a scalar)
    - physicsSample: key to retrive the correct MDT from the fileDir dictionary 
    - lumi: Luminosity to normalize the sample to 
    - i_mc: DSID to read in the xsec, k_factor, and generator filter eff from 
            xsec.tsv file.
    '''
    sum_weights_initial = 0
    treeName = "MetaData_EventCount_XhhMiniNtuple"

    for myFile in glob(fileDir[physicsSample] + "user.*.MiniNTuple.root"):
        print(myFile)
        metadata = uproot.open(myFile)[treeName]
        sum_weights_initial += metadata.allvalues[3]

    print("sum_weights_initial",sum_weights_initial)

    #assert 'SMNR' in physicsSample # I've hard-coded the mc channel # rn
    #i_mc = 450000
    db_entry = read_tsv()

    print(f'Reading entry for DSID {i_mc}:')
    sample_weight = lumi
    sample_weight *= db_entry.loc[i_mc,"xsec"]
    sample_weight *= db_entry.loc[i_mc,"k_factor"]
    sample_weight *= db_entry.loc[i_mc,"gen_filter_eff"]
    sample_weight /= sum_weights_initial

    print("  xsec",db_entry.loc[i_mc,"xsec"])
    print("  k_factor",db_entry.loc[i_mc,"k_factor"])
    print("  gen_filter_eff",db_entry.loc[i_mc,"gen_filter_eff"])
    print("  sample_weight",sample_weight)

    if df is None:
        return sample_weight
    df['mc_sf'] *= sample_weight


def concat_dfs():

	# Get the sample weights for all of the files that you were running over
	sample_weight = 0
	pass

def miniNtupleExists(filename):
	'''
	Check if the XhhMiniNtuple is inside this file, which is not always the
	case for data.
	'''

	treeName = "XhhMiniNtuple"
	myFile = uproot.open(filename)

	return b"XhhMiniNtuple;1" in myFile.allkeys()


def getBasketBoundaries(filename):
	'''
	Get the boundaries for the baskets in the root file, returned as a list of
	tuples for boundaries [entry_start, entry_stop).

	If the XhhMiniNtuple is NOT inside this file, return an empty list (sometimes
	the case for data).
	'''

	treeName = "XhhMiniNtuple"
	myFile = uproot.open(filename)

	if b"XhhMiniNtuple;1" in myFile.allkeys():
		return list(myFile[treeName].clusters())
	else:
		return []

def getNumEntries(filename):
	'''
	Get the number of events in the root file.

	If the XhhMiniNtuple is NOT inside this file, return 0 (sometimes the case for data).
	'''

	treeName = "XhhMiniNtuple"
	myFile = uproot.open(filename)

	if b"XhhMiniNtuple;1" in myFile.allkeys():
		return myFile[treeName].numentries
	else:
		return 0


if __name__ == '__main__':

	from argparse import ArgumentParser

	# Start off just using the same OptionParser arguments that Zihao used.
	p = ArgumentParser()
	p.add_argument('--physicsSample', type=str, default='SMNR',
				   help="The physics sample to run over: rn either SMNR or data_15")

	p.add_argument('--f', type=float, default=0.22,
                   help="The pseudotag factor: default 0.22 for the int note result for 2015.")

	p.add_argument('--period', type=str, default='',
                   help="The period for data taking, should only be passed for data, the default is an empty string.")

	p.add_argument('--mc', type=str, default='mc16a',
                   help="mc campaign: only used if 'data' is not inside the physicsSample flag")

	p.add_argument('--filename', type=str, default='',
               	help="The .root file for the miniNtuple you want to run over.")

	p.add_argument('--prodTag', type=str, default='MAY2019',
				help="The production tag for the miniNtuple. I'm going to try to keep the default corresponding to the latest one.")

	p.add_argument('--batch', action='store_true',
               	help="Submit the individual jobs on the (slac) batch cluster")

	# I don't have all the files downloaded to do this study yet
	p.add_argument('--jetCollection', type=str, default='PFlow',
               	help="The jet collection to do the analysis over")

	p.add_argument('--truth',action='store_true',help="Save truth for bs and hs")

	p.add_argument('--batchSize', type=int, default=5000,help="Size of the jobs to submit to the batch system")
	p.add_argument('--entry_start', type=int, default=-1,
                   help="The entry to start from. The default value of -1 corresponds to reading the entire file")
	p.add_argument('--entry_stop', type=int, default=-1,
                   help="The entry to stop before. The default value of -1 corresponds to reading the entire file")

	p.add_argument('--nSelectedJets',type=int,default=4,help="Number of jets to save (for GNN studies)")
	p.add_argument('--pTcut',type=int,default=40,help="pT cut to use for the jets (default 40 GeV)")
	p.add_argument('--btagAlg',type=str,default="MV2c10",help="The b-tagging alg to save for each jet")

	args = p.parse_args()

	physicsSample, mc, jetCollection = args.physicsSample, args.mc, args.jetCollection
	prodTag, truth = args.prodTag, args.truth
	nSelectedJets, pTcut,btagAlg = args.nSelectedJets, args.pTcut, args.btagAlg

	if 'data' not in physicsSample:
		physicsSample += f'_{mc}'

	batchSize = args.batchSize
	entry_start, entry_stop = args.entry_start, args.entry_stop
	print("entry", entry_start, entry_stop)

	print('Running over the {} sample'.format(physicsSample))
	#print('Using triggers from {}'.format(year))
	print('Look at the {} jet collection'.format(jetCollection))

	subDir = '{}_{}-{}'.format(physicsSample,jetCollection,prodTag)
	key = subDir # b/c the miniNtuple doesn't care about the # of jets selected
	if nSelectedJets != 4:
		subDir += f"-{nSelectedJets}jets"
	if pTcut != 40:
		subDir += f"-{pTcut}GeV"
	if btagAlg != "MV2c10":
		subDir += f"-{btagAlg}"

	# Create the new directory if it doesn't exist
	outputDir = '../data/{}'.format(subDir)
	if not os.path.exists(outputDir):
		print('Creating new directory',outputDir)
		os.makedirs(outputDir)

	if len(args.filename) > 0:

		outputDir += '/files'
		if not os.path.exists(outputDir):
			print('Creating new directory',outputDir)
			os.makedirs(outputDir)

		fileNum = int(re.findall(r'\d+', args.filename)[-1])
		period = "_period{}".format(args.period) if len(args.period) > 0 else ''
		entries = "" if entry_stop == -1 or entry_stop == -1 else "_entry_{}_{}".format(entry_start,entry_stop)
		outputfile = '{}/df{}_f_{}_{:06d}{}.h5'.format(outputDir, period, args.f, fileNum,entries)

		run_one(args.filename, outputfile, args.f, truth, entry_start, entry_stop, nSelectedJets, pTcut, btagAlg)

	else:

		print('Looping over all of the {} files and submitting to the LSF batch.'.format(subDir))
		filenames = fileDir[key] + "/user.*.MiniNTuple.root"

		for filename in glob( filenames ):
			print(filename)

			pythonCmd =  "python analysis.py --filename " + filename

			# Find the file number associated to this Ntuple
			fileNum = int(re.findall(r'\d+', filename)[-1])

			# Append all of the other arg parser arguments except filename
			for k,v in vars(args).items():
				if k != 'filename' and k != 'batch' and k != 'period' and k != 'entry_start' and k != 'entry_stop' and k!= 'truth':
					pythonCmd += ' --{} {}'.format(k,v)

			if truth: pythonCmd += " --truth"

			# For data, I also need to keep track of the period
			if 'data' in physicsSample:
				period = filename.split('.')[3]
				pythonCmd += ' --period {}'.format(period[-1])
				periodTag = '_' + period
			else:
				periodTag = ''

			# Get the boundaries for the entries corresponding to the baskets of the root file
			#entrysteps = getBasketBoundaries(filename)
			#if len(entrysteps) == 0:
			#	print("Error: file does not exist")
			#	continue
			#for entries in entrysteps:

			entries = getNumEntries(filename)
			if entries == 0:
				print("XhhMiniNtuple not in file - skipping.")
				continue

			entrysteps = np.arange(0,entries+batchSize,batchSize)

			for entry_start,entry_stop in zip(entrysteps[:-1],entrysteps[1:]):

				myPythonCmd = "{} --entry_start {} --entry_stop {}".format(pythonCmd,entry_start,entry_stop)

				outputfile = '{}/df{}_f_{}_{:06d}_entry_{}_{}.h5'.format(outputDir+'/files', periodTag, args.f, fileNum, entry_start,entry_stop)
				if os.path.exists(outputfile):
					# Don't bother resubmitting a job that's already done
					print("Skipping event: file already exists")
					continue

				print(myPythonCmd)

				if args.batch:
					job_name = '{}{}-{:06d}_entry_{}_{}'.format(subDir,periodTag,fileNum,entry_start,entry_stop)
					bsubCmd = 'bsub -W 80:00 -q atlas-t3 -oo log_files/{}.txt -J {} {}'.format(job_name, job_name, myPythonCmd)
					os.system(bsubCmd)
				else:
					os.system(myPythonCmd)

				#break
			#break
