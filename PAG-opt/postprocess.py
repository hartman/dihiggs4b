'''
Some plotting functions to use in some other notebooks
'''

import numpy as np
from dask.array import Array
from dask.core import flatten
from dask.highlevelgraph import HighLevelGraph

import os
os.sys.path.append('../../RRevolution/code')
from analysis import getXhh

def getMinQ(df):
    return df.assign(minQ=df[['Db_h1_j1','Db_h1_j2','Db_h2_j1','Db_h2_j2']].values.min(axis=1))

def redoSort(df):
    '''
    Goal: reorder the m_h1, m_h2 by the HC_pt and make a *new* kinematic_region 
    flag as well.
    '''
    
    # Get new HC masses
    mask = df.pT_h1>df.pT_h2
    m_lead = np.where(mask, df.m_h1, df.m_h2)
    m_subl = np.where(mask, df.m_h2, df.m_h1)
    
    # Get a new kinematic_region flag
    kr = 3 * np.ones(len(df))
    
    Xhh=getXhh(m_lead, m_subl)
    kr[Xhh < 1.6] = 0
    
    VR_mask = (np.sqrt((m_lead - 120*1.03)**2 + (m_subl-110*1.03)**2) < 30) 
    kr[VR_mask & (kr != 0)] = 1
    
    CR_mask = (np.sqrt((m_lead - 120*1.05)**2 + (m_subl-110*1.05)**2) < 45) 
    kr[CR_mask & (kr != 1) & (kr != 0)] = 2

    return df.assign(m_lead=m_lead).assign(m_subl=m_subl).assign(Xhh_ptSort=Xhh).assign(kr_new=kr)


def _block_hist2d(x,y, bins, range=None, weights=None):
    return np.histogram2d(x,y, bins, range=range, weights=weights)[0][np.newaxis]

def histogram2d(a, b, bins=None, range=None):
    """
    
    Modified from the dask.array.histogram source code
    
    Blocked variant of :func:`numpy.histogram2d`.

    """
    
    binsx = np.linspace(*range[0],bins+1)
    binsy = np.linspace(*range[1],bins+1)
    
    name='hist2d'
    # Map the histogram to all bins, forming a 3D array of histograms, stacked for each chunk
    dsk = {
        (name, i, 0): (_block_hist2d, k,l, [bins,bins], range)
        for i, (k,l) in enumerate(zip(flatten(a.__dask_keys__()),flatten(b.__dask_keys__())))
    }
    dtype = np.histogram([])[0].dtype

    deps = (a,b) #+ deps

    graph = HighLevelGraph.from_collections(name, dsk, dependencies=deps)

    # Turn graph into a 2D Array of shape (nchunks, nbins)
    nchunks = len(list(flatten(a.__dask_keys__())))
    #nbins = bins.size - 1  # since `bins` is 1D
    chunks = ((1,) * nchunks, (bins,))
    mapped = Array(graph, name, chunks, dtype=dtype)
    # Sum over chunks to get the final histogram
    n = mapped.sum(axis=0)
    return n, binsx, binsy

def significance(n,b,db):
    '''
    Implments (1) from ATL-COM-GEN-2018-026
    '''
    
    z2 = n * np.log( np.where( n==0, 1, (n*(b+db**2))/(b**2+n*db**2)) )
    z2 -= (b**2/db**2) * np.log(1 + (db**2 * (n-b))/(b*(b+db**2)))
    z2 *= 2
    
    return np.sign(n-b) * np.sqrt(z2)

def cutflowPriv(df,ntag=4):
    '''
    Recreate the cutflow from my private fw
    '''

    # '4 good jets, >= 2 tagged', 'Multi Tagged', 'Valid', 'Trigger Buckets', 'dEta_hh', 'Main Xwt',
    # 'Signal', 'Validation', 'Control'
    x = np.zeros(9)
    
    tagged = (df.ntag >= 4) if ntag == 4 else (df.ntag  == ntag)
    m = np.ones(len(df)).astype(bool)
    
    for i, mi in enumerate([np.ones(len(df)).astype(bool),tagged,np.ones(len(df)).astype(bool),
                            df.bucket!=0,df.abs_deta_hh<1.5,df.X_wt > 1.5]):
    
        m = m & mi
        x[i] = np.sum(df.loc[m,'mc_sf'])
        print(x[i])
    for k in range(3):
        
        x[i+1+k] = df.loc[m & (df.kinematic_region==k),'mc_sf'].sum()
    
    return x

def calc_bstrap(arr, col, yr, norm, norm_IQR, bi,r,tag_str='ntag'):
    '''
    Sean's function for the stat errors:
    https://gitlab.cern.ch/sgasioro/non-resonant-studies/-/blob/master/utils.py#L32-42
    
    Now modified for ntag_pag
    
    '''
    n2,_ = np.histogram(arr[arr[tag_str]==2][col],bi,r,
                        weights=arr[arr[tag_str]==2][f'NN_d24_weight_bstrap_med_{yr}']*norm)
        
    n_IQRup,_ = np.histogram(arr[arr[tag_str]==2][col],bi,r,
                             #weights=(arr[arr[tag_str]==2][f'NN_d24_weight_bstrap_med_{yr}']*norm
                             weights=(arr[arr[tag_str]==2][f'NN_d24_weight_bstrap_med_{yr}']
                                       +arr[arr[tag_str]==2][f'NN_d24_weight_bstrap_IQR_{yr}']/2.))

    return abs(n_IQRup*np.sum(n2)/np.sum(n_IQRup)+n2*norm_IQR/2.-n2)



