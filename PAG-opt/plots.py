import numpy as np

def draw_SR(x=124, y=117, res_x=0.1, res_y=0.1, Xhh=1.6):
    '''
    Inputs:
    - x: m_h1 SR center
    - y: m_h2 SR center
    - res_x, res_y: default 0.1 for 10% resolution
    - Xhh: SR cutoff for the Xhh variable (default 1.6) 
    '''
    
    #res = 0.1 # assuming 10% resolution
    
    SR_x = np.linspace(x / (1 + res_x * Xhh)+1e-7, x / (1 - res_x * Xhh))
    alpha = np.power((SR_x - x)/(res_x * SR_x),2)
    SR_y1 = y / (1 + res_y * np.sqrt(np.power(Xhh,2) - alpha))
    SR_y2 = y / (1 - res_y * np.sqrt(np.power(Xhh,2) - alpha))
    
    xx = np.hstack([SR_x, SR_x[::-1]])
    yy = np.hstack([SR_y1,SR_y2[::-1]])
    
    return xx,yy

def draw_reg(x,y,eps1,eps2,r):
    '''
    Underlying code is the *same* for the CR and VR,
    j the defaults change
    '''
    
    x_arr = np.linspace(x*(1+eps1)-r, x*(1+eps1)+r-1e-7)
    
    beta = np.sqrt(r**2 - np.power(x_arr-x*(1+eps1), 2))
    y1_arr = y*(1+eps2) + beta
    y2_arr = y*(1+eps2) - beta
    
    xx = np.hstack([x_arr,x_arr[::-1]])
    yy = np.hstack([y1_arr,y2_arr[::-1]])
    
    return xx,yy

def draw_VR(x=124,y=117,delta1=0.03,delta2=0.03,r_VR=30):
    return draw_reg(x,y,delta1,delta2,r_VR)

def draw_CR(x=124,y=117,eps1=0.05,eps2=0.05,r_CR=45):
    return draw_reg(x,y,eps1,eps2,r_CR)

