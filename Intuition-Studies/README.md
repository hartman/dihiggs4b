Exploratory studies for familiarizing myself with the analysis choices and Ntuples for the hh -> 4b analysis.

The folders hold the various notebooks that I've been looking into for the various substudies.

- `bTagOptimization` is for the studies I've started for swapping out the b-tagging algorithms and propagating this information through the resolved analysis.

- `IntNote` are just a couple of the plots I was making to understand the equations for the $\Delta R$ selection for the resolved analysis, and visually compare the ATLAS and CMS implementation of this cut.

- `SemiMerged` is where I looked at the acceptance x efficiency plots for the tradeoff between the resolved + boosted analyses. I used datatheif to extract the data points out of the ATLAS paper, and the .txt files in this notebook are the data extracted from these plots. This Notebook also has a list of the cuts used for the resolved + boosted analyses.

- `MC_Sim` is the MC simulation that I wrote when I started my rotation project with Michael. There's not in this directory right now, because I wasn't clear how I should try to modify it for the studies that I want to do in the future. 

