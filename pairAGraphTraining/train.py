'''
Implementation of the pairAGraph models under consideration
'''

import numpy as np
import pandas as pd
import yaml, json 
import torch, torch.nn.functional as F
from time import time 

import os
os.sys.path += ['../RRevolution/code','../RRevolution/pairingMVAs']
from utils import getSubDir
from transformers import pairAGraph

from prepareData import getNumPairs, getGNNDataLoaders

from argparse import ArgumentParser

def my_nll_loss(input, target, sample_weight=None):
    '''
    Goal: Implementation of the NLL loss that can accept sample weights.
    '''
    
    batch_size = target.shape[0]
    Li = - sample_weight * input[torch.arange(batch_size),target]
    return torch.mean(Li)
    
def check_loss(loader,model,device='cpu',masked=False): 
    '''
    Calculate the loss
    '''
    
    model.eval()
    L = 0
    
    model = model.to(device=device)
    
    with torch.no_grad():
        for xi,yi,wi,bi in loader:
            
            xi = xi.to(device=device)
            yi = yi.to(device=device)
            wi = wi.to(device=device)
            bi = bi.to(device=device)
            
            logits, edgeWeights = model(xi) 
            logp = F.log_softmax(logits, 1)
            loss = my_nll_loss(logp, yi, wi)

            L += loss.item()
            
    return L
    
def check_accuracy(loader, model, btag=None, njets=None, device='cpu'):

    '''
    Check the accuracy of the model
    Inputs:
    - loader: A DataLoader object, i.e, for the val or test st
    - model: A Pytorch model to check the accuracy on
    - btag: Mask only events w/ this many btags for the accuracy
    - njets: Mask only events w/ this many jets for the accuracy
    '''
    
    num_correct = 0
    num_samples = 0
    model.eval()  # set model to evaluation mode

    model = model.to(device=device)

    with torch.no_grad():
        for xi, yi, wi, bi in loader:

            ni = xi.shape[1]
            if (njets is not None) and ni != njets:
                continue

            xi = xi.to(device=device)
            yi = yi.to(device=device)
            wi = wi.to(device=device)
            bi = bi.to(device=device)

            logits, _ = model(xi)
            _, y_pred = torch.max(logits,1)

            bmask = torch.ones_like(bi).bool() if btag is None else bi>=btag if (btag == 4) else bi==btag

            num_correct += torch.sum(wi[(yi==y_pred) & bmask])
            num_samples += torch.sum(wi[bmask])

        acc = (num_correct / num_samples).cpu().numpy() if num_samples > 0 else 0

    return float(acc)


def train(model,loader_train,loader_val,device='cpu',modelDir='',nSelectedJets=5,
          lr=0.01,nEpochs=50,patience=5):
    '''
    Goal: Implement the training loop with early stopping.
    '''

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)

    keys = ["_".join([s,m]) for s in ['train','val'] for m in ['loss','acc','acc_3b','acc_4b','acc_4j','acc_5j']]
    metrics = {k:[] for k in keys}

    model.to(device=device)

    bestEpoch = 0 
    bestValLoss = np.inf  

    for epoch in range(nEpochs):

        L = 0
        model.train()

        for xi,yi,wi,bi in loader_train:

            xi = xi.to(device=device)
            yi = yi.to(device=device)
            wi = wi.to(device=device)
            bi = bi.to(device=device)
            
            optimizer.zero_grad()

            logits, edgeWeights = model(xi) 
            logp = F.log_softmax(logits, 1)
            loss = my_nll_loss(logp, yi, wi)

            loss.backward()
            optimizer.step()

            L += loss.item()

        # Save the model (to load in later)
        torch.save(model.state_dict(), os.path.join(modelDir,f'model_epoch{epoch}.pt'))
    
        metrics['train_loss'].append(L)
        metrics['val_loss'].append(check_loss(loader_val,model,device=device))

        for s, loader in zip(['train','val'],[loader_train, loader_val]):
            metrics[f'{s}_acc'].append(   check_accuracy(loader,model,device=device))
            metrics[f'{s}_acc_3b'].append(check_accuracy(loader,model,btag=3,device=device))
            metrics[f'{s}_acc_4b'].append(check_accuracy(loader,model,btag=4,device=device))
            for nj in range(4,nSelectedJets+1):
                metrics[f'{s}_acc_{nj}j'].append(check_accuracy(loader,model,njets=nj,device=device))

        myVars = (epoch, L, metrics['val_loss'][-1]*4, metrics['train_acc'][-1], metrics['val_acc'][-1])
        print("Epoch {:2d} | Train loss {: .3f} | Val loss {: .3f} | Train acc {:.3f} | Val acc {:.3f}".format(*myVars))
        
        # Check if the validation acc improved
        if metrics['val_loss'][-1] < bestValLoss:
            bestValLoss = metrics['val_loss'][-1]
            bestEpoch = epoch
        
        # See if it's time to break out of the training loop 
        if epoch - bestEpoch > patience: break
    
        filename = os.path.join(modelDir, "loss_acc.json")
        with open(filename, 'w') as varfile:
            json.dump(metrics, varfile)

    # Also save the model w/ the best validation loss 
    model.load_state_dict(torch.load(os.path.join(modelDir,f'model_epoch{bestEpoch}.pt')))
    torch.save(model.state_dict(), os.path.join(modelDir,f'model.pt'))

if __name__ == '__main__':

    p = ArgumentParser()
    
    p.add_argument('--config',type=str,help='config file which passes (most) of the relevant settings')
    p.add_argument('--modelBaseDir',type=str,default='../RRevolution/pairingMVAs/models',
                   help='models directory to infer the file name and write the files to, default ../RRevolution/pairingMVAs/models')

    p.add_argument('--num_folds', type=int, default=2, help='# of folds for the test sets')
    p.add_argument('--k', type=int, default=0, help='Fold to leave out of events for the training: [0, ..., num_folds - 1]')

    p.add_argument('--gpuNum','--gpu_num',type=int,default=0,help='which gpu to use (default 0)')
    
    args = p.parse_args()
    
    # Load in the config file
    with open(args.config, 'r') as yaml_file:
        configs = yaml.load(yaml_file, Loader=yaml.FullLoader)
    
    dataConfig = configs['dataset']
    modelConfig = configs['modelParams']
    optConfig = configs['training']
    model =  configs['model']

    k, num_folds = args.k, args.num_folds 
    assert (k >= 0) and (k <= num_folds - 1) # Needs to be a valid fold 
    trainTag = f'_{k+1}of{num_folds}' 

    subDir = getSubDir(**dataConfig)
    modelDir = os.path.join(args.modelBaseDir,subDir)
    if not os.path.exists(modelDir): os.mkdir(modelDir)

    configName = args.config.split('/')[-1].replace('.yaml','')
    modelDir = os.path.join(modelDir,configName+trainTag)
    if not os.path.exists(modelDir): os.mkdir(modelDir)

    # Load in the training + validation data 
    loader_train, loader_val = getGNNDataLoaders(k=k,num_folds=num_folds,**dataConfig)

    # Load in the model
    jetVars = dataConfig['jetVars']
    model = pairAGraph(inpt_dim=len(jetVars),njets=dataConfig['nSelectedJets'],**modelConfig)

    params = sum([np.prod(p.size()) for p in model.parameters()])
    print(f"{params} trainable parameters")

    device = f'cuda:{args.gpuNum}' if torch.cuda.is_available() else 'cpu'
    print(device)
    print(modelDir)

    # Train the model (and time it)
    start = time()
    train(model, loader_train, loader_val, device, modelDir, dataConfig['nSelectedJets'], **optConfig)
    stop = time()
    dt = stop - start

    hrs = dt // 3600
    mins = (dt - hrs*3600) // 60
    secs = dt % 60
    print(f'Training time: {hrs} hr {mins} min, {secs} s')

