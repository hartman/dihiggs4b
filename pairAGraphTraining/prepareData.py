'''
Goal: Run over the signal samples and to some simple data files for training

What's needed for inputs?
- Signal samples
- The MNT prod tag to run over
- Sort to apply to the jets (i.e, pT, btag, or Db)
- Jet cuts (pT, eta, # of jets)

Also - this function *assumes* the  RRevolution framework is parallel to this
pairAGraphTraining folder to read in the relevant MNT files I've downloaded 
on the slac machines.

Dec 2020

'''

import numpy as np
import pandas as pd
from glob import glob
from itertools import product
import json 

import torch
from torch.utils.data import Dataset, ConcatDataset, DataLoader
from torch.utils.data.sampler import Sampler

import os
os.sys.path.append('../RRevolution/code/')
from analysis import processDf
from preprocessUtils import scale
from utils import fileDirSLAC as fileDir, mcToYr, getSubDir

from argparse import ArgumentParser

def getNumPairs(n):
    '''
    Get the number of valid pairs given n jets
    '''
    return int( np.prod( np.arange(n, n-4, -1) ) / 8 )


jet_map = {'btag': 'is_DL1r_FixedCutBEff_77',
           'Db': 'Quantile_DL1r_Continuous', 
           'jvc': 'JetVertexCharge_discriminant' 
		  }

def prepareData(nSelectedJets=5, jetVars = ['pt','eta','phi','E','btag','Db','jvc'],
                physicsSample='SMNR',mcs=['mc16a','mc16d','mc16e'],prodTag='JUN2020',
                sort='Db',pTcut=40,ntag=2):
    '''
    Goal: Get the dataframes in a format where it's easy to load them.
    Does the necessary preprocessing as well.
    '''
    
    # Step 1: Load in the DataFrame
    dfs = {nj:[] for nj in range(4,nSelectedJets+1)} 
    for mc in mcs:

        key = f'{physicsSample}_{mc}-{prodTag}'
        subDir = getSubDir(physicsSample,mc,prodTag,nSelectedJets,pTcut)
        fDir = f'../data/{subDir}'

        print('Running over',key,':')

        # Get the MNT name
        for filename in glob(fileDir[key]+'*.root'):
            print('  ',filename)
            # Process the df
            sortTag = jet_map[sort] if sort in jet_map.keys() else sort
            dfi = processDf(filename,nJetsMax=nSelectedJets,pT_min=pTcut,sort=sortTag,
                            year=mcToYr[mc],hashFile='../RRevolution/code/hashMap.json')	
            dfi = dfi[(dfi.ntag >=ntag) & (dfi.correctPair != -1)]

            for nj in range(4,nSelectedJets+1):
                if nj == nSelectedJets:
                    dfs[nj].append( dfi[dfi.njets >= nj] )
                else:
                    dfs[nj].append( dfi[dfi.njets == nj] )

    print('Concatenating dfs')
    df_nj = { nj: pd.concat(v) for nj,v in dfs.items()} 
    
    # Step 2: Prepare a jet array for the preprocessing 
    lens = [len(df) for k, df in df_nj.items()]
    N = sum( lens )
    X = np.zeros((N,nSelectedJets,len(jetVars))) 
    bounds = [0] + list(np.add.accumulate(np.array(lens)))
    
    for nj, i_min, i_max in zip(range(4,nSelectedJets+1),bounds[:-1],bounds[1:]):
        df = df_nj[nj]
        jcols = ['j{}_{}'.format(i,v) for i,v in product(range(nj),jetVars)]
        X[i_min:i_max,:nj] = df[jcols].values.reshape(-1,nj,len(jetVars))

    # Step 3: Normalize the features (classic ml preprocessing)
    mask = ~ np.all(X==0, axis=-1)

    # pT and E: Take the log before normalizing
    for vi in [0,3]:
        X[:,:,vi][mask] = np.log(X[:,:,vi][mask]-pTcut)

    mc = 'mc16' + ''.join([m[-1] for m in mcs])
    trainDir = getSubDir(physicsSample,mc,prodTag,nSelectedJets,pTcut)

    scalingFile = f"configs/scale_{trainDir}_{sort}_sort.json"
    scale(X,mask,[0,3,6],['pt','E','jvc'],filename=scalingFile,savevars=True)

    # btag - shift by 0.5
    X[:,:,4][mask] -= 0.5 

    # Db: Just shift st the bins are centered at 0
    X[:,:,5][mask] -= 3

    # Step 4: append the preprocessed inputs to the dfs and save
    ecols = ['njets','ntag','mc_sf','eventNumber','trigger','mcEventWeight','correctPair']
    fDir = f'../data/{trainDir}/'
    if not os.path.exists(fDir):
       print('Making folder',fDir)
       os.mkdir(fDir)

    outputFile = f"{fDir}/df_{ntag}b_{sort}_sort_correct.h5"

    for nj, i_min, i_max in zip(range(4,nSelectedJets+1), bounds[:-1], bounds[1:]):

        # Add the extra cols to the df
        df = df_nj[nj]
        jcols = [f'ml_j{i}_{v}' for i,v in product(range(nj),jetVars)]	
        for c in jcols: df[c] = 0

        print('df[jcols]',df[jcols].values.shape)
        print('X[i_min:i_max,:nj]',X[i_min:i_max,:nj].shape)

        # Include the preprocessed inputs
        df[jcols] = X[i_min:i_max,:nj].reshape(-1,nj*len(jetVars))

        # Save the dfs
        cols = ecols + jcols
        df[cols].to_hdf(outputFile, key=f'df_{nj}j', mode='a')

class pagDataset(Dataset):
    
    def __init__(self, filename, nj=4, jetVars = ['pt','eta','phi','E','Db'],
                 ntag=2, k=0, num_folds=2):        
        '''
        Goal: Load in the jet level variables into a pytorch dataframe to be ready
        for training.
        '''

        super(pagDataset, self).__init__()

        df = pd.read_hdf(filename,key=f'df_{nj}j')

        # Apply the extra masks on the dataset
        assert ntag == 2 # haven't added functionality to do tigher cuts on ntag yet 

        print(f'Getting pagDataset for {ntag}b, holding out fold {k+1}/{num_folds}')
        mask = df.eventNumber % num_folds != k
        df = df[mask]

        print(len(df),' train + val events')

        # Put into the torch tensors
        X = np.dstack([df[['ml_j{}_{}'.format(i,v) for i in range(nj)]].values for v in jetVars])
        
        self.x    = torch.from_numpy(X).float()
        self.y    = torch.from_numpy(df['correctPair'].values).long()
        self.w    = torch.from_numpy(df['mc_sf'].values).float()
        self.ntag = torch.from_numpy(df['ntag'].values).float()

    def __len__(self):
        return self.x.shape[0] 

    def __getitem__(self, idx):
        return self.x[idx], self.y[idx], self.w[idx], self.ntag[idx]

class nJetsSampler(Sampler):
    '''
    Custom sampler which samples batches based on the # of jets.
    '''
    
    def __init__(self, dset, idx_classes, batch_size=50, shuffle=True,drop_last=False):
        '''
        Inputs:
        - dset: An instance of a ConcatDataset
        - Ns: List of the length of the individual subdatasets inside the dset
        - batch_size: mini-batch size to use for each of the classes
        '''
        
        self.dset = dset
        self.idx_classes = idx_classes
        
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.drop_last = drop_last
    
        self.Ns = [len(idx_c) for idx_c in idx_classes]
    
    def __iter__(self):

        # Get a list of the batches
        idx_lists = [] 
        for N, idx_c in zip(self.Ns, self.idx_classes):
            
            # boundaries for the mini-batches for this class
            bs = np.arange(0, N+self.batch_size, self.batch_size) 

            if self.shuffle:
                r = torch.randperm(N) # shuffle for this class 
                list_c = [ idx_c[r[j_min:j_max]] for j_min, j_max in zip(bs[:-1],bs[1:])]
            else:
                list_c = [ idx_c[j_min:j_max] for j_min, j_max in zip(bs[:-1],bs[1:])]
            
            if len(list_c) > 0 and not self.drop_last:
                idx_lists += list_c  
        
        # Randomly order the mini-batches
        num_batches = len(idx_lists)
        if self.shuffle:
            mb_idx = torch.randperm(num_batches) 
        else: 
            mb_idx = torch.arange(num_batches)

        # Now the part that returns the batches!
        for mbi in mb_idx:
            yield idx_lists[mbi]

    def __len__(self):
        # number of mini-batches 
        if self.drop_last:
            return sum([N // self.batch_size for N in self.Ns])
        else:
            return sum([(N + self.batch_size - 1)// self.batch_size for N in self.Ns])

    def lenClass(self,i):
        # number of mini-batches / class 
        if self.drop_last:
            return self.Ns[i] // self.batch_size 
        else:
            return (self.Ns[i] + self.batch_size - 1)// self.batch_size


def getGNNDataLoaders(batch_size=256, trainFrac=.8, physicsSample='SMNR',
                      mc='mc16ade',prodTag='JUN20',nSelectedJets=5,
                      jetVars = ['pt','eta','phi','E','Db'], sort='Db', pTcut=40, ntag=2,
                      k=0, num_folds=2,drop_last=False):
    '''
    Return the DataLoaders for my pairAGraph training.
    '''

    # Load in the preprocesseded data and access the training part of the dataset    
    trainDir = getSubDir(physicsSample,mc,prodTag,nSelectedJets,pTcut)
    
    filename = f"../data/{trainDir}/df_{ntag}b_{sort}_sort_correct.h5"

    # Loop over the # of jets
    dsets, idx_tr, idx_val, Ns = [], [], [], []
    for nj in range(4, nSelectedJets+1):
     
        dsi = pagDataset(filename, nj, jetVars, ntag, k, num_folds)
        dsets.append(dsi)

        N = len(dsi)
        N_tot = sum(Ns)
        indices = torch.arange(N_tot,N_tot+N) 
        r = torch.randperm(N)

        idx_tr.append(indices[r][:int(trainFrac*N)])
        idx_val.append(indices[r][int(trainFrac*N):])
        Ns.append(N)

    # Concatenate the datasets + construct the dataloaders
    dset = ConcatDataset(dsets)

    loader_train = DataLoader(dset, batch_sampler=nJetsSampler(dset, idx_tr, batch_size, shuffle=True, drop_last=drop_last))
    loader_val   = DataLoader(dset, batch_sampler=nJetsSampler(dset, idx_val,batch_size, shuffle=False))

    return loader_train, loader_val


if __name__ == '__main__':

    p = ArgumentParser()
    
    # Paramters for the training dataset
    p.add_argument('--nSelectedJets', type=int, default=5, help='max # of jets to include (default 5)')
    p.add_argument('--prodTag', type=str, default='JUN20', help='Prod tag to use for the training (default JUN20)')
    p.add_argument('--physicsSample', type=str, default='SMNR', help='physics sample to train on (default SMNR)')
    p.add_argument('--mc', type=str, default='mc16a,mc16d,mc16e', 
                   help='Comma separated list of mc campaigns to use (default, mc16a,mc16d,mc16e)')
    p.add_argument('--sort', type=str, default='Db', help='Sort option to use for the jet selection (default Db)')
    p.add_argument('--pTcut', type=int, default=40, help='pT cut used for the jet selection (default 40)')
    p.add_argument('--ntag', type=int, default=2, help='Min cut on the # of b-tags to use in the training (default 2)')

    args = p.parse_args()

    nSelectedJets, prodTag = args.nSelectedJets, args.prodTag
    physicsSample, mcs = args.physicsSample, args.mc.split(',')
    sort, pTcut, ntag = args.sort, args.pTcut, args.ntag

    prepareData(nSelectedJets=nSelectedJets, physicsSample=physicsSample,mcs=mcs,
                prodTag=prodTag,sort=sort,pTcut=pTcut,ntag=ntag)
