# pairAGraph Training


## Step 1: MNTs to dfs

Done by running prepareData.py with the relevant arguments.

## Step 2:

In train.py.

(Include details for sw setups and gpus - if available.) 

## Step 3: Evaluate the performance

The analysis selection is done in the parallel RRevolution repo, and the code in this folder assumes
that the RRevolution folder is parallel to this pairAGraphTraining folder.

