'''
Goal: Define an error bar from the set of flow trainings.

nicole Hartman
Dec 2021

'''
import numpy as np

def getEigenvariations(h_stack, nSeeds=25,alpha=0):
    '''
    For a list of variation histograms, return the list of eigenvalues
    and the variation templates.

    Inputs:
    - h_stack

    Outputs:
    - lmbda: eigenvalues (in descending order)
    - u: variation
    '''

    # Normalize each variation histogram
    norms = h_stack.sum(axis=1,keepdims=True)

    h_norm = h_stack / norms

    # Compute the bin averages vector
    h_avg = h_norm.mean(axis=0,keepdims=True)


    # Construct the sample covariance matrix
    Qis = np.expand_dims(h_norm - h_avg,axis=-1) * np.expand_dims(h_norm - h_avg,axis=1)

    Q = np.sum(Qis,axis=0) / (nSeeds - 1)

    Q += alpha * np.eye(h_avg.shape[1])

    # Retrieve the eigendecomposition
    lmbda,u = np.linalg.eigh(Q)

    # The eigh fuct returns the eigenvalues from smallest to largest,
    # so reverse the order
    lmbda = lmbda[::-1]
    u = u[:,::-1]

    # Scale the variation histograms by lmbda
    # Recall, each column of u is a separate variation
    #u = u * np.sqrt(lmbda.reshape(1,-1))

    return lmbda,u
