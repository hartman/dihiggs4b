import numpy as np

def significance(n,b,db):
    '''
    Implments (1) from ATL-COM-GEN-2018-026
    '''
    
    z2 = n * np.log( np.where( n==0, 1, (n*(b+db**2))/(b**2+n*db**2)) )
    z2 -= (b**2/db**2) * np.log(1 + (db**2 * (n-b))/(b*(b+db**2)))
    z2 *= 2
    
    return np.sign(n-b) * np.sqrt(z2)
