'''
Model prediction
'''
import pandas as pd
import numpy as np
from tqdm import tqdm
import json
from glob import glob

import os
os.sys.path.append('../Flow-Models/')
from densityEstimate import *
from densityPlots import *
from time import time 


def run_one(modelDir,cols,yrShort=16,MDpT=False):

    start = time()
    
    # Down sample dataset
    subDir = f'data{yrShort}_PFlow-FEB20-5jets'
    tag = '_SM_2b_p_0.01'
    seedTag = ''
    prodTag = 'FEB20'
    ntag = 2

    s = f'm_h1>{126-45} & m_h1<{126+45} & m_h2>{116-45} & m_h2<{116+45}'
    d = data(5, yrShort, prodTag, tag, ntag, s)

    mask = (d.df.abs_deta_hh < 1.5) 
    if MDpT:
        mask = mask & d.df.MDpT
    d.mlPrepare(mask,cols,N=-1)

    nSeeds = 25
    nPreds = 10

    # histogram setup
    hists = []
    myVars = ['m_hh','m_hh_cor2','abs_deta_hh','absCosThetaStar',
              'pT_h1', 'pT_h2', 'eta_h1', 'eta_h2', 'dphi_hh']

    bins = 50
    myRanges = [(200,1200),(250,1250),(0,1.75),(0,1),
                (0,400),(0,400),(-3,3),(-3,3),(-.5,np.pi)]

    # Parse the file name for the relevant hps
    modelConfig = modelDir.split('/')[-1]
    vs = modelConfig.split('_')
    
    L = int(vs[16])
    H = int(vs[19])
    nb = int(vs[20])
    K = int(vs[23])
    lr = float(vs[27])
    beta = float(vs[28]) #if len(vs) == 31 else 0
    print(vs[-2])
    if 'data' in vs[-1]:
        p = float(vs[-3][1:])
        iter = int(float(vs[-2][4:]))
    else:
        p = float(vs[-2][1:])
        iter = int(float(vs[-1][4:]))
    
    # Loop over the seeds and access the losses and model predictions
    hist_list = {k:[] for k in myVars}
    
    for seed in tqdm(range(nSeeds)):
    
        seedTag = f'_seed{seed}' if seed != 10 else ''
        seedDir = os.path.join(*modelDir.split('/')[:-1]) + seedTag
        seedDir = os.path.join(seedDir,modelConfig)

        print(seedDir)
        try:

            preTrain = f'_{vs[-1]}' if 'data' in vs[-1] else ''
            # Load in the models and get the predictions
            ni = myNSF(seedDir.split('/')[3], lr, beta, cols, iter, L, H, nb, # num blocks
                       K, 3, 'lu', 'rq-coupling', p,
                       modelBaseDir='../Flow-Models/models', load_model=True,
                       torch_seed=seed, debug=True, tag=preTrain)

            # Predict
            npi = RealNVP_Plots(ni,save=False,mode=1)
            dfi = npi.pred_hh(d.X_SR,d.Y_SR,d.scalar,mask,d.df,nPreds=nPreds,cols=cols,plot=False)
            dfi['abs_deta_hh'] = np.abs(dfi.eta_h1 - dfi.eta_h2)
            
            # Make histograms
            for c,r in zip(myVars,myRanges):
                hist_list[c].append(np.histogram(dfi[c],bins,r)[0] / nPreds)
            
        except FileNotFoundError:
            
            print(f'Model w/ seed {seed} didn\'t train in',modelConfig)
                    
    # After looping through the seeds - if the training for all 25 of the seeds finished, 
    # let's calc the mean and std deviation and *save* the result!!!
    physicsSample = modelDir.split('/')[3].split('_')[0]
    print(physicsSample)
    yrTrain = int(physicsSample[4:])
    yrTag = f'_{yrShort}' if yrTrain != yrShort else ''
    print(yrTrain,yrShort,yrTag)
    print(yrTrain==yrShort)
    histFile = os.path.join(modelDir,f'hists1d{yrTag}{preTrain}.json')
    
    out = {}

    for c in myVars:
        stack = np.vstack(hist_list[c])
        out[f'{c}_mean'] = list(np.mean(stack,axis=0))
        out[f'{c}_std']  = list(np.std( stack,axis=0))

    with open(histFile, 'w') as varfile:
        json.dump(out, varfile)
    
    end = time()
    dt = end - start

    print(f'Time',dt//60,'min')
    

if __name__ == '__main__':

    from argparse import ArgumentParser

    p = ArgumentParser()
    p.add_argument('--MDpT', action='store_true', help="Apply the MDpT cuts for training / eval")
    p.add_argument('--batch', action='store_true', help="Submit eval over a set of seeds to a batch queue")
    p.add_argument('--modelDir',type=str,default='',help='modelDir to evaluate over')
    p.add_argument('--yrTrain',type=int,default=16,help='year to eval over (one of 16,17,18, or 161718)')
    p.add_argument('--yrEval', type=int,default=16,help='year to eval over (one of 16,17, or 18)')
    p.add_argument('--tag', type=str,default='',help='tag to postpend to describe the training dataset')
    
    args = p.parse_args()


    cols = ['log_pT_h1','log_pT_h2','eta_h1','eta_h2','log_dphi_hh']
    if len(args.modelDir) > 0:
        run_one(args.modelDir,cols,args.yrEval,args.MDpT)
        
    else:
        # Access the wildward file name    
        subDir = f'data{args.yrTrain}_PFlow-FEB20-5jets'
        tag = '_SM_2b_p_0.01'
        ntag = 2

        colTag = '_'.join(cols)
        mdTag = '_MDpT' if args.MDpT else ''
        wildFile = f'../Flow-Models/models/{subDir}{tag}_{ntag}b_detaCut{mdTag}/nsf_rq-coupling_{colTag}_lu_*_layers_H_*_?_blocks_K_?_B_3_lr_*_p*_iter*{args.tag}'
        if len(tag) ==0:
            wildFile += '[!18]'

        print(wildFile)
        print(len(glob(wildFile)),'options with these configs')

        # Loop through the model directories
        for modelDir in tqdm(glob(wildFile)):

            if args.batch:
                pythonCmd = f'python batchEval.py --modelDir {modelDir} --yrTrain {args.yrTrain} --yrEval {args.yrEval}'
                if args.MDpT: pythonCmd += ' --MDpT'

                job = '_'.join(modelDir.split('/')[3:])
                job += f'_eval{args.yrEval}'
                print(job)
                bsubCmd = f"bsub -R \'select[centos7 && bubble]\' -W 25:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
                os.system(bsubCmd)

            else:
                print(modelDir)
                run_one(modelDir,cols,args.yrEval,args.MDpT)

