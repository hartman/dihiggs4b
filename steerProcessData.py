'''
steerProcessData.py

Loop over the options for the # of b-tags and kinematic regions 
and submit the jobs to the grid.

'''

import itertools
import os

from argparse import ArgumentParser

if __name__ == '__main__':

	# Start off just using the same OptionParser arguments that Zihao used.
	p = ArgumentParser()
	p.add_argument('--physicsSample', type=str, default='data_16',
					help="The physics sample to run over")
	
	p.add_argument('--prodTag', type=str, default='MAY2019',
					help="The production tag for the miniNtuple.")
	
	p.add_argument('--nSelectedJets',type=int,default=5,help="Number of jets for GNN")
	p.add_argument('--pTcut',type=int,default=40,help="pT cut to use for the jets (default 40 GeV)")
	
	#p.add_argument('--colTag',type=str,default='_rw_vars')
	p.add_argument('--colConfig',type=str,default='rwConfig.json',help='If not an empty string, '\
					+'pass the name of a .json file that has the list of the columns saved')
	
	args = p.parse_args()


	physicsSample, prodTag = args.physicsSample, args.prodTag 
	nSelectedJets, pTcut = args.nSelectedJets, args.pTcut

	#colTag = args.colTag 
	colConfig = args.colConfig 
	colTag = "_"+colConfig[:-5]

	region = 'all'
	nbtags = -1
	
	subDir = f'{physicsSample}_PFlow-{prodTag}'
	if nSelectedJets != 4:
		subDir += f"-{nSelectedJets}jets"
	if pTcut != 40:
		subDir += f"-{pTcut}GeV"
	
	periods = ['A','B','C','D','E','F','G','H','I','J','K','L']
	for period in periods:
		
		pythonCmd =  f"python processData.py --physicsSample {physicsSample} "
		pythonCmd += f"--prodTag {prodTag} --nSelectedJets {nSelectedJets} --pTcut {pTcut} "
		pythonCmd += f"--period {period} --region {region} --nbtags {nbtags} "
		pythonCmd += f"--colConfig {colConfig}"
		
		job = f'{subDir}_period{period}_{nbtags}b_{region}{colTag}'
		singEnv = "singularity exec docker://gitlab-registry.cern.ch/hartman/ml-gpu/ml-gpu:latest "
		fName = f"tmp_{job}.sh"
		bsubCmd = f'bsub -W 80:00 -q atlas-t3 -oo GraphNN/log_files/{job}.txt -J {job} {singEnv} ./{fName}'
		
		# Write the script that you're going to run in the image
		f = open(fName,"w")
		f.write("#!/bin/sh\n\n")
		f.write("cd GraphNN\n")
		f.write(pythonCmd)
		f.close()
		
		os.system(f"chmod a+x {fName}")
		
		print("Submitting the job to the batch")
		print(bsubCmd)
		os.system(bsubCmd)
		#break
